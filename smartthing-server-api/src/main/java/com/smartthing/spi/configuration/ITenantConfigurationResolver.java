/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.configuration;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.resource.IResource;

/**
 * Pluggable resolver for interacting with global configuration data.
 * 
 * @author Derek
 */
public interface ITenantConfigurationResolver {

    /**
     * Get the global configuration resolver.
     * 
     * @return
     * @throws SmartThingException
     */
    public IGlobalConfigurationResolver getGlobalConfigurationResolver() throws SmartThingException;

    /**
     * Indicates a configuration exists for the tenant.
     * 
     * @return
     */
    public boolean hasValidConfiguration();

    /**
     * Indicates if a staged configuration exists for the tenant.
     * 
     * @return
     */
    public boolean hasStagedConfiguration();

    /**
     * Get tenant resource that corresponds to path.
     * 
     * @param path
     * @return
     * @throws SmartThingException
     */
    public IResource getResourceForPath(String path) throws SmartThingException;

    /**
     * Get an asset resource based on relative path.
     * 
     * @param path
     * @return
     * @throws SmartThingException
     */
    public IResource getAssetResource(String path) throws SmartThingException;

    /**
     * Get a script resource based on relative path.
     * 
     * @param path
     * @return
     * @throws SmartThingException
     */
    public IResource getScriptResource(String path) throws SmartThingException;

    /**
     * Gets the active configuration resource for a given tenant.
     * 
     * @return
     * @throws SmartThingException
     */
    public IResource getActiveTenantConfiguration() throws SmartThingException;

    /**
     * Copies all resources from the tenant template into the tenant.
     * 
     * @return
     * @throws SmartThingException
     */
    public void copyTenantTemplateResources() throws SmartThingException;

    /**
     * Gets the staged configuration resource for a given tenant. Returns null
     * if no configuration is staged.
     * 
     * @return
     * @throws SmartThingException
     */
    public IResource getStagedTenantConfiguration() throws SmartThingException;

    /**
     * Stage a new tenant configuration. This stores the new configuration
     * separately from the active configuration. The staged configuration will
     * be made active the next time the tenant is restarted.
     * 
     * @param content
     * @return
     * @throws SmartThingException
     */
    public IResource stageTenantConfiguration(byte[] content) throws SmartThingException;

    /**
     * Transition the staged tenant configuration to the active tenant
     * configuration, backing up the active configuration in the process.
     * 
     * @throws SmartThingException
     */
    public void transitionStagedToActiveTenantConfiguration() throws SmartThingException;
}