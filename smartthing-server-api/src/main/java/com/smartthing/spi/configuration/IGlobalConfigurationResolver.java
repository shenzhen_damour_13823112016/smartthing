/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.configuration;

import java.net.URI;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.resource.IResource;
import com.smartthing.spi.resource.IResourceManager;
import com.smartthing.spi.system.IVersion;

/**
 * Pluggable resolver for interacting with global configuration data.
 * 
 * @author Derek
 */
public interface IGlobalConfigurationResolver {

    /**
     * Gets the URI for the root folder on the filesystem where configuration
     * elements can be stored. This is being phased out by use of the
     * {@link IResourceManager} implementation and will be removed.
     * 
     * @return
     * @throws SmartThingException
     */
    @Deprecated
    public URI getFilesystemConfigurationRoot() throws SmartThingException;

    /**
     * Gets a resource for the given global path.
     * 
     * @param path
     * @return
     * @throws SmartThingException
     */
    public IResource getResourceForPath(String path) throws SmartThingException;

    /**
     * Get an asset resource based on relative path.
     * 
     * @param path
     * @return
     * @throws SmartThingException
     */
    public IResource getAssetResource(String path) throws SmartThingException;

    /**
     * Get a script resource based on relative path.
     * 
     * @param path
     * @return
     * @throws SmartThingException
     */
    public IResource getScriptResource(String path) throws SmartThingException;

    /**
     * Get the global configuration resource.
     * 
     * @param version
     * @return
     * @throws SmartThingException
     */
    public IResource getGlobalConfiguration(IVersion version) throws SmartThingException;

    /**
     * Get server state information as a resource.
     * 
     * @param version
     * @return
     * @throws SmartThingException
     */
    public IResource resolveServerState(IVersion version) throws SmartThingException;

    /**
     * Store information about server state.
     * 
     * @param version
     * @param data
     * @throws SmartThingException
     */
    public void storeServerState(IVersion version, byte[] data) throws SmartThingException;
}