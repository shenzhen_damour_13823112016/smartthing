/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.server;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.system.IVersion;

/**
 * Common interface implemented by SiteWhere applications.
 * 
 * @author Derek
 */
public interface ISmartThingApplication {

    /**
     * Get SiteWhere server class managed by this application.
     * 
     * @return
     * @throws SmartThingException
     */
    public Class<? extends ISmartThingServer> getServerClass() throws SmartThingException;

    /**
     * Get class that provides version information.
     * 
     * @return
     * @throws SmartThingException
     */
    public Class<? extends IVersion> getVersionClass() throws SmartThingException;

    /**
     * Creates progress monitor used when the application is started.
     * 
     * @return
     */
    public ILifecycleProgressMonitor createStartupProgressMonitor();
}