/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.server;

import java.util.List;
import java.util.Map;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.smartthing.spi.ServerStartupException;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.IAssetManagement;
import com.smartthing.spi.asset.IAssetModuleManager;
import com.smartthing.spi.configuration.IGlobalConfigurationResolver;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.IDeviceManagementCacheProvider;
import com.smartthing.spi.device.communication.IDeviceCommunication;
import com.smartthing.spi.device.event.IDeviceEventManagement;
import com.smartthing.spi.device.event.IEventProcessing;
import com.smartthing.spi.resource.IResourceManager;
import com.smartthing.spi.scheduling.IScheduleManagement;
import com.smartthing.spi.scheduling.IScheduleManager;
import com.smartthing.spi.search.external.ISearchProviderManager;
import com.smartthing.spi.server.debug.ITracer;
import com.smartthing.spi.server.groovy.IGroovyConfiguration;
import com.smartthing.spi.server.groovy.ITenantGroovyConfiguration;
import com.smartthing.spi.server.hazelcast.IHazelcastConfiguration;
import com.smartthing.spi.server.lifecycle.ILifecycleComponent;
import com.smartthing.spi.server.lifecycle.ILifecycleHierarchyRoot;
import com.smartthing.spi.server.tenant.ISmartThingTenantEngine;
import com.smartthing.spi.server.tenant.ITenantTemplateManager;
import com.smartthing.spi.system.IVersion;
import com.smartthing.spi.tenant.ITenant;
import com.smartthing.spi.tenant.ITenantManagement;
import com.smartthing.spi.user.IUserManagement;

/**
 * Interface for interacting with core SiteWhere server functionality.
 * 
 * @author Derek
 */
public interface ISmartThingServer extends ILifecycleComponent, ILifecycleHierarchyRoot {

    /** SiteWhere home environment variable name */
    public static final String ENV_SMARTTHING_HOME = "smartthing.home";

    /**
     * Get version information.
     * 
     * @return
     */
    public IVersion getVersion();

    /**
     * Get Spring parser class name for handling bean definitions.
     * 
     * @return
     */
    public String getConfigurationParserClassname();

    /**
     * Get Spring parser class name for handling tenant bean definitions.
     * 
     * @return
     */
    public String getTenantConfigurationParserClassname();

    /**
     * Get persistent server state information.
     * 
     * @return
     */
    public ISmartThingServerState getServerState();

    /**
     * Gets runtime information about the server.
     * 
     * @param includeHistorical
     * @return
     * @throws SmartThingException
     */
    public ISmartThingServerRuntime getServerRuntimeInformation(boolean includeHistorical) throws SmartThingException;

    /**
     * Returns exception if one was thrown on startup.
     * 
     * @return
     */
    public ServerStartupException getServerStartupError();

    /**
     * Set server startup error reason.
     * 
     * @param e
     */
    public void setServerStartupError(ServerStartupException e);

    /**
     * Get tracer for debug operations.
     * 
     * @return
     */
    public ITracer getTracer();

    /**
     * Get the Hazelcast configuration for this node.
     * 
     * @return
     */
    public IHazelcastConfiguration getHazelcastConfiguration();

    /**
     * Get the common Groovy configuration for this node.
     * 
     * @return
     */
    public IGroovyConfiguration getGroovyConfiguration();

    /**
     * Get resource manager for bootstrapping the system.
     * 
     * @return
     */
    public IResourceManager getBootstrapResourceManager();

    /**
     * Get resource manager for resolving runtime resource references.
     * 
     * @return
     */
    public IResourceManager getRuntimeResourceManager();

    /**
     * Get class that can be used to location the Spring configuration context.
     * 
     * @return
     */
    public IGlobalConfigurationResolver getConfigurationResolver();

    /**
     * Get tenant template manager implementation.
     * 
     * @return
     */
    public ITenantTemplateManager getTenantTemplateManager();

    /**
     * Get a tenant based on its authentication token.
     * 
     * @param authToken
     * @return
     * @throws SmartThingException
     */
    public ITenant getTenantByAuthToken(String authToken) throws SmartThingException;

    /**
     * Get list of tenants a given user can access.
     * 
     * @param userId
     * @param requireStarted
     * @return
     * @throws SmartThingException
     */
    public List<ITenant> getAuthorizedTenants(String userId, boolean requireStarted) throws SmartThingException;

    /**
     * Get tenant engines indexed by tenant id.
     * 
     * @return
     */
    public Map<String, ISmartThingTenantEngine> getTenantEnginesById();

    /**
     * Get a tenant engine by tenant id. If a tenant exists but the engine has
     * not been initialized, the tenant engine will be initialized and started.
     * 
     * @param tenantId
     * @return
     * @throws SmartThingException
     */
    public ISmartThingTenantEngine getTenantEngine(String tenantId) throws SmartThingException;

    /**
     * Called when tenant information has been updated so that cached data is
     * kept current.
     * 
     * @param tenant
     * @throws SmartThingException
     */
    public void onTenantUpdated(ITenant tenant) throws SmartThingException;

    /**
     * Called when a tenant is deleted.
     * 
     * @param tenant
     * @throws SmartThingException
     */
    public void onTenantDeleted(ITenant tenant) throws SmartThingException;

    /**
     * Get the user management implementation.
     * 
     * @return
     */
    public IUserManagement getUserManagement();

    /**
     * Get the tenant management implementation.
     * 
     * @return
     */
    public ITenantManagement getTenantManagement();

    /**
     * Get device management implementation for tenant.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public IDeviceManagement getDeviceManagement(ITenant tenant) throws SmartThingException;

    /**
     * Get device event management implementation for tenant.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public IDeviceEventManagement getDeviceEventManagement(ITenant tenant) throws SmartThingException;

    /**
     * Get device management cache provider for tenant.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public IDeviceManagementCacheProvider getDeviceManagementCacheProvider(ITenant tenant) throws SmartThingException;

    /**
     * Get asset management implementation for the given tenant.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public IAssetManagement getAssetManagement(ITenant tenant) throws SmartThingException;

    /**
     * Get schedule management implementation for the given tenant.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public IScheduleManagement getScheduleManagement(ITenant tenant) throws SmartThingException;

    /**
     * Get device communication subsystem for the given tenant.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public IDeviceCommunication getDeviceCommunication(ITenant tenant) throws SmartThingException;

    /**
     * Get the event processing subsystem for the given tenant.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public IEventProcessing getEventProcessing(ITenant tenant) throws SmartThingException;

    /**
     * Get asset module manager for tenant.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public IAssetModuleManager getAssetModuleManager(ITenant tenant) throws SmartThingException;

    /**
     * Get search provider manager for tenant.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public ISearchProviderManager getSearchProviderManager(ITenant tenant) throws SmartThingException;

    /**
     * Get schedule manager for tenant.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public IScheduleManager getScheduleManager(ITenant tenant) throws SmartThingException;

    /**
     * Get Groovy configuration for tenant.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public ITenantGroovyConfiguration getTenantGroovyConfiguration(ITenant tenant) throws SmartThingException;

    /**
     * Get the metrics registry.
     * 
     * @return
     */
    public MetricRegistry getMetricRegistry();

    /**
     * Get the health check registry.
     * 
     * @return
     */
    public HealthCheckRegistry getHealthCheckRegistry();
}