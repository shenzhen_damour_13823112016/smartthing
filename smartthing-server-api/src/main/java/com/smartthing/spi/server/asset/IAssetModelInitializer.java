/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.server.asset;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.IAssetManagement;
import com.smartthing.spi.asset.IAssetModuleManager;
import com.smartthing.spi.server.IModelInitializer;

/**
 * Class that initializes the asset model with data needed to bootstrap the
 * system.
 * 
 * @author Derek
 */
public interface IAssetModelInitializer extends IModelInitializer {

    /**
     * Initialize the asset model.
     * 
     * @param assetModuleManager
     * @param assetManagement
     * @throws SmartThingException
     */
    public void initialize(IAssetModuleManager assetModuleManager, IAssetManagement assetManagement)
	    throws SmartThingException;
}