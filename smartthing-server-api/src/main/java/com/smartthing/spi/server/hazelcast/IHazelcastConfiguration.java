package com.smartthing.spi.server.hazelcast;

import com.hazelcast.core.HazelcastInstance;
import com.smartthing.spi.server.lifecycle.ILifecycleComponent;

/**
 * Wraps Hazelcast instance configuration.
 * 
 * @author Derek
 */
public interface IHazelcastConfiguration extends ILifecycleComponent {

    /**
     * Get Hazelcast instance for this node.
     * 
     * @return
     */
    public HazelcastInstance getHazelcastInstance();
}
