/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.server.tenant;

import org.springframework.context.ApplicationContext;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.IAssetManagement;
import com.smartthing.spi.asset.IAssetModuleManager;
import com.smartthing.spi.command.ICommandResponse;
import com.smartthing.spi.configuration.IGlobalConfigurationResolver;
import com.smartthing.spi.configuration.ITenantConfigurationResolver;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.IDeviceManagementCacheProvider;
import com.smartthing.spi.device.communication.IDeviceCommunication;
import com.smartthing.spi.device.event.IDeviceEventManagement;
import com.smartthing.spi.device.event.IEventProcessing;
import com.smartthing.spi.scheduling.IScheduleManagement;
import com.smartthing.spi.scheduling.IScheduleManager;
import com.smartthing.spi.search.external.ISearchProviderManager;
import com.smartthing.spi.server.ITenantRuntimeState;
import com.smartthing.spi.server.groovy.ITenantGroovyConfiguration;
import com.smartthing.spi.server.lifecycle.ILifecycleHierarchyRoot;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * A SiteWhere tenant engine wraps up the processing pipeline and data storage
 * for a single SiteWhere tenant.
 * 
 * @author Derek
 */
public interface ISmartThingTenantEngine extends ITenantLifecycleComponent, ILifecycleHierarchyRoot {

    /**
     * Get resolver for global configuration elements.
     * 
     * @return
     */
    public IGlobalConfigurationResolver getGlobalConfigurationResolver();

    /**
     * Get resolver for tenant configuration elements.
     * 
     * @return
     */
    public ITenantConfigurationResolver getTenantConfigurationResolver();

    /**
     * Get Spring {@link ApplicationContext} used to configure the tenant
     * engine.
     * 
     * @return
     */
    public ApplicationContext getSpringContext();

    /**
     * Get tenant-scoped Groovy configuration.
     * 
     * @return
     */
    public ITenantGroovyConfiguration getGroovyConfiguration();

    /**
     * Get the device management implementation.
     * 
     * @return
     */
    public IDeviceManagement getDeviceManagement();

    /**
     * Get the device event management implementation.
     * 
     * @return
     */
    public IDeviceEventManagement getDeviceEventManagement();

    /**
     * Get the asset management implementation.
     * 
     * @return
     */
    public IAssetManagement getAssetManagement();

    /**
     * Get the schedule management implementation.
     * 
     * @return
     * @throws SmartThingException
     */
    public IScheduleManagement getScheduleManagement() throws SmartThingException;

    /**
     * Get service for bootstrapping tenant data.
     * 
     * @return
     */
    public ITenantBootstrapService getBootstrapService();

    /**
     * Get the device management cache provider implementation.
     * 
     * @return
     */
    public IDeviceManagementCacheProvider getDeviceManagementCacheProvider();

    /**
     * Get the device communication subsystem implementation.
     * 
     * @return
     */
    public IDeviceCommunication getDeviceCommunication();

    /**
     * Get the event processing subsystem implementation.
     * 
     * @return
     */
    public IEventProcessing getEventProcessing();

    /**
     * Get the asset modules manager instance.
     * 
     * @return
     */
    public IAssetModuleManager getAssetModuleManager();

    /**
     * Get the search provider manager implementation.
     * 
     * @return
     */
    public ISearchProviderManager getSearchProviderManager();

    /**
     * Get the schedule manager implementation.
     * 
     * @return
     * @throws SmartThingException
     */
    public IScheduleManager getScheduleManager() throws SmartThingException;

    /**
     * Get current runtime state of engine.
     * 
     * @return
     */
    public ITenantRuntimeState getEngineState();

    /**
     * Get state information that is persisted across engine restarts.
     * 
     * @return
     * @throws SmartThingException
     */
    public ITenantPersistentState getPersistentState() throws SmartThingException;

    /**
     * Persist the engine state.
     * 
     * @param state
     * @throws SmartThingException
     */
    public void persistState(ITenantPersistentState state) throws SmartThingException;

    /**
     * Issue a command to the tenant engine.
     * 
     * @param command
     * @param monitor
     * @return
     * @throws SmartThingException
     */
    public ICommandResponse issueCommand(String command, ILifecycleProgressMonitor monitor) throws SmartThingException;
}