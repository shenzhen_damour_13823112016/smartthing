/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.server.scheduling;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.scheduling.IScheduleManagement;
import com.smartthing.spi.server.IModelInitializer;

/**
 * Class that initializes the schedule model with data needed to bootstrap the
 * system.
 * 
 * @author Derek
 */
public interface IScheduleModelInitializer extends IModelInitializer {

    /**
     * Intialize schedule management.
     * 
     * @param scheduleManagement
     * @throws SmartThingException
     */
    public void initialize(IScheduleManagement scheduleManagement) throws SmartThingException;
}