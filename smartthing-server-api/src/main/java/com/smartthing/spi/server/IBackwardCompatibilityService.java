package com.smartthing.spi.server;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;

/**
 * Helps with migration of previous server versions to provide compatibility for
 * users that drop the new WAR into an existing server instance.
 * 
 * @author Derek
 */
public interface IBackwardCompatibilityService {

    /**
     * Actions executed before server is initialized.
     * 
     * @param monitor
     * @throws SmartThingException
     */
    public void beforeServerInitialize(ILifecycleProgressMonitor monitor) throws SmartThingException;

    /**
     * Actions executed before server is started.
     * 
     * @param monitor
     * @throws SmartThingException
     */
    public void beforeServerStart(ILifecycleProgressMonitor monitor) throws SmartThingException;

    /**
     * Actions executed after server is started.
     * 
     * @param monitor
     * @throws SmartThingException
     */
    public void afterServerStart(ILifecycleProgressMonitor monitor) throws SmartThingException;
}