/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.rest.model.server;

import com.smartthing.spi.server.ISmartThingServerState;

/**
 * Model implementation of {@link ISmartThingServerState}.
 * 
 * @author Derek
 */
public class SmartThingServerState implements ISmartThingServerState {

    /** Unique node id */
    private String nodeId;

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServerState#getNodeId()
     */
    public String getNodeId() {
	return nodeId;
    }

    public void setNodeId(String nodeId) {
	this.nodeId = nodeId;
    }
}