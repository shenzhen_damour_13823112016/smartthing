/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.twilio;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.device.communication.sms.SmsParameters;
import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceNestingContext;
import com.smartthing.spi.device.command.IDeviceCommandExecution;
import com.smartthing.spi.device.communication.ICommandDeliveryProvider;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Account;

/**
 * Implementation of {@link ICommandDeliveryProvider} that sends an SMS message
 * via Twilio.
 * 
 * @author Derek
 */
public class TwilioCommandDeliveryProvider extends TenantLifecycleComponent
	implements ICommandDeliveryProvider<String, SmsParameters> {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Account SID */
    private String accountSid;

    /** Auth token */
    private String authToken;

    /** Phone number to send command from */
    private String fromPhoneNumber;

    /** Client for Twilio REST calls */
    private TwilioRestClient twilio;

    /** Twilio account */
    private Account account;

    public TwilioCommandDeliveryProvider() {
	super(LifecycleComponentType.CommandDeliveryProvider);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
     * .server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	if (getAccountSid() == null) {
	    throw new SmartThingException("Twilio command delivery provider missing account SID.");
	}
	if (getAuthToken() == null) {
	    throw new SmartThingException("Twilio command delivery provider missing auth token.");
	}
	this.twilio = new TwilioRestClient(getAccountSid(), getAuthToken());
	this.account = twilio.getAccount();
	LOGGER.info("Twilio delivery provider started. Calls will originate from " + getFromPhoneNumber() + ".");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.communication.ICommandDeliveryProvider#deliver(
     * com.smartthing .spi.device.IDeviceNestingContext,
     * com.smartthing.spi.device.IDeviceAssignment,
     * com.smartthing.spi.device.command.IDeviceCommandExecution,
     * java.lang.Object, java.lang.Object)
     */
    @Override
    public void deliver(IDeviceNestingContext nested, IDeviceAssignment assignment, IDeviceCommandExecution execution,
	    String encoded, SmsParameters params) throws SmartThingException {
	LOGGER.info("Delivering SMS command to " + params.getSmsPhoneNumber() + ".");
	sendSms(encoded, getFromPhoneNumber(), params.getSmsPhoneNumber());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.ICommandDeliveryProvider#
     * deliverSystemCommand (com.smartthing.spi.device.IDeviceNestingContext,
     * com.smartthing.spi.device.IDeviceAssignment, java.lang.Object,
     * java.lang.Object)
     */
    @Override
    public void deliverSystemCommand(IDeviceNestingContext nested, IDeviceAssignment assignment, String encoded,
	    SmsParameters params) throws SmartThingException {
	throw new UnsupportedOperationException();
    }

    /**
     * Send an SMS message.
     * 
     * @param message
     * @param from
     * @param to
     * @throws SmartThingException
     */
    protected void sendSms(String message, String from, String to) throws SmartThingException {
	MessageFactory messageFactory = account.getMessageFactory();
	List<NameValuePair> params = new ArrayList<NameValuePair>();
	params.add(new BasicNameValuePair("To", to));
	params.add(new BasicNameValuePair("From", from));
	params.add(new BasicNameValuePair("Body", message));
	try {
	    messageFactory.create(params);
	} catch (TwilioRestException e) {
	    throw new SmartThingException("Unable to send Twilio SMS message.", e);
	}
    }

    public String getAccountSid() {
	return accountSid;
    }

    public void setAccountSid(String accountSid) {
	this.accountSid = accountSid;
    }

    public String getAuthToken() {
	return authToken;
    }

    public void setAuthToken(String authToken) {
	this.authToken = authToken;
    }

    public String getFromPhoneNumber() {
	return fromPhoneNumber;
    }

    public void setFromPhoneNumber(String fromPhoneNumber) {
	this.fromPhoneNumber = fromPhoneNumber;
    }
}