import messagesEn from './locales/en'
import messagesCn from './locales/cn'

const messages = {
  en: {
    message: messagesEn
  },
  cn: {
    message: messagesCn
  }
}

export default messages
