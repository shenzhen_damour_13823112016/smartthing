/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hazelcast;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.ITopic;
import com.smartthing.SmartThing;
import com.smartthing.device.event.processor.FilteredOutboundEventProcessor;
import com.smartthing.device.marshaling.DeviceCommandInvocationMarshalHelper;
import com.smartthing.rest.model.device.event.DeviceAlert;
import com.smartthing.rest.model.device.event.DeviceCommandInvocation;
import com.smartthing.rest.model.device.event.DeviceCommandResponse;
import com.smartthing.rest.model.device.event.DeviceLocation;
import com.smartthing.rest.model.device.event.DeviceMeasurements;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.server.hazelcast.IHazelcastConfiguration;
import com.smartthing.spi.server.hazelcast.ISmartThingHazelcast;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;

/**
 * Sends processed device events out on Hazelcast topics for further processing.
 * 
 * @author Derek
 */
public class HazelcastEventProcessor extends FilteredOutboundEventProcessor {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Topic for device measurements */
    private ITopic<DeviceMeasurements> measurementsTopic;

    /** Topic for device locations */
    private ITopic<DeviceLocation> locationsTopic;

    /** Topic for device alerts */
    private ITopic<DeviceAlert> alertsTopic;

    /** Topic for device command invocations */
    private ITopic<DeviceCommandInvocation> commandInvocationsTopic;

    /** Topic for device command responses */
    private ITopic<DeviceCommandResponse> commandResponsesTopic;

    /** Used for marshaling command invocations */
    private DeviceCommandInvocationMarshalHelper invocationHelper;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.event.processor.FilteredOutboundEventProcessor#start
     * (com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Required for filters.
	super.start(monitor);

	IHazelcastConfiguration hzConfig =SmartThing.getServer().getHazelcastConfiguration();
	this.invocationHelper = new DeviceCommandInvocationMarshalHelper(getTenant(), true);
	this.measurementsTopic = hzConfig.getHazelcastInstance().getTopic(ISmartThingHazelcast.TOPIC_MEASUREMENTS_ADDED);
	this.locationsTopic = hzConfig.getHazelcastInstance().getTopic(ISmartThingHazelcast.TOPIC_LOCATION_ADDED);
	this.alertsTopic = hzConfig.getHazelcastInstance().getTopic(ISmartThingHazelcast.TOPIC_ALERT_ADDED);
	this.commandInvocationsTopic = hzConfig.getHazelcastInstance()
		.getTopic(ISmartThingHazelcast.TOPIC_COMMAND_INVOCATION_ADDED);
	this.commandResponsesTopic = hzConfig.getHazelcastInstance()
		.getTopic(ISmartThingHazelcast.TOPIC_COMMAND_RESPONSE_ADDED);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onMeasurementsNotFiltered(com.smartthing.spi.device.event.
     * IDeviceMeasurements)
     */
    @Override
    public void onMeasurementsNotFiltered(IDeviceMeasurements measurements) throws SmartThingException {
	DeviceMeasurements marshaled = DeviceMeasurements.copy(measurements);
	measurementsTopic.publish(marshaled);
	LOGGER.debug("Published measurements event to Hazelcast (id=" + measurements.getId() + ")");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * onLocationNotFiltered(com.smartthing.spi.device.event.IDeviceLocation)
     */
    @Override
    public void onLocationNotFiltered(IDeviceLocation location) throws SmartThingException {
	DeviceLocation marshaled = DeviceLocation.copy(location);
	locationsTopic.publish(marshaled);
	LOGGER.debug("Published location event to Hazelcast (id=" + location.getId() + ")");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * onAlertNotFiltered(com.smartthing.spi.device.event.IDeviceAlert)
     */
    @Override
    public void onAlertNotFiltered(IDeviceAlert alert) throws SmartThingException {
	DeviceAlert marshaled = DeviceAlert.copy(alert);
	alertsTopic.publish(marshaled);
	LOGGER.debug("Published alert event to Hazelcast (id=" + alert.getId() + ")");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onStateChangeNotFiltered(com.smartthing.spi.device.event.
     * IDeviceStateChange)
     */
    @Override
    public void onStateChangeNotFiltered(IDeviceStateChange state) throws SmartThingException {
	LOGGER.debug(
		"Hazelcast received state change of type: " + state.getCategory().name() + ":" + state.getNewState());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * onCommandInvocationNotFiltered
     * (com.smartthing.spi.device.event.IDeviceCommandInvocation)
     */
    @Override
    public void onCommandInvocationNotFiltered(IDeviceCommandInvocation invocation) throws SmartThingException {
	DeviceCommandInvocation converted = invocationHelper.convert(invocation);
	commandInvocationsTopic.publish(converted);
	LOGGER.debug("Published command invocation event to Hazelcast (id=" + invocation.getId() + ")");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * onCommandResponseNotFiltered(com.smartthing.spi.device.event.
     * IDeviceCommandResponse)
     */
    @Override
    public void onCommandResponseNotFiltered(IDeviceCommandResponse response) throws SmartThingException {
	DeviceCommandResponse marshaled = DeviceCommandResponse.copy(response);
	commandResponsesTopic.publish(marshaled);
	LOGGER.debug("Published command response event to Hazelcast (id=" + response.getId() + ")");
    }
}