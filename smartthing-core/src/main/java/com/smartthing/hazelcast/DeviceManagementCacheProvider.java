/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hazelcast;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.cache.CacheType;
import com.smartthing.spi.cache.ICache;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceManagementCacheProvider;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.ISite;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Implements {@link IDeviceManagementCacheProvider} using Hazelcast as a
 * distributed cache.
 * 
 * @author Derek
 */
public class DeviceManagementCacheProvider extends TenantLifecycleComponent implements IDeviceManagementCacheProvider {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Name of site cache */
    private static final String SITE_CACHE = "siteCache";

    /** Name of device specification cache */
    private static final String SPECIFICATION_CACHE = "specificationCache";

    /** Name of device cache */
    private static final String DEVICE_CACHE = "deviceCache";

    /** Name of assignment cache */
    private static final String ASSIGNMENT_CACHE = "assignmentCache";

    public DeviceManagementCacheProvider() {
	super(LifecycleComponentType.CacheProvider);
    }

    /** Cache for sites */
    private HazelcastCache<ISite> siteCache;

    /** Cache for device specifications */
    private HazelcastCache<IDeviceSpecification> specificationCache;

    /** Cache for devices */
    private HazelcastCache<IDevice> deviceCache;

    /** Cache for device assignments */
    private HazelcastCache<IDeviceAssignment> assignmentCache;

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.server.lifecycle.LifecycleComponent#initialize(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void initialize(ILifecycleProgressMonitor monitor) throws SmartThingException {
	this.siteCache = new HazelcastCache<ISite>(this, HazelcastCache.getNameForTenantCache(getTenant(), SITE_CACHE),
		CacheType.SiteCache, false);
	this.specificationCache = new HazelcastCache<IDeviceSpecification>(this,
		HazelcastCache.getNameForTenantCache(getTenant(), SPECIFICATION_CACHE),
		CacheType.DeviceSpecificationCache, false);
	this.deviceCache = new HazelcastCache<IDevice>(this,
		HazelcastCache.getNameForTenantCache(getTenant(), DEVICE_CACHE), CacheType.DeviceCache, false);
	this.assignmentCache = new HazelcastCache<IDeviceAssignment>(this,
		HazelcastCache.getNameForTenantCache(getTenant(), ASSIGNMENT_CACHE), CacheType.DeviceAssignmentCache,
		false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
     * .server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagementCacheProvider#getSiteCache()
     */
    @Override
    public ICache<String, ISite> getSiteCache() throws SmartThingException {
	return siteCache;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagementCacheProvider#
     * getDeviceSpecificationCache ()
     */
    @Override
    public ICache<String, IDeviceSpecification> getDeviceSpecificationCache() throws SmartThingException {
	return specificationCache;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagementCacheProvider#getDeviceCache()
     */
    @Override
    public ICache<String, IDevice> getDeviceCache() throws SmartThingException {
	return deviceCache;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagementCacheProvider#
     * getDeviceAssignmentCache()
     */
    @Override
    public ICache<String, IDeviceAssignment> getDeviceAssignmentCache() throws SmartThingException {
	return assignmentCache;
    }
}