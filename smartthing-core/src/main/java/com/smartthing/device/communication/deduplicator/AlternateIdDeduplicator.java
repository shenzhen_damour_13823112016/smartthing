/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.communication.deduplicator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.SmartThing;
import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.communication.IDecodedDeviceRequest;
import com.smartthing.spi.device.communication.IDeviceEventDeduplicator;
import com.smartthing.spi.device.event.IDeviceEvent;
import com.smartthing.spi.device.event.request.IDeviceEventCreateRequest;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Implementation of {@link IDeviceEventDeduplicator} that checks the alternate
 * id (if present) in an event against the index already stored in the
 * datastore. If the alternate id is already present, the event is considered a
 * duplicate.
 * 
 * @author Derek
 */
public class AlternateIdDeduplicator extends TenantLifecycleComponent implements IDeviceEventDeduplicator {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    public AlternateIdDeduplicator() {
	super(LifecycleComponentType.DeviceEventDeduplicator);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceEventDeduplicator#
     * isDuplicate(com.smartthing.spi.device.communication.IDecodedDeviceRequest)
     */
    @Override
    public boolean isDuplicate(IDecodedDeviceRequest<?> request) throws SmartThingException {
	if (request.getRequest() instanceof IDeviceEventCreateRequest) {
	    String alternateId = ((IDeviceEventCreateRequest) request.getRequest()).getAlternateId();
	    if (alternateId != null) {
		IDeviceEvent existing =SmartThing.getServer().getDeviceEventManagement(getTenant())
			.getDeviceEventByAlternateId(alternateId);
		if (existing != null) {
		    LOGGER.info("Found event with same alternate id. Will be treated as duplicate.");
		    return true;
		}
		return false;
	    }
	}
	return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }
}