/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.communication.mqtt;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceNestingContext;
import com.smartthing.spi.device.command.IDeviceCommandExecution;
import com.smartthing.spi.device.communication.ICommandDeliveryParameterExtractor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Implements {@link ICommandDeliveryParameterExtractor} for
 * {@link MqttParameters}, allowing expressions to be defined such that the
 * device hardware id may be included in the topic name to target a specific
 * device.
 * 
 * @author Derek
 */
public class HardwareIdMqttParameterExtractor extends TenantLifecycleComponent
	implements ICommandDeliveryParameterExtractor<MqttParameters> {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Default command topic */
    public static final String DEFAULT_COMMAND_TOPIC = "SiteWhere/command/%s";

    /** Default system topic */
    public static final String DEFAULT_SYSTEM_TOPIC = "SiteWhere/system/%s";

    /** Command topic prefix */
    private String commandTopicExpr = DEFAULT_COMMAND_TOPIC;

    /** System topic prefix */
    private String systemTopicExpr = DEFAULT_SYSTEM_TOPIC;

    public HardwareIdMqttParameterExtractor() {
	super(LifecycleComponentType.CommandParameterExtractor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.
     * ICommandDeliveryParameterExtractor#
     * extractDeliveryParameters(com.smartthing.spi.device.IDeviceNestingContext,
     * com.smartthing.spi.device.IDeviceAssignment,
     * com.smartthing.spi.device.command.IDeviceCommandExecution)
     */
    @Override
    public MqttParameters extractDeliveryParameters(IDeviceNestingContext nesting, IDeviceAssignment assignment,
	    IDeviceCommandExecution execution) throws SmartThingException {
	MqttParameters params = new MqttParameters();

	String commandTopic = String.format(getCommandTopicExpr(), nesting.getGateway().getHardwareId());
	params.setCommandTopic(commandTopic);

	String systemTopic = String.format(getSystemTopicExpr(), nesting.getGateway().getHardwareId());
	params.setSystemTopic(systemTopic);

	return params;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    public String getCommandTopicExpr() {
	return commandTopicExpr;
    }

    public void setCommandTopicExpr(String commandTopicExpr) {
	this.commandTopicExpr = commandTopicExpr;
    }

    public String getSystemTopicExpr() {
	return systemTopicExpr;
    }

    public void setSystemTopicExpr(String systemTopicExpr) {
	this.systemTopicExpr = systemTopicExpr;
    }
}