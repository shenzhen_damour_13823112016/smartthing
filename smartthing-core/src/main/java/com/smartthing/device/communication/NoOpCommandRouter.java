/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.communication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceNestingContext;
import com.smartthing.spi.device.command.IDeviceCommandExecution;
import com.smartthing.spi.device.command.ISystemCommand;
import com.smartthing.spi.device.communication.IOutboundCommandRouter;

/**
 * Implementation of {@link IOutboundCommandRouter} that does nothing with
 * commands. Used as default for situations where command routing is not needed.
 * 
 * @author Derek
 */
public class NoOpCommandRouter extends OutboundCommandRouter {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IOutboundCommandRouter#
     * routeCommand(com. sitewhere.spi.device.command.IDeviceCommandExecution,
     * com.smartthing.spi.device.IDeviceNestingContext,
     * com.smartthing.spi.device.IDeviceAssignment)
     */
    @Override
    public void routeCommand(IDeviceCommandExecution execution, IDeviceNestingContext nesting,
	    IDeviceAssignment assignment) throws SmartThingException {
	LOGGER.warn("Ignoring routing of command. Add a command router to allow commands to be sent to destinations.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IOutboundCommandRouter#
     * routeSystemCommand (com.smartthing.spi.device.command.ISystemCommand,
     * com.smartthing.spi.device.IDeviceNestingContext,
     * com.smartthing.spi.device.IDeviceAssignment)
     */
    @Override
    public void routeSystemCommand(ISystemCommand command, IDeviceNestingContext nesting, IDeviceAssignment assignment)
	    throws SmartThingException {
	LOGGER.warn(
		"Ignoring routing of system command. Add a command router to allow commands to be sent to destinations.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }
}