/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.communication;

import java.util.ArrayList;
import java.util.List;

import com.smartthing.device.communication.symbology.SymbolGeneratorManager;
import com.smartthing.server.batch.BatchOperationManager;
import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.batch.IBatchOperationManager;
import com.smartthing.spi.device.command.ISystemCommand;
import com.smartthing.spi.device.communication.ICommandDestination;
import com.smartthing.spi.device.communication.ICommandProcessingStrategy;
import com.smartthing.spi.device.communication.IDeviceCommunication;
import com.smartthing.spi.device.communication.IDeviceStreamManager;
import com.smartthing.spi.device.communication.IInboundEventSource;
import com.smartthing.spi.device.communication.IOutboundCommandRouter;
import com.smartthing.spi.device.communication.IRegistrationManager;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.presence.IDevicePresenceManager;
import com.smartthing.spi.device.symbology.ISymbolGeneratorManager;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Base class for implementations of {@link IDeviceCommunication}. Takes care of
 * starting and stopping nested components in the correct order.
 * 
 * @author Derek
 */
public abstract class DeviceCommunication extends TenantLifecycleComponent implements IDeviceCommunication {

    /** Configured registration manager */
    private IRegistrationManager registrationManager = new RegistrationManager();

    /** Configured symbol generator manager */
    private ISymbolGeneratorManager symbolGeneratorManager = new SymbolGeneratorManager();

    /** Configured batch operation manager */
    private IBatchOperationManager batchOperationManager = new BatchOperationManager();

    /** Configured device stream manager */
    private IDeviceStreamManager deviceStreamManager = new DeviceStreamManager();

    /** Configured device presence manager */
    private IDevicePresenceManager devicePresenceManager;

    /** Configured list of inbound event sources */
    private List<IInboundEventSource<?>> inboundEventSources = new ArrayList<IInboundEventSource<?>>();

    /** Configured command processing strategy */
    private ICommandProcessingStrategy commandProcessingStrategy = new DefaultCommandProcessingStrategy();

    /** Configured outbound command router */
    private IOutboundCommandRouter outboundCommandRouter = new NoOpCommandRouter();

    /** Configured list of command destinations */
    private List<ICommandDestination<?, ?>> commandDestinations = new ArrayList<ICommandDestination<?, ?>>();

    public DeviceCommunication() {
	super(LifecycleComponentType.DeviceCommunication);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#start(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	getLifecycleComponents().clear();

	// Start command processing strategy.
	if (getCommandProcessingStrategy() == null) {
	    throw new SmartThingException("No command processing strategy configured for communication subsystem.");
	}
	startNestedComponent(getCommandProcessingStrategy(), monitor, true);

	// Start command destinations.
	if (getCommandDestinations() != null) {
	    for (ICommandDestination<?, ?> destination : getCommandDestinations()) {
		startNestedComponent(destination, monitor, false);
	    }
	}

	// Start outbound command router.
	if (getOutboundCommandRouter() == null) {
	    throw new SmartThingException("No command router for communication subsystem.");
	}
	getOutboundCommandRouter().initialize(getCommandDestinations());
	startNestedComponent(getOutboundCommandRouter(), monitor, true);

	// Start registration manager.
	if (getRegistrationManager() == null) {
	    throw new SmartThingException("No registration manager configured for communication subsystem.");
	}
	startNestedComponent(getRegistrationManager(), monitor, true);

	// Start symbol generator manager.
	if (getSymbolGeneratorManager() == null) {
	    throw new SmartThingException("No symbol generator manager configured for communication subsystem.");
	}
	startNestedComponent(getSymbolGeneratorManager(), monitor, true);

	// Start batch operation manager.
	if (getBatchOperationManager() == null) {
	    throw new SmartThingException("No batch operation manager configured for communication subsystem.");
	}
	startNestedComponent(getBatchOperationManager(), monitor, true);

	// Start device stream manager.
	if (getDeviceStreamManager() == null) {
	    throw new SmartThingException("No device stream manager configured for communication subsystem.");
	}
	startNestedComponent(getDeviceStreamManager(), monitor, true);

	// Start device presence manager if configured.
	if (getDevicePresenceManager() != null) {
	    startNestedComponent(getDevicePresenceManager(), monitor, true);
	}

	// Start device event sources.
	if (getInboundEventSources() != null) {
	    for (IInboundEventSource<?> processor : getInboundEventSources()) {
		startNestedComponent(processor, monitor, false);
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ILifecycleComponent#stop(com.smartthing
     * .spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Stop inbound event sources.
	if (getInboundEventSources() != null) {
	    for (IInboundEventSource<?> processor : getInboundEventSources()) {
		processor.lifecycleStop(monitor);
	    }
	}

	// Stop device stream manager.
	if (getDevicePresenceManager() != null) {
	    getDevicePresenceManager().lifecycleStop(monitor);
	}

	// Stop device stream manager.
	if (getDeviceStreamManager() != null) {
	    getDeviceStreamManager().lifecycleStop(monitor);
	}

	// Stop batch operation manager.
	if (getBatchOperationManager() != null) {
	    getBatchOperationManager().lifecycleStop(monitor);
	}

	// Stop symbol generator manager.
	if (getSymbolGeneratorManager() != null) {
	    getSymbolGeneratorManager().lifecycleStop(monitor);
	}

	// Stop registration manager.
	if (getRegistrationManager() != null) {
	    getRegistrationManager().lifecycleStop(monitor);
	}

	// Stop command processing strategy.
	if (getCommandProcessingStrategy() != null) {
	    getCommandProcessingStrategy().lifecycleStop(monitor);
	}

	// Start command destinations.
	if (getCommandDestinations() != null) {
	    for (ICommandDestination<?, ?> destination : getCommandDestinations()) {
		destination.lifecycleStop(monitor);
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceCommunication#
     * deliverCommand(com. sitewhere.spi.device.event.IDeviceCommandInvocation)
     */
    @Override
    public void deliverCommand(IDeviceCommandInvocation invocation) throws SmartThingException {
	getCommandProcessingStrategy().deliverCommand(this, invocation);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceCommunication#
     * deliverSystemCommand (java.lang.String,
     * com.smartthing.spi.device.command.ISystemCommand)
     */
    @Override
    public void deliverSystemCommand(String hardwareId, ISystemCommand command) throws SmartThingException {
	getCommandProcessingStrategy().deliverSystemCommand(this, hardwareId, command);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceCommunication#
     * getRegistrationManager ()
     */
    public IRegistrationManager getRegistrationManager() {
	return registrationManager;
    }

    public void setRegistrationManager(IRegistrationManager registrationManager) {
	this.registrationManager = registrationManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceCommunication#
     * getSymbolGeneratorManager ()
     */
    public ISymbolGeneratorManager getSymbolGeneratorManager() {
	return symbolGeneratorManager;
    }

    public void setSymbolGeneratorManager(ISymbolGeneratorManager symbolGeneratorManager) {
	this.symbolGeneratorManager = symbolGeneratorManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceCommunication#
     * getBatchOperationManager ()
     */
    public IBatchOperationManager getBatchOperationManager() {
	return batchOperationManager;
    }

    public void setBatchOperationManager(IBatchOperationManager batchOperationManager) {
	this.batchOperationManager = batchOperationManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceCommunication#
     * getDeviceStreamManager ()
     */
    public IDeviceStreamManager getDeviceStreamManager() {
	return deviceStreamManager;
    }

    public void setDeviceStreamManager(IDeviceStreamManager deviceStreamManager) {
	this.deviceStreamManager = deviceStreamManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceCommunication#
     * getDevicePresenceManager()
     */
    public IDevicePresenceManager getDevicePresenceManager() {
	return devicePresenceManager;
    }

    public void setDevicePresenceManager(IDevicePresenceManager devicePresenceManager) {
	this.devicePresenceManager = devicePresenceManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceCommunication#
     * getInboundEventSources ()
     */
    public List<IInboundEventSource<?>> getInboundEventSources() {
	return inboundEventSources;
    }

    public void setInboundEventSources(List<IInboundEventSource<?>> inboundEventSources) {
	this.inboundEventSources = inboundEventSources;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceCommunication#
     * getCommandProcessingStrategy()
     */
    public ICommandProcessingStrategy getCommandProcessingStrategy() {
	return commandProcessingStrategy;
    }

    public void setCommandProcessingStrategy(ICommandProcessingStrategy commandProcessingStrategy) {
	this.commandProcessingStrategy = commandProcessingStrategy;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceCommunication#
     * getOutboundCommandRouter ()
     */
    public IOutboundCommandRouter getOutboundCommandRouter() {
	return outboundCommandRouter;
    }

    public void setOutboundCommandRouter(IOutboundCommandRouter outboundCommandRouter) {
	this.outboundCommandRouter = outboundCommandRouter;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceCommunication#
     * getCommandDestinations ()
     */
    public List<ICommandDestination<?, ?>> getCommandDestinations() {
	return commandDestinations;
    }

    public void setCommandDestinations(List<ICommandDestination<?, ?>> commandDestinations) {
	this.commandDestinations = commandDestinations;
    }
}