package com.smartthing.device.communication.decoder.composite;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.communication.ICompositeDeviceEventDecoder.IDecoderChoice;
import com.smartthing.spi.device.communication.ICompositeDeviceEventDecoder.IDeviceContext;
import com.smartthing.spi.device.communication.IDeviceEventDecoder;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Decoder choice which applies if the device implements a given device
 * specification.
 * 
 * @author Derek
 *
 * @param <T>
 */
public class DeviceSpecificationDecoderChoice<T> extends TenantLifecycleComponent implements IDecoderChoice<T> {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Specification token to match */
    private String deviceSpecificationToken;

    /** Decoder used if context applies */
    private IDeviceEventDecoder<T> deviceEventDecoder;

    public DeviceSpecificationDecoderChoice() {
	super(LifecycleComponentType.DeviceEventDecoder);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.ICompositeDeviceEventDecoder.
     * IDecoderChoice#appliesTo(com.smartthing.spi.device.communication.
     * ICompositeDeviceEventDecoder.IDeviceContext)
     */
    @Override
    public boolean appliesTo(IDeviceContext<T> criteria) {
	if (criteria.getDeviceSpecification().getToken().equals(getDeviceSpecificationToken())) {
	    return true;
	}
	return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.ICompositeDeviceEventDecoder.
     * IDecoderChoice#getDeviceEventDecoder()
     */
    public IDeviceEventDecoder<T> getDeviceEventDecoder() {
	return deviceEventDecoder;
    }

    public void setDeviceEventDecoder(IDeviceEventDecoder<T> deviceEventDecoder) {
	this.deviceEventDecoder = deviceEventDecoder;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#start(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	startNestedComponent(getDeviceEventDecoder(), monitor, "Event decoder startup failed.", true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ILifecycleComponent#stop(com.smartthing
     * .spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
	getDeviceEventDecoder().lifecycleStop(monitor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    public String getDeviceSpecificationToken() {
	return deviceSpecificationToken;
    }

    public void setDeviceSpecificationToken(String deviceSpecificationToken) {
	this.deviceSpecificationToken = deviceSpecificationToken;
    }
}