/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.communication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.SmartThing;
import com.smartthing.rest.model.device.command.DeviceStreamAckCommand;
import com.smartthing.rest.model.device.command.SendDeviceStreamDataCommand;
import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.command.DeviceStreamStatus;
import com.smartthing.spi.device.communication.IDeviceStreamManager;
import com.smartthing.spi.device.event.IDeviceStreamData;
import com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest;
import com.smartthing.spi.device.event.request.ISendDeviceStreamDataRequest;
import com.smartthing.spi.device.streaming.IDeviceStream;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Default {@link IDeviceStreamManager} implementation.
 * 
 * @author Derek
 */
public class DeviceStreamManager extends TenantLifecycleComponent implements IDeviceStreamManager {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    public DeviceStreamManager() {
	super(LifecycleComponentType.DeviceStreamManager);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceStreamManager#
     * handleDeviceStreamRequest (java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest)
     */
    @Override
    public void handleDeviceStreamRequest(String hardwareId, IDeviceStreamCreateRequest request)
	    throws SmartThingException {
	IDeviceAssignment assignment = getCurrentAssignment(hardwareId);

	DeviceStreamAckCommand ack = new DeviceStreamAckCommand();
	ack.setStreamId(request.getStreamId());
	IDeviceStream existing =SmartThing.getServer().getDeviceManagement(getTenant())
		.getDeviceStream(assignment.getToken(), request.getStreamId());
	if (existing != null) {
	    ack.setStatus(DeviceStreamStatus.DeviceStreamExists);
	} else {
	    try {
		SmartThing.getServer().getDeviceManagement(getTenant()).createDeviceStream(assignment.getToken(),
			request);
		ack.setStatus(DeviceStreamStatus.DeviceStreamCreated);
	    } catch (SmartThingException e) {
		LOGGER.error("Unable to create device stream.", e);
		ack.setStatus(DeviceStreamStatus.DeviceStreamFailed);
	    }
	}
	SmartThing.getServer().getDeviceCommunication(getTenant()).deliverSystemCommand(hardwareId, ack);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceStreamManager#
     * handleDeviceStreamDataRequest(java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest)
     */
    @Override
    public void handleDeviceStreamDataRequest(String hardwareId, IDeviceStreamDataCreateRequest request)
	    throws SmartThingException {
	IDeviceAssignment assignment = getCurrentAssignment(hardwareId);
	SmartThing.getServer().getDeviceEventManagement(getTenant()).addDeviceStreamData(assignment.getToken(), request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IDeviceStreamManager#
     * handleSendDeviceStreamDataRequest(java.lang.String,
     * com.smartthing.spi.device.event.request.ISendDeviceStreamDataRequest)
     */
    @Override
    public void handleSendDeviceStreamDataRequest(String hardwareId, ISendDeviceStreamDataRequest request)
	    throws SmartThingException {
	IDeviceAssignment assignment = getCurrentAssignment(hardwareId);
	IDeviceStreamData data =SmartThing.getServer().getDeviceEventManagement(getTenant())
		.getDeviceStreamData(assignment.getToken(), request.getStreamId(), request.getSequenceNumber());
	SendDeviceStreamDataCommand command = new SendDeviceStreamDataCommand();
	command.setStreamId(request.getStreamId());
	command.setSequenceNumber(request.getSequenceNumber());
	command.setHardwareId(hardwareId);
	if (data != null) {
	    command.setData(data.getData());
	} else {
	    command.setData(new byte[0]);
	}
	SmartThing.getServer().getDeviceCommunication(getTenant()).deliverSystemCommand(hardwareId, command);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /**
     * Get the current assignment or throw errors if it can not be resolved.
     * 
     * @param hardwareId
     * @return
     * @throws SmartThingException
     */
    protected IDeviceAssignment getCurrentAssignment(String hardwareId) throws SmartThingException {
	IDevice device =SmartThing.getServer().getDeviceManagement(getTenant()).getDeviceByHardwareId(hardwareId);
	if (device == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidHardwareId, ErrorLevel.ERROR);
	}
	if (device.getAssignmentToken() == null) {
	    throw new SmartThingSystemException(ErrorCode.DeviceNotAssigned, ErrorLevel.ERROR);
	}
	return SmartThing.getServer().getDeviceManagement(getTenant())
		.getDeviceAssignmentByToken(device.getAssignmentToken());
    }
}