/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.communication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceNestingContext;
import com.smartthing.spi.device.command.IDeviceCommandExecution;
import com.smartthing.spi.device.communication.ICommandDeliveryParameterExtractor;
import com.smartthing.spi.device.communication.ICommandDeliveryProvider;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Placeholder object for {@link ICommandDeliveryProvider} that do not require
 * parameters.
 * 
 * @author Derek
 */
public class NullParameters {

    /**
     * Implementation of {@link ICommandDeliveryParameterExtractor} that returns
     * {@link NullParameters}.
     * 
     * @author Derek
     */
    public static class Extractor extends TenantLifecycleComponent
	    implements ICommandDeliveryParameterExtractor<NullParameters> {

	/** Static logger instance */
	private static Logger LOGGER = LogManager.getLogger();

	/** Value to be returned */
	private NullParameters parameters = new NullParameters();

	public Extractor() {
	    super(LifecycleComponentType.CommandParameterExtractor);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.smartthing.spi.device.communication.
	 * ICommandDeliveryParameterExtractor#
	 * extractDeliveryParameters(com.smartthing.spi.device.
	 * IDeviceNestingContext, com.smartthing.spi.device.IDeviceAssignment,
	 * com.smartthing.spi.device.command.IDeviceCommandExecution)
	 */
	@Override
	public NullParameters extractDeliveryParameters(IDeviceNestingContext nesting, IDeviceAssignment assignment,
		IDeviceCommandExecution execution) throws SmartThingException {
	    return parameters;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
	 */
	@Override
	public Logger getLogger() {
	    return LOGGER;
	}
    }
}