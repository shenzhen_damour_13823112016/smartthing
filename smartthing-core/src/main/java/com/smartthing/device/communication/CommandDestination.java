/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.communication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceNestingContext;
import com.smartthing.spi.device.command.IDeviceCommandExecution;
import com.smartthing.spi.device.command.ISystemCommand;
import com.smartthing.spi.device.communication.ICommandDeliveryParameterExtractor;
import com.smartthing.spi.device.communication.ICommandDeliveryProvider;
import com.smartthing.spi.device.communication.ICommandDestination;
import com.smartthing.spi.device.communication.ICommandExecutionEncoder;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Default implementation of {@link ICommandDestination}.
 * 
 * @author Derek
 * 
 * @param <T>
 */
public class CommandDestination<T, P> extends TenantLifecycleComponent implements ICommandDestination<T, P> {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Unique destination id */
    private String destinationId;

    /** Configured command execution encoder */
    private ICommandExecutionEncoder<T> commandExecutionEncoder;

    /** Configured command delivery parameter extractor */
    private ICommandDeliveryParameterExtractor<P> commandDeliveryParameterExtractor;

    /** Configured command delivery provider */
    private ICommandDeliveryProvider<T, P> commandDeliveryProvider;

    public CommandDestination() {
	super(LifecycleComponentType.CommandDestination);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.communication.ICommandDestination#deliverCommand
     * (com.smartthing .spi.device.command.IDeviceCommandExecution,
     * com.smartthing.spi.device.IDeviceNestingContext,
     * com.smartthing.spi.device.IDeviceAssignment)
     */
    @Override
    public void deliverCommand(IDeviceCommandExecution execution, IDeviceNestingContext nesting,
	    IDeviceAssignment assignment) throws SmartThingException {
	T encoded = getCommandExecutionEncoder().encode(execution, nesting, assignment);
	if (encoded != null) {
	    P params = getCommandDeliveryParameterExtractor().extractDeliveryParameters(nesting, assignment, execution);
	    getCommandDeliveryProvider().deliver(nesting, assignment, execution, encoded, params);
	} else {
	    LOGGER.info("Skipping command delivery. Encoder returned null.");
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.ICommandDestination#
     * deliverSystemCommand (com.smartthing.spi.device.command.ISystemCommand,
     * com.smartthing.spi.device.IDeviceNestingContext,
     * com.smartthing.spi.device.IDeviceAssignment)
     */
    @Override
    public void deliverSystemCommand(ISystemCommand command, IDeviceNestingContext nesting,
	    IDeviceAssignment assignment) throws SmartThingException {
	T encoded = getCommandExecutionEncoder().encodeSystemCommand(command, nesting, assignment);
	if (encoded != null) {
	    P params = getCommandDeliveryParameterExtractor().extractDeliveryParameters(nesting, assignment, null);
	    getCommandDeliveryProvider().deliverSystemCommand(nesting, assignment, encoded, params);
	} else {
	    LOGGER.info("Skipping system command delivery. Encoder returned null.");
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#start(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Clear the component list.
	getLifecycleComponents().clear();

	// Start command execution encoder.
	if (getCommandExecutionEncoder() == null) {
	    throw new SmartThingException("No command execution encoder configured for destination.");
	}
	startNestedComponent(getCommandExecutionEncoder(), monitor, true);

	// Start command execution encoder.
	if (getCommandDeliveryParameterExtractor() == null) {
	    throw new SmartThingException("No command delivery parameter extractor configured for destination.");
	}
	startNestedComponent(getCommandDeliveryParameterExtractor(), monitor, true);

	// Start command delivery provider.
	if (getCommandDeliveryProvider() == null) {
	    throw new SmartThingException("No command delivery provider configured for destination.");
	}
	startNestedComponent(getCommandDeliveryProvider(), monitor, true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.server.lifecycle.LifecycleComponent#getComponentName()
     */
    @Override
    public String getComponentName() {
	return "Command Destination (" + getDestinationId() + ")";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ILifecycleComponent#stop(com.smartthing
     * .spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Stop command execution encoder.
	if (getCommandExecutionEncoder() != null) {
	    getCommandExecutionEncoder().lifecycleStop(monitor);
	}

	// Stop command delivery provider.
	if (getCommandDeliveryProvider() != null) {
	    getCommandDeliveryProvider().lifecycleStop(monitor);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.ICommandDestination#
     * getDestinationId()
     */
    public String getDestinationId() {
	return destinationId;
    }

    public void setDestinationId(String destinationId) {
	this.destinationId = destinationId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.ICommandDestination#
     * getCommandExecutionEncoder ()
     */
    public ICommandExecutionEncoder<T> getCommandExecutionEncoder() {
	return commandExecutionEncoder;
    }

    public void setCommandExecutionEncoder(ICommandExecutionEncoder<T> commandExecutionEncoder) {
	this.commandExecutionEncoder = commandExecutionEncoder;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.ICommandDestination#
     * getCommandDeliveryParameterExtractor()
     */
    public ICommandDeliveryParameterExtractor<P> getCommandDeliveryParameterExtractor() {
	return commandDeliveryParameterExtractor;
    }

    public void setCommandDeliveryParameterExtractor(
	    ICommandDeliveryParameterExtractor<P> commandDeliveryParameterExtractor) {
	this.commandDeliveryParameterExtractor = commandDeliveryParameterExtractor;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.ICommandDestination#
     * getCommandDeliveryProvider ()
     */
    public ICommandDeliveryProvider<T, P> getCommandDeliveryProvider() {
	return commandDeliveryProvider;
    }

    public void setCommandDeliveryProvider(ICommandDeliveryProvider<T, P> commandDeliveryProvider) {
	this.commandDeliveryProvider = commandDeliveryProvider;
    }
}