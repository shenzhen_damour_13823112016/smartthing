/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.communication;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.SmartThing;
import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.IDeviceNestingContext;
import com.smartthing.spi.device.command.IDeviceCommand;
import com.smartthing.spi.device.command.IDeviceCommandExecution;
import com.smartthing.spi.device.command.ISystemCommand;
import com.smartthing.spi.device.communication.ICommandExecutionBuilder;
import com.smartthing.spi.device.communication.ICommandProcessingStrategy;
import com.smartthing.spi.device.communication.ICommandTargetResolver;
import com.smartthing.spi.device.communication.IDeviceCommunication;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Default implementation of {@link ICommandProcessingStrategy}.
 * 
 * @author Derek
 */
public class DefaultCommandProcessingStrategy extends TenantLifecycleComponent implements ICommandProcessingStrategy {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Configured command target resolver */
    private ICommandTargetResolver commandTargetResolver = new DefaultCommandTargetResolver();

    /** Configured command execution builder */
    private ICommandExecutionBuilder commandExecutionBuilder = new DefaultCommandExecutionBuilder();

    public DefaultCommandProcessingStrategy() {
	super(LifecycleComponentType.CommandProcessingStrategy);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.ICommandProcessingStrategy#
     * deliverCommand
     * (com.smartthing.spi.device.communication.IDeviceCommunication,
     * com.smartthing.spi.device.event.IDeviceCommandInvocation)
     */
    @Override
    public void deliverCommand(IDeviceCommunication communication, IDeviceCommandInvocation invocation)
	    throws SmartThingException {
	LOGGER.debug("Command processing strategy handling invocation.");
	IDeviceCommand command =SmartThing.getServer().getDeviceManagement(getTenant())
		.getDeviceCommandByToken(invocation.getCommandToken());
	if (command != null) {
	    IDeviceCommandExecution execution = getCommandExecutionBuilder().createExecution(command, invocation);
	    List<IDeviceAssignment> assignments = getCommandTargetResolver().resolveTargets(invocation);
	    for (IDeviceAssignment assignment : assignments) {
		IDevice device =SmartThing.getServer().getDeviceManagement(getTenant())
			.getDeviceForAssignment(assignment);
		if (device == null) {
		    throw new SmartThingException("Targeted assignment references device that does not exist.");
		}

		IDeviceNestingContext nesting = NestedDeviceSupport.calculateNestedDeviceInformation(device,
			getTenant());
		communication.getOutboundCommandRouter().routeCommand(execution, nesting, assignment);
	    }
	} else {
	    throw new SmartThingException("Invalid command referenced from invocation.");
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.ICommandProcessingStrategy#
     * deliverSystemCommand
     * (com.smartthing.spi.device.communication.IDeviceCommunication,
     * java.lang.String, com.smartthing.spi.device.command.ISystemCommand)
     */
    @Override
    public void deliverSystemCommand(IDeviceCommunication communication, String hardwareId, ISystemCommand command)
	    throws SmartThingException {
	IDeviceManagement management =SmartThing.getServer().getDeviceManagement(getTenant());
	IDevice device = management.getDeviceByHardwareId(hardwareId);
	if (device == null) {
	    throw new SmartThingException("Targeted assignment references device that does not exist.");
	}
	IDeviceAssignment assignment = management.getCurrentDeviceAssignment(device);
	IDeviceNestingContext nesting = NestedDeviceSupport.calculateNestedDeviceInformation(device, getTenant());
	communication.getOutboundCommandRouter().routeSystemCommand(command, nesting, assignment);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#start(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	getLifecycleComponents().clear();

	// Start command execution builder.
	if (getCommandExecutionBuilder() == null) {
	    throw new SmartThingException("No command execution builder configured for command processing.");
	}
	startNestedComponent(getCommandExecutionBuilder(), monitor, true);

	// Start command target resolver.
	if (getCommandTargetResolver() == null) {
	    throw new SmartThingException("No command target resolver configured for command processing.");
	}
	startNestedComponent(getCommandTargetResolver(), monitor, true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ILifecycleComponent#stop(com.smartthing
     * .spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Stop command execution builder.
	if (getCommandExecutionBuilder() != null) {
	    getCommandExecutionBuilder().lifecycleStop(monitor);
	}

	// Stop command target resolver.
	if (getCommandTargetResolver() != null) {
	    getCommandTargetResolver().lifecycleStop(monitor);
	}
    }

    public ICommandTargetResolver getCommandTargetResolver() {
	return commandTargetResolver;
    }

    public void setCommandTargetResolver(ICommandTargetResolver commandTargetResolver) {
	this.commandTargetResolver = commandTargetResolver;
    }

    public ICommandExecutionBuilder getCommandExecutionBuilder() {
	return commandExecutionBuilder;
    }

    public void setCommandExecutionBuilder(ICommandExecutionBuilder commandExecutionBuilder) {
	this.commandExecutionBuilder = commandExecutionBuilder;
    }
}