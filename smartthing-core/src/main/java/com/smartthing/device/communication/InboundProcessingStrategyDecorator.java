/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.communication;

import com.smartthing.server.lifecycle.LifecycleComponentDecorator;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.communication.IDecodedDeviceRequest;
import com.smartthing.spi.device.communication.IInboundProcessingStrategy;
import com.smartthing.spi.device.event.request.IDeviceAlertCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceCommandResponseCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceLocationCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceMappingCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceMeasurementsCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceRegistrationRequest;
import com.smartthing.spi.device.event.request.IDeviceStateChangeCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest;
import com.smartthing.spi.device.event.request.ISendDeviceStreamDataRequest;
import com.smartthing.spi.tenant.ITenant;

/**
 * Allows additional functionality to be triggered as part of the inbound
 * processing strategy.
 * 
 * @author Derek
 */
public class InboundProcessingStrategyDecorator extends LifecycleComponentDecorator
	implements IInboundProcessingStrategy {

    /** Wrapped instance */
    private IInboundProcessingStrategy delegate;

    public InboundProcessingStrategyDecorator(IInboundProcessingStrategy delegate) {
	super(delegate);
	this.delegate = delegate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent#setTenant(
     * com. sitewhere.spi.tenant.ITenant)
     */
    @Override
    public void setTenant(ITenant tenant) {
	delegate.setTenant(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent#getTenant()
     */
    @Override
    public ITenant getTenant() {
	return delegate.getTenant();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IInboundProcessingStrategy#
     * processRegistration(com.smartthing.spi.device.communication.
     * IDecodedDeviceRequest)
     */
    @Override
    public void processRegistration(IDecodedDeviceRequest<IDeviceRegistrationRequest> request)
	    throws SmartThingException {
	delegate.processRegistration(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IInboundProcessingStrategy#
     * processDeviceCommandResponse(com.smartthing.spi.device.communication.
     * IDecodedDeviceRequest)
     */
    @Override
    public void processDeviceCommandResponse(IDecodedDeviceRequest<IDeviceCommandResponseCreateRequest> request)
	    throws SmartThingException {
	delegate.processDeviceCommandResponse(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IInboundProcessingStrategy#
     * processDeviceMeasurements(com.smartthing.spi.device.communication.
     * IDecodedDeviceRequest)
     */
    @Override
    public void processDeviceMeasurements(IDecodedDeviceRequest<IDeviceMeasurementsCreateRequest> request)
	    throws SmartThingException {
	delegate.processDeviceMeasurements(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IInboundProcessingStrategy#
     * processDeviceLocation(com.smartthing.spi.device.communication.
     * IDecodedDeviceRequest)
     */
    @Override
    public void processDeviceLocation(IDecodedDeviceRequest<IDeviceLocationCreateRequest> request)
	    throws SmartThingException {
	delegate.processDeviceLocation(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IInboundProcessingStrategy#
     * processDeviceAlert(com.smartthing.spi.device.communication.
     * IDecodedDeviceRequest)
     */
    @Override
    public void processDeviceAlert(IDecodedDeviceRequest<IDeviceAlertCreateRequest> request) throws SmartThingException {
	delegate.processDeviceAlert(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IInboundProcessingStrategy#
     * processDeviceStateChange(com.smartthing.spi.device.communication.
     * IDecodedDeviceRequest)
     */
    @Override
    public void processDeviceStateChange(IDecodedDeviceRequest<IDeviceStateChangeCreateRequest> request)
	    throws SmartThingException {
	delegate.processDeviceStateChange(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IInboundProcessingStrategy#
     * processDeviceStream(com.smartthing.spi.device.communication.
     * IDecodedDeviceRequest)
     */
    @Override
    public void processDeviceStream(IDecodedDeviceRequest<IDeviceStreamCreateRequest> request)
	    throws SmartThingException {
	delegate.processDeviceStream(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IInboundProcessingStrategy#
     * processDeviceStreamData(com.smartthing.spi.device.communication.
     * IDecodedDeviceRequest)
     */
    @Override
    public void processDeviceStreamData(IDecodedDeviceRequest<IDeviceStreamDataCreateRequest> request)
	    throws SmartThingException {
	delegate.processDeviceStreamData(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IInboundProcessingStrategy#
     * processSendDeviceStreamData(com.smartthing.spi.device.communication.
     * IDecodedDeviceRequest)
     */
    @Override
    public void processSendDeviceStreamData(IDecodedDeviceRequest<ISendDeviceStreamDataRequest> request)
	    throws SmartThingException {
	delegate.processSendDeviceStreamData(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IInboundProcessingStrategy#
     * processCreateDeviceMapping(com.smartthing.spi.device.communication.
     * IDecodedDeviceRequest)
     */
    @Override
    public void processCreateDeviceMapping(IDecodedDeviceRequest<IDeviceMappingCreateRequest> request)
	    throws SmartThingException {
	delegate.processCreateDeviceMapping(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IInboundProcessingStrategy#
     * sendToInboundProcessingChain(com.smartthing.spi.device.communication.
     * IDecodedDeviceRequest)
     */
    @Override
    public void sendToInboundProcessingChain(IDecodedDeviceRequest<?> request) throws SmartThingException {
	delegate.sendToInboundProcessingChain(request);
    }
}