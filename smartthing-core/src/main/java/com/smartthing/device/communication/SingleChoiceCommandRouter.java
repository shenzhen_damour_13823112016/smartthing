/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.communication;

import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceNestingContext;
import com.smartthing.spi.device.command.IDeviceCommandExecution;
import com.smartthing.spi.device.command.ISystemCommand;
import com.smartthing.spi.device.communication.ICommandDestination;
import com.smartthing.spi.device.communication.IOutboundCommandRouter;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;

/**
 * Implementation of {@link IOutboundCommandRouter} that assumes a single
 * {@link ICommandDestination} is available and delivers commands to it.
 * 
 * @author Derek
 */
public class SingleChoiceCommandRouter extends OutboundCommandRouter {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Destinations that will deliver all commands */
    private ICommandDestination<?, ?> destination;

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IOutboundCommandRouter#
     * routeCommand(com. sitewhere.spi.device.command.IDeviceCommandExecution,
     * com.smartthing.spi.device.IDeviceNestingContext,
     * com.smartthing.spi.device.IDeviceAssignment)
     */
    @Override
    public void routeCommand(IDeviceCommandExecution execution, IDeviceNestingContext nesting,
	    IDeviceAssignment assignment) throws SmartThingException {
	destination.deliverCommand(execution, nesting, assignment);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.communication.IOutboundCommandRouter#
     * routeSystemCommand (com.smartthing.spi.device.command.ISystemCommand,
     * com.smartthing.spi.device.IDeviceNestingContext,
     * com.smartthing.spi.device.IDeviceAssignment)
     */
    @Override
    public void routeSystemCommand(ISystemCommand command, IDeviceNestingContext nesting, IDeviceAssignment assignment)
	    throws SmartThingException {
	destination.deliverSystemCommand(command, nesting, assignment);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
     * .server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	if (getDestinations().size() != 1) {
	    throw new SmartThingException(
		    "Expected exactly one destination for command routing but found " + getDestinations().size() + ".");
	}
	Iterator<ICommandDestination<?, ?>> it = getDestinations().values().iterator();
	this.destination = it.next();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }
}