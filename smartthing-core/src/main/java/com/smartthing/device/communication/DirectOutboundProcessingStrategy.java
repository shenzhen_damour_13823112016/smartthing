/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.communication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.SmartThing;
import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.communication.IOutboundProcessingStrategy;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.device.event.processor.IOutboundEventProcessorChain;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Implementation of {@link IOutboundProcessingStrategy} that sends messages
 * directly to the {@link IOutboundEventProcessorChain}.
 * 
 * @author Derek
 */
public class DirectOutboundProcessingStrategy extends TenantLifecycleComponent implements IOutboundProcessingStrategy {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Cached reference to outbound processor chain */
    private IOutboundEventProcessorChain outbound;

    public DirectOutboundProcessingStrategy() {
	super(LifecycleComponentType.OutboundProcessingStrategy);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#start(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ILifecycleComponent#stop(com.smartthing
     * .spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onMeasurements (com.smartthing.spi.device.event.IDeviceMeasurements)
     */
    @Override
    public void onMeasurements(IDeviceMeasurements measurements) throws SmartThingException {
	getOutboundEventProcessorChain().onMeasurements(measurements);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onLocation(com .sitewhere.spi.device.event.IDeviceLocation)
     */
    @Override
    public void onLocation(IDeviceLocation location) throws SmartThingException {
	getOutboundEventProcessorChain().onLocation(location);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IOutboundEventProcessor#onAlert(
     * com. sitewhere .spi.device.event.IDeviceAlert)
     */
    @Override
    public void onAlert(IDeviceAlert alert) throws SmartThingException {
	getOutboundEventProcessorChain().onAlert(alert);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onCommandInvocation
     * (com.smartthing.spi.device.event.IDeviceCommandInvocation)
     */
    @Override
    public void onCommandInvocation(IDeviceCommandInvocation invocation) throws SmartThingException {
	getOutboundEventProcessorChain().onCommandInvocation(invocation);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onCommandResponse (com.smartthing.spi.device.event.IDeviceCommandResponse)
     */
    @Override
    public void onCommandResponse(IDeviceCommandResponse response) throws SmartThingException {
	getOutboundEventProcessorChain().onCommandResponse(response);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onStateChange(com. sitewhere.spi.device.event.IDeviceStateChange)
     */
    @Override
    public void onStateChange(IDeviceStateChange state) throws SmartThingException {
	getOutboundEventProcessorChain().onStateChange(state);
    }

    /**
     * Cache the registration manager implementation rather than looking it up
     * each time.
     * 
     * @return
     * @throws SmartThingException
     */
    protected IOutboundEventProcessorChain getOutboundEventProcessorChain() throws SmartThingException {
	if (outbound == null) {
	    outbound =SmartThing.getServer().getEventProcessing(getTenant()).getOutboundEventProcessorChain();
	}
	return outbound;
    }
}