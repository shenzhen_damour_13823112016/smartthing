/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.communication.symbology;

import java.net.URI;
import java.net.URISyntaxException;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.ISite;
import com.smartthing.spi.device.symbology.IEntityUriProvider;

/**
 * Default implementation of {@link IEntityUriProvider}.
 * 
 * @author Derek
 */
public class DefaultEntityUriProvider implements IEntityUriProvider {

    /** SiteWhere protocol prefix */
    public static final String SMARTTHING_PROTOCOL = "smartthing://";

    /** Singleton instance */
    public static DefaultEntityUriProvider INSTANCE;

    protected DefaultEntityUriProvider() {
    }

    public static DefaultEntityUriProvider getInstance() {
	if (INSTANCE == null) {
	    INSTANCE = new DefaultEntityUriProvider();
	}
	return INSTANCE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.symbology.IEntityUriProvider#getSiteIdentifier(
     * com.smartthing .spi.device.ISite)
     */
    @Override
    public URI getSiteIdentifier(ISite site) throws SmartThingException {
	return createUri(SMARTTHING_PROTOCOL + "site/" + site.getToken());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.symbology.IEntityUriProvider#
     * getDeviceSpecificationIdentifier
     * (com.smartthing.spi.device.IDeviceSpecification)
     */
    @Override
    public URI getDeviceSpecificationIdentifier(IDeviceSpecification specification) throws SmartThingException {
	return createUri(SMARTTHING_PROTOCOL + "specification/" + specification.getToken());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.symbology.IEntityUriProvider#getDeviceIdentifier
     * (com.smartthing .spi.device.IDevice)
     */
    @Override
    public URI getDeviceIdentifier(IDevice device) throws SmartThingException {
	return createUri(SMARTTHING_PROTOCOL + "device/" + device.getHardwareId());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.symbology.IEntityUriProvider#
     * getDeviceAssignmentIdentifier
     * (com.smartthing.spi.device.IDeviceAssignment)
     */
    @Override
    public URI getDeviceAssignmentIdentifier(IDeviceAssignment assignment) throws SmartThingException {
	return createUri(SMARTTHING_PROTOCOL + "assignment/" + assignment.getToken());
    }

    /**
     * Create a URI.
     * 
     * @param uri
     * @return
     */
    protected URI createUri(String uri) {
	try {
	    return new URI(uri);
	} catch (URISyntaxException e) {
	    throw new RuntimeException(e);
	}
    }
}