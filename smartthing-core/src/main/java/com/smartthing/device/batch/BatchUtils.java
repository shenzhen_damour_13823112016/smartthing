/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.batch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.smartthing.SmartThing;
import com.smartthing.device.group.DeviceGroupUtils;
import com.smartthing.rest.model.search.device.DeviceSearchCriteria;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.request.IBatchCommandForCriteriaRequest;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.search.device.IDeviceSearchCriteria;
import com.smartthing.spi.tenant.ITenant;

/**
 * Utility methods for batch operations.
 * 
 * @author Derek
 */
public class BatchUtils {

    /**
     * Get hardware ids based on the given criteria.
     * 
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public static List<String> getHardwareIds(IBatchCommandForCriteriaRequest criteria, ITenant tenant)
	    throws SmartThingException {
	if (criteria.getSpecificationToken() == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidDeviceSpecificationToken, ErrorLevel.ERROR);
	}

	boolean hasGroup = false;
	boolean hasGroupsWithRole = false;
	if ((criteria.getGroupToken() != null) && (criteria.getGroupToken().trim().length() > 0)) {
	    hasGroup = true;
	}
	if ((criteria.getGroupsWithRole() != null) && (criteria.getGroupsWithRole().trim().length() > 0)) {
	    hasGroupsWithRole = true;
	}
	if (hasGroup && hasGroupsWithRole) {
	    throw new SmartThingException("Only one of groupToken or groupsWithRole may be specified.");
	}

	IDeviceSearchCriteria deviceSearch = new DeviceSearchCriteria(criteria.getSpecificationToken(),
		criteria.getSiteToken(), false, 1, 0, criteria.getStartDate(), criteria.getEndDate());

	Collection<IDevice> matches;
	if (hasGroup) {
	    matches = DeviceGroupUtils.getDevicesInGroup(criteria.getGroupToken(), deviceSearch, tenant);
	} else if (hasGroupsWithRole) {
	    matches = DeviceGroupUtils.getDevicesInGroupsWithRole(criteria.getGroupsWithRole(), deviceSearch, tenant);
	} else {
	    matches =SmartThing.getServer().getDeviceManagement(tenant).listDevices(false, deviceSearch).getResults();
	}
	List<String> hardwareIds = new ArrayList<String>();
	for (IDevice match : matches) {
	    hardwareIds.add(match.getHardwareId());
	}
	return hardwareIds;
    }
}