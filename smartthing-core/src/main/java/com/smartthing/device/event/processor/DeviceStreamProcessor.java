/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.event.processor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.SmartThing;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest;
import com.smartthing.spi.device.event.request.ISendDeviceStreamDataRequest;

/**
 * Handles requests related to device streams.
 * 
 * @author Derek
 */
public class DeviceStreamProcessor extends InboundEventProcessor {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IInboundEventProcessor#
     * onDeviceStreamCreateRequest(java.lang.String, java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest)
     */
    @Override
    public void onDeviceStreamCreateRequest(String hardwareId, String originator, IDeviceStreamCreateRequest request)
	    throws SmartThingException {
	SmartThing.getServer().getDeviceCommunication(getTenant()).getDeviceStreamManager()
		.handleDeviceStreamRequest(hardwareId, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IInboundEventProcessor#
     * onDeviceStreamDataCreateRequest(java.lang.String, java.lang.String,
     * java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest)
     */
    @Override
    public void onDeviceStreamDataCreateRequest(String hardwareId, String originator,
	    IDeviceStreamDataCreateRequest request) throws SmartThingException {
	SmartThing.getServer().getDeviceCommunication(getTenant()).getDeviceStreamManager()
		.handleDeviceStreamDataRequest(hardwareId, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.InboundEventProcessor#
     * onSendDeviceStreamDataRequest(java.lang.String, java.lang.String,
     * com.smartthing.spi.device.event.request.ISendDeviceStreamDataRequest)
     */
    @Override
    public void onSendDeviceStreamDataRequest(String hardwareId, String originator,
	    ISendDeviceStreamDataRequest request) throws SmartThingException {
	SmartThing.getServer().getDeviceCommunication(getTenant()).getDeviceStreamManager()
		.handleSendDeviceStreamDataRequest(hardwareId, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }
}