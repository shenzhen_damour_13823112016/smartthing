/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.event.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.device.event.processor.IOutboundEventProcessor;
import com.smartthing.spi.device.event.processor.IOutboundEventProcessorChain;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;
import com.smartthing.spi.server.lifecycle.LifecycleStatus;

/**
 * Default implementation of {@link IOutboundEventProcessorChain} interface.
 * 
 * @author Derek
 */
public class DefaultOutboundEventProcessorChain extends TenantLifecycleComponent
	implements IOutboundEventProcessorChain {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Indicates whether processing is enabled */
    private boolean processingEnabled = false;

    /** List of event processors */
    private List<IOutboundEventProcessor> processors = new ArrayList<IOutboundEventProcessor>();

    public DefaultOutboundEventProcessorChain() {
	super(LifecycleComponentType.OutboundProcessorChain);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#start(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	getLifecycleComponents().clear();
	for (IOutboundEventProcessor processor : getProcessors()) {
	    startNestedComponent(processor, monitor, false);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ILifecycleComponent#stop(com.smartthing
     * .spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
	for (IOutboundEventProcessor processor : getProcessors()) {
	    processor.lifecycleStop(monitor);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IOutboundEventProcessorChain#
     * setProcessingEnabled(boolean)
     */
    @Override
    public void setProcessingEnabled(boolean enabled) {
	this.processingEnabled = enabled;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IOutboundEventProcessorChain#
     * isProcessingEnabled()
     */
    @Override
    public boolean isProcessingEnabled() {
	return processingEnabled;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onMeasurements (com.smartthing.spi.device.event.IDeviceMeasurements)
     */
    @Override
    public void onMeasurements(IDeviceMeasurements measurements) throws SmartThingException {
	if (isProcessingEnabled()) {
	    for (IOutboundEventProcessor processor : getProcessors()) {
		try {
		    if (processor.getLifecycleStatus() == LifecycleStatus.Started) {
			processor.onMeasurements(measurements);
		    } else {
			logSkipped(processor);
		    }
		} catch (SmartThingException e) {
		    LOGGER.error(e);
		}
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onLocation(com .sitewhere.spi.device.event.IDeviceLocation)
     */
    @Override
    public void onLocation(IDeviceLocation location) throws SmartThingException {
	if (isProcessingEnabled()) {
	    for (IOutboundEventProcessor processor : getProcessors()) {
		try {
		    if (processor.getLifecycleStatus() == LifecycleStatus.Started) {
			processor.onLocation(location);
		    } else {
			logSkipped(processor);
		    }
		} catch (SmartThingException e) {
		    LOGGER.error(e);
		}
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IOutboundEventProcessor#onAlert(
     * com. sitewhere .spi.device.event.IDeviceAlert)
     */
    @Override
    public void onAlert(IDeviceAlert alert) throws SmartThingException {
	if (isProcessingEnabled()) {
	    for (IOutboundEventProcessor processor : getProcessors()) {
		try {
		    if (processor.getLifecycleStatus() == LifecycleStatus.Started) {
			processor.onAlert(alert);
		    } else {
			logSkipped(processor);
		    }
		} catch (SmartThingException e) {
		    LOGGER.error(e);
		}
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onCommandInvocation
     * (com.smartthing.spi.device.event.IDeviceCommandInvocation)
     */
    @Override
    public void onCommandInvocation(IDeviceCommandInvocation invocation) throws SmartThingException {
	if (isProcessingEnabled()) {
	    for (IOutboundEventProcessor processor : getProcessors()) {
		try {
		    if (processor.getLifecycleStatus() == LifecycleStatus.Started) {
			processor.onCommandInvocation(invocation);
		    } else {
			logSkipped(processor);
		    }
		} catch (SmartThingException e) {
		    LOGGER.error(e);
		}
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onCommandResponse (com.smartthing.spi.device.event.IDeviceCommandResponse)
     */
    @Override
    public void onCommandResponse(IDeviceCommandResponse response) throws SmartThingException {
	if (isProcessingEnabled()) {
	    for (IOutboundEventProcessor processor : getProcessors()) {
		try {
		    if (processor.getLifecycleStatus() == LifecycleStatus.Started) {
			processor.onCommandResponse(response);
		    } else {
			logSkipped(processor);
		    }
		} catch (SmartThingException e) {
		    LOGGER.error(e);
		}
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onStateChange(com. sitewhere.spi.device.event.IDeviceStateChange)
     */
    @Override
    public void onStateChange(IDeviceStateChange state) throws SmartThingException {
	if (isProcessingEnabled()) {
	    for (IOutboundEventProcessor processor : getProcessors()) {
		try {
		    if (processor.getLifecycleStatus() == LifecycleStatus.Started) {
			processor.onStateChange(state);
		    } else {
			logSkipped(processor);
		    }
		} catch (SmartThingException e) {
		    LOGGER.error(e);
		}
	    }
	}
    }

    /**
     * Output log message indicating a processor was skipped.
     * 
     * @param processor
     */
    protected void logSkipped(IOutboundEventProcessor processor) {
	getLogger().warn("Skipping event processor " + processor.getComponentName() + " because its state is '"
		+ processor.getLifecycleStatus() + "'");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IOutboundEventProcessorChain#
     * getProcessors ()
     */
    @Override
    public List<IOutboundEventProcessor> getProcessors() {
	return processors;
    }

    public void setProcessors(List<IOutboundEventProcessor> processors) {
	this.processors = processors;
    }
}