/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.event.processor.filter;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.event.IDeviceEvent;

/**
 * Includes or excludes events for devices using a given specification.
 * 
 * @author Derek
 */
public class SpecificationFilter extends DeviceEventFilter {

    /** Specification token to match */
    private String specificationToken;

    /** Operation filter performs */
    private FilterOperation operation = FilterOperation.Include;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IDeviceEventFilter#isFiltered(
     * com.smartthing .spi.device.event.IDeviceEvent,
     * com.smartthing.spi.device.IDevice,
     * com.smartthing.spi.device.IDeviceAssignment)
     */
    @Override
    public boolean isFiltered(IDeviceEvent event, IDevice device, IDeviceAssignment assignment)
	    throws SmartThingException {
	if (getSpecificationToken().equals(device.getSpecificationToken())) {
	    return (getOperation() != FilterOperation.Include);
	}
	return (getOperation() == FilterOperation.Include);
    }

    public String getSpecificationToken() {
	return specificationToken;
    }

    public void setSpecificationToken(String specificationToken) {
	this.specificationToken = specificationToken;
    }

    public FilterOperation getOperation() {
	return operation;
    }

    public void setOperation(FilterOperation operation) {
	this.operation = operation;
    }
}