/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.event.processor;

import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.processor.IInboundEventProcessor;
import com.smartthing.spi.device.event.request.IDeviceAlertCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceCommandResponseCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceLocationCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceMappingCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceMeasurementsCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceRegistrationRequest;
import com.smartthing.spi.device.event.request.IDeviceStateChangeCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest;
import com.smartthing.spi.device.event.request.ISendDeviceStreamDataRequest;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Default implementation of {@link IInboundEventProcessor} interface with
 * nothing implemented.
 * 
 * @author Derek
 */
public abstract class InboundEventProcessor extends TenantLifecycleComponent implements IInboundEventProcessor {

    public InboundEventProcessor() {
	super(LifecycleComponentType.InboundEventProcessor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IInboundEventProcessor#
     * onRegistrationRequest (java.lang.String, java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceRegistrationRequest)
     */
    @Override
    public void onRegistrationRequest(String hardwareId, String originator, IDeviceRegistrationRequest request)
	    throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IInboundEventProcessor#
     * onDeviceCommandResponseRequest(java.lang.String, java.lang.String,
     * com.smartthing.spi.device.event.request.
     * IDeviceCommandResponseCreateRequest)
     */
    @Override
    public void onDeviceCommandResponseRequest(String hardwareId, String originator,
	    IDeviceCommandResponseCreateRequest request) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IInboundEventProcessor#
     * onDeviceMeasurementsCreateRequest(java.lang.String, java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceMeasurementsCreateRequest)
     */
    @Override
    public void onDeviceMeasurementsCreateRequest(String hardwareId, String originator,
	    IDeviceMeasurementsCreateRequest request) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IInboundEventProcessor#
     * onDeviceLocationCreateRequest(java.lang.String, java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceLocationCreateRequest)
     */
    @Override
    public void onDeviceLocationCreateRequest(String hardwareId, String originator,
	    IDeviceLocationCreateRequest request) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IInboundEventProcessor#
     * onDeviceAlertCreateRequest(java.lang.String, java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceAlertCreateRequest)
     */
    @Override
    public void onDeviceAlertCreateRequest(String hardwareId, String originator, IDeviceAlertCreateRequest request)
	    throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IInboundEventProcessor#
     * onDeviceStateChangeCreateRequest(java.lang.String, java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceStateChangeCreateRequest)
     */
    @Override
    public void onDeviceStateChangeCreateRequest(String hardwareId, String originator,
	    IDeviceStateChangeCreateRequest request) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IInboundEventProcessor#
     * onDeviceStreamCreateRequest(java.lang.String, java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest)
     */
    @Override
    public void onDeviceStreamCreateRequest(String hardwareId, String originator, IDeviceStreamCreateRequest request)
	    throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IInboundEventProcessor#
     * onDeviceStreamDataCreateRequest(java.lang.String, java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest)
     */
    @Override
    public void onDeviceStreamDataCreateRequest(String hardwareId, String originator,
	    IDeviceStreamDataCreateRequest request) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IInboundEventProcessor#
     * onSendDeviceStreamDataRequest(java.lang.String, java.lang.String,
     * com.smartthing.spi.device.event.request.ISendDeviceStreamDataRequest)
     */
    @Override
    public void onSendDeviceStreamDataRequest(String hardwareId, String originator,
	    ISendDeviceStreamDataRequest request) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IInboundEventProcessor#
     * onDeviceMappingCreateRequest(java.lang.String, java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceMappingCreateRequest)
     */
    @Override
    public void onDeviceMappingCreateRequest(String hardwareId, String originator, IDeviceMappingCreateRequest request)
	    throws SmartThingException {
    }
}