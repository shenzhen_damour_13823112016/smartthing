/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.event.processor;

import java.util.ArrayList;
import java.util.List;

import com.smartthing.SmartThing;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceEvent;
import com.smartthing.spi.device.event.IDeviceEventManagement;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.device.event.processor.IDeviceEventFilter;
import com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;

/**
 * Extends {@link OutboundEventProcessor} with filtering functionality.
 * 
 * @author Derek
 */
public abstract class FilteredOutboundEventProcessor extends OutboundEventProcessor
	implements IFilteredOutboundEventProcessor {

    /** List of filters in order they should be applied */
    private List<IDeviceEventFilter> filters = new ArrayList<IDeviceEventFilter>();

    /** Device management implementation */
    private IDeviceManagement deviceManagement;

    /** Event management implementation */
    private IDeviceEventManagement eventManagement;

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#start(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	this.deviceManagement =SmartThing.getServer().getDeviceManagement(getTenant());
	this.eventManagement =SmartThing.getServer().getDeviceEventManagement(getTenant());

	getLifecycleComponents().clear();
	for (IDeviceEventFilter filter : filters) {
	    startNestedComponent(filter, monitor, true);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ILifecycleComponent#stop(com.smartthing
     * .spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
	for (IDeviceEventFilter filter : filters) {
	    filter.stop(monitor);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.OutboundEventProcessor#
     * onMeasurements(com. sitewhere.spi.device.event.IDeviceMeasurements)
     */
    @Override
    public final void onMeasurements(IDeviceMeasurements measurements) throws SmartThingException {
	if (!isFiltered(measurements)) {
	    onMeasurementsNotFiltered(measurements);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * onMeasurementsNotFiltered(com.smartthing.spi.device.event.
     * IDeviceMeasurements)
     */
    @Override
    public void onMeasurementsNotFiltered(IDeviceMeasurements measurements) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.event.processor.OutboundEventProcessor#onLocation(
     * com. sitewhere .spi.device.event.IDeviceLocation)
     */
    @Override
    public final void onLocation(IDeviceLocation location) throws SmartThingException {
	if (!isFiltered(location)) {
	    onLocationNotFiltered(location);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * onLocationNotFiltered(com.smartthing.spi.device.event.IDeviceLocation)
     */
    @Override
    public void onLocationNotFiltered(IDeviceLocation location) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.event.processor.OutboundEventProcessor#onAlert(com.
     * sitewhere .spi.device.event.IDeviceAlert)
     */
    @Override
    public final void onAlert(IDeviceAlert alert) throws SmartThingException {
	if (!isFiltered(alert)) {
	    onAlertNotFiltered(alert);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * onAlertNotFiltered(com.smartthing.spi.device.event.IDeviceAlert)
     */
    @Override
    public void onAlertNotFiltered(IDeviceAlert alert) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.event.processor.OutboundEventProcessor#onStateChange
     * (com. sitewhere.spi.device.event.IDeviceStateChange)
     */
    @Override
    public final void onStateChange(IDeviceStateChange state) throws SmartThingException {
	if (!isFiltered(state)) {
	    onStateChangeNotFiltered(state);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * onStateChangeNotFiltered(com.smartthing.spi.device.event.
     * IDeviceStateChange)
     */
    @Override
    public void onStateChangeNotFiltered(IDeviceStateChange state) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.OutboundEventProcessor#
     * onCommandInvocation
     * (com.smartthing.spi.device.event.IDeviceCommandInvocation)
     */
    @Override
    public final void onCommandInvocation(IDeviceCommandInvocation invocation) throws SmartThingException {
	if (!isFiltered(invocation)) {
	    onCommandInvocationNotFiltered(invocation);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * onCommandInvocationNotFiltered
     * (com.smartthing.spi.device.event.IDeviceCommandInvocation)
     */
    @Override
    public void onCommandInvocationNotFiltered(IDeviceCommandInvocation invocation) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.OutboundEventProcessor#
     * onCommandResponse(com .sitewhere.spi.device.event.IDeviceCommandResponse)
     */
    @Override
    public final void onCommandResponse(IDeviceCommandResponse response) throws SmartThingException {
	if (!isFiltered(response)) {
	    onCommandResponseNotFiltered(response);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * onCommandResponseNotFiltered(com.smartthing.spi.device.event.
     * IDeviceCommandResponse)
     */
    @Override
    public void onCommandResponseNotFiltered(IDeviceCommandResponse response) throws SmartThingException {
    }

    /**
     * Indicates if an event is filtered.
     * 
     * @param event
     * @return
     * @throws SmartThingException
     */
    protected boolean isFiltered(IDeviceEvent event) throws SmartThingException {
	IDeviceAssignment assignment = deviceManagement.getDeviceAssignmentByToken(event.getDeviceAssignmentToken());
	if (assignment == null) {
	    throw new SmartThingException("Device assignment for event not found.");
	}
	IDevice device = deviceManagement.getDeviceByHardwareId(assignment.getDeviceHardwareId());
	if (device == null) {
	    throw new SmartThingException("Device assignment references unknown device.");
	}
	for (IDeviceEventFilter filter : filters) {
	    if (filter.isFiltered(event, device, assignment)) {
		return true;
	    }
	}
	return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * getFilters ()
     */
    public List<IDeviceEventFilter> getFilters() {
	return filters;
    }

    public void setFilters(List<IDeviceEventFilter> filters) {
	this.filters = filters;
    }

    public IDeviceManagement getDeviceManagement() {
	return deviceManagement;
    }

    public void setDeviceManagement(IDeviceManagement deviceManagement) {
	this.deviceManagement = deviceManagement;
    }

    public IDeviceEventManagement getEventManagement() {
	return eventManagement;
    }

    public void setEventManagement(IDeviceEventManagement eventManagement) {
	this.eventManagement = eventManagement;
    }
}