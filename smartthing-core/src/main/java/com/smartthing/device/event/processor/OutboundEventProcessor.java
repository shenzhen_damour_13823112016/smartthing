/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.event.processor;

import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.device.event.processor.IOutboundEventProcessor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Default implementation of {@link IOutboundEventProcessor}.
 * 
 * @author Derek
 */
public abstract class OutboundEventProcessor extends TenantLifecycleComponent implements IOutboundEventProcessor {

    public OutboundEventProcessor() {
	super(LifecycleComponentType.OutboundEventProcessor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onMeasurements (com.smartthing.spi.device.event.IDeviceMeasurements)
     */
    @Override
    public void onMeasurements(IDeviceMeasurements measurements) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onLocation(com .sitewhere.spi.device.event.IDeviceLocation)
     */
    @Override
    public void onLocation(IDeviceLocation location) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IOutboundEventProcessor#onAlert(
     * com. sitewhere .spi.device.event.IDeviceAlert)
     */
    @Override
    public void onAlert(IDeviceAlert alert) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onCommandInvocation
     * (com.smartthing.spi.device.event.IDeviceCommandInvocation)
     */
    @Override
    public void onCommandInvocation(IDeviceCommandInvocation invocation) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onCommandResponse (com.smartthing.spi.device.event.IDeviceCommandResponse)
     */
    @Override
    public void onCommandResponse(IDeviceCommandResponse response) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.IOutboundEventProcessor#
     * onStateChange(com. sitewhere.spi.device.event.IDeviceStateChange)
     */
    @Override
    public void onStateChange(IDeviceStateChange state) throws SmartThingException {
    }
}