/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.event.processor.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.processor.IDeviceEventFilter;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Abstract base class for common filtering functionality.
 * 
 * @author Derek
 */
public abstract class DeviceEventFilter extends TenantLifecycleComponent implements IDeviceEventFilter {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    public DeviceEventFilter() {
	super(LifecycleComponentType.OutboundEventProcessorFilter);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#start(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ILifecycleComponent#stop(com.smartthing
     * .spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
    }

    @Override
    public Logger getLogger() {
	return LOGGER;
    }
}