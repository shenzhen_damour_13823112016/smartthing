/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device.marshaling;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.rest.model.asset.HardwareAsset;
import com.smartthing.rest.model.common.MetadataProviderEntity;
import com.smartthing.rest.model.device.DeviceSpecification;
import com.smartthing.rest.model.device.element.DeviceElementSchema;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.IAssetModuleManager;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.tenant.ITenant;

/**
 * Configurable helper class that allows {@link DeviceSpecification} model
 * objects to be created from {@link IDeviceSpecification} SPI objects.
 * 
 * @author dadams
 */
public class DeviceSpecificationMarshalHelper {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Tenant */
    private ITenant tenant;

    /**
     * Indicates whether device specification asset information is to be
     * included
     */
    private boolean includeAsset = true;

    public DeviceSpecificationMarshalHelper(ITenant tenant) {
	this.tenant = tenant;
    }

    /**
     * Convert a device specification for marshaling.
     * 
     * @param source
     * @param manager
     * @return
     * @throws SmartThingException
     */
    public DeviceSpecification convert(IDeviceSpecification source, IAssetModuleManager manager)
	    throws SmartThingException {
	DeviceSpecification spec = new DeviceSpecification();
	MetadataProviderEntity.copy(source, spec);
	spec.setToken(source.getToken());
	spec.setName(source.getName());
	HardwareAsset asset = (HardwareAsset) manager.getAssetById(source.getAssetModuleId(), source.getAssetId());

	// Handle case where referenced asset is not found.
	if (asset == null) {
	    LOGGER.warn("Device specification has reference to non-existent asset.");
	    asset = new InvalidAsset();
	}

	spec.setAssetId(asset.getId());
	spec.setAssetName(asset.getName());
	spec.setAssetImageUrl(asset.getImageUrl());
	if (isIncludeAsset()) {
	    spec.setAsset(asset);
	}
	spec.setAssetModuleId(source.getAssetModuleId());
	spec.setContainerPolicy(source.getContainerPolicy());
	spec.setDeviceElementSchema((DeviceElementSchema) source.getDeviceElementSchema());
	return spec;
    }

    public boolean isIncludeAsset() {
	return includeAsset;
    }

    public DeviceSpecificationMarshalHelper setIncludeAsset(boolean includeAsset) {
	this.includeAsset = includeAsset;
	return this;
    }

    public ITenant getTenant() {
	return tenant;
    }

    public void setTenant(ITenant tenant) {
	this.tenant = tenant;
    }
}