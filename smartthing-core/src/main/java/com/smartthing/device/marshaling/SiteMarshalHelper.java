package com.smartthing.device.marshaling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.SmartThing;
import com.smartthing.rest.model.device.DeviceAssignment;
import com.smartthing.rest.model.device.Site;
import com.smartthing.rest.model.device.Zone;
import com.smartthing.rest.model.search.SearchCriteria;
import com.smartthing.rest.model.search.device.AssignmentSearchCriteria;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.DeviceAssignmentStatus;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.ISite;
import com.smartthing.spi.device.IZone;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.tenant.ITenant;

/**
 * Configurable helper class that allows {@link Site} model objects to be
 * created from {@link ISite} SPI objects.
 * 
 * @author Derek
 */
public class SiteMarshalHelper {

    /** Static logger instance */
    @SuppressWarnings("unused")
    private static Logger LOGGER = LogManager.getLogger();

    /** Tenant */
    private ITenant tenant;

    /** Indicates whether assignments for site should be included */
    private boolean includeAssignements = false;

    /** Indicates whether zones are to be included */
    private boolean includeZones = false;

    /** Device assignment marshal helper */
    private DeviceAssignmentMarshalHelper assignmentHelper;

    public SiteMarshalHelper(ITenant tenant) {
	this.tenant = tenant;

	this.assignmentHelper = new DeviceAssignmentMarshalHelper(tenant);
	assignmentHelper.setIncludeDevice(true);
	assignmentHelper.setIncludeAsset(true);
	assignmentHelper.setIncludeSite(false);
    }

    /**
     * Convert the SPI into a model object based on marshaling parameters.
     * 
     * @param source
     * @return
     * @throws SmartThingException
     */
    public Site convert(ISite source) throws SmartThingException {
	Site site = Site.copy(source);
	if (isIncludeAssignements()) {
	    AssignmentSearchCriteria criteria = new AssignmentSearchCriteria(1, 0);
	    criteria.setStatus(DeviceAssignmentStatus.Active);
	    ISearchResults<IDeviceAssignment> matches = getDeviceManagement(getTenant())
		    .getDeviceAssignmentsForSite(site.getToken(), criteria);
	    List<DeviceAssignment> assignments = new ArrayList<DeviceAssignment>();
	    for (IDeviceAssignment match : matches.getResults()) {
		assignments.add(assignmentHelper.convert(match,SmartThing.getServer().getAssetModuleManager(tenant)));
	    }
	    site.setDeviceAssignments(assignments);
	}
	if (isIncludeZones()) {
	    ISearchResults<IZone> matches = getDeviceManagement(getTenant()).listZones(source.getToken(),
		    SearchCriteria.ALL);
	    List<Zone> zones = new ArrayList<Zone>();
	    List<IZone> reordered = matches.getResults();
	    Collections.sort(reordered, new Comparator<IZone>() {

		@Override
		public int compare(IZone z0, IZone z1) {
		    return z0.getName().compareTo(z1.getName());
		}
	    });
	    for (IZone match : matches.getResults()) {
		zones.add(Zone.copy(match));
	    }
	    site.setZones(zones);
	}
	return site;
    }

    /**
     * Get device management implementation.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    protected IDeviceManagement getDeviceManagement(ITenant tenant) throws SmartThingException {
	return SmartThing.getServer().getDeviceManagement(tenant);
    }

    public ITenant getTenant() {
	return tenant;
    }

    public void setTenant(ITenant tenant) {
	this.tenant = tenant;
    }

    public boolean isIncludeAssignements() {
	return includeAssignements;
    }

    public void setIncludeAssignements(boolean includeAssignements) {
	this.includeAssignements = includeAssignements;
    }

    public boolean isIncludeZones() {
	return includeZones;
    }

    public void setIncludeZones(boolean includeZones) {
	this.includeZones = includeZones;
    }
}