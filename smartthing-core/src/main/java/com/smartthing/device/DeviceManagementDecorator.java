/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.device;

import java.util.List;

import com.smartthing.server.lifecycle.LifecycleComponentDecorator;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.common.IMetadataProvider;
import com.smartthing.spi.device.DeviceAssignmentStatus;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceAssignmentState;
import com.smartthing.spi.device.IDeviceElementMapping;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.IDeviceStatus;
import com.smartthing.spi.device.ISite;
import com.smartthing.spi.device.IZone;
import com.smartthing.spi.device.batch.IBatchElement;
import com.smartthing.spi.device.batch.IBatchOperation;
import com.smartthing.spi.device.command.IDeviceCommand;
import com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest;
import com.smartthing.spi.device.group.IDeviceGroup;
import com.smartthing.spi.device.group.IDeviceGroupElement;
import com.smartthing.spi.device.request.IBatchCommandInvocationRequest;
import com.smartthing.spi.device.request.IBatchElementUpdateRequest;
import com.smartthing.spi.device.request.IBatchOperationCreateRequest;
import com.smartthing.spi.device.request.IBatchOperationUpdateRequest;
import com.smartthing.spi.device.request.IDeviceAssignmentCreateRequest;
import com.smartthing.spi.device.request.IDeviceCommandCreateRequest;
import com.smartthing.spi.device.request.IDeviceCreateRequest;
import com.smartthing.spi.device.request.IDeviceGroupCreateRequest;
import com.smartthing.spi.device.request.IDeviceGroupElementCreateRequest;
import com.smartthing.spi.device.request.IDeviceSpecificationCreateRequest;
import com.smartthing.spi.device.request.IDeviceStatusCreateRequest;
import com.smartthing.spi.device.request.ISiteCreateRequest;
import com.smartthing.spi.device.request.IZoneCreateRequest;
import com.smartthing.spi.device.streaming.IDeviceStream;
import com.smartthing.spi.search.IDateRangeSearchCriteria;
import com.smartthing.spi.search.ISearchCriteria;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.search.device.IAssignmentSearchCriteria;
import com.smartthing.spi.search.device.IAssignmentsForAssetSearchCriteria;
import com.smartthing.spi.search.device.IBatchElementSearchCriteria;
import com.smartthing.spi.search.device.IDeviceSearchCriteria;
import com.smartthing.spi.tenant.ITenant;

/**
 * Allows classes to inject themselves as a facade around an existing device
 * management implementation. By default all methods just pass calls to the
 * underlying delegate.
 * 
 * @author Derek
 */
public class DeviceManagementDecorator extends LifecycleComponentDecorator implements IDeviceManagement {

    /** Delegate instance */
    private IDeviceManagement delegate;

    public DeviceManagementDecorator(IDeviceManagement delegate) {
	super(delegate);
	this.delegate = delegate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent#setTenant(
     * com. sitewhere .spi.user.ITenant)
     */
    @Override
    public void setTenant(ITenant tenant) {
	delegate.setTenant(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent#getTenant()
     */
    @Override
    public ITenant getTenant() {
	return delegate.getTenant();
    }

    @Override
    public IDeviceSpecification createDeviceSpecification(IDeviceSpecificationCreateRequest request)
	    throws SmartThingException {
	return delegate.createDeviceSpecification(request);
    }

    @Override
    public IDeviceSpecification getDeviceSpecificationByToken(String token) throws SmartThingException {
	return delegate.getDeviceSpecificationByToken(token);
    }

    @Override
    public IDeviceSpecification updateDeviceSpecification(String token, IDeviceSpecificationCreateRequest request)
	    throws SmartThingException {
	return delegate.updateDeviceSpecification(token, request);
    }

    @Override
    public ISearchResults<IDeviceSpecification> listDeviceSpecifications(boolean includeDeleted,
	    ISearchCriteria criteria) throws SmartThingException {
	return delegate.listDeviceSpecifications(includeDeleted, criteria);
    }

    @Override
    public IDeviceSpecification deleteDeviceSpecification(String token, boolean force) throws SmartThingException {
	return delegate.deleteDeviceSpecification(token, force);
    }

    @Override
    public IDeviceCommand createDeviceCommand(IDeviceSpecification spec, IDeviceCommandCreateRequest request)
	    throws SmartThingException {
	return delegate.createDeviceCommand(spec, request);
    }

    @Override
    public IDeviceCommand getDeviceCommandByToken(String token) throws SmartThingException {
	return delegate.getDeviceCommandByToken(token);
    }

    @Override
    public IDeviceCommand updateDeviceCommand(String token, IDeviceCommandCreateRequest request)
	    throws SmartThingException {
	return delegate.updateDeviceCommand(token, request);
    }

    @Override
    public List<IDeviceCommand> listDeviceCommands(String token, boolean includeDeleted) throws SmartThingException {
	return delegate.listDeviceCommands(token, includeDeleted);
    }

    @Override
    public IDeviceCommand deleteDeviceCommand(String token, boolean force) throws SmartThingException {
	return delegate.deleteDeviceCommand(token, force);
    }

    @Override
    public IDeviceStatus createDeviceStatus(String specToken, IDeviceStatusCreateRequest request)
	    throws SmartThingException {
	return delegate.createDeviceStatus(specToken, request);
    }

    @Override
    public IDeviceStatus getDeviceStatusByCode(String specToken, String code) throws SmartThingException {
	return delegate.getDeviceStatusByCode(specToken, code);
    }

    @Override
    public IDeviceStatus updateDeviceStatus(String specToken, String code, IDeviceStatusCreateRequest request)
	    throws SmartThingException {
	return delegate.updateDeviceStatus(specToken, code, request);
    }

    @Override
    public List<IDeviceStatus> listDeviceStatuses(String specToken) throws SmartThingException {
	return delegate.listDeviceStatuses(specToken);
    }

    @Override
    public IDeviceStatus deleteDeviceStatus(String specToken, String code) throws SmartThingException {
	return delegate.deleteDeviceStatus(specToken, code);
    }

    @Override
    public IDevice createDevice(IDeviceCreateRequest device) throws SmartThingException {
	return delegate.createDevice(device);
    }

    @Override
    public IDevice getDeviceByHardwareId(String hardwareId) throws SmartThingException {
	return delegate.getDeviceByHardwareId(hardwareId);
    }

    @Override
    public IDevice updateDevice(String hardwareId, IDeviceCreateRequest request) throws SmartThingException {
	return delegate.updateDevice(hardwareId, request);
    }

    @Override
    public IDeviceAssignment getCurrentDeviceAssignment(IDevice device) throws SmartThingException {
	return delegate.getCurrentDeviceAssignment(device);
    }

    @Override
    public ISearchResults<IDevice> listDevices(boolean includeDeleted, IDeviceSearchCriteria criteria)
	    throws SmartThingException {
	return delegate.listDevices(includeDeleted, criteria);
    }

    @Override
    public IDevice createDeviceElementMapping(String hardwareId, IDeviceElementMapping mapping)
	    throws SmartThingException {
	return delegate.createDeviceElementMapping(hardwareId, mapping);
    }

    @Override
    public IDevice deleteDeviceElementMapping(String hardwareId, String path) throws SmartThingException {
	return delegate.deleteDeviceElementMapping(hardwareId, path);
    }

    @Override
    public IDevice deleteDevice(String hardwareId, boolean force) throws SmartThingException {
	return delegate.deleteDevice(hardwareId, force);
    }

    @Override
    public IDeviceAssignment createDeviceAssignment(IDeviceAssignmentCreateRequest request) throws SmartThingException {
	return delegate.createDeviceAssignment(request);
    }

    @Override
    public IDeviceAssignment getDeviceAssignmentByToken(String token) throws SmartThingException {
	return delegate.getDeviceAssignmentByToken(token);
    }

    @Override
    public IDeviceAssignment deleteDeviceAssignment(String token, boolean force) throws SmartThingException {
	return delegate.deleteDeviceAssignment(token, force);
    }

    @Override
    public IDevice getDeviceForAssignment(IDeviceAssignment assignment) throws SmartThingException {
	return delegate.getDeviceForAssignment(assignment);
    }

    @Override
    public ISite getSiteForAssignment(IDeviceAssignment assignment) throws SmartThingException {
	return delegate.getSiteForAssignment(assignment);
    }

    @Override
    public IDeviceAssignment updateDeviceAssignmentMetadata(String token, IMetadataProvider metadata)
	    throws SmartThingException {
	return delegate.updateDeviceAssignmentMetadata(token, metadata);
    }

    @Override
    public IDeviceAssignment updateDeviceAssignmentStatus(String token, DeviceAssignmentStatus status)
	    throws SmartThingException {
	return delegate.updateDeviceAssignmentStatus(token, status);
    }

    @Override
    public IDeviceAssignment updateDeviceAssignmentState(String token, IDeviceAssignmentState state)
	    throws SmartThingException {
	return delegate.updateDeviceAssignmentState(token, state);
    }

    @Override
    public IDeviceAssignment endDeviceAssignment(String token) throws SmartThingException {
	return delegate.endDeviceAssignment(token);
    }

    @Override
    public ISearchResults<IDeviceAssignment> getDeviceAssignmentsWithLastInteraction(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException {
	return delegate.getDeviceAssignmentsWithLastInteraction(siteToken, criteria);
    }

    @Override
    public ISearchResults<IDeviceAssignment> getMissingDeviceAssignments(String siteToken, ISearchCriteria criteria)
	    throws SmartThingException {
	return delegate.getMissingDeviceAssignments(siteToken, criteria);
    }

    @Override
    public ISearchResults<IDeviceAssignment> getDeviceAssignmentHistory(String hardwareId, ISearchCriteria criteria)
	    throws SmartThingException {
	return delegate.getDeviceAssignmentHistory(hardwareId, criteria);
    }

    @Override
    public ISearchResults<IDeviceAssignment> getDeviceAssignmentsForSite(String siteToken,
	    IAssignmentSearchCriteria criteria) throws SmartThingException {
	return delegate.getDeviceAssignmentsForSite(siteToken, criteria);
    }

    @Override
    public ISearchResults<IDeviceAssignment> getDeviceAssignmentsForAsset(String assetModuleId, String assetId,
	    IAssignmentsForAssetSearchCriteria criteria) throws SmartThingException {
	return delegate.getDeviceAssignmentsForAsset(assetModuleId, assetId, criteria);
    }

    @Override
    public IDeviceStream createDeviceStream(String assignmentToken, IDeviceStreamCreateRequest request)
	    throws SmartThingException {
	return delegate.createDeviceStream(assignmentToken, request);
    }

    @Override
    public IDeviceStream getDeviceStream(String assignmentToken, String streamId) throws SmartThingException {
	return delegate.getDeviceStream(assignmentToken, streamId);
    }

    @Override
    public ISearchResults<IDeviceStream> listDeviceStreams(String assignmentToken, ISearchCriteria criteria)
	    throws SmartThingException {
	return delegate.listDeviceStreams(assignmentToken, criteria);
    }

    @Override
    public ISite createSite(ISiteCreateRequest request) throws SmartThingException {
	return delegate.createSite(request);
    }

    @Override
    public ISite deleteSite(String siteToken, boolean force) throws SmartThingException {
	return delegate.deleteSite(siteToken, force);
    }

    @Override
    public ISite updateSite(String siteToken, ISiteCreateRequest request) throws SmartThingException {
	return delegate.updateSite(siteToken, request);
    }

    @Override
    public ISite getSiteByToken(String token) throws SmartThingException {
	return delegate.getSiteByToken(token);
    }

    @Override
    public ISearchResults<ISite> listSites(ISearchCriteria criteria) throws SmartThingException {
	return delegate.listSites(criteria);
    }

    @Override
    public IZone createZone(ISite site, IZoneCreateRequest request) throws SmartThingException {
	return delegate.createZone(site, request);
    }

    @Override
    public IZone updateZone(String token, IZoneCreateRequest request) throws SmartThingException {
	return delegate.updateZone(token, request);
    }

    @Override
    public IZone getZone(String zoneToken) throws SmartThingException {
	return delegate.getZone(zoneToken);
    }

    @Override
    public ISearchResults<IZone> listZones(String siteToken, ISearchCriteria criteria) throws SmartThingException {
	return delegate.listZones(siteToken, criteria);
    }

    @Override
    public IZone deleteZone(String zoneToken, boolean force) throws SmartThingException {
	return delegate.deleteZone(zoneToken, force);
    }

    @Override
    public IDeviceGroup createDeviceGroup(IDeviceGroupCreateRequest request) throws SmartThingException {
	return delegate.createDeviceGroup(request);
    }

    @Override
    public IDeviceGroup updateDeviceGroup(String token, IDeviceGroupCreateRequest request) throws SmartThingException {
	return delegate.updateDeviceGroup(token, request);
    }

    @Override
    public IDeviceGroup getDeviceGroup(String token) throws SmartThingException {
	return delegate.getDeviceGroup(token);
    }

    @Override
    public ISearchResults<IDeviceGroup> listDeviceGroups(boolean includeDeleted, ISearchCriteria criteria)
	    throws SmartThingException {
	return delegate.listDeviceGroups(includeDeleted, criteria);
    }

    @Override
    public ISearchResults<IDeviceGroup> listDeviceGroupsWithRole(String role, boolean includeDeleted,
	    ISearchCriteria criteria) throws SmartThingException {
	return delegate.listDeviceGroupsWithRole(role, includeDeleted, criteria);
    }

    @Override
    public List<IDeviceGroupElement> addDeviceGroupElements(String groupToken,
	    List<IDeviceGroupElementCreateRequest> elements, boolean ignoreDuplicates) throws SmartThingException {
	return delegate.addDeviceGroupElements(groupToken, elements, ignoreDuplicates);
    }

    @Override
    public List<IDeviceGroupElement> removeDeviceGroupElements(String groupToken,
	    List<IDeviceGroupElementCreateRequest> elements) throws SmartThingException {
	return delegate.removeDeviceGroupElements(groupToken, elements);
    }

    @Override
    public ISearchResults<IDeviceGroupElement> listDeviceGroupElements(String groupToken, ISearchCriteria criteria)
	    throws SmartThingException {
	return delegate.listDeviceGroupElements(groupToken, criteria);
    }

    @Override
    public IDeviceGroup deleteDeviceGroup(String token, boolean force) throws SmartThingException {
	return delegate.deleteDeviceGroup(token, force);
    }

    @Override
    public IBatchOperation createBatchOperation(IBatchOperationCreateRequest request) throws SmartThingException {
	return delegate.createBatchOperation(request);
    }

    @Override
    public IBatchOperation updateBatchOperation(String token, IBatchOperationUpdateRequest request)
	    throws SmartThingException {
	return delegate.updateBatchOperation(token, request);
    }

    @Override
    public IBatchOperation getBatchOperation(String token) throws SmartThingException {
	return delegate.getBatchOperation(token);
    }

    @Override
    public ISearchResults<IBatchOperation> listBatchOperations(boolean includeDeleted, ISearchCriteria criteria)
	    throws SmartThingException {
	return delegate.listBatchOperations(includeDeleted, criteria);
    }

    @Override
    public IBatchOperation deleteBatchOperation(String token, boolean force) throws SmartThingException {
	return delegate.deleteBatchOperation(token, force);
    }

    @Override
    public ISearchResults<IBatchElement> listBatchElements(String batchToken, IBatchElementSearchCriteria criteria)
	    throws SmartThingException {
	return delegate.listBatchElements(batchToken, criteria);
    }

    @Override
    public IBatchElement updateBatchElement(String operationToken, long index, IBatchElementUpdateRequest request)
	    throws SmartThingException {
	return delegate.updateBatchElement(operationToken, index, request);
    }

    @Override
    public IBatchOperation createBatchCommandInvocation(IBatchCommandInvocationRequest request)
	    throws SmartThingException {
	return delegate.createBatchCommandInvocation(request);
    }

    public IDeviceManagement getDelegate() {
	return delegate;
    }

    public void setDelegate(IDeviceManagement delegate) {
	this.delegate = delegate;
    }
}