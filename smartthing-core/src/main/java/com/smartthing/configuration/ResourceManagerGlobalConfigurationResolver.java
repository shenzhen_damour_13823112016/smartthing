/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.configuration;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.rest.model.resource.request.ResourceCreateRequest;
import com.smartthing.server.resource.SmartThingHomeResourceManager;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.configuration.IDefaultResourcePaths;
import com.smartthing.spi.configuration.IGlobalConfigurationResolver;
import com.smartthing.spi.resource.IMultiResourceCreateResponse;
import com.smartthing.spi.resource.IResource;
import com.smartthing.spi.resource.IResourceCreateError;
import com.smartthing.spi.resource.IResourceManager;
import com.smartthing.spi.resource.ResourceCreateMode;
import com.smartthing.spi.resource.ResourceType;
import com.smartthing.spi.resource.request.IResourceCreateRequest;
import com.smartthing.spi.system.IVersion;

/**
 * Resolves global configuration settings within a Tomcat instance.
 * 
 * @author Derek
 */
public class ResourceManagerGlobalConfigurationResolver implements IGlobalConfigurationResolver {

    /** Static logger instance */
    public static Logger LOGGER = LogManager.getLogger();

    /** Resource manager implementation */
    private IResourceManager resourceManager;

    /** Deprecated link to file system configuration root */
    private URI configurationRoot;

    public ResourceManagerGlobalConfigurationResolver(IResourceManager resourceManager) {
	this.resourceManager = resourceManager;
	try {
	    configurationRoot = SmartThingHomeResourceManager.calculateConfigurationPath().toURI();
	} catch (SmartThingException e) {
	    LOGGER.error("Error locating system configuration root.", e);
	    configurationRoot = null;
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.configuration.IGlobalConfigurationResolver#
     * getFilesystemConfigurationRoot()
     */
    @Override
    public URI getFilesystemConfigurationRoot() throws SmartThingException {
	if (configurationRoot != null) {
	    return configurationRoot;
	}
	throw new SmartThingException("Configuration root not set.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.configuration.IGlobalConfigurationResolver#
     * getResourceForPath(java.lang.String)
     */
    @Override
    public IResource getResourceForPath(String path) throws SmartThingException {
	return getResourceManager().getGlobalResource(path);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.configuration.IGlobalConfigurationResolver#
     * getAssetResource(java.lang.String)
     */
    @Override
    public IResource getAssetResource(String path) throws SmartThingException {
	return getResourceManager().getGlobalResource(IDefaultResourcePaths.ASSETS_FOLDER + File.separator + path);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.configuration.IGlobalConfigurationResolver#
     * getScriptResource(java.lang.String)
     */
    @Override
    public IResource getScriptResource(String path) throws SmartThingException {
	return getResourceManager().getGlobalResource(IDefaultResourcePaths.SCRIPTS_FOLDER + File.separator + path);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.configuration.IGlobalConfigurationResolver#
     * getGlobalConfiguration(com.smartthing.spi.system.IVersion)
     */
    @Override
    public IResource getGlobalConfiguration(IVersion version) throws SmartThingException {
	return getResourceManager().getGlobalResource(IDefaultResourcePaths.GLOBAL_CONFIG_FILE_NAME);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.configuration.IGlobalConfigurationResolver#
     * resolveServerState(com.smartthing.spi.system.IVersion)
     */
    @Override
    public IResource resolveServerState(IVersion version) throws SmartThingException {
	return getResourceManager().getGlobalResource(IDefaultResourcePaths.STATE_FILE_NAME);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.configuration.IGlobalConfigurationResolver#
     * storeServerState(com.smartthing.spi.system.IVersion, byte[])
     */
    @Override
    public void storeServerState(IVersion version, byte[] data) throws SmartThingException {
	List<IResourceCreateRequest> requests = new ArrayList<IResourceCreateRequest>();
	ResourceCreateRequest request = new ResourceCreateRequest();
	request.setPath(IDefaultResourcePaths.STATE_FILE_NAME);
	request.setResourceType(ResourceType.ConfigurationFile);
	request.setContent(data);
	requests.add(request);
	IMultiResourceCreateResponse response = getResourceManager().createGlobalResources(requests,
		ResourceCreateMode.PUSH_NEW_VERSION);
	if (response.getErrors().size() > 0) {
	    IResourceCreateError error = response.getErrors().get(0);
	    throw new SmartThingException("Unable to save server state: " + error.getReason().name());
	}
    }

    public IResourceManager getResourceManager() {
	return resourceManager;
    }

    public void setResourceManager(IResourceManager resourceManager) {
	this.resourceManager = resourceManager;
    }
}