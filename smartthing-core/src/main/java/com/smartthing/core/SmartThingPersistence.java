/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

import com.smartthing.SmartThing;
import com.smartthing.rest.model.asset.Asset;
import com.smartthing.rest.model.asset.AssetCategory;
import com.smartthing.rest.model.asset.HardwareAsset;
import com.smartthing.rest.model.asset.LocationAsset;
import com.smartthing.rest.model.asset.PersonAsset;
import com.smartthing.rest.model.common.MetadataProvider;
import com.smartthing.rest.model.common.MetadataProviderEntity;
import com.smartthing.rest.model.device.Device;
import com.smartthing.rest.model.device.DeviceAssignment;
import com.smartthing.rest.model.device.DeviceElementMapping;
import com.smartthing.rest.model.device.DeviceSpecification;
import com.smartthing.rest.model.device.DeviceStatus;
import com.smartthing.rest.model.device.Site;
import com.smartthing.rest.model.device.SiteMapData;
import com.smartthing.rest.model.device.Zone;
import com.smartthing.rest.model.device.batch.BatchElement;
import com.smartthing.rest.model.device.batch.BatchOperation;
import com.smartthing.rest.model.device.command.DeviceCommand;
import com.smartthing.rest.model.device.element.DeviceElementSchema;
import com.smartthing.rest.model.device.event.DeviceAlert;
import com.smartthing.rest.model.device.event.DeviceCommandInvocation;
import com.smartthing.rest.model.device.event.DeviceCommandResponse;
import com.smartthing.rest.model.device.event.DeviceEvent;
import com.smartthing.rest.model.device.event.DeviceEventBatchResponse;
import com.smartthing.rest.model.device.event.DeviceLocation;
import com.smartthing.rest.model.device.event.DeviceMeasurements;
import com.smartthing.rest.model.device.event.DeviceStateChange;
import com.smartthing.rest.model.device.event.DeviceStreamData;
import com.smartthing.rest.model.device.group.DeviceGroup;
import com.smartthing.rest.model.device.group.DeviceGroupElement;
import com.smartthing.rest.model.device.request.BatchOperationCreateRequest;
import com.smartthing.rest.model.device.request.DeviceCreateRequest;
import com.smartthing.rest.model.device.streaming.DeviceStream;
import com.smartthing.rest.model.scheduling.Schedule;
import com.smartthing.rest.model.scheduling.ScheduledJob;
import com.smartthing.rest.model.search.tenant.TenantSearchCriteria;
import com.smartthing.rest.model.tenant.Tenant;
import com.smartthing.rest.model.tenant.request.TenantCreateRequest;
import com.smartthing.rest.model.user.GrantedAuthority;
import com.smartthing.rest.model.user.User;
import com.smartthing.security.LoginManager;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.asset.AssetType;
import com.smartthing.spi.asset.IAssetCategory;
import com.smartthing.spi.asset.request.IAssetCategoryCreateRequest;
import com.smartthing.spi.asset.request.IAssetCreateRequest;
import com.smartthing.spi.asset.request.IHardwareAssetCreateRequest;
import com.smartthing.spi.asset.request.ILocationAssetCreateRequest;
import com.smartthing.spi.asset.request.IPersonAssetCreateRequest;
import com.smartthing.spi.common.ILocation;
import com.smartthing.spi.device.DeviceAssignmentStatus;
import com.smartthing.spi.device.DeviceAssignmentType;
import com.smartthing.spi.device.DeviceContainerPolicy;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceElementMapping;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.IDeviceStatus;
import com.smartthing.spi.device.batch.ElementProcessingStatus;
import com.smartthing.spi.device.batch.OperationType;
import com.smartthing.spi.device.command.ICommandParameter;
import com.smartthing.spi.device.command.IDeviceCommand;
import com.smartthing.spi.device.element.IDeviceElementSchema;
import com.smartthing.spi.device.event.AlertLevel;
import com.smartthing.spi.device.event.AlertSource;
import com.smartthing.spi.device.event.CommandStatus;
import com.smartthing.spi.device.event.IDeviceEventBatch;
import com.smartthing.spi.device.event.IDeviceEventManagement;
import com.smartthing.spi.device.event.request.IDeviceAlertCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceCommandInvocationCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceCommandResponseCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceEventCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceLocationCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceMeasurementsCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStateChangeCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest;
import com.smartthing.spi.device.request.IBatchCommandInvocationRequest;
import com.smartthing.spi.device.request.IBatchElementUpdateRequest;
import com.smartthing.spi.device.request.IBatchOperationCreateRequest;
import com.smartthing.spi.device.request.IBatchOperationUpdateRequest;
import com.smartthing.spi.device.request.IDeviceAssignmentCreateRequest;
import com.smartthing.spi.device.request.IDeviceCommandCreateRequest;
import com.smartthing.spi.device.request.IDeviceCreateRequest;
import com.smartthing.spi.device.request.IDeviceGroupCreateRequest;
import com.smartthing.spi.device.request.IDeviceGroupElementCreateRequest;
import com.smartthing.spi.device.request.IDeviceSpecificationCreateRequest;
import com.smartthing.spi.device.request.IDeviceStatusCreateRequest;
import com.smartthing.spi.device.request.ISiteCreateRequest;
import com.smartthing.spi.device.request.IZoneCreateRequest;
import com.smartthing.spi.device.util.DeviceSpecificationUtils;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.scheduling.ScheduledJobState;
import com.smartthing.spi.scheduling.request.IScheduleCreateRequest;
import com.smartthing.spi.scheduling.request.IScheduledJobCreateRequest;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.search.user.ITenantSearchCriteria;
import com.smartthing.spi.server.tenant.ISmartThingTenantEngine;
import com.smartthing.spi.tenant.ITenant;
import com.smartthing.spi.tenant.ITenantManagement;
import com.smartthing.spi.tenant.request.ITenantCreateRequest;
import com.smartthing.spi.user.request.IGrantedAuthorityCreateRequest;
import com.smartthing.spi.user.request.IUserCreateRequest;


/**
 * Common methods needed by device service provider implementations.
 *
 * @author Derek
 */
public class SmartThingPersistence {

    /** Password encoder */
    private static MessageDigestPasswordEncoder passwordEncoder = new ShaPasswordEncoder();

    /** Regular expression used to validate hardware ids */
    private static final Pattern HARDWARE_ID_REGEX = Pattern.compile("^[\\w-]+$");
    /** fire */
    private static final String VERSION = "FIRE";

    /**
     * Initialize entity fields.
     *
     * @param entity
     * @throws SmartThingException
     */
    public static void initializeEntityMetadata(MetadataProviderEntity entity) throws SmartThingException {
	entity.setCreatedDate(new Date());
	entity.setCreatedBy(LoginManager.getCurrentlyLoggedInUser().getUsername());
	entity.setDeleted(false);
    }

    /**
     * Set updated fields.
     *
     * @param entity
     * @throws SmartThingException
     */
    public static void setUpdatedEntityMetadata(MetadataProviderEntity entity) throws SmartThingException {
	entity.setUpdatedDate(new Date());
	entity.setUpdatedBy(LoginManager.getCurrentlyLoggedInUser().getUsername());
    }

    /**
     * Requires that a String field be a non null, non space-filled value.
     *
     * @param field
     * @throws SmartThingException
     */
    protected static void require(String field) throws SmartThingException {
	if (StringUtils.isBlank(field)) {
	    throw new SmartThingSystemException(ErrorCode.IncompleteData, ErrorLevel.ERROR);
	}
    }

    /**
     * Require that a non-String field be non-null.
     *
     * @param field
     * @throws SmartThingException
     */
    protected static void requireNotNull(Object field) throws SmartThingException {
	if (field == null) {
	    throw new SmartThingSystemException(ErrorCode.IncompleteData, ErrorLevel.ERROR);
	}
    }

    /**
     * Requires that a String field be a non null, non space-filled value.
     *
     * @param field
     * @throws SmartThingException
     */
    protected static void requireFormat(String field, String regex, ErrorCode ifFails) throws SmartThingException {
	require(field);
	if (!field.matches(regex)) {
	    throw new SmartThingSystemException(ifFails, ErrorLevel.ERROR);
	}
    }

    /**
     * Detect whether the request has an updated value.
     *
     * @param request
     * @param target
     * @return
     */
    protected static boolean isUpdated(Object request, Object target) {
	return (request != null) && (!request.equals(target));
    }

    /**
     * Common logic for creating new device specification and populating it from
     * request.
     *
     * @param request
     * @param token
     * @return
     * @throws SmartThingException
     */
    public static DeviceSpecification deviceSpecificationCreateLogic(IDeviceSpecificationCreateRequest request,
	    String token) throws SmartThingException {
	DeviceSpecification spec = new DeviceSpecification();

	// Unique token is required.
	require(token);
	spec.setToken(token);

	// Name is required.
	require(request.getName());
	spec.setName(request.getName());

	// Asset module id is required.
	require(request.getAssetModuleId());
	spec.setAssetModuleId(request.getAssetModuleId());

	// Asset id is required.
	require(request.getAssetId());
	spec.setAssetId(request.getAssetId());

	// Container policy is required.
	requireNotNull(request.getContainerPolicy());
	spec.setContainerPolicy(request.getContainerPolicy());

	// If composite container policy and no device element schema, create
	// empty schema.
	if (request.getContainerPolicy() == DeviceContainerPolicy.Composite) {
	    IDeviceElementSchema schema = request.getDeviceElementSchema();
	    if (schema == null) {
		schema = new DeviceElementSchema();
	    }
	    spec.setDeviceElementSchema((DeviceElementSchema) schema);
	}

	MetadataProvider.copy(request.getMetadata(), spec);
	SmartThingPersistence.initializeEntityMetadata(spec);
	return spec;
    }

    /**
     * Common logic for updating a device specification from request.
     *
     * @param request
     * @param target
     * @throws SmartThingException
     */
    public static void deviceSpecificationUpdateLogic(IDeviceSpecificationCreateRequest request,
	    DeviceSpecification target) throws SmartThingException {
	if (request.getName() != null) {
	    target.setName(request.getName());
	}
	if (request.getContainerPolicy() != null) {
	    target.setContainerPolicy(request.getContainerPolicy());
	}
	// Only allow schema to be set if new or existing container policy is
	// 'composite'.
	if (target.getContainerPolicy() == DeviceContainerPolicy.Composite) {
	    if (request.getContainerPolicy() == DeviceContainerPolicy.Standalone) {
		target.setDeviceElementSchema(null);
	    } else {
		IDeviceElementSchema schema = request.getDeviceElementSchema();
		if (schema == null) {
		    schema = new DeviceElementSchema();
		}
		target.setDeviceElementSchema((DeviceElementSchema) schema);
	    }
	}
	if (request.getAssetModuleId() != null) {
	    target.setAssetModuleId(request.getAssetModuleId());
	}
	if (request.getAssetId() != null) {
	    target.setAssetId(request.getAssetId());
	}
	if (request.getMetadata() != null) {
	    target.getMetadata().clear();
	    MetadataProvider.copy(request.getMetadata(), target);
	}
	SmartThingPersistence.setUpdatedEntityMetadata(target);
    }

    /**
     * Common logic for creating new device command and populating it from request.
     *
     * @param request
     * @param token
     * @return
     * @throws SmartThingException
     */
    public static DeviceCommand deviceCommandCreateLogic(IDeviceSpecification spec, IDeviceCommandCreateRequest request,
	    String token, List<IDeviceCommand> existing) throws SmartThingException {
	DeviceCommand command = new DeviceCommand();

	// Token is required.
	require(token);
	command.setToken(token);

	// Name is required.
	require(request.getName());
	command.setName(request.getName());

	command.setSpecificationToken(spec.getToken());
	command.setNamespace(request.getNamespace());
	command.setDescription(request.getDescription());
	command.getParameters().addAll(request.getParameters());

	checkDuplicateCommand(command, existing);

	MetadataProvider.copy(request.getMetadata(), command);
	SmartThingPersistence.initializeEntityMetadata(command);
	return command;
    }

    /**
     * Checks whether a command is already in the given list (same name and
     * namespace).
     *
     * @param command
     * @param existing
     * @throws SmartThingException
     */
    protected static void checkDuplicateCommand(DeviceCommand command, List<IDeviceCommand> existing)
	    throws SmartThingException {
	boolean duplicate = false;
	for (IDeviceCommand current : existing) {
	    if (current.getName().equals(command.getName())) {
		if (current.getNamespace() == null) {
		    if (command.getNamespace() == null) {
			duplicate = true;
			break;
		    }
		} else if (current.getNamespace().equals(command.getNamespace())) {
		    duplicate = true;
		    break;
		}
	    }
	}
	if (duplicate) {
	    throw new SmartThingSystemException(ErrorCode.DeviceCommandExists, ErrorLevel.ERROR);
	}
    }

    /**
     * Common logic for updating a device command from request.
     *
     * @param request
     * @param target
     * @param existing
     * @throws SmartThingException
     */
    public static void deviceCommandUpdateLogic(IDeviceCommandCreateRequest request, DeviceCommand target,
	    List<IDeviceCommand> existing) throws SmartThingException {
	if (request.getName() != null) {
	    target.setName(request.getName());
	}
	if (request.getNamespace() != null) {
	    target.setNamespace(request.getNamespace());
	}

	// Make sure the update will not result in a duplicate.
	checkDuplicateCommand(target, existing);

	if (request.getDescription() != null) {
	    target.setDescription(request.getDescription());
	}
	if (request.getParameters() != null) {
	    target.getParameters().clear();
	    target.getParameters().addAll(request.getParameters());
	}
	if (request.getMetadata() != null) {
	    target.getMetadata().clear();
	    MetadataProvider.copy(request.getMetadata(), target);
	}
	SmartThingPersistence.setUpdatedEntityMetadata(target);
    }

    /**
     * Common logic for creating new device status and populating it from request.
     *
     * @param spec
     * @param request
     * @param existing
     * @return
     * @throws SmartThingException
     */
    public static DeviceStatus deviceStatusCreateLogic(IDeviceSpecification spec, IDeviceStatusCreateRequest request,
	    List<IDeviceStatus> existing) throws SmartThingException {
	DeviceStatus status = new DeviceStatus();

	// Code is required.
	require(request.getCode());
	status.setCode(request.getCode());

	// Name is required.
	require(request.getName());
	status.setName(request.getName());

	status.setSpecificationToken(spec.getToken());
	status.setBackgroundColor(request.getBackgroundColor());
	status.setForegroundColor(request.getForegroundColor());
	status.setBorderColor(request.getBorderColor());
	status.setIcon(request.getIcon());

	checkDuplicateStatus(status, existing);

	MetadataProvider.copy(request.getMetadata(), status);
	return status;
    }

    /**
     * Common logic for updating a device status from request.
     *
     * @param request
     * @param target
     * @param existing
     * @throws SmartThingException
     */
    public static void deviceStatusUpdateLogic(IDeviceStatusCreateRequest request, DeviceStatus target,
	    List<IDeviceStatus> existing) throws SmartThingException {
	if (isUpdated(request.getCode(), target.getCode())) {
	    target.setCode(request.getCode());
	    checkDuplicateStatus(target, existing);
	}
	if (request.getName() != null) {
	    target.setName(request.getName());
	}
	if (request.getBackgroundColor() != null) {
	    target.setBackgroundColor(request.getBackgroundColor());
	}
	if (request.getForegroundColor() != null) {
	    target.setForegroundColor(request.getForegroundColor());
	}
	if (request.getBorderColor() != null) {
	    target.setBorderColor(request.getBorderColor());
	}
	if (request.getIcon() != null) {
	    target.setIcon(request.getIcon());
	}
	if (request.getMetadata() != null) {
	    target.getMetadata().clear();
	    MetadataProvider.copy(request.getMetadata(), target);
	}
    }

    /**
     * Checks whether a command is already in the given list (same name and
     * namespace).
     *
     * @param status
     * @param existing
     * @throws SmartThingException
     */
    protected static void checkDuplicateStatus(DeviceStatus status, List<IDeviceStatus> existing)
	    throws SmartThingException {
	for (IDeviceStatus current : existing) {
	    if (current.getCode().equals(status.getCode())) {
		throw new SmartThingSystemException(ErrorCode.DeviceStatusExists, ErrorLevel.ERROR);
	    }
	}
    }

    /**
     * Common logic for creating new device object and populating it from request.
     *
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static Device deviceCreateLogic(IDeviceCreateRequest request) throws SmartThingException {
	Device device = new Device();

	// Require hardware id and verify that it is valid.
	require(request.getHardwareId());
	Matcher matcher = HARDWARE_ID_REGEX.matcher(request.getHardwareId());
	if (!matcher.matches()) {
	    throw new SmartThingSystemException(ErrorCode.MalformedHardwareId, ErrorLevel.ERROR);
	}
	device.setHardwareId(request.getHardwareId());

	require(request.getSiteToken());
	device.setSiteToken(request.getSiteToken());

	require(request.getSpecificationToken());
	device.setSpecificationToken(request.getSpecificationToken());

	device.setComments(request.getComments());
	device.setStatus(request.getStatus());

	MetadataProvider.copy(request.getMetadata(), device);
	SmartThingPersistence.initializeEntityMetadata(device);
	return device;
    }

    /**
     * Common logic for updating a device object from request.
     *
     * @param request
     * @param target
     * @throws SmartThingException
     */
    public static void deviceUpdateLogic(IDeviceCreateRequest request, Device target) throws SmartThingException {
	// Can not update the hardware id on a device.
	if ((request.getHardwareId() != null) && (!request.getHardwareId().equals(target.getHardwareId()))) {
	    throw new SmartThingSystemException(ErrorCode.DeviceHardwareIdCanNotBeChanged, ErrorLevel.ERROR);
	}
	if (request.getSiteToken() != null) {
	    target.setSiteToken(request.getSiteToken());
	}
	if (request.getSpecificationToken() != null) {
	    target.setSpecificationToken(request.getSpecificationToken());
	}
	if (request.isRemoveParentHardwareId() == Boolean.TRUE) {
	    target.setParentHardwareId(null);
	}
	if (request.getParentHardwareId() != null) {
	    target.setParentHardwareId(request.getParentHardwareId());
	}
	if (request.getDeviceElementMappings() != null) {
	    List<DeviceElementMapping> mappings = new ArrayList<DeviceElementMapping>();
	    for (IDeviceElementMapping mapping : request.getDeviceElementMappings()) {
		mappings.add(DeviceElementMapping.copy(mapping));
	    }
	    target.setDeviceElementMappings(mappings);
	}
	if (request.getComments() != null) {
	    target.setComments(request.getComments());
	}
	if (request.getStatus() != null) {
	    target.setStatus(request.getStatus());
	}
	if (request.getMetadata() != null) {
	    target.getMetadata().clear();
	    MetadataProvider.copy(request.getMetadata(), target);
	}
	SmartThingPersistence.setUpdatedEntityMetadata(target);
    }

    /**
     * Encapsulates logic for creating a new {@link IDeviceElementMapping}.
     *
     * @param management
     * @param hardwareId
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static IDevice deviceElementMappingCreateLogic(IDeviceManagement management, String hardwareId,
	    IDeviceElementMapping request) throws SmartThingException {
	IDevice device = management.getDeviceByHardwareId(hardwareId);
	if (device == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidHardwareId, ErrorLevel.ERROR);
	}
	IDevice mapped = management.getDeviceByHardwareId(request.getHardwareId());
	if (mapped == null) {
	    throw new SmartThingException("Device referenced by mapping does not exist.");
	}

	// Check whether target device is already parented to another device.
	if (mapped.getParentHardwareId() != null) {
	    throw new SmartThingSystemException(ErrorCode.DeviceParentMappingExists, ErrorLevel.ERROR);
	}

	// Verify that requested path is valid on device specification.
	IDeviceSpecification specification = management.getDeviceSpecificationByToken(device.getSpecificationToken());
	DeviceSpecificationUtils.getDeviceSlotByPath(specification, request.getDeviceElementSchemaPath());

	// Verify that there is not an existing mapping for the path.
	List<IDeviceElementMapping> existing = device.getDeviceElementMappings();
	List<DeviceElementMapping> newMappings = new ArrayList<DeviceElementMapping>();
	for (IDeviceElementMapping mapping : existing) {
	    if (mapping.getDeviceElementSchemaPath().equals(request.getDeviceElementSchemaPath())) {
		throw new SmartThingSystemException(ErrorCode.DeviceElementMappingExists, ErrorLevel.ERROR);
	    }
	    newMappings.add(DeviceElementMapping.copy(mapping));
	}
	DeviceElementMapping newMapping = DeviceElementMapping.copy(request);
	newMappings.add(newMapping);

	// Add parent backreference for nested device.
	DeviceCreateRequest nested = new DeviceCreateRequest();
	nested.setParentHardwareId(hardwareId);
	management.updateDevice(mapped.getHardwareId(), nested);

	// Update device with new mapping.
	DeviceCreateRequest update = new DeviceCreateRequest();
	update.setDeviceElementMappings(newMappings);
	IDevice updated = management.updateDevice(hardwareId, update);
	return updated;
    }

    /**
     * Encapsulates logic for deleting an {@link IDeviceElementMapping}.
     *
     * @param management
     * @param hardwareId
     * @param path
     * @return
     * @throws SmartThingException
     */
    public static IDevice deviceElementMappingDeleteLogic(IDeviceManagement management, String hardwareId, String path)
	    throws SmartThingException {
	IDevice device = management.getDeviceByHardwareId(hardwareId);
	if (device == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidHardwareId, ErrorLevel.ERROR);
	}

	// Verify that mapping exists and build list without deleted mapping.
	List<IDeviceElementMapping> existing = device.getDeviceElementMappings();
	List<DeviceElementMapping> newMappings = new ArrayList<DeviceElementMapping>();
	IDeviceElementMapping match = null;
	for (IDeviceElementMapping mapping : existing) {
	    if (mapping.getDeviceElementSchemaPath().equals(path)) {
		match = mapping;
	    } else {
		newMappings.add(DeviceElementMapping.copy(mapping));
	    }
	}
	if (match == null) {
	    throw new SmartThingSystemException(ErrorCode.DeviceElementMappingDoesNotExist, ErrorLevel.ERROR);
	}

	// Remove parent reference from nested device.
	IDevice mapped = management.getDeviceByHardwareId(match.getHardwareId());
	if (mapped != null) {
	    DeviceCreateRequest nested = new DeviceCreateRequest();
	    nested.setRemoveParentHardwareId(true);
	    management.updateDevice(mapped.getHardwareId(), nested);
	}

	// Update device with new mappings.
	DeviceCreateRequest update = new DeviceCreateRequest();
	update.setDeviceElementMappings(newMappings);
	IDevice updated = management.updateDevice(hardwareId, update);
	return updated;
    }

    /**
     * Common logic for creating new site object and populating it from request.
     *
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static Site siteCreateLogic(ISiteCreateRequest request) throws SmartThingException {
	Site site = new Site();

	if (request.getToken() != null) {
	    site.setToken(request.getToken());
	} else {
	    site.setToken(UUID.randomUUID().toString());
	}

	site.setName(request.getName());
	site.setDescription(request.getDescription());
	site.setImageUrl(request.getImageUrl());
	site.setMap(SiteMapData.copy(request.getMap()));

	SmartThingPersistence.initializeEntityMetadata(site);
	MetadataProvider.copy(request.getMetadata(), site);
	return site;
    }

    /**
     * Common logic for copying data from site update request to existing site.
     *
     * @param request
     * @param target
     * @throws SmartThingException
     */
    public static void siteUpdateLogic(ISiteCreateRequest request, Site target) throws SmartThingException {
	if (request.getName() != null) {
	    target.setName(request.getName());
	}
	if (request.getDescription() != null) {
	    target.setDescription(request.getDescription());
	}
	if (request.getImageUrl() != null) {
	    target.setImageUrl(request.getImageUrl());
	}
	if (request.getMap() != null) {
	    target.setMap(SiteMapData.copy(request.getMap()));
	}
	if (request.getMetadata() != null) {
	    target.getMetadata().clear();
	    MetadataProvider.copy(request.getMetadata(), target);
	}
	SmartThingPersistence.setUpdatedEntityMetadata(target);
    }

    /**
     * Common logic for creating a device assignment from a request.
     *
     * @param source
     * @param device
     * @return
     * @throws SmartThingException
     */
    public static DeviceAssignment deviceAssignmentCreateLogic(IDeviceAssignmentCreateRequest source, IDevice device)
	    throws SmartThingException {
	DeviceAssignment newAssignment = new DeviceAssignment();
	newAssignment.setToken(source.getToken());

	// Copy site token from device.
	newAssignment.setSiteToken(device.getSiteToken());

	if (source.getDeviceHardwareId() == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidHardwareId, ErrorLevel.ERROR);
	}
	newAssignment.setDeviceHardwareId(source.getDeviceHardwareId());

	requireNotNull(source.getAssignmentType());
	newAssignment.setAssignmentType(source.getAssignmentType());

	if (source.getAssignmentType() == DeviceAssignmentType.Associated) {
	    if (source.getAssetModuleId() == null) {
		throw new SmartThingSystemException(ErrorCode.InvalidAssetReferenceId, ErrorLevel.ERROR);
	    }
	    newAssignment.setAssetModuleId(source.getAssetModuleId());

	    if (source.getAssetId() == null) {
		throw new SmartThingSystemException(ErrorCode.InvalidAssetReferenceId, ErrorLevel.ERROR);
	    }
	    newAssignment.setAssetId(source.getAssetId());
	}

	newAssignment.setActiveDate(new Date());
	newAssignment.setStatus(DeviceAssignmentStatus.Active);

	SmartThingPersistence.initializeEntityMetadata(newAssignment);
	MetadataProvider.copy(source.getMetadata(), newAssignment);

	return newAssignment;
    }

    /**
     * Logic applied before deleting a device assignment.
     *
     * @param assignment
     * @throws SmartThingException
     */
    public static void deviceAssignmentDeleteLogic(IDeviceAssignment assignment) throws SmartThingException {
	if (assignment.getReleasedDate() == null) {
	    throw new SmartThingSystemException(ErrorCode.CanNotDeleteActiveAssignment, ErrorLevel.ERROR);
	}
    }

    /**
     * Executes logic to process a batch of device events.
     *
     * @param assignmentToken
     * @param batch
     * @param management
     * @return
     * @throws SmartThingException
     */
    public static DeviceEventBatchResponse deviceEventBatchLogic(String assignmentToken, IDeviceEventBatch batch,
	    IDeviceEventManagement management) throws SmartThingException {
	DeviceEventBatchResponse response = new DeviceEventBatchResponse();
	for (IDeviceMeasurementsCreateRequest measurements : batch.getMeasurements()) {
	    response.getCreatedMeasurements().add(management.addDeviceMeasurements(assignmentToken, measurements));
	}
	for (IDeviceLocationCreateRequest location : batch.getLocations()) {
	    response.getCreatedLocations().add(management.addDeviceLocation(assignmentToken, location));
	}
	for (IDeviceAlertCreateRequest alert : batch.getAlerts()) {
	    response.getCreatedAlerts().add(management.addDeviceAlert(assignmentToken, alert));
	}
	return response;
    }

    /**
     * Common creation logic for all device events.
     *
     * @param request
     * @param assignment
     * @param target
     * @throws SmartThingException
     */
    public static void deviceEventCreateLogic(IDeviceEventCreateRequest request, IDeviceAssignment assignment,
	    DeviceEvent target) throws SmartThingException {
	target.setAlternateId(request.getAlternateId());
	target.setSiteToken(assignment.getSiteToken());
	target.setDeviceAssignmentToken(assignment.getToken());
	target.setAssignmentType(assignment.getAssignmentType());
	target.setAssetModuleId(assignment.getAssetModuleId());
	target.setAssetId(assignment.getAssetId());
	if (request.getEventDate() != null) {
	    target.setEventDate(request.getEventDate());
	} else {
	    target.setEventDate(new Date());
	}
	target.setReceivedDate(new Date());
	if("FIRE".equals(VERSION)){
		MetadataProvider.copy(assignment.getMetadata(), target);
	}
	MetadataProvider.copy(request.getMetadata(), target);
    }

    /**
     * Common logic for updating a device event (Only metadata can be updated).
     *
     * @param request metadata
     * @param target
     * @throws SmartThingException
     */
    public static void deviceEventUpdateLogic(IDeviceEventCreateRequest request, DeviceEvent target)
	    throws SmartThingException {
	if (request.getMetadata() != null) {
	    target.getMetadata().clear();
	    MetadataProvider.copy(request.getMetadata(), target);
	}
    }

    /**
     * Common logic for creating {@link DeviceMeasurements} from
     * {@link IDeviceMeasurementsCreateRequest}.
     *
     * @param request
     * @param assignment
     * @return
     * @throws SmartThingException
     */
    public static DeviceMeasurements deviceMeasurementsCreateLogic(IDeviceMeasurementsCreateRequest request,
	    IDeviceAssignment assignment) throws SmartThingException {
	DeviceMeasurements measurements = new DeviceMeasurements();
	deviceEventCreateLogic(request, assignment, measurements);
	for (String key : request.getMeasurements().keySet()) {
	    measurements.addOrReplaceMeasurement(key, request.getMeasurement(key));
	}
	return measurements;
    }

    /**
     * Common logic for creating {@link DeviceLocation} from
     * {@link IDeviceLocationCreateRequest}.
     *
     * @param assignment
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static DeviceLocation deviceLocationCreateLogic(IDeviceAssignment assignment,
	    IDeviceLocationCreateRequest request) throws SmartThingException {
	DeviceLocation location = new DeviceLocation();
	deviceEventCreateLogic(request, assignment, location);
	location.setLatitude(request.getLatitude());
	location.setLongitude(request.getLongitude());
	location.setElevation(request.getElevation());
	return location;
    }

    /**
     * Common logic for creating {@link DeviceAlert} from
     * {@link IDeviceAlertCreateRequest}.
     *
     * @param assignment
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static DeviceAlert deviceAlertCreateLogic(IDeviceAssignment assignment, IDeviceAlertCreateRequest request)
	    throws SmartThingException {
	DeviceAlert alert = new DeviceAlert();
	deviceEventCreateLogic(request, assignment, alert);

	if (request.getSource() != null) {
	    alert.setSource(request.getSource());
	} else {
	    alert.setSource(AlertSource.Device);
	}

	if (request.getLevel() != null) {
	    alert.setLevel(request.getLevel());
	} else {
	    alert.setLevel(AlertLevel.Info);
	}

	alert.setType(request.getType());
	alert.setMessage(request.getMessage());
	return alert;
    }

    /**
     * Common logic for creating {@link DeviceStream} from
     * {@link IDeviceStreamCreateRequest}.
     *
     * @param assignment
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static DeviceStream deviceStreamCreateLogic(IDeviceAssignment assignment, IDeviceStreamCreateRequest request)
	    throws SmartThingException {
	DeviceStream stream = new DeviceStream();
	stream.setAssignmentToken(assignment.getToken());

	// Verify the stream id is specified and contains only valid characters.
	require(request.getStreamId());
	if (!request.getStreamId().matches("^[a-zA-Z0-9_\\-]+$")) {
	    throw new SmartThingSystemException(ErrorCode.InvalidCharsInStreamId, ErrorLevel.ERROR);
	}
	stream.setStreamId(request.getStreamId());

	// Content type is required.
	require(request.getContentType());
	stream.setContentType(request.getContentType());

	MetadataProvider.copy(request.getMetadata(), stream);
	SmartThingPersistence.initializeEntityMetadata(stream);
	return stream;
    }

    /**
     * Common logic for creating {@link DeviceStreamData} from
     * {@link IDeviceStreamDataCreateRequest}.
     *
     * @param assignment
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static DeviceStreamData deviceStreamDataCreateLogic(IDeviceAssignment assignment,
	    IDeviceStreamDataCreateRequest request) throws SmartThingException {
	DeviceStreamData streamData = new DeviceStreamData();
	deviceEventCreateLogic(request, assignment, streamData);

	require(request.getStreamId());
	streamData.setStreamId(request.getStreamId());

	requireNotNull(request.getSequenceNumber());
	streamData.setSequenceNumber(request.getSequenceNumber());

	streamData.setData(request.getData());
	return streamData;
    }

    /**
     * Common logic for creating {@link DeviceCommandInvocation} from an
     * {@link IDeviceCommandInvocationCreateRequest}.
     *
     * @param assignment
     * @param command
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static DeviceCommandInvocation deviceCommandInvocationCreateLogic(IDeviceAssignment assignment,
	    IDeviceCommand command, IDeviceCommandInvocationCreateRequest request) throws SmartThingException {
	requireNotNull(request.getInitiator());
	requireNotNull(request.getTarget());

	for (ICommandParameter parameter : command.getParameters()) {
	    checkData(parameter, request.getParameterValues());
	}
	DeviceCommandInvocation ci = new DeviceCommandInvocation();
	deviceEventCreateLogic(request, assignment, ci);
	ci.setCommandToken(command.getToken());
	ci.setInitiator(request.getInitiator());
	ci.setInitiatorId(request.getInitiatorId());
	ci.setTarget(request.getTarget());
	ci.setTargetId(request.getTargetId());
	ci.setParameterValues(request.getParameterValues());
	if (request.getStatus() != null) {
	    ci.setStatus(request.getStatus());
	} else {
	    ci.setStatus(CommandStatus.Pending);
	}
	return ci;
    }

    /**
     * Verify that data supplied for command parameters is valid.
     *
     * @param parameter
     * @param values
     * @throws SmartThingException
     */
    protected static void checkData(ICommandParameter parameter, Map<String, String> values) throws SmartThingException {

	String value = values.get(parameter.getName());
	boolean parameterValueIsNull = (value == null);
	boolean parameterValueIsEmpty = true;

	if (!parameterValueIsNull) {
	    value = value.trim();
	    parameterValueIsEmpty = value.length() == 0;
	}

	// Handle the required parameters first
	if (parameter.isRequired()) {
	    if (parameterValueIsNull) {
		throw new SmartThingException("Required parameter '" + parameter.getName() + "' is missing.");
	    }

	    if (parameterValueIsEmpty) {
		throw new SmartThingException(
			"Required parameter '" + parameter.getName() + "' has no value assigned to it.");
	    }
	} else if (parameterValueIsNull || parameterValueIsEmpty) {
	    return;
	}

	switch (parameter.getType()) {
	case Fixed32:
	case Fixed64:
	case Int32:
	case Int64:
	case SFixed32:
	case SFixed64:
	case SInt32:
	case SInt64:
	case UInt32:
	case UInt64: {
	    try {
		Long.parseLong(value);
	    } catch (NumberFormatException e) {
		throw new SmartThingException("Parameter '" + parameter.getName() + "' must be integral.");
	    }
	}
	case Float: {
	    try {
		Float.parseFloat(value);
	    } catch (NumberFormatException e) {
		throw new SmartThingException("Parameter '" + parameter.getName() + "' must be a float.");
	    }
	}
	case Double: {
	    try {
		Double.parseDouble(value);
	    } catch (NumberFormatException e) {
		throw new SmartThingException("Parameter '" + parameter.getName() + "' must be a double.");
	    }
	}
	default:
	}
    }

    /**
     * Common logic for creating a {@link DeviceCommandResponse} from an
     * {@link IDeviceCommandResponseCreateRequest}.
     *
     * @param assignment
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static DeviceCommandResponse deviceCommandResponseCreateLogic(IDeviceAssignment assignment,
	    IDeviceCommandResponseCreateRequest request) throws SmartThingException {
	if (request.getOriginatingEventId() == null) {
	    throw new SmartThingSystemException(ErrorCode.IncompleteData, ErrorLevel.ERROR);
	}
	DeviceCommandResponse response = new DeviceCommandResponse();
	deviceEventCreateLogic(request, assignment, response);
	response.setOriginatingEventId(request.getOriginatingEventId());
	response.setResponseEventId(request.getResponseEventId());
	response.setResponse(request.getResponse());
	return response;
    }

    /**
     * Common logic for creating a {@link DeviceStateChange} from an
     * {@link IDeviceStateChangeCreateRequest}.
     *
     * @param assignment
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static DeviceStateChange deviceStateChangeCreateLogic(IDeviceAssignment assignment,
	    IDeviceStateChangeCreateRequest request) throws SmartThingException {
	if (request.getCategory() == null) {
	    throw new SmartThingSystemException(ErrorCode.IncompleteData, ErrorLevel.ERROR);
	}
	if (request.getType() == null) {
	    throw new SmartThingSystemException(ErrorCode.IncompleteData, ErrorLevel.ERROR);
	}
	DeviceStateChange state = new DeviceStateChange();
	deviceEventCreateLogic(request, assignment, state);
	state.setCategory(request.getCategory());
	state.setType(request.getType());
	state.setPreviousState(request.getPreviousState());
	state.setNewState(request.getNewState());
	if (request.getData() != null) {
	    state.getData().putAll(request.getData());
	}
	return state;
    }

    /**
     * Common logic for creating a zone based on an incoming request.
     *
     * @param source
     * @param siteToken
     * @param uuid
     * @return
     * @throws SmartThingException
     */
    public static Zone zoneCreateLogic(IZoneCreateRequest source, String siteToken, String uuid)
	    throws SmartThingException {
	Zone zone = new Zone();
	zone.setToken(uuid);
	zone.setSiteToken(siteToken);
	zone.setName(source.getName());
	zone.setBorderColor(source.getBorderColor());
	zone.setFillColor(source.getFillColor());
	zone.setOpacity(source.getOpacity());

	SmartThingPersistence.initializeEntityMetadata(zone);
	MetadataProvider.copy(source.getMetadata(), zone);

	for (ILocation coordinate : source.getCoordinates()) {
	    zone.getCoordinates().add(coordinate);
	}
	return zone;
    }

    /**
     * Common code for copying information from an update request to an existing
     * zone.
     *
     * @param request
     * @param target
     * @throws SmartThingException
     */
    public static void zoneUpdateLogic(IZoneCreateRequest request, Zone target) throws SmartThingException {
	target.setName(request.getName());
	target.setBorderColor(request.getBorderColor());
	target.setFillColor(request.getFillColor());
	target.setOpacity(request.getOpacity());

	target.getCoordinates().clear();
	for (ILocation coordinate : request.getCoordinates()) {
	    target.getCoordinates().add(coordinate);
	}

	if (request.getMetadata() != null) {
	    target.getMetadata().clear();
	    MetadataProvider.copy(request.getMetadata(), target);
	}
	SmartThingPersistence.setUpdatedEntityMetadata(target);
    }

    /**
     * Common logic for creating a device group based on an incoming request.
     *
     * @param source
     * @param uuid
     * @return
     * @throws SmartThingException
     */
    public static DeviceGroup deviceGroupCreateLogic(IDeviceGroupCreateRequest source, String uuid)
	    throws SmartThingException {
	DeviceGroup group = new DeviceGroup();
	group.setToken(uuid);
	group.setName(source.getName());
	group.setDescription(source.getDescription());
	if (source.getRoles() != null) {
	    group.getRoles().addAll(source.getRoles());
	}

	SmartThingPersistence.initializeEntityMetadata(group);
	MetadataProvider.copy(source.getMetadata(), group);
	return group;
    }

    /**
     * Common logic for updating an existing device group.
     *
     * @param request
     * @param target
     * @throws SmartThingException
     */
    public static void deviceGroupUpdateLogic(IDeviceGroupCreateRequest request, DeviceGroup target)
	    throws SmartThingException {
	target.setName(request.getName());
	target.setDescription(request.getDescription());

	if (request.getRoles() != null) {
	    target.getRoles().clear();
	    target.getRoles().addAll(request.getRoles());
	}

	if (request.getMetadata() != null) {
	    target.getMetadata().clear();
	    MetadataProvider.copy(request.getMetadata(), target);
	}
	SmartThingPersistence.setUpdatedEntityMetadata(target);
    }

    /**
     * Common logic for creating a new device group element.
     *
     * @param source
     * @param groupToken
     * @param index
     * @return
     * @throws SmartThingException
     */
    public static DeviceGroupElement deviceGroupElementCreateLogic(IDeviceGroupElementCreateRequest source,
	    String groupToken, long index) throws SmartThingException {
	DeviceGroupElement element = new DeviceGroupElement();
	element.setGroupToken(groupToken);
	element.setIndex(index);
	element.setType(source.getType());
	element.setElementId(source.getElementId());
	element.setRoles(source.getRoles());
	return element;
    }

    /**
     * Common logic for creating a batch operation based on an incoming request.
     *
     * @param source
     * @param uuid
     * @return
     * @throws SmartThingException
     */
    public static BatchOperation batchOperationCreateLogic(IBatchOperationCreateRequest source, String uuid)
	    throws SmartThingException {
	BatchOperation batch = new BatchOperation();
	batch.setToken(uuid);
	batch.setOperationType(source.getOperationType());
	batch.getParameters().putAll(source.getParameters());

	SmartThingPersistence.initializeEntityMetadata(batch);
	MetadataProvider.copy(source.getMetadata(), batch);
	return batch;
    }

    /**
     * Common logic for updating batch operation information.
     *
     * @param request
     * @param target
     * @throws SmartThingException
     */
    public static void batchOperationUpdateLogic(IBatchOperationUpdateRequest request, BatchOperation target)
	    throws SmartThingException {
	if (request.getProcessingStatus() != null) {
	    target.setProcessingStatus(request.getProcessingStatus());
	}
	if (request.getProcessingStartedDate() != null) {
	    target.setProcessingStartedDate(request.getProcessingStartedDate());
	}
	if (request.getProcessingEndedDate() != null) {
	    target.setProcessingEndedDate(request.getProcessingEndedDate());
	}

	if (request.getMetadata() != null) {
	    target.getMetadata().clear();
	    MetadataProvider.copy(request.getMetadata(), target);
	}
	SmartThingPersistence.setUpdatedEntityMetadata(target);
    }

    /**
     * Common logic for creating a batch operation element.
     *
     * @param batchOperationToken
     * @param hardwareId
     * @param index
     * @return
     * @throws SmartThingException
     */
    public static BatchElement batchElementCreateLogic(String batchOperationToken, String hardwareId, long index)
	    throws SmartThingException {
	BatchElement element = new BatchElement();
	element.setBatchOperationToken(batchOperationToken);
	element.setHardwareId(hardwareId);
	element.setIndex(index);
	element.setProcessingStatus(ElementProcessingStatus.Unprocessed);
	element.setProcessedDate(null);
	return element;
    }

    /**
     * Common logic for updating a batch operation element.
     *
     * @param request
     * @param element
     * @throws SmartThingException
     */
    public static void batchElementUpdateLogic(IBatchElementUpdateRequest request, BatchElement element)
	    throws SmartThingException {
	if (request.getProcessingStatus() != null) {
	    element.setProcessingStatus(request.getProcessingStatus());
	}
	if (request.getProcessedDate() != null) {
	    element.setProcessedDate(request.getProcessedDate());
	}
	if (request.getMetadata() != null) {
	    element.getMetadata().clear();
	    MetadataProvider.copy(request.getMetadata(), element);
	}
    }

    /**
     * Encodes batch command invocation parameters into the generic
     * {@link IBatchOperationCreateRequest} format.
     *
     * @param request
     * @param uuid
     * @return
     * @throws SmartThingException
     */
    public static IBatchOperationCreateRequest batchCommandInvocationCreateLogic(IBatchCommandInvocationRequest request,
	    String uuid) throws SmartThingException {
	BatchOperationCreateRequest batch = new BatchOperationCreateRequest();
	batch.setToken(uuid);
	batch.setOperationType(OperationType.InvokeCommand);
	batch.setHardwareIds(request.getHardwareIds());
	batch.getParameters().put(IBatchCommandInvocationRequest.PARAM_COMMAND_TOKEN, request.getCommandToken());
	Map<String, String> params = new HashMap<String, String>();
	for (String key : request.getParameterValues().keySet()) {
	    params.put(key, request.getParameterValues().get(key));
	}
	batch.setMetadata(params);
	return batch;
    }

    /**
     * Common logic for creating a user based on an incoming request.
     *
     * @param source
     * @param encodePassword
     * @return
     * @throws SmartThingException
     */
    public static User userCreateLogic(IUserCreateRequest source, boolean encodePassword) throws SmartThingException {
	String password = (encodePassword) ? passwordEncoder.encodePassword(source.getPassword(), null)
		: source.getPassword();

	User user = new User();

	require(source.getUsername());
	user.setUsername(source.getUsername());

	user.setHashedPassword(password);
	user.setFirstName(source.getFirstName());
	user.setLastName(source.getLastName());
	user.setLastLogin(null);
	user.setStatus(source.getStatus());
	user.setAuthorities(source.getAuthorities());

	MetadataProvider.copy(source, user);
	SmartThingPersistence.initializeEntityMetadata(user);
	return user;
    }

    /**
     * Common code for copying information from an update request to an existing
     * user.
     *
     * @param source
     * @param target
     * @param encodePassword
     * @throws SmartThingException
     */
    public static void userUpdateLogic(IUserCreateRequest source, User target, boolean encodePassword)
	    throws SmartThingException {
	if (source.getUsername() != null) {
	    target.setUsername(source.getUsername());
	}
	if (source.getPassword() != null) {
	    String password = (encodePassword) ? passwordEncoder.encodePassword(source.getPassword(), null)
		    : source.getPassword();
	    target.setHashedPassword(password);
	}
	if (source.getFirstName() != null) {
	    target.setFirstName(source.getFirstName());
	}
	if (source.getLastName() != null) {
	    target.setLastName(source.getLastName());
	}
	if (source.getStatus() != null) {
	    target.setStatus(source.getStatus());
	}
	if (source.getAuthorities() != null) {
	    target.setAuthorities(source.getAuthorities());
	}
	if (source.getMetadata() != null) {
	    target.getMetadata().clear();
	    MetadataProvider.copy(source, target);
	}
	SmartThingPersistence.setUpdatedEntityMetadata(target);
    }

    /**
     * Common logic for deleting a user. Takes care of related tasks such as
     * deleting user id from tenant authorized users.
     *
     * @param username
     * @throws SmartThingException
     */
    public static void userDeleteLogic(String username) throws SmartThingException {
	ITenantManagement management =SmartThing.getServer().getTenantManagement();
	ISearchResults<ITenant> tenants = management.listTenants(new TenantSearchCriteria(1, 0));
	for (ITenant tenant : tenants.getResults()) {
	    if (tenant.getAuthorizedUserIds().contains(username)) {
		TenantCreateRequest request = new TenantCreateRequest();
		List<String> ids = tenant.getAuthorizedUserIds();
		ids.remove(username);
		request.setAuthorizedUserIds(ids);
		request.setMetadata(null);
		management.updateTenant(tenant.getId(), request);
	    }
	}
    }

    /**
     * Common logic for creating a granted authority based on an incoming request.
     *
     * @param source
     * @return
     * @throws SmartThingException
     */
    public static GrantedAuthority grantedAuthorityCreateLogic(IGrantedAuthorityCreateRequest source)
	    throws SmartThingException {
	GrantedAuthority auth = new GrantedAuthority();

	require(source.getAuthority());
	auth.setAuthority(source.getAuthority());

	auth.setDescription(source.getDescription());
	auth.setParent(source.getParent());
	auth.setGroup(source.isGroup());
	return auth;
    }

    /**
     * Common logic for creating a tenant.
     *
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static Tenant tenantCreateLogic(ITenantCreateRequest request) throws SmartThingException {
	Tenant tenant = new Tenant();

	// Id is required.
	requireFormat(request.getId(), "[\\w]*", ErrorCode.TenantIdFormat);
	tenant.setId(request.getId());

	// Name is required.
	require(request.getName());
	tenant.setName(request.getName());

	// Logo is required.
	require(request.getLogoUrl());
	tenant.setLogoUrl(request.getLogoUrl());

	// Auth token is required.
	require(request.getAuthenticationToken());
	tenant.setAuthenticationToken(request.getAuthenticationToken());

	// Tenant template is required.
	require(request.getTenantTemplateId());
	tenant.setTenantTemplateId(request.getTenantTemplateId());

	tenant.getAuthorizedUserIds().addAll(request.getAuthorizedUserIds());

	MetadataProvider.copy(request.getMetadata(), tenant);
	SmartThingPersistence.initializeEntityMetadata(tenant);

	return tenant;
    }

    /**
     * Common logic for updating an existing tenant.
     *
     * @param request
     * @param existing
     * @return
     * @throws SmartThingException
     */
    public static Tenant tenantUpdateLogic(ITenantCreateRequest request, Tenant existing) throws SmartThingException {
	if ((request.getId() != null) && (!request.getId().equals(existing.getId()))) {
	    throw new SmartThingException("Can not change the id of an existing tenant.");
	}

	if (request.getTenantTemplateId() != null) {
	    if (!request.getTenantTemplateId().equals(existing.getTenantTemplateId())) {
		throw new SmartThingException("Can not change the template of an existing tenant.");
	    }
	}

	if (request.getName() != null) {
	    existing.setName(request.getName());
	}

	if (request.getLogoUrl() != null) {
	    existing.setLogoUrl(request.getLogoUrl());
	}

	if (request.getAuthenticationToken() != null) {
	    existing.setAuthenticationToken(request.getAuthenticationToken());
	}

	if (request.getAuthorizedUserIds() != null) {
	    existing.getAuthorizedUserIds().clear();
	    existing.getAuthorizedUserIds().addAll(request.getAuthorizedUserIds());
	}

	if (request.getMetadata() != null) {
	    existing.getMetadata().clear();
	    MetadataProvider.copy(request.getMetadata(), existing);
	}
	SmartThingPersistence.setUpdatedEntityMetadata(existing);

	return existing;
    }

    /**
     * Common tenant list logic.
     *
     * @param results
     * @param criteria
     * @throws SmartThingException
     */
    public static void tenantListLogic(List<ITenant> results, ITenantSearchCriteria criteria)
	    throws SmartThingException {
	if (criteria.isIncludeRuntimeInfo()) {
	    for (ITenant tenant : results) {
		ISmartThingTenantEngine engine =SmartThing.getServer().getTenantEngine(tenant.getId());
		if (engine != null) {
		    ((Tenant) tenant).setEngineState(engine.getEngineState());
		}
	    }
	}
    }

    /**
     * Common logic for creating an asset category.
     *
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static AssetCategory assetCategoryCreateLogic(IAssetCategoryCreateRequest request)
	    throws SmartThingException {
	AssetCategory category = new AssetCategory();

	require(request.getId());
	category.setId(request.getId());

	// Name is required.
	require(request.getName());
	category.setName(request.getName());

	// Type is required.
	requireNotNull(request.getAssetType());
	category.setAssetType(request.getAssetType());

	return category;
    }

    /**
     * Common logic for updating an existing asset category.
     *
     * @param request
     * @param existing
     * @return
     * @throws SmartThingException
     */
    public static AssetCategory assetCategoryUpdateLogic(IAssetCategoryCreateRequest request, AssetCategory existing)
	    throws SmartThingException {
	if (!request.getId().equals(existing.getId())) {
	    throw new SmartThingException("Can not change the id of an existing asset category.");
	}

	if (request.getAssetType() != existing.getAssetType()) {
	    throw new SmartThingException("Can not change the asset type of an existing asset category.");
	}

	if (request.getName() != null) {
	    existing.setName(request.getName());
	}

	return existing;
    }

    /**
     * Handle base logic common to all asset types.
     *
     * @param category
     * @param request
     * @param asset
     * @throws SmartThingException
     */
    public static void assetCreateLogic(IAssetCategory category, IAssetCreateRequest request, Asset asset)
	    throws SmartThingException {
	asset.setType(category.getAssetType());

	require(category.getId());
	asset.setAssetCategoryId(category.getId());

	require(request.getId());
	asset.setId(request.getId());

	require(request.getName());
	asset.setName(request.getName());

	require(request.getImageUrl());
	asset.setImageUrl(request.getImageUrl());

	asset.getProperties().putAll(request.getProperties());
    }

    /**
     * Common logic for updating assets.
     *
     * @param asset
     * @param request
     * @throws SmartThingException
     */
    public static void assetUpdateLogic(Asset asset, IAssetCreateRequest request) throws SmartThingException {
	if (!asset.getId().equals(request.getId())) {
	    throw new SmartThingException("Asset id can not be changed.");
	}

	if (request.getName() != null) {
	    asset.setName(request.getName());
	}
	if (request.getImageUrl() != null) {
	    asset.setImageUrl(request.getImageUrl());
	}
	if (request.getProperties() != null) {
	    asset.getProperties().clear();
	    asset.getProperties().putAll(request.getProperties());
	}
    }

    /**
     * Handle common logic for creating a person asset.
     *
     * @param category
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static PersonAsset personAssetCreateLogic(IAssetCategory category, IPersonAssetCreateRequest request)
	    throws SmartThingException {
	if (category.getAssetType() != AssetType.Person) {
	    throw new SmartThingSystemException(ErrorCode.AssetTypeNotAllowed, ErrorLevel.ERROR);
	}

	PersonAsset person = new PersonAsset();
	assetCreateLogic(category, request, person);

	person.setUserName(request.getUserName());
	person.setEmailAddress(request.getEmailAddress());
	person.getRoles().addAll(request.getRoles());

	return person;
    }

    /**
     * Handle common logic for updating a person asset.
     *
     * @param person
     * @param request
     * @throws SmartThingException
     */
    public static void personAssetUpdateLogic(PersonAsset person, IPersonAssetCreateRequest request)
	    throws SmartThingException {
	assetUpdateLogic(person, request);

	if (request.getUserName() != null) {
	    person.setUserName(request.getUserName());
	}
	if (request.getEmailAddress() != null) {
	    person.setEmailAddress(request.getEmailAddress());
	}
	if (request.getRoles() != null) {
	    person.getRoles().clear();
	    person.getRoles().addAll(request.getRoles());
	}
    }

    /**
     * Handle common logic for creating a hardware asset.
     *
     * @param category
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static HardwareAsset hardwareAssetCreateLogic(IAssetCategory category, IHardwareAssetCreateRequest request)
	    throws SmartThingException {
	if ((category.getAssetType() != AssetType.Hardware) && (category.getAssetType() != AssetType.Device)) {
	    throw new SmartThingSystemException(ErrorCode.AssetTypeNotAllowed, ErrorLevel.ERROR);
	}

	HardwareAsset hardware = new HardwareAsset();
	assetCreateLogic(category, request, hardware);

	hardware.setSku(request.getSku());
	hardware.setDescription(request.getDescription());

	return hardware;
    }

    /**
     * Handle common logic for updating a hardware asset.
     *
     * @param hardware
     * @param request
     * @throws SmartThingException
     */
    public static void hardwareAssetUpdateLogic(HardwareAsset hardware, IHardwareAssetCreateRequest request)
	    throws SmartThingException {
	assetUpdateLogic(hardware, request);

	if (request.getSku() != null) {
	    hardware.setSku(request.getSku());
	}
	if (request.getDescription() != null) {
	    hardware.setDescription(request.getDescription());
	}
    }

    /**
     * Handle common logic for creating a location asset.
     *
     * @param category
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static LocationAsset locationAssetCreateLogic(IAssetCategory category, ILocationAssetCreateRequest request)
	    throws SmartThingException {
	if (category.getAssetType() != AssetType.Location) {
	    throw new SmartThingSystemException(ErrorCode.AssetTypeNotAllowed, ErrorLevel.ERROR);
	}

	LocationAsset loc = new LocationAsset();
	assetCreateLogic(category, request, loc);

	loc.setLatitude(request.getLatitude());
	loc.setLongitude(request.getLongitude());
	loc.setElevation(request.getElevation());

	return loc;
    }

    /**
     * Handle common logic for updating a location asset.
     *
     * @param location
     * @param request
     * @throws SmartThingException
     */
    public static void locationAssetUpdateLogic(LocationAsset location, ILocationAssetCreateRequest request)
	    throws SmartThingException {
	assetUpdateLogic(location, request);

	if (request.getLatitude() != null) {
	    location.setLatitude(request.getLatitude());
	}
	if (request.getLongitude() != null) {
	    location.setLongitude(request.getLongitude());
	}
	if (request.getElevation() != null) {
	    location.setElevation(request.getElevation());
	}
    }

    /**
     * Handle common logic for creating a schedule.
     *
     * @param request
     * @param token
     * @return
     * @throws SmartThingException
     */
    public static Schedule scheduleCreateLogic(IScheduleCreateRequest request, String token) throws SmartThingException {
	Schedule schedule = new Schedule();

	// Unique token is required.
	if (token == null) {
	    throw new SmartThingSystemException(ErrorCode.IncompleteData, ErrorLevel.ERROR);
	}
	schedule.setToken(token);

	// Name is required.
	if (request.getName() == null) {
	    throw new SmartThingSystemException(ErrorCode.IncompleteData, ErrorLevel.ERROR);
	}
	schedule.setName(request.getName());

	// Trigger type is required.
	if (request.getTriggerType() == null) {
	    throw new SmartThingSystemException(ErrorCode.IncompleteData, ErrorLevel.ERROR);
	}
	schedule.setTriggerType(request.getTriggerType());
	schedule.setTriggerConfiguration(request.getTriggerConfiguration());

	schedule.setStartDate(request.getStartDate());
	schedule.setEndDate(request.getEndDate());

	SmartThingPersistence.initializeEntityMetadata(schedule);
	MetadataProvider.copy(request.getMetadata(), schedule);

	return schedule;
    }

    /**
     * Handle common logic for updating a schedule.
     *
     * @param schedule
     * @param request
     * @throws SmartThingException
     */
    public static void scheduleUpdateLogic(Schedule schedule, IScheduleCreateRequest request)
	    throws SmartThingException {
	if (request.getName() != null) {
	    schedule.setName(request.getName());
	}
	if (request.getTriggerType() != null) {
	    schedule.setTriggerType(request.getTriggerType());
	}
	if (request.getTriggerConfiguration() != null) {
	    schedule.getTriggerConfiguration().clear();
	    schedule.getTriggerConfiguration().putAll(request.getTriggerConfiguration());
	}

	schedule.setStartDate(request.getStartDate());
	schedule.setEndDate(request.getEndDate());

	SmartThingPersistence.setUpdatedEntityMetadata(schedule);
    }

    /**
     * Handle common logic for creating a scheduled job.
     *
     * @param request
     * @param token
     * @return
     * @throws SmartThingException
     */
    public static ScheduledJob scheduledJobCreateLogic(IScheduledJobCreateRequest request, String token)
	    throws SmartThingException {
	ScheduledJob job = new ScheduledJob();

	// Unique token is required.
	if (token == null) {
	    throw new SmartThingSystemException(ErrorCode.IncompleteData, ErrorLevel.ERROR);
	}
	job.setToken(token);

	// Schedule token is required.
	if (request.getScheduleToken() == null) {
	    throw new SmartThingSystemException(ErrorCode.IncompleteData, ErrorLevel.ERROR);
	}
	job.setScheduleToken(request.getScheduleToken());

	// Job type is required.
	if (request.getJobType() == null) {
	    throw new SmartThingSystemException(ErrorCode.IncompleteData, ErrorLevel.ERROR);
	}
	job.setJobType(request.getJobType());
	job.setJobConfiguration(request.getJobConfiguration());

	job.setJobState(ScheduledJobState.Unsubmitted);

	SmartThingPersistence.initializeEntityMetadata(job);
	MetadataProvider.copy(request.getMetadata(), job);

	return job;
    }

    /**
     * Handle common logic for updating a scheduled job.
     *
     * @param job
     * @param request
     * @throws SmartThingException
     */
    public static void scheduledJobUpdateLogic(ScheduledJob job, IScheduledJobCreateRequest request)
	    throws SmartThingException {
	if (request.getScheduleToken() != null) {
	    job.setScheduleToken(request.getScheduleToken());
	}
	if (request.getJobType() != null) {
	    job.setJobType(request.getJobType());
	}
	if (request.getJobConfiguration() != null) {
	    job.getJobConfiguration().clear();
	    job.getJobConfiguration().putAll(request.getJobConfiguration());
	}
	if (request.getJobState() != null) {
	    job.setJobState(request.getJobState());
	}
	SmartThingPersistence.setUpdatedEntityMetadata(job);
    }

    /**
     * Common logic for encoding a plaintext password.
     *
     * @param plaintext
     * @return
     */
    public static String encodePassword(String plaintext) {
	return passwordEncoder.encodePassword(plaintext, null);
    }
}
