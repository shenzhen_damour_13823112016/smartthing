package com.smartthing.server;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.smartthing.common.MarshalUtils;
import com.smartthing.server.resource.SmartThingHomeResourceManager;
import com.smartthing.server.tenant.TenantTemplate;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.configuration.IDefaultResourcePaths;
import com.smartthing.spi.server.IBackwardCompatibilityService;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;

/**
 * Helps with migration of previous server versions to provide compatibility for
 * users that drop the new WAR into an existing server instance.
 * 
 * @author Derek
 */
public class BackwardCompatibilityService implements IBackwardCompatibilityService {

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.IBackwardCompatibilityService#
     * beforeServerInitialize(com.smartthing.spi.server.lifecycle.
     * ILifecycleProgressMonitor)
     */
    @Override
    public void beforeServerInitialize(ILifecycleProgressMonitor monitor) throws SmartThingException {
	migrateOldTemplateToNew(monitor);
    }

    /**
     * Migrate the old template format to the new one. Note that no data
     * initializers will be fired.
     * 
     * @param monitor
     * @throws SmartThingException
     */
    @SuppressWarnings("deprecation")
    protected void migrateOldTemplateToNew(ILifecycleProgressMonitor monitor) throws SmartThingException {
	File home = SmartThingHomeResourceManager.calculateConfigurationPath();
	File oldTemplate = new File(home, "tenant-template");
	if (oldTemplate.exists()) {
	    File templates = new File(home, IDefaultResourcePaths.TEMPLATES_FOLDER_NAME);
	    if (!templates.exists()) {
		if (!templates.mkdirs()) {
		    throw new SmartThingException("Unable to create new templates folder to transition old data.");
		}
	    }
	    File compatibility = new File(templates, "compatibility");
	    if (!oldTemplate.renameTo(compatibility)) {
		throw new SmartThingException("Unable to rename old templates folder to new format.");
	    }
	    TenantTemplate template = new TenantTemplate();
	    template.setId("compatibility");
	    template.setName("Pre-1.9.0 SiteWhere Template");
	    String content = MarshalUtils.marshalJsonAsPrettyString(template);
	    File templateFile = new File(compatibility, IDefaultResourcePaths.TEMPLATE_JSON_FILE_NAME);
	    try {
		FileUtils.writeStringToFile(templateFile, content, false);
	    } catch (IOException e) {
		throw new SmartThingException("Unable to write template file.");
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.IBackwardCompatibilityService#beforeServerStart(
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void beforeServerStart(ILifecycleProgressMonitor monitor) throws SmartThingException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.IBackwardCompatibilityService#afterServerStart(
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void afterServerStart(ILifecycleProgressMonitor monitor) throws SmartThingException {
    }
}