/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.server.asset;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.server.lifecycle.LifecycleProgressContext;
import com.smartthing.server.lifecycle.LifecycleProgressMonitor;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.IAsset;
import com.smartthing.spi.asset.IAssetCategory;
import com.smartthing.spi.asset.IAssetManagement;
import com.smartthing.spi.asset.IAssetModuleManager;
import com.smartthing.spi.asset.IHardwareAsset;
import com.smartthing.spi.asset.ILocationAsset;
import com.smartthing.spi.asset.IPersonAsset;
import com.smartthing.spi.asset.request.IAssetCategoryCreateRequest;
import com.smartthing.spi.asset.request.IHardwareAssetCreateRequest;
import com.smartthing.spi.asset.request.ILocationAssetCreateRequest;
import com.smartthing.spi.asset.request.IPersonAssetCreateRequest;
import com.smartthing.spi.server.lifecycle.LifecycleStatus;

/**
 * Trigger actions based on asset management API calls.
 * 
 * @author Derek
 */
public class AssetManagementTriggers extends AssetManagementDecorator {

    /** Static logger instance */
    @SuppressWarnings("unused")
    private static Logger LOGGER = LogManager.getLogger();

    /** Asset module manager reference */
    private IAssetModuleManager assetModuleManager;

    public AssetManagementTriggers(IAssetManagement delegate, IAssetModuleManager assetModuleManager) {
	super(delegate);
	this.assetModuleManager = assetModuleManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.asset.AssetManagementDecorator#createAssetCategory(
     * com.smartthing.spi.asset.request.IAssetCategoryCreateRequest)
     */
    @Override
    public IAssetCategory createAssetCategory(IAssetCategoryCreateRequest request) throws SmartThingException {
	IAssetCategory category = super.createAssetCategory(request);
	IAssetModuleManager manager = getAssetModuleManager();
	if ((manager != null) && (manager.getLifecycleStatus() == LifecycleStatus.Started)) {
	    manager.onAssetCategoryAdded(category, new LifecycleProgressMonitor(
		    new LifecycleProgressContext(1, "Add asset module for new category.")));
	}
	return category;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.asset.AssetManagementDecorator#updateAssetCategory(
     * java.lang.String,
     * com.smartthing.spi.asset.request.IAssetCategoryCreateRequest)
     */
    @Override
    public IAssetCategory updateAssetCategory(String categoryId, IAssetCategoryCreateRequest request)
	    throws SmartThingException {
	IAssetCategory category = super.updateAssetCategory(categoryId, request);
	IAssetModuleManager manager = getAssetModuleManager();
	if ((manager != null) && (manager.getLifecycleStatus() == LifecycleStatus.Started)) {
	    manager.onAssetCategoryUpdated(category, new LifecycleProgressMonitor(
		    new LifecycleProgressContext(1, "Reload asset module for updated category.")));
	}
	return category;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.asset.AssetManagementDecorator#deleteAssetCategory(
     * java.lang.String)
     */
    @Override
    public IAssetCategory deleteAssetCategory(String categoryId) throws SmartThingException {
	IAssetCategory category = super.deleteAssetCategory(categoryId);
	IAssetModuleManager manager = getAssetModuleManager();
	if ((manager != null) && (manager.getLifecycleStatus() == LifecycleStatus.Started)) {
	    manager.onAssetCategoryRemoved(category, new LifecycleProgressMonitor(
		    new LifecycleProgressContext(1, "Remove asset module for deleted category.")));
	}
	return category;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.asset.AssetManagementDecorator#createPersonAsset(
     * java.lang .String,
     * com.smartthing.spi.asset.request.IPersonAssetCreateRequest)
     */
    @Override
    public IPersonAsset createPersonAsset(String categoryId, IPersonAssetCreateRequest request)
	    throws SmartThingException {
	IPersonAsset asset = super.createPersonAsset(categoryId, request);
	refreshAssetModule(categoryId, asset);
	return asset;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.asset.AssetManagementDecorator#updatePersonAsset(
     * java.lang .String, java.lang.String,
     * com.smartthing.spi.asset.request.IPersonAssetCreateRequest)
     */
    @Override
    public IPersonAsset updatePersonAsset(String categoryId, String assetId, IPersonAssetCreateRequest request)
	    throws SmartThingException {
	IPersonAsset asset = super.updatePersonAsset(categoryId, assetId, request);
	refreshAssetModule(categoryId, asset);
	return asset;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.asset.AssetManagementDecorator#createHardwareAsset(
     * java.lang .String,
     * com.smartthing.spi.asset.request.IHardwareAssetCreateRequest)
     */
    @Override
    public IHardwareAsset createHardwareAsset(String categoryId, IHardwareAssetCreateRequest request)
	    throws SmartThingException {
	IHardwareAsset asset = super.createHardwareAsset(categoryId, request);
	refreshAssetModule(categoryId, asset);
	return asset;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.asset.AssetManagementDecorator#updateHardwareAsset(
     * java.lang .String, java.lang.String,
     * com.smartthing.spi.asset.request.IHardwareAssetCreateRequest)
     */
    @Override
    public IHardwareAsset updateHardwareAsset(String categoryId, String assetId, IHardwareAssetCreateRequest request)
	    throws SmartThingException {
	IHardwareAsset asset = super.updateHardwareAsset(categoryId, assetId, request);
	refreshAssetModule(categoryId, asset);
	return asset;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.asset.AssetManagementDecorator#createLocationAsset(
     * java.lang .String,
     * com.smartthing.spi.asset.request.ILocationAssetCreateRequest)
     */
    @Override
    public ILocationAsset createLocationAsset(String categoryId, ILocationAssetCreateRequest request)
	    throws SmartThingException {
	ILocationAsset asset = super.createLocationAsset(categoryId, request);
	refreshAssetModule(categoryId, asset);
	return asset;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.asset.AssetManagementDecorator#updateLocationAsset(
     * java.lang .String, java.lang.String,
     * com.smartthing.spi.asset.request.ILocationAssetCreateRequest)
     */
    @Override
    public ILocationAsset updateLocationAsset(String categoryId, String assetId, ILocationAssetCreateRequest request)
	    throws SmartThingException {
	ILocationAsset asset = super.updateLocationAsset(categoryId, assetId, request);
	refreshAssetModule(categoryId, asset);
	return asset;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.asset.AssetManagementDecorator#deleteAsset(java.lang
     * .String, java.lang.String)
     */
    @Override
    public IAsset deleteAsset(String categoryId, String assetId) throws SmartThingException {
	IAsset asset = super.deleteAsset(categoryId, assetId);
	refreshAssetModule(categoryId, asset);
	return asset;
    }

    /**
     * Push an updated asset to its associated asset module.
     * 
     * @param categoryId
     * @param asset
     * @throws SmartThingException
     */
    protected <T extends IAsset> void refreshAssetModule(String categoryId, T asset) throws SmartThingException {
	IAssetModuleManager manager = getAssetModuleManager();
	if ((manager != null) && (manager.getLifecycleStatus() == LifecycleStatus.Started)) {
	    manager.getModule(categoryId)
		    .refresh(new LifecycleProgressMonitor(new LifecycleProgressContext(1, "Refresh asset module.")));
	}
    }

    public IAssetModuleManager getAssetModuleManager() {
	return assetModuleManager;
    }

    public void setAssetModuleManager(IAssetModuleManager assetModuleManager) {
	this.assetModuleManager = assetModuleManager;
    }
}