package com.smartthing.server.asset.datastore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.rest.model.search.SearchCriteria;
import com.smartthing.server.lifecycle.LifecycleProgressContext;
import com.smartthing.server.lifecycle.LifecycleProgressMonitor;
import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.IAsset;
import com.smartthing.spi.asset.IAssetCategory;
import com.smartthing.spi.asset.IAssetManagement;
import com.smartthing.spi.asset.IAssetModule;
import com.smartthing.spi.asset.IAssetModuleManager;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Manages asset modules that are loaded from the datastore.
 * 
 * @author Derek
 */
public class DatastoreAssetModuleManager extends TenantLifecycleComponent implements IAssetModuleManager {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Map of datastore modules by id */
    private Map<String, IAssetModule<?>> dsModulesById = new HashMap<String, IAssetModule<?>>();

    /** Asset management implementation */
    private IAssetManagement assetManagement;

    public DatastoreAssetModuleManager() {
	super(LifecycleComponentType.Other);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
     * .server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	getLifecycleComponents().clear();

	refreshDatastoreModules(
		new LifecycleProgressMonitor(new LifecycleProgressContext(1, "Refreshing datastore asset modules")));

	for (IAssetModule<?> module : dsModulesById.values()) {
	    startNestedComponent(module, monitor, true);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#stop(com.smartthing.spi.
     * server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
	for (IAssetModule<?> module : dsModulesById.values()) {
	    module.lifecycleStop(monitor);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.asset.IAssetModuleManager#getModule(java.lang.String)
     */
    @Override
    public IAssetModule<?> getModule(String assetModuleId) throws SmartThingException {
	return dsModulesById.get(assetModuleId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.asset.IAssetModuleManager#listModules()
     */
    @Override
    public List<IAssetModule<?>> listModules() throws SmartThingException {
	List<IAssetModule<?>> modules = new ArrayList<IAssetModule<?>>();
	for (IAssetModule<?> module : dsModulesById.values()) {
	    modules.add(module);
	}
	return modules;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.asset.IAssetModuleManager#refreshModules(com.smartthing.
     * spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void refreshModules(ILifecycleProgressMonitor monitor) throws SmartThingException {
	for (IAssetModule<?> module : dsModulesById.values()) {
	    module.refresh(monitor);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.asset.IAssetModuleManager#onAssetCategoryAdded(com.
     * sitewhere.spi.asset.IAssetCategory,
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void onAssetCategoryAdded(IAssetCategory category, ILifecycleProgressMonitor monitor)
	    throws SmartThingException {
	addAssetCategoryModule(category, monitor);
	LOGGER.info("Asset module added for category '" + category.getName() + "'.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.asset.IAssetModuleManager#onAssetCategoryUpdated(com.
     * sitewhere.spi.asset.IAssetCategory,
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void onAssetCategoryUpdated(IAssetCategory category, ILifecycleProgressMonitor monitor)
	    throws SmartThingException {
	IAssetModule<?> module = getModule(category.getId());
	if (module != null) {
	    module.refresh(monitor);
	} else {
	    LOGGER.warn("Update received for non-existent asset module. Category: " + category.getId());
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.asset.IAssetModuleManager#onAssetCategoryRemoved(com.
     * sitewhere.spi.asset.IAssetCategory,
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void onAssetCategoryRemoved(IAssetCategory category, ILifecycleProgressMonitor monitor)
	    throws SmartThingException {
	IAssetModule<?> module = dsModulesById.get(category.getId());
	if (module != null) {
	    module.lifecycleStop(monitor);
	    dsModulesById.remove(category.getId());
	    LOGGER.info("Asset module removed for category '" + category.getName() + "'.");
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.asset.IAssetModuleManager#getAssetById(java.lang.
     * String, java.lang.String)
     */
    @Override
    public IAsset getAssetById(String assetModuleId, String id) throws SmartThingException {
	IAssetModule<?> match = assertAssetModule(assetModuleId);
	return match.getAsset(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.asset.IAssetModuleManager#search(java.lang.String,
     * java.lang.String)
     */
    @Override
    public List<? extends IAsset> search(String assetModuleId, String criteria) throws SmartThingException {
	IAssetModule<?> match = assertAssetModule(assetModuleId);
	List<? extends IAsset> results = match.search(criteria);
	Collections.sort(results);
	return results;
    }

    /**
     * Refresh list of datastore asset modules.
     * 
     * @param monitor
     * @throws SmartThingException
     */
    protected void refreshDatastoreModules(ILifecycleProgressMonitor monitor) throws SmartThingException {
	dsModulesById.clear();
	ISearchResults<IAssetCategory> categories = getAssetManagement().listAssetCategories(SearchCriteria.ALL);
	LOGGER.info("Refreshing asset modules for " + categories.getNumResults() + " asset categories.");
	for (IAssetCategory category : categories.getResults()) {
	    addAssetCategoryModule(category, monitor);
	    LOGGER.info("Added module for '" + category.getName() + " (" + category.getId() + ").");
	}
    }

    /**
     * Add an asset module for the given {@link IAssetCategory}.
     * 
     * @param category
     * @param monitor
     * @throws SmartThingException
     */
    protected void addAssetCategoryModule(IAssetCategory category, ILifecycleProgressMonitor monitor)
	    throws SmartThingException {
	switch (category.getAssetType()) {
	case Device:
	case Hardware: {
	    HardwareAssetModule module = new HardwareAssetModule(category, getAssetManagement());
	    initializeDatastoreModule(category, module, monitor);
	    break;
	}
	case Person: {
	    PersonAssetModule module = new PersonAssetModule(category, getAssetManagement());
	    initializeDatastoreModule(category, module, monitor);
	    break;
	}
	case Location: {
	    LocationAssetModule module = new LocationAssetModule(category, getAssetManagement());
	    initializeDatastoreModule(category, module, monitor);
	    break;
	}
	}
    }

    /**
     * Initialize a datastore module.
     * 
     * @param category
     * @param module
     * @param monitor
     * @throws SmartThingException
     */
    protected void initializeDatastoreModule(IAssetCategory category, IAssetModule<?> module,
	    ILifecycleProgressMonitor monitor) throws SmartThingException {
	dsModulesById.put(category.getId(), module);
    }

    /**
     * Get an asset module by unique id. Throw exception if not found.
     * 
     * @param id
     * @return
     * @throws SmartThingException
     */
    protected IAssetModule<?> assertAssetModule(String id) throws SmartThingException {
	IAssetModule<?> match = dsModulesById.get(id);
	if (match == null) {
	    throw new SmartThingException("Invalid asset module id: " + id);
	}
	return match;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    public IAssetManagement getAssetManagement() {
	return assetManagement;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.asset.IAssetModuleManager#setAssetManagement(com.
     * sitewhere.spi.asset.IAssetManagement)
     */
    public void setAssetManagement(IAssetManagement assetManagement) {
	this.assetManagement = assetManagement;
    }
}