/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.server.asset;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.server.asset.datastore.DatastoreAssetModuleManager;
import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.asset.IAsset;
import com.smartthing.spi.asset.IAssetCategory;
import com.smartthing.spi.asset.IAssetManagement;
import com.smartthing.spi.asset.IAssetModule;
import com.smartthing.spi.asset.IAssetModuleManager;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Manages the list of modules
 * 
 * @author dadams
 */
public class AssetModuleManager extends TenantLifecycleComponent implements IAssetModuleManager {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** List of asset modules */
    private List<IAssetModule<?>> modules;

    /** Map of asset modules by unique id */
    private Map<String, IAssetModule<?>> modulesById = new HashMap<String, IAssetModule<?>>();

    /** Manages asset modules loaded from the datastore */
    private DatastoreAssetModuleManager dsModuleManager = new DatastoreAssetModuleManager();

    public AssetModuleManager() {
	super(LifecycleComponentType.AssetModuleManager);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.asset.IAssetModuleManager#setAssetManagement(com.
     * sitewhere.spi.asset.IAssetManagement)
     */
    @Override
    public void setAssetManagement(IAssetManagement assetManagement) {
	dsModuleManager.setAssetManagement(assetManagement);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.server.lifecycle.LifecycleComponent#initialize(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void initialize(ILifecycleProgressMonitor monitor) throws SmartThingException {
	initializeNestedComponent(dsModuleManager, monitor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
     * .server.lifecycle.ILifecycleProgressMonitor)
     */
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	getLifecycleComponents().clear();

	modulesById.clear();
	for (IAssetModule<?> module : modules) {
	    startNestedComponent(module, monitor, true);
	    modulesById.put(module.getId(), module);
	}

	// Start datastore module manager as nested component.
	startNestedComponent(dsModuleManager, monitor, true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#stop(com.smartthing.spi.
     * server.lifecycle.ILifecycleProgressMonitor)
     */
    public void stop(ILifecycleProgressMonitor monitor) {
	for (IAssetModule<?> module : modulesById.values()) {
	    module.lifecycleStop(monitor);
	}

	// Stop datastore module manager.
	dsModuleManager.lifecycleStop(monitor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.asset.IAssetModuleManager#getModule(java.lang.String)
     */
    @Override
    public IAssetModule<?> getModule(String assetModuleId) throws SmartThingException {
	IAssetModule<?> module = modulesById.get(assetModuleId);
	if (module == null) {
	    module = dsModuleManager.getModule(assetModuleId);
	}
	return module;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.asset.IAssetModuleManager#listModules()
     */
    @Override
    public List<IAssetModule<?>> listModules() throws SmartThingException {
	List<IAssetModule<?>> modules = new ArrayList<IAssetModule<?>>();
	for (IAssetModule<?> module : modulesById.values()) {
	    modules.add(module);
	}
	modules.addAll(dsModuleManager.listModules());
	Collections.sort(modules, new AssetModuleComparator());
	return modules;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.asset.IAssetModuleManager#getAssetById(java.lang.
     * String, java.lang.String)
     */
    public IAsset getAssetById(String assetModuleId, String id) throws SmartThingException {
	IAssetModule<?> match = getModule(assetModuleId);
	if (match != null) {
	    return match.getAsset(id);
	}
	throw new SmartThingSystemException(ErrorCode.InvalidAssetCategoryId, ErrorLevel.ERROR);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.asset.IAssetModuleManager#search(java.lang.String,
     * java.lang.String)
     */
    public List<? extends IAsset> search(String assetModuleId, String criteria) throws SmartThingException {
	IAssetModule<?> match = getModule(assetModuleId);
	if (match != null) {
	    List<? extends IAsset> results = match.search(criteria);
	    Collections.sort(results);
	    return results;
	}
	throw new SmartThingSystemException(ErrorCode.InvalidAssetCategoryId, ErrorLevel.ERROR);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.asset.IAssetModuleManager#refreshModules(com.smartthing.
     * spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    public void refreshModules(ILifecycleProgressMonitor monitor) throws SmartThingException {
	for (IAssetModule<?> module : modulesById.values()) {
	    module.refresh(monitor);
	}
	dsModuleManager.refreshModules(monitor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.asset.IAssetModuleManager#onAssetCategoryAdded(com.
     * sitewhere.spi.asset.IAssetCategory,
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void onAssetCategoryAdded(IAssetCategory category, ILifecycleProgressMonitor monitor)
	    throws SmartThingException {
	dsModuleManager.onAssetCategoryAdded(category, monitor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.asset.IAssetModuleManager#onAssetCategoryUpdated(com.
     * sitewhere.spi.asset.IAssetCategory,
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void onAssetCategoryUpdated(IAssetCategory category, ILifecycleProgressMonitor monitor)
	    throws SmartThingException {
	dsModuleManager.onAssetCategoryUpdated(category, monitor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.asset.IAssetModuleManager#onAssetCategoryRemoved(com.
     * sitewhere.spi.asset.IAssetCategory,
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void onAssetCategoryRemoved(IAssetCategory category, ILifecycleProgressMonitor monitor)
	    throws SmartThingException {
	dsModuleManager.onAssetCategoryRemoved(category, monitor);
    }

    public List<IAssetModule<?>> getModules() {
	return modules;
    }

    public void setModules(List<IAssetModule<?>> modules) {
	this.modules = modules;
    }

    /**
     * Used for sorting asset modules.
     * 
     * @author Derek
     */
    private class AssetModuleComparator implements Comparator<IAssetModule<?>> {

	@Override
	public int compare(IAssetModule<?> o1, IAssetModule<?> o2) {
	    return o1.getName().compareTo(o2.getName());
	}
    }
}