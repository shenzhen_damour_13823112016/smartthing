/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.server;

import java.util.List;
import java.util.Map;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.smartthing.server.lifecycle.LifecycleComponentDecorator;
import com.smartthing.spi.ServerStartupException;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.IAssetManagement;
import com.smartthing.spi.asset.IAssetModuleManager;
import com.smartthing.spi.configuration.IGlobalConfigurationResolver;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.IDeviceManagementCacheProvider;
import com.smartthing.spi.device.communication.IDeviceCommunication;
import com.smartthing.spi.device.event.IDeviceEventManagement;
import com.smartthing.spi.device.event.IEventProcessing;
import com.smartthing.spi.resource.IResourceManager;
import com.smartthing.spi.scheduling.IScheduleManagement;
import com.smartthing.spi.scheduling.IScheduleManager;
import com.smartthing.spi.search.external.ISearchProviderManager;
import com.smartthing.spi.server.ISmartThingServer;
import com.smartthing.spi.server.ISmartThingServerRuntime;
import com.smartthing.spi.server.ISmartThingServerState;
import com.smartthing.spi.server.debug.ITracer;
import com.smartthing.spi.server.groovy.IGroovyConfiguration;
import com.smartthing.spi.server.groovy.ITenantGroovyConfiguration;
import com.smartthing.spi.server.hazelcast.IHazelcastConfiguration;
import com.smartthing.spi.server.lifecycle.ILifecycleComponent;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.tenant.ISmartThingTenantEngine;
import com.smartthing.spi.server.tenant.ITenantTemplateManager;
import com.smartthing.spi.system.IVersion;
import com.smartthing.spi.tenant.ITenant;
import com.smartthing.spi.tenant.ITenantManagement;
import com.smartthing.spi.user.IUserManagement;

/**
 * Wraps an {@link ISmartThingServer} implementation so new functionality can be
 * triggered by API calls withhout changing core server logic.
 * 
 * @author Derek
 */
public class SmartThingServerDecorator extends LifecycleComponentDecorator implements ISmartThingServer {

    /** Delegate instance being wrapped */
    private ISmartThingServer server;

    public SmartThingServerDecorator(ISmartThingServer server) {
	super(server);
	this.server = server;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getVersion()
     */
    @Override
    public IVersion getVersion() {
	return server.getVersion();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getConfigurationParserClassname
     * ()
     */
    @Override
    public String getConfigurationParserClassname() {
	return server.getConfigurationParserClassname();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#
     * getTenantConfigurationParserClassname()
     */
    @Override
    public String getTenantConfigurationParserClassname() {
	return server.getTenantConfigurationParserClassname();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getServerState()
     */
    @Override
    public ISmartThingServerState getServerState() {
	return server.getServerState();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getServerState(boolean)
     */
    @Override
    public ISmartThingServerRuntime getServerRuntimeInformation(boolean includeHistorical) throws SmartThingException {
	return server.getServerRuntimeInformation(includeHistorical);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponentDecorator#initialize(com
     * .sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void initialize(ILifecycleProgressMonitor monitor) throws SmartThingException {
	server.initialize(monitor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getServerStartupError()
     */
    @Override
    public ServerStartupException getServerStartupError() {
	return server.getServerStartupError();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#setServerStartupError(com.
     * sitewhere.spi .ServerStartupException)
     */
    @Override
    public void setServerStartupError(ServerStartupException e) {
	server.setServerStartupError(e);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getTracer()
     */
    @Override
    public ITracer getTracer() {
	return server.getTracer();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getHazelcastConfiguration()
     */
    @Override
    public IHazelcastConfiguration getHazelcastConfiguration() {
	return server.getHazelcastConfiguration();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getGroovyConfiguration()
     */
    @Override
    public IGroovyConfiguration getGroovyConfiguration() {
	return server.getGroovyConfiguration();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getBootstrapResourceManager()
     */
    @Override
    public IResourceManager getBootstrapResourceManager() {
	return server.getBootstrapResourceManager();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getRuntimeResourceManager()
     */
    @Override
    public IResourceManager getRuntimeResourceManager() {
	return server.getRuntimeResourceManager();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getConfigurationResolver()
     */
    @Override
    public IGlobalConfigurationResolver getConfigurationResolver() {
	return server.getConfigurationResolver();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getTenantTemplateManager()
     */
    @Override
    public ITenantTemplateManager getTenantTemplateManager() {
	return server.getTenantTemplateManager();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getTenantByAuthToken(java.lang.
     * String)
     */
    @Override
    public ITenant getTenantByAuthToken(String authToken) throws SmartThingException {
	return server.getTenantByAuthToken(authToken);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getAuthorizedTenants(java.lang.
     * String, boolean)
     */
    @Override
    public List<ITenant> getAuthorizedTenants(String userId, boolean requireStarted) throws SmartThingException {
	return server.getAuthorizedTenants(userId, requireStarted);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getTenantEnginesById()
     */
    @Override
    public Map<String, ISmartThingTenantEngine> getTenantEnginesById() {
	return server.getTenantEnginesById();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getTenantEngine(java.lang.
     * String)
     */
    @Override
    public ISmartThingTenantEngine getTenantEngine(String tenantId) throws SmartThingException {
	return server.getTenantEngine(tenantId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#onTenantUpdated(com.smartthing.
     * spi.tenant.ITenant)
     */
    @Override
    public void onTenantUpdated(ITenant tenant) throws SmartThingException {
	server.onTenantUpdated(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#onTenantDeleted(com.smartthing.
     * spi.tenant.ITenant)
     */
    @Override
    public void onTenantDeleted(ITenant tenant) throws SmartThingException {
	server.onTenantDeleted(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getUserManagement()
     */
    @Override
    public IUserManagement getUserManagement() {
	return server.getUserManagement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getTenantManagement()
     */
    @Override
    public ITenantManagement getTenantManagement() {
	return server.getTenantManagement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getDeviceManagement(com.
     * sitewhere.spi .user.ITenant)
     */
    @Override
    public IDeviceManagement getDeviceManagement(ITenant tenant) throws SmartThingException {
	return server.getDeviceManagement(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getDeviceEventManagement(com.
     * sitewhere .spi.user.ITenant)
     */
    @Override
    public IDeviceEventManagement getDeviceEventManagement(ITenant tenant) throws SmartThingException {
	return server.getDeviceEventManagement(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#
     * getDeviceManagementCacheProvider(com. sitewhere.spi.user.ITenant)
     */
    @Override
    public IDeviceManagementCacheProvider getDeviceManagementCacheProvider(ITenant tenant) throws SmartThingException {
	return server.getDeviceManagementCacheProvider(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getAssetManagement(com.
     * sitewhere.spi. user.ITenant)
     */
    @Override
    public IAssetManagement getAssetManagement(ITenant tenant) throws SmartThingException {
	return server.getAssetManagement(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getScheduleManagement(com.
     * sitewhere.spi .user.ITenant)
     */
    @Override
    public IScheduleManagement getScheduleManagement(ITenant tenant) throws SmartThingException {
	return server.getScheduleManagement(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getDeviceCommunication(com.
     * sitewhere. spi.user.ITenant)
     */
    @Override
    public IDeviceCommunication getDeviceCommunication(ITenant tenant) throws SmartThingException {
	return server.getDeviceCommunication(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getEventProcessing(com.
     * sitewhere.spi. user.ITenant)
     */
    @Override
    public IEventProcessing getEventProcessing(ITenant tenant) throws SmartThingException {
	return server.getEventProcessing(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getAssetModuleManager(com.
     * sitewhere.spi .user.ITenant)
     */
    @Override
    public IAssetModuleManager getAssetModuleManager(ITenant tenant) throws SmartThingException {
	return server.getAssetModuleManager(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getSearchProviderManager(com.
     * sitewhere .spi.user.ITenant)
     */
    @Override
    public ISearchProviderManager getSearchProviderManager(ITenant tenant) throws SmartThingException {
	return server.getSearchProviderManager(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getScheduleManager(com.
     * sitewhere.spi. user.ITenant)
     */
    @Override
    public IScheduleManager getScheduleManager(ITenant tenant) throws SmartThingException {
	return server.getScheduleManager(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getTenantGroovyConfiguration(
     * com.smartthing.spi.tenant.ITenant)
     */
    @Override
    public ITenantGroovyConfiguration getTenantGroovyConfiguration(ITenant tenant) throws SmartThingException {
	return server.getTenantGroovyConfiguration(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#
     * getRegisteredLifecycleComponents()
     */
    @Override
    public List<ILifecycleComponent> getRegisteredLifecycleComponents() {
	return server.getRegisteredLifecycleComponents();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getLifecycleComponentById(java.
     * lang. String )
     */
    @Override
    public ILifecycleComponent getLifecycleComponentById(String id) {
	return server.getLifecycleComponentById(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getMetricRegistry()
     */
    @Override
    public MetricRegistry getMetricRegistry() {
	return server.getMetricRegistry();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getHealthCheckRegistry()
     */
    @Override
    public HealthCheckRegistry getHealthCheckRegistry() {
	return server.getHealthCheckRegistry();
    }
}