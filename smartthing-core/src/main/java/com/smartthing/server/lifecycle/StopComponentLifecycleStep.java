package com.smartthing.server.lifecycle;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.lifecycle.ILifecycleComponent;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.ILifecycleStep;

/**
 * Implementaton of {@link ILifecycleStep} that stops a single component.
 * 
 * @author Derek
 */
public class StopComponentLifecycleStep extends ComponentOperationLifecycleStep {

    public StopComponentLifecycleStep(ILifecycleComponent owner, ILifecycleComponent component, String name) {
	super(owner, component, name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ILifecycleStep#execute(com.smartthing.
     * spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void execute(ILifecycleProgressMonitor monitor) throws SmartThingException {
	if (getComponent() != null) {
	    getComponent().lifecycleStop(monitor);
	}
    }
}
