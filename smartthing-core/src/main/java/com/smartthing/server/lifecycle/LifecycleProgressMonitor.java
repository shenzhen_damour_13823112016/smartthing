package com.smartthing.server.lifecycle;

import java.util.ArrayDeque;
import java.util.Deque;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.monitoring.IProgressErrorMessage;
import com.smartthing.spi.monitoring.IProgressMessage;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressContext;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleProgressUtils;

/**
 * Default implementation of {@link ILifecycleProgressMonitor}.
 * 
 * @author Derek
 */
public class LifecycleProgressMonitor implements ILifecycleProgressMonitor {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Stack for nested progress tracking */
    private Deque<ILifecycleProgressContext> contextStack = new ArrayDeque<ILifecycleProgressContext>();

    public LifecycleProgressMonitor(ILifecycleProgressContext initialContext) {
	contextStack.push(initialContext);
	try {
	    LifecycleProgressUtils.startProgressOperation(this, initialContext.getTaskName());
	} catch (SmartThingException e) {
	    throw new RuntimeException("Unable to create progress monitor.", e);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.monitoring.IProgressReporter#reportProgress(com.
     * sitewhere.spi.monitoring.IProgressMessage)
     */
    @Override
    public void reportProgress(IProgressMessage message) throws SmartThingException {
	LOGGER.info("[PROGRESS][" + message.getTaskName() + "]: (" + message.getProgressPercentage() + "%) "
		+ message.getMessage());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.monitoring.IProgressReporter#reportError(com.smartthing.
     * spi.monitoring.IProgressErrorMessage)
     */
    @Override
    public void reportError(IProgressErrorMessage error) throws SmartThingException {
	LOGGER.info("[ERROR][" + error.getTaskName() + "]: (" + error.getProgressPercentage() + "%) "
		+ error.getMessage() + "[" + error.getLevel().name() + "]");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor#pushContext(
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressContext)
     */
    @Override
    public void pushContext(ILifecycleProgressContext context) throws SmartThingException {
	getContextStack().push(context);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor#
     * startProgress(java.lang.String)
     */
    @Override
    public void startProgress(String operation) throws SmartThingException {
	LifecycleProgressUtils.startProgressOperation(this, operation);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor#
     * finishProgress()
     */
    @Override
    public void finishProgress() throws SmartThingException {
	LifecycleProgressUtils.finishProgressOperation(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor#popContext()
     */
    @Override
    public ILifecycleProgressContext popContext() throws SmartThingException {
	return getContextStack().pop();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor#
     * getContextStack()
     */
    public Deque<ILifecycleProgressContext> getContextStack() {
	return contextStack;
    }

    public void setContextStack(Deque<ILifecycleProgressContext> contextStack) {
	this.contextStack = contextStack;
    }
}