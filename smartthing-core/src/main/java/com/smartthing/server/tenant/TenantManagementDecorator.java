/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.server.tenant;

import com.smartthing.server.lifecycle.LifecycleComponentDecorator;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.search.user.ITenantSearchCriteria;
import com.smartthing.spi.tenant.ITenant;
import com.smartthing.spi.tenant.ITenantManagement;
import com.smartthing.spi.tenant.request.ITenantCreateRequest;

/**
 * Uses decorator pattern to allow behaviors to be injected around tenant
 * management API calls.
 * 
 * @author Derek
 */
public class TenantManagementDecorator extends LifecycleComponentDecorator implements ITenantManagement {

    /** Delegate */
    private ITenantManagement delegate;

    public TenantManagementDecorator(ITenantManagement delegate) {
	super(delegate);
	this.delegate = delegate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.tenant.ITenantManagement#createTenant(com.smartthing.spi
     * .tenant. request.ITenantCreateRequest)
     */
    @Override
    public ITenant createTenant(ITenantCreateRequest request) throws SmartThingException {
	return delegate.createTenant(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.tenant.ITenantManagement#updateTenant(java.lang.String,
     * com.smartthing.spi.tenant.request.ITenantCreateRequest)
     */
    @Override
    public ITenant updateTenant(String id, ITenantCreateRequest request) throws SmartThingException {
	return delegate.updateTenant(id, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.tenant.ITenantManagement#getTenantById(java.lang.
     * String)
     */
    @Override
    public ITenant getTenantById(String id) throws SmartThingException {
	return delegate.getTenantById(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.tenant.ITenantManagement#getTenantByAuthenticationToken
     * (java.lang .String)
     */
    @Override
    public ITenant getTenantByAuthenticationToken(String token) throws SmartThingException {
	return delegate.getTenantByAuthenticationToken(token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.tenant.ITenantManagement#listTenants(com.smartthing.spi.
     * search. user.ITenantSearchCriteria)
     */
    @Override
    public ISearchResults<ITenant> listTenants(ITenantSearchCriteria criteria) throws SmartThingException {
	return delegate.listTenants(criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.tenant.ITenantManagement#deleteTenant(java.lang.String,
     * boolean)
     */
    @Override
    public ITenant deleteTenant(String tenantId, boolean force) throws SmartThingException {
	return delegate.deleteTenant(tenantId, force);
    }
}