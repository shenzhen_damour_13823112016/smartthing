/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.server.tenant;

import java.util.concurrent.Callable;

import com.smartthing.spi.command.ICommandResponse;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.tenant.ISmartThingTenantEngine;

/**
 * Base class for commands executed on a tenant engine.
 * 
 * @author Derek
 */
public abstract class TenantEngineCommand implements Callable<ICommandResponse> {

    /** Progress monitor for commands */
    private ILifecycleProgressMonitor progressMonitor;

    /** Tenant engine */
    private ISmartThingTenantEngine engine;

    public ILifecycleProgressMonitor getProgressMonitor() {
	return progressMonitor;
    }

    public void setProgressMonitor(ILifecycleProgressMonitor progressMonitor) {
	this.progressMonitor = progressMonitor;
    }

    public ISmartThingTenantEngine getEngine() {
	return engine;
    }

    public void setEngine(ISmartThingTenantEngine engine) {
	this.engine = engine;
    }
}