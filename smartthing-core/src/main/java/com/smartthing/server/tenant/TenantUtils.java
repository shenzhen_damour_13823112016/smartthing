/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.server.tenant;

import com.smartthing.SmartThing;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.resource.IResource;
import com.smartthing.spi.server.tenant.ISmartThingTenantEngine;

/**
 * Utility class for common tenant functionality.
 * 
 * @author Derek
 */
public class TenantUtils {

    /**
     * Get resource representation of the active configuration for a tenant.
     * 
     * @param tenantId
     * @return
     * @throws SmartThingException
     */
    public static IResource getActiveTenantConfiguration(String tenantId) throws SmartThingException {
	ISmartThingTenantEngine engine =SmartThing.getServer().getTenantEngine(tenantId);
	if (engine == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidTenantEngineId, ErrorLevel.ERROR);
	}
	return engine.getTenantConfigurationResolver().getActiveTenantConfiguration();
    }

    /**
     * Get resource representation of the staged configuration for a tenant.
     * 
     * @param tenantId
     * @return
     * @throws SmartThingException
     */
    public static IResource getStagedTenantConfiguration(String tenantId) throws SmartThingException {
	ISmartThingTenantEngine engine =SmartThing.getServer().getTenantEngine(tenantId);
	if (engine == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidTenantEngineId, ErrorLevel.ERROR);
	}
	return engine.getTenantConfigurationResolver().getStagedTenantConfiguration();
    }

    /**
     * Stage a new configuration for the given tenant.
     * 
     * @param tenantId
     * @param configuration
     * @throws SmartThingException
     */
    public static void stageTenantConfiguration(String tenantId, String configuration) throws SmartThingException {
	ISmartThingTenantEngine engine =SmartThing.getServer().getTenantEngine(tenantId);
	if (engine == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidTenantEngineId, ErrorLevel.ERROR);
	}
	engine.getTenantConfigurationResolver().stageTenantConfiguration(configuration.getBytes());
    }
}