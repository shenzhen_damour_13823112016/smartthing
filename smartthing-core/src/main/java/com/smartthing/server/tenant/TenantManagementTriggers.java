/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.server.tenant;

import com.smartthing.SmartThing;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.tenant.ITenant;
import com.smartthing.spi.tenant.ITenantManagement;
import com.smartthing.spi.tenant.request.ITenantCreateRequest;

/**
 * System behaviors that are triggered by calls to the tenant management APIs.
 * 
 * @author Derek
 */
public class TenantManagementTriggers extends TenantManagementDecorator {

    public TenantManagementTriggers(ITenantManagement delegate) {
	super(delegate);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.tenant.TenantManagementDecorator#updateTenant(java.
     * lang. String, com.smartthing.spi.tenant.request.ITenantCreateRequest)
     */
    @Override
    public ITenant updateTenant(String id, ITenantCreateRequest request) throws SmartThingException {
	ITenant updated = super.updateTenant(id, request);
	SmartThing.getServer().onTenantUpdated(updated);
	return updated;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.tenant.TenantManagementDecorator#deleteTenant(java.
     * lang.String, boolean)
     */
    @Override
    public ITenant deleteTenant(String tenantId, boolean force) throws SmartThingException {
	ITenant deleted = super.deleteTenant(tenantId, force);
	if (force) {
	   SmartThing.getServer().getRuntimeResourceManager().deleteTenantResources(tenantId);
	}
	SmartThing.getServer().onTenantDeleted(deleted);
	return deleted;
    }
}