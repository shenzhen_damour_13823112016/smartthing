/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.server.user;

import java.util.List;

import com.smartthing.server.lifecycle.LifecycleComponentDecorator;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.user.IGrantedAuthority;
import com.smartthing.spi.user.IGrantedAuthoritySearchCriteria;
import com.smartthing.spi.user.IUser;
import com.smartthing.spi.user.IUserManagement;
import com.smartthing.spi.user.IUserSearchCriteria;
import com.smartthing.spi.user.request.IGrantedAuthorityCreateRequest;
import com.smartthing.spi.user.request.IUserCreateRequest;

/**
 * Uses decorator pattern to allow behaviors to be injected around user
 * management API calls.
 * 
 * @author Derek
 */
public class UserManagementDecorator extends LifecycleComponentDecorator implements IUserManagement {

    /** Delegate */
    private IUserManagement delegate;

    public UserManagementDecorator(IUserManagement delegate) {
	super(delegate);
	this.delegate = delegate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#createUser(com.smartthing.spi.user.
     * request.IUserCreateRequest, boolean)
     */
    @Override
    public IUser createUser(IUserCreateRequest request, boolean encodePassword) throws SmartThingException {
	return delegate.createUser(request, encodePassword);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#importUser(com.smartthing.spi.user.
     * IUser, boolean)
     */
    @Override
    public IUser importUser(IUser user, boolean overwrite) throws SmartThingException {
	return delegate.importUser(user, overwrite);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#authenticate(java.lang.String,
     * java.lang.String, boolean)
     */
    @Override
    public IUser authenticate(String username, String password, boolean updateLastLogin) throws SmartThingException {
	return delegate.authenticate(username, password, updateLastLogin);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.user.IUserManagement#updateUser(java.lang.String,
     * com.smartthing.spi.user.request.IUserCreateRequest, boolean)
     */
    @Override
    public IUser updateUser(String username, IUserCreateRequest request, boolean encodePassword)
	    throws SmartThingException {
	return delegate.updateUser(username, request, encodePassword);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.user.IUserManagement#getUserByUsername(java.lang.
     * String)
     */
    @Override
    public IUser getUserByUsername(String username) throws SmartThingException {
	return delegate.getUserByUsername(username);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#getGrantedAuthorities(java.lang.
     * String)
     */
    @Override
    public List<IGrantedAuthority> getGrantedAuthorities(String username) throws SmartThingException {
	return delegate.getGrantedAuthorities(username);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#addGrantedAuthorities(java.lang.
     * String, java.util.List)
     */
    @Override
    public List<IGrantedAuthority> addGrantedAuthorities(String username, List<String> authorities)
	    throws SmartThingException {
	return delegate.addGrantedAuthorities(username, authorities);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#removeGrantedAuthorities(java.lang
     * .String, java.util.List)
     */
    @Override
    public List<IGrantedAuthority> removeGrantedAuthorities(String username, List<String> authorities)
	    throws SmartThingException {
	return delegate.removeGrantedAuthorities(username, authorities);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#listUsers(com.smartthing.spi.user.
     * IUserSearchCriteria)
     */
    @Override
    public List<IUser> listUsers(IUserSearchCriteria criteria) throws SmartThingException {
	return delegate.listUsers(criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.user.IUserManagement#deleteUser(java.lang.String,
     * boolean)
     */
    @Override
    public IUser deleteUser(String username, boolean force) throws SmartThingException {
	return delegate.deleteUser(username, force);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.user.IUserManagement#createGrantedAuthority(com.
     * sitewhere.spi. user.request.IGrantedAuthorityCreateRequest)
     */
    @Override
    public IGrantedAuthority createGrantedAuthority(IGrantedAuthorityCreateRequest request) throws SmartThingException {
	return delegate.createGrantedAuthority(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#getGrantedAuthorityByName(java.
     * lang.String)
     */
    @Override
    public IGrantedAuthority getGrantedAuthorityByName(String name) throws SmartThingException {
	return delegate.getGrantedAuthorityByName(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#updateGrantedAuthority(java.lang.
     * String, com.smartthing.spi.user.request.IGrantedAuthorityCreateRequest)
     */
    @Override
    public IGrantedAuthority updateGrantedAuthority(String name, IGrantedAuthorityCreateRequest request)
	    throws SmartThingException {
	return delegate.updateGrantedAuthority(name, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.user.IUserManagement#listGrantedAuthorities(com.
     * sitewhere.spi. user.IGrantedAuthoritySearchCriteria)
     */
    @Override
    public List<IGrantedAuthority> listGrantedAuthorities(IGrantedAuthoritySearchCriteria criteria)
	    throws SmartThingException {
	return delegate.listGrantedAuthorities(criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#deleteGrantedAuthority(java.lang.
     * String)
     */
    @Override
    public void deleteGrantedAuthority(String authority) throws SmartThingException {
	delegate.deleteGrantedAuthority(authority);
    }
}