package com.smartthing.server.resource;

import java.io.File;

import com.smartthing.server.SmartThingServer;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;

/**
 * Extends {@link FileSystemResourceManager} to set root of resource tree based
 * on the SiteWhere home ('smartthing.home') environment variable.
 * 
 * @author Derek
 */
public class SmartThingHomeResourceManager extends FileSystemResourceManager {

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.server.resource.FileSystemResourceManager#start(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	setRootFolder(calculateConfigurationPath());
	super.start(monitor);
    }

    /**
     * Calculate the system configuration path relative to the SiteWhere home
     * environment variable.
     * 
     * @return
     * @throws SmartThingException
     */
    public static File calculateConfigurationPath() throws SmartThingException {
	File swFolder = SmartThingServer.getSiteWhereHomeFolder();
	File confDir = new File(swFolder, "conf");
	if (!confDir.exists()) {
	    throw new SmartThingException(
		    "'SiteWhere configuration folder does not exist. Looking in: " + confDir.getAbsolutePath());
	}
	return confDir;
    }
}