/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.SmartThing.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.server;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.smartthing.SmartThing;
import com.smartthing.common.MarshalUtils;
import com.smartthing.configuration.ConfigurationUtils;
import com.smartthing.configuration.ResourceManagerGlobalConfigurationResolver;
import com.smartthing.core.Boilerplate;
import com.smartthing.groovy.configuration.GroovyConfiguration;
import com.smartthing.hazelcast.HazelcastConfiguration;
import com.smartthing.rest.model.search.tenant.TenantSearchCriteria;
import com.smartthing.rest.model.server.SmartThingServerRuntime;
import com.smartthing.rest.model.server.SmartThingServerRuntime.GeneralInformation;
import com.smartthing.rest.model.server.SmartThingServerRuntime.JavaInformation;
import com.smartthing.rest.model.server.SmartThingServerState;
import com.smartthing.rest.model.server.TenantPersistentState;
import com.smartthing.rest.model.user.User;
import com.smartthing.security.SitewhereAuthentication;
import com.smartthing.security.SitewhereUserDetails;
import com.smartthing.server.debug.NullTracer;
import com.smartthing.server.jvm.JvmHistoryMonitor;
import com.smartthing.server.lifecycle.CompositeLifecycleStep;
import com.smartthing.server.lifecycle.LifecycleComponent;
import com.smartthing.server.lifecycle.LifecycleProgressContext;
import com.smartthing.server.lifecycle.LifecycleProgressMonitor;
import com.smartthing.server.lifecycle.SimpleLifecycleStep;
import com.smartthing.server.lifecycle.StartComponentLifecycleStep;
import com.smartthing.server.lifecycle.StopComponentLifecycleStep;
import com.smartthing.server.resource.SmartThingHomeResourceManager;
import com.smartthing.server.tenant.TenantManagementTriggers;
import com.smartthing.server.tenant.TenantTemplateManager;
import com.smartthing.spi.ServerStartupException;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.asset.IAssetManagement;
import com.smartthing.spi.asset.IAssetModuleManager;
import com.smartthing.spi.configuration.IGlobalConfigurationResolver;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.IDeviceManagementCacheProvider;
import com.smartthing.spi.device.communication.IDeviceCommunication;
import com.smartthing.spi.device.event.IDeviceEventManagement;
import com.smartthing.spi.device.event.IEventProcessing;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.resource.IResource;
import com.smartthing.spi.resource.IResourceManager;
import com.smartthing.spi.scheduling.IScheduleManagement;
import com.smartthing.spi.scheduling.IScheduleManager;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.search.external.ISearchProviderManager;
import com.smartthing.spi.server.IBackwardCompatibilityService;
import com.smartthing.spi.server.ISmartThingServer;
import com.smartthing.spi.server.ISmartThingServerRuntime;
import com.smartthing.spi.server.ISmartThingServerState;
import com.smartthing.spi.server.debug.ITracer;
import com.smartthing.spi.server.groovy.IGroovyConfiguration;
import com.smartthing.spi.server.groovy.ITenantGroovyConfiguration;
import com.smartthing.spi.server.hazelcast.IHazelcastConfiguration;
import com.smartthing.spi.server.lifecycle.ICompositeLifecycleStep;
import com.smartthing.spi.server.lifecycle.IDiscoverableTenantLifecycleComponent;
import com.smartthing.spi.server.lifecycle.ILifecycleComponent;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;
import com.smartthing.spi.server.lifecycle.LifecycleStatus;
import com.smartthing.spi.server.tenant.ISmartThingTenantEngine;
import com.smartthing.spi.server.tenant.ITenantModelInitializer;
import com.smartthing.spi.server.tenant.ITenantPersistentState;
import com.smartthing.spi.server.tenant.ITenantTemplateManager;
import com.smartthing.spi.server.user.IUserModelInitializer;
import com.smartthing.spi.system.IVersion;
import com.smartthing.spi.system.IVersionChecker;
import com.smartthing.spi.tenant.ITenant;
import com.smartthing.spi.tenant.ITenantManagement;
import com.smartthing.spi.user.IGrantedAuthority;
import com.smartthing.spi.user.IUserManagement;

/**
 * Implementation of {@link ISmartThingServer} for community edition.
 * 
 * @author Derek Adams
 */
public class SmartThingServer extends LifecycleComponent implements ISmartThingServer {

    /** Number of threads in pool for starting tenants */
    private static final int TENANT_OPERATION_PARALLELISM = 5;

    /** Private logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Spring context for server */
    public static ApplicationContext SERVER_SPRING_CONTEXT;

    /** Contains version information */
    private IVersion version;

    /** Version checker implementation */
    private IVersionChecker versionChecker;

    /** Persistent server state information */
    private ISmartThingServerState serverState;

    /** Contains runtime information about the server */
    private ISmartThingServerRuntime serverRuntime;

    /** Server startup error */
    private ServerStartupException serverStartupError;

    /** Provides hierarchical tracing for debugging */
    protected ITracer tracer = new NullTracer();

    /** Hazelcast configuration for this node */
    protected IHazelcastConfiguration hazelcastConfiguration;

    /** Groovy configuration for this node */
    protected IGroovyConfiguration groovyConfiguration;

    /** Bootstrap resource manager implementation */
    protected IResourceManager bootstrapResourceManager;

    /** Runtime resource manager implementation */
    protected IResourceManager runtimeResourceManager;

    /** Allows Spring configuration to be resolved */
    protected IGlobalConfigurationResolver configurationResolver;

    /** Tenant template manager implementation */
    protected ITenantTemplateManager tenantTemplateManager;

    /** Interface to user management implementation */
    protected IUserManagement userManagement;

    /** Interface to tenant management implementation */
    protected ITenantManagement tenantManagement;

    /** Components registered to participate in SiteWhere server lifecycle */
    private List<ILifecycleComponent> registeredLifecycleComponents = new ArrayList<ILifecycleComponent>();

    /** Map of component ids to lifecycle components */
    private Map<String, ILifecycleComponent> lifecycleComponentsById = new HashMap<String, ILifecycleComponent>();

    /** Map of tenants by authentication token */
    private Map<String, ITenant> tenantsByAuthToken = new ConcurrentHashMap<String, ITenant>();

    /** Map of tenant engines by tenant id */
    private Map<String, ISmartThingTenantEngine> tenantEnginesById = new ConcurrentHashMap<String, ISmartThingTenantEngine>();

    /** Metric regsitry */
    private MetricRegistry metricRegistry = new MetricRegistry();

    /** Health check registry */
    private HealthCheckRegistry healthCheckRegistry = new HealthCheckRegistry();

    /** Timestamp when server was started */
    private Long uptime;

    /** Records historical values for JVM */
    private JvmHistoryMonitor jvmHistory = new JvmHistoryMonitor(this);

    /** Thread for executing JVM history monitor */
    private ExecutorService executor;

    /** Thread pool for starting tenants in parallel */
    private ExecutorService tenantOperations;

    /** Supports migrating old server version to new format */
    private IBackwardCompatibilityService backwardCompatibilityService = new BackwardCompatibilityService();

    public SmartThingServer() {
	super(LifecycleComponentType.System);

	// Set version information.
	this.version = SmartThing.getVersion();
    }

    /**
     * Get Spring application context for Atlas server objects.
     * 
     * @return
     */
    public static ApplicationContext getServerSpringContext() {
	return SERVER_SPRING_CONTEXT;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getVersion()
     */
    public IVersion getVersion() {
	return version;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getConfigurationParserClassname ()
     */
    @Override
    public String getConfigurationParserClassname() {
	return "com.smartthing.spring.handler.ConfigurationParser";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#
     * getTenantConfigurationParserClassname()
     */
    @Override
    public String getTenantConfigurationParserClassname() {
	return "com.smartthing.spring.handler.TenantConfigurationParser";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getServerState()
     */
    @Override
    public ISmartThingServerState getServerState() {
	return serverState;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getServerState(boolean)
     */
    public ISmartThingServerRuntime getServerRuntimeInformation(boolean includeHistorical) throws SmartThingException {
	this.serverRuntime = computeServerRuntime(includeHistorical);
	return serverRuntime;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getServerStartupError()
     */
    public ServerStartupException getServerStartupError() {
	return serverStartupError;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#setServerStartupError(com.
     * SmartThing.spi .ServerStartupException)
     */
    public void setServerStartupError(ServerStartupException e) {
	this.serverStartupError = e;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getTracer()
     */
    public ITracer getTracer() {
	return tracer;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getHazelcastConfiguration()
     */
    @Override
    public IHazelcastConfiguration getHazelcastConfiguration() {
	return hazelcastConfiguration;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getGroovyConfiguration()
     */
    @Override
    public IGroovyConfiguration getGroovyConfiguration() {
	return groovyConfiguration;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getBootstrapResourceManager()
     */
    @Override
    public IResourceManager getBootstrapResourceManager() {
	return bootstrapResourceManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getRuntimeResourceManager()
     */
    @Override
    public IResourceManager getRuntimeResourceManager() {
	return runtimeResourceManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getConfigurationResolver()
     */
    public IGlobalConfigurationResolver getConfigurationResolver() {
	return configurationResolver;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getTenantTemplateManager()
     */
    @Override
    public ITenantTemplateManager getTenantTemplateManager() {
	return tenantTemplateManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getAuthorizedTenants(java.lang.
     * String, boolean)
     */
    @Override
    public List<ITenant> getAuthorizedTenants(String userId, boolean requireStarted) throws SmartThingException {
	ISearchResults<ITenant> tenants = SmartThing.getServer().getTenantManagement()
		.listTenants(new TenantSearchCriteria(1, 0));
	List<ITenant> matches = new ArrayList<ITenant>();
	for (ITenant tenant : tenants.getResults()) {
	    if (tenant.getAuthorizedUserIds().contains(userId)) {
		if (requireStarted) {
		    ISmartThingTenantEngine engine = getTenantEngine(tenant.getId());
		    if ((engine == null) || (engine.getLifecycleStatus() != LifecycleStatus.Started)) {
			continue;
		    }
		}
		matches.add(tenant);
	    }
	}
	return matches;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getTenantEnginesById()
     */
    @Override
    public Map<String, ISmartThingTenantEngine> getTenantEnginesById() {
	return tenantEnginesById;
    }

    public void setTenantEnginesById(Map<String, ISmartThingTenantEngine> tenantEnginesById) {
	this.tenantEnginesById = tenantEnginesById;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getTenantEngine(java.lang.
     * String)
     */
    @Override
    public ISmartThingTenantEngine getTenantEngine(String tenantId) throws SmartThingException {
	ISmartThingTenantEngine engine = getTenantEnginesById().get(tenantId);
	if (engine == null) {
	    ITenant tenant = getTenantManagement().getTenantById(tenantId);
	    if (tenant == null) {
		return null;
	    }
	    engine = initializeTenantEngine(tenant);
	}
	return engine;
    }

    /**
     * If we have an auth token entry for the tenant, remove it.
     * 
     * @param tenant
     */
    protected void removeTenantForAuthToken(ITenant tenant) {
	for (ITenant current : tenantsByAuthToken.values()) {
	    if (current.getId().equals(tenant.getId())) {
		tenantsByAuthToken.remove(current.getId());
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#onTenantUpdated(com.smartthing.
     * spi.tenant.ITenant)
     */
    @Override
    public void onTenantUpdated(ITenant tenant) throws SmartThingException {
	// Account for updated authentication token.
	removeTenantForAuthToken(tenant);
	tenantsByAuthToken.put(tenant.getAuthenticationToken(), tenant);

	// Update tenant information in tenant engine.
	ISmartThingTenantEngine engine = getTenantEnginesById().get(tenant.getId());
	if (engine != null) {
	    engine.setTenant(tenant);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#onTenantDeleted(com.smartthing.
     * spi.tenant.ITenant)
     */
    @Override
    public void onTenantDeleted(ITenant tenant) throws SmartThingException {
	// Remove from auth token cache.
	removeTenantForAuthToken(tenant);

	// Shutdown tenant engine and remove it.
	ISmartThingTenantEngine engine = getTenantEnginesById().get(tenant.getId());
	if (engine != null) {
	    if (engine.getLifecycleStatus() == LifecycleStatus.Started) {
		stopAndTerminateTenantEngine(engine, new LifecycleProgressMonitor(
			new LifecycleProgressContext(1, "Shut down deleted tenant engine.")));
	    }
	    getTenantEnginesById().remove(tenant.getId());
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getUserManagement()
     */
    public IUserManagement getUserManagement() {
	return userManagement;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getTenantManagement()
     */
    @Override
    public ITenantManagement getTenantManagement() {
	return tenantManagement;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getDeviceManagement(com.
     * SmartThing.spi .user.ITenant)
     */
    @Override
    public IDeviceManagement getDeviceManagement(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = assureTenantEngine(tenant);
	return engine.getDeviceManagement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getDeviceEventManagement(com.
     * sitewhere .spi.user.ITenant)
     */
    @Override
    public IDeviceEventManagement getDeviceEventManagement(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = assureTenantEngine(tenant);
	return engine.getDeviceEventManagement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#
     * getDeviceManagementCacheProvider(com. SmartThing.spi.user.ITenant)
     */
    @Override
    public IDeviceManagementCacheProvider getDeviceManagementCacheProvider(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = assureTenantEngine(tenant);
	return engine.getDeviceManagementCacheProvider();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getAssetManagement(com.
     * SmartThing.spi. user.ITenant)
     */
    @Override
    public IAssetManagement getAssetManagement(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = assureTenantEngine(tenant);
	return engine.getAssetManagement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getScheduleManagement(com.
     * SmartThing.spi .user.ITenant)
     */
    @Override
    public IScheduleManagement getScheduleManagement(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = assureTenantEngine(tenant);
	return engine.getScheduleManagement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getDeviceCommunication(com.
     * SmartThing. spi.user.ITenant)
     */
    @Override
    public IDeviceCommunication getDeviceCommunication(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = assureTenantEngine(tenant);
	return engine.getDeviceCommunication();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getEventProcessing(com.
     * SmartThing.spi. user.ITenant)
     */
    @Override
    public IEventProcessing getEventProcessing(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = assureTenantEngine(tenant);
	return engine.getEventProcessing();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getAssetModuleManager(com.
     * SmartThing.spi .user.ITenant)
     */
    @Override
    public IAssetModuleManager getAssetModuleManager(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = assureTenantEngine(tenant);
	return engine.getAssetModuleManager();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getSearchProviderManager(com.
     * sitewhere .spi.user.ITenant)
     */
    @Override
    public ISearchProviderManager getSearchProviderManager(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = assureTenantEngine(tenant);
	return engine.getSearchProviderManager();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getScheduleManager(com.
     * SmartThing.spi. user.ITenant)
     */
    @Override
    public IScheduleManager getScheduleManager(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = assureTenantEngine(tenant);
	return engine.getScheduleManager();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getTenantGroovyConfiguration(
     * com.smartthing.spi.tenant.ITenant)
     */
    @Override
    public ITenantGroovyConfiguration getTenantGroovyConfiguration(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = assureTenantEngine(tenant);
	return engine.getGroovyConfiguration();
    }

    /**
     * Get tenant engine for tenant. Throw an exception if not found.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    protected ISmartThingTenantEngine assureTenantEngine(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = getTenantEnginesById().get(tenant.getId());
	if (engine == null) {
	    throw new SmartThingException("No engine registered for tenant.");
	}
	return engine;
    }

    /**
     * Get a tenant engine by authentication token. Throw and exception if not
     * found.
     * 
     * @param tenantAuthToken
     * @return
     * @throws SmartThingException
     */
    protected ISmartThingTenantEngine assureTenantEngine(String tenantAuthToken) throws SmartThingException {
	ITenant tenant = tenantsByAuthToken.get(tenantAuthToken);
	if (tenant == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidTenantAuthToken, ErrorLevel.ERROR);
	}
	ISmartThingTenantEngine engine = getTenantEnginesById().get(tenant.getId());
	if (engine == null) {
	    throw new SmartThingException("Tenant found for auth token, but no engine registered for tenant.");
	}
	return engine;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.ISmartThingServer#getTenantByAuthToken(java.lang.
     * String)
     */
    public ITenant getTenantByAuthToken(String authToken) throws SmartThingException {
	return tenantsByAuthToken.get(authToken);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getMetricRegistry()
     */
    public MetricRegistry getMetricRegistry() {
	return metricRegistry;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#getHealthCheckRegistry()
     */
    public HealthCheckRegistry getHealthCheckRegistry() {
	return healthCheckRegistry;
    }

    /**
     * Returns a fake account used for operations on the data model done by the
     * system.
     * 
     * @return
     * @throws SmartThingException
     */
    public static SitewhereAuthentication getSystemAuthentication() throws SmartThingException {
	User fake = new User();
	fake.setUsername("system");
	SitewhereUserDetails details = new SitewhereUserDetails(fake, new ArrayList<IGrantedAuthority>());
	SitewhereAuthentication auth = new SitewhereAuthentication(details, null);
	return auth;
    }

    /**
     * Access folder pointed to by SiteWhere home environment variable.
     * 
     * @return
     * @throws SmartThingException
     */
    public static File getSiteWhereHomeFolder() throws SmartThingException {
	String sitewhere = System.getProperty(ISmartThingServer.ENV_SMARTTHING_HOME);
	if (sitewhere == null) {
	    // Support fallback environment variable name.
	    sitewhere = System.getProperty("SMARTTHING_HOME");
	    if (sitewhere == null) {
		throw new SmartThingException(
			"SmartThing home environment variable (" + ISmartThingServer.ENV_SMARTTHING_HOME + ") not set.");
	    }
	}
	File swFolder = new File(sitewhere);
	if (!swFolder.exists()) {
	    throw new SmartThingException(
		    "SmartThing home folder does not exist. Looking in: " + swFolder.getAbsolutePath());
	}
	return swFolder;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.ISmartThingServer#initialize(com.smartthing.spi.
     * server.lifecycle.ILifecycleProgressMonitor)
     */
    public void initialize(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Start logging metrics to the console.
	logMetricsToConsole();

	// Handle backward compatibility.
	backwardCompatibilityService.beforeServerInitialize(monitor);

	// Initialize bootstrap resource manager.
	initializeBootstrapResourceManager();
	LOGGER.info("Bootstrap resources loading from: " + getBootstrapResourceManager().getClass().getCanonicalName());
	getBootstrapResourceManager().start(monitor);

	// Initialize persistent state.
	initializeServerState();

	// Initialize the Hazelcast instance.
	initializeHazelcastConfiguration();
	getHazelcastConfiguration().start(monitor);

	// Initialize the Groovy configuration.
	initializeGroovyConfiguration();

	// Initialize Spring.
	initializeSpringContext();

	// Initialize discoverable beans.
	initializeDiscoverableBeans(monitor);

	// Initialize version checker.
	initializeVersionChecker();

	// Initialize tracer.
	initializeTracer();

	// Initialize management implementations.
	initializeManagementImplementations();

	// Initialize runtime resource manager.
	initializeRuntimeResourceManager();
	LOGGER.info("Runtime resources loading from: " + getRuntimeResourceManager().getClass().getCanonicalName());
	getRuntimeResourceManager().start(monitor);

	// Initialize tenant template manager.
	initializeTenantTemplateManager();

	// Show banner containing server information.
	showServerBanner();
    }

    /**
     * Displays the server information banner in the log.
     */
    protected void showServerBanner() {
	String os = System.getProperty("os.name") + " (" + System.getProperty("os.version") + ")";
	String java = System.getProperty("java.vendor") + " (" + System.getProperty("java.version") + ")";

	// Print version information.
	List<String> messages = new ArrayList<String>();
	messages.add("SmartThing Server " + version.getEdition());
	messages.add("Version: " + version.getVersionIdentifier() + "." + version.getBuildTimestamp());
	messages.add("Node id: " + serverState.getNodeId());
	addBannerMessages(messages);
	messages.add("Operating System: " + os);
	messages.add("Java Runtime: " + java);
	messages.add("");
	messages.add("Copyright (c) 2009-2016 SmartThing, LLC");
	String message = Boilerplate.boilerplate(messages, "*");
	LOGGER.info("\n" + message + "\n");
    }

    /**
     * Allows subclasses to add their own banner messages.
     * 
     * @param messages
     */
    protected void addBannerMessages(List<String> messages) {
	messages.add("");
    }

    /**
     * Initialize the server state information.
     * 
     * @throws SmartThingException
     */
    protected void initializeServerState() throws SmartThingException {
	IResource stateResource = getConfigurationResolver().resolveServerState(getVersion());
	SmartThingServerState state = null;
	if (stateResource == null) {
	    state = new SmartThingServerState();
	    state.setNodeId(UUID.randomUUID().toString());
	    getConfigurationResolver().storeServerState(version, MarshalUtils.marshalJson(state));
	} else {
	    state = MarshalUtils.unmarshalJson(stateResource.getContent(), SmartThingServerState.class);
	}
	this.serverState = state;
    }

    /**
     * Initialize Hazelcast configuration.
     * 
     * @throws SmartThingException
     */
    protected void initializeHazelcastConfiguration() throws SmartThingException {
	IResource resource = getBootstrapResourceManager().getGlobalResource(HazelcastConfiguration.CONFIG_FILE_NAME);
	if (resource == null) {
	    throw new SmartThingException(
		    "Base Hazelcast configuration resource not found: " + HazelcastConfiguration.CONFIG_FILE_NAME);
	}
	this.hazelcastConfiguration = new HazelcastConfiguration(resource);
    }

    /**
     * Initialize the Groovy configuration.
     * 
     * @throws SmartThingException
     */
    protected void initializeGroovyConfiguration() throws SmartThingException {
	this.groovyConfiguration = new GroovyConfiguration();
    }

    /**
     * Verifies and loads the Spring configuration file.
     * 
     * @throws SmartThingException
     */
    protected void initializeSpringContext() throws SmartThingException {
	IResource global = getConfigurationResolver().getGlobalConfiguration(getVersion());
	SERVER_SPRING_CONTEXT = ConfigurationUtils.buildGlobalContext(global, getVersion());
    }

    /**
     * Initialize beans marked with {@link IDiscoverableTenantLifecycleComponent}
     * interface and add them as registered components.
     * 
     * @param monitor
     * @throws SmartThingException
     */
    protected void initializeDiscoverableBeans(ILifecycleProgressMonitor monitor) throws SmartThingException {
	Map<String, IDiscoverableTenantLifecycleComponent> components = SERVER_SPRING_CONTEXT
		.getBeansOfType(IDiscoverableTenantLifecycleComponent.class);
	getRegisteredLifecycleComponents().clear();

	LOGGER.info("Registering " + components.size() + " discoverable components.");
	for (IDiscoverableTenantLifecycleComponent component : components.values()) {
	    LOGGER.info("Registering " + component.getComponentName() + ".");
	    initializeNestedComponent(component, monitor);
	    getRegisteredLifecycleComponents().add(component);
	}
    }

    /**
     * Initialize debug tracing implementation.
     * 
     * @throws SmartThingException
     */
    protected void initializeVersionChecker() throws SmartThingException {
	try {
	    this.versionChecker = (IVersionChecker) SERVER_SPRING_CONTEXT
		    .getBean(SmartThingServerBeans.BEAN_VERSION_CHECK);
	} catch (NoSuchBeanDefinitionException e) {
	    LOGGER.info("Version checking not enabled.");
	}
    }

    /**
     * Initialize debug tracing implementation.
     * 
     * @throws SmartThingException
     */
    protected void initializeTracer() throws SmartThingException {
	try {
	    this.tracer = (ITracer) SERVER_SPRING_CONTEXT.getBean(SmartThingServerBeans.BEAN_TRACER);
	    LOGGER.info("Tracer implementation using: " + tracer.getClass().getName());
	} catch (NoSuchBeanDefinitionException e) {
	    LOGGER.info("No Tracer implementation configured.");
	    this.tracer = new NullTracer();
	}
    }

    /**
     * Initialize the tenant template manager.
     * 
     * @throws SmartThingException
     */
    protected void initializeTenantTemplateManager() throws SmartThingException {
	this.tenantTemplateManager = new TenantTemplateManager(getRuntimeResourceManager());
    }

    /**
     * Initialize management implementations.
     * 
     * @throws SmartThingException
     */
    protected void initializeManagementImplementations() throws SmartThingException {

	// Initialize user management.
	initializeUserManagement();

	// Initialize tenant management.
	initializeTenantManagement();
    }

    /**
     * Verify and initialize user management implementation.
     * 
     * @throws SmartThingException
     */
    protected void initializeUserManagement() throws SmartThingException {
	try {
	    this.userManagement = (IUserManagement) SERVER_SPRING_CONTEXT
		    .getBean(SmartThingServerBeans.BEAN_USER_MANAGEMENT);
	} catch (NoSuchBeanDefinitionException e) {
	    throw new SmartThingException("No user management implementation configured.");
	}
    }

    /**
     * Verify and initialize tenant management implementation.
     * 
     * @throws SmartThingException
     */
    protected void initializeTenantManagement() throws SmartThingException {
	try {
	    ITenantManagement implementation = (ITenantManagement) SERVER_SPRING_CONTEXT
		    .getBean(SmartThingServerBeans.BEAN_TENANT_MANAGEMENT);
	    this.tenantManagement = new TenantManagementTriggers(implementation);
	} catch (NoSuchBeanDefinitionException e) {
	    throw new SmartThingException("No user management implementation configured.");
	}
    }

    /**
     * Initialize bootstrap resource manager.
     * 
     * @throws SmartThingException
     */
    protected void initializeBootstrapResourceManager() throws SmartThingException {
	this.bootstrapResourceManager = new SmartThingHomeResourceManager();
	this.configurationResolver = new ResourceManagerGlobalConfigurationResolver(bootstrapResourceManager);
    }

    /**
     * Initialize runtime resource manager and swap configuration resolver to use
     * it.
     * 
     * @throws SmartThingException
     */
    protected void initializeRuntimeResourceManager() throws SmartThingException {
	this.runtimeResourceManager = new SmartThingHomeResourceManager();
	this.configurationResolver = new ResourceManagerGlobalConfigurationResolver(runtimeResourceManager);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
     * .server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Organizes steps for starting server.
	ICompositeLifecycleStep start = new CompositeLifecycleStep("Started Server");

	// Initialize tenant engines.
	start.addStep(new SimpleLifecycleStep("Prepared server") {

	    @Override
	    public void execute(ILifecycleProgressMonitor monitor) throws SmartThingException {
		// Handle backward compatibility.
		backwardCompatibilityService.beforeServerStart(monitor);

		// Clear the component list.
		getLifecycleComponents().clear();
	    }
	});

	// Start base service services.
	startBaseServices(start);

	// Start management implementations.
	startManagementImplementations(start);

	// Initialize tenant engines.
	initializeTenantEngines(start);

	// Start tenant engines.
	startTenantEngines(start);

	// Finish operations for starting server.
	start.addStep(new SimpleLifecycleStep("Finished server startup") {

	    @Override
	    public void execute(ILifecycleProgressMonitor monitor) throws SmartThingException {
		// Force refresh on components-by-id map.
		lifecycleComponentsById = buildComponentMap();

		// Set uptime timestamp.
		SmartThingServer.this.uptime = System.currentTimeMillis();

		// Schedule JVM monitor.
		executor = Executors.newFixedThreadPool(2);
		executor.execute(jvmHistory);

		// If version checker configured, perform in a separate thread.
		if (versionChecker != null) {
		    executor.execute(versionChecker);
		}

		// Handle backward compatibility.
		backwardCompatibilityService.afterServerStart(monitor);
	    }
	});

	// Start the operation and report progress.
	start.execute(monitor);
    }

    /**
     * Log metrics to console.
     */
    protected void logMetricsToConsole() {
	String useMetricsStr = System.getProperty("smartthing.metrics");
	if ((useMetricsStr != null) && ("true".equals(useMetricsStr))) {
	    Slf4jReporter reporter = Slf4jReporter.forRegistry(SmartThing.getServer().getMetricRegistry())
		    .convertRatesTo(TimeUnit.SECONDS).convertDurationsTo(TimeUnit.MILLISECONDS).build();
	    reporter.start(10, TimeUnit.SECONDS);
	}
    }

    /**
     * Start basic services required by other components.
     * 
     * @param start
     * @throws SmartThingException
     */
    protected void startBaseServices(ICompositeLifecycleStep start) throws SmartThingException {
	// Organizes steps for starting base services.
	ICompositeLifecycleStep base = new CompositeLifecycleStep("Started Base Services");

	// Start the Groovy configuration.
	base.addStep(new StartComponentLifecycleStep(this, getGroovyConfiguration(), "Started Groovy scripting engine",
		"Groovy startup failed.", true));

	// Start all lifecycle components.
	for (ILifecycleComponent component : getRegisteredLifecycleComponents()) {
	    base.addStep(new StartComponentLifecycleStep(this, component, "Started " + component.getComponentName(),
		    component.getComponentName() + " startup failed.", true));
	}

	// Start the tenant template manager.
	base.addStep(new StartComponentLifecycleStep(this, getTenantTemplateManager(),
		"Started tenant template manager", "Tenant template manager startup failed.", true));

	start.addStep(base);
    }

    /**
     * Add management implementation startup to composite operation.
     * 
     * @param start
     * @throws SmartThingException
     */
    protected void startManagementImplementations(ICompositeLifecycleStep start) throws SmartThingException {
	// Organizes steps for starting management implementations.
	ICompositeLifecycleStep mgmt = new CompositeLifecycleStep("Started Management Implementations");

	// Start user management.
	mgmt.addStep(new StartComponentLifecycleStep(this, getUserManagement(),
		"Started user management implementation", "User management startup failed.", true));

	// Start tenant management.
	mgmt.addStep(new StartComponentLifecycleStep(this, getTenantManagement(),
		"Started tenant management implementation", "Tenant management startup failed", true));

	// Populate user data if requested.
	mgmt.addStep(new SimpleLifecycleStep("Verified user model") {

	    @Override
	    public void execute(ILifecycleProgressMonitor monitor) throws SmartThingException {
		verifyUserModel();
	    }
	});

	// Populate tenant data if requested.
	mgmt.addStep(new SimpleLifecycleStep("Verified tenant model") {

	    @Override
	    public void execute(ILifecycleProgressMonitor monitor) throws SmartThingException {
		verifyTenantModel();
	    }
	});

	start.addStep(mgmt);
    }

    /**
     * Initialize tenant engines.
     * 
     * @param start
     * @throws SmartThingException
     */
    protected void initializeTenantEngines(ICompositeLifecycleStep start) throws SmartThingException {
	// Initialize tenant engines.
	start.addStep(new SimpleLifecycleStep("Initialized tenant engines") {

	    @Override
	    public void execute(ILifecycleProgressMonitor monitor) throws SmartThingException {
		// Create thread pool for initializing tenants in parallel.
		if (tenantOperations != null) {
		    tenantOperations.shutdownNow();
		}
		tenantOperations = Executors.newFixedThreadPool(TENANT_OPERATION_PARALLELISM,
			new TenantOperationsThreadFactory());

		TenantSearchCriteria criteria = new TenantSearchCriteria(1, 0);
		ISearchResults<ITenant> tenants = getTenantManagement().listTenants(criteria);
		LOGGER.info("About to initialize " + tenants.getNumResults() + " tenant engines.");
		for (ITenant tenant : tenants.getResults()) {
		    tenantOperations.execute(new Runnable() {

			@Override
			public void run() {
			    try {
				initializeTenantEngine(tenant);
			    } catch (SmartThingException e) {
				LOGGER.error("Tenant engine initialization failed.", e);
			    }
			}
		    });
		}

		// Wait for tenant initialization operations to finish.
		tenantOperations.shutdown();
		try {
		    tenantOperations.awaitTermination(5, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
		    LOGGER.info("Interrupted while waiting for tenants to start.");
		}
	    }
	});
    }

    /**
     * Initialize a tenant engine.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    protected ISmartThingTenantEngine initializeTenantEngine(ITenant tenant) throws SmartThingException {
	ISmartThingTenantEngine engine = createTenantEngine(tenant, SERVER_SPRING_CONTEXT, getConfigurationResolver());
	initializeNestedComponent(engine, new LifecycleProgressMonitor(
		new LifecycleProgressContext(1, "Initializing tenant engine '" + tenant.getName() + "'")));
	if (engine.getLifecycleStatus() != LifecycleStatus.InitializationError) {
	    registerTenant(tenant, engine);
	    return engine;
	} else {
	    LOGGER.warn("Tenant engine initialization failed for '" + tenant.getId() + "'", engine.getLifecycleError());
	    return null;
	}
    }

    /**
     * Start tenant engines.
     * 
     * @param start
     * @throws SmartThingException
     */
    protected void startTenantEngines(ICompositeLifecycleStep start) throws SmartThingException {
	// Initialize tenant engines.
	start.addStep(new SimpleLifecycleStep("Started tenant engines") {

	    @Override
	    public void execute(ILifecycleProgressMonitor monitor) throws SmartThingException {
		// Create thread pool for starting tenants in parallel.
		if (tenantOperations != null) {
		    tenantOperations.shutdownNow();
		}
		tenantOperations = Executors.newFixedThreadPool(TENANT_OPERATION_PARALLELISM,
			new TenantOperationsThreadFactory());

		// Start tenant engines.
		for (ISmartThingTenantEngine engine : getTenantEnginesById().values()) {

		    // Find state or create initial state as needed.
		    ITenantPersistentState state = engine.getPersistentState();
		    if (state == null) {
			TenantPersistentState newState = new TenantPersistentState();
			newState.setDesiredState(LifecycleStatus.Started);
			newState.setLastKnownState(LifecycleStatus.Starting);
			engine.persistState(newState);
			state = newState;
		    }

		    // Do not start if desired state is 'Stopped'.
		    if (state.getDesiredState() != LifecycleStatus.Stopped) {
			tenantOperations.execute(new Runnable() {

			    @Override
			    public void run() {
				try {
				    startTenantEngine(engine);
				} catch (SmartThingException e) {
				    LOGGER.error("Tenant engine startup failed.", e);
				}
			    }
			});
		    }
		}
		tenantOperations.shutdown();
		try {
		    tenantOperations.awaitTermination(5, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
		    LOGGER.info("Interrupted while waiting for tenants to start.");
		}
	    }
	});
    }

    /**
     * Start a tenant engine.
     * 
     * @param engine
     * @throws SmartThingException
     */
    protected void startTenantEngine(ISmartThingTenantEngine engine) throws SmartThingException {
	if (engine.getLifecycleStatus() != LifecycleStatus.LifecycleError) {
	    LifecycleProgressMonitor monitor = new LifecycleProgressMonitor(
		    new LifecycleProgressContext(1, "Starting tenant engine '" + engine.getTenant().getName() + "'"));
	    startNestedComponent(engine, monitor,
		    "Tenant engine '" + engine.getTenant().getName() + "' startup failed.", false);
	    engine.logState();
	} else {
	    getLifecycleComponents().put(engine.getComponentId(), engine);
	    LOGGER.info("Skipping startup for tenant engine '" + engine.getTenant().getName()
		    + "' due to initialization errors.");
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.server.lifecycle.LifecycleComponent#getComponentName()
     */
    @Override
    public String getComponentName() {
	return "SiteWhere Server " + getVersion().getEditionIdentifier() + " " + getVersion().getVersionIdentifier();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleHierarchyRoot#
     * getRegisteredLifecycleComponents()
     */
    public List<ILifecycleComponent> getRegisteredLifecycleComponents() {
	return registeredLifecycleComponents;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleHierarchyRoot#
     * getLifecycleComponentById(java.lang.String)
     */
    @Override
    public ILifecycleComponent getLifecycleComponentById(String id) {
	return lifecycleComponentsById.get(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#stop(com.smartthing.spi.
     * server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Organizes steps for stopping server.
	ICompositeLifecycleStep stop = new CompositeLifecycleStep("Stopped Server");

	// Shut down services.
	stop.addStep(new SimpleLifecycleStep("Stopped monitoring services") {

	    @Override
	    public void execute(ILifecycleProgressMonitor monitor) throws SmartThingException {
		if (executor != null) {
		    executor.shutdownNow();
		    executor = null;
		}
	    }
	});

	// Stop tenant engines.
	stopTenantEngines(stop);

	// Stop management implementations.
	stopManagementImplementations(stop);

	// Stop base server services.
	stopBaseServices(stop);

	// Execute stop operation and report progress.
	stop.execute(monitor);
    }

    /**
     * Stop base server services.
     * 
     * @param stop
     * @throws SmartThingException
     */
    protected void stopBaseServices(ICompositeLifecycleStep stop) throws SmartThingException {
	// Stop all lifecycle components.
	for (ILifecycleComponent component : getRegisteredLifecycleComponents()) {
	    stop.addStep(new StopComponentLifecycleStep(this, component, "Stopped " + component.getComponentName()));
	}

	// Stop the tenant template manager.
	stop.addStep(
		new StopComponentLifecycleStep(this, getTenantTemplateManager(), "Stopped tenant template manager"));

	// Stop the Groovy configuration.
	stop.addStep(new StopComponentLifecycleStep(this, getGroovyConfiguration(), "Stopped Groovy script engine"));
    }

    /**
     * Stop management implementations.
     * 
     * @param monitor
     * @throws SmartThingException
     */
    protected void stopManagementImplementations(ICompositeLifecycleStep stop) throws SmartThingException {
	// Stop tenant management implementation.
	stop.addStep(new StopComponentLifecycleStep(this, getTenantManagement(), "Stopped tenant management"));

	// Stop user management implementation.
	stop.addStep(new StopComponentLifecycleStep(this, getUserManagement(), "Stopped user management"));
    }

    /**
     * Stop tenant engines.
     * 
     * @param stop
     * @throws SmartThingException
     */
    protected void stopTenantEngines(ICompositeLifecycleStep stop) throws SmartThingException {
	stop.addStep(new SimpleLifecycleStep("Stopped tenant engines") {

	    @Override
	    public void execute(ILifecycleProgressMonitor monitor) throws SmartThingException {

		// Create thread pool for stopping tenants in parallel.
		if (tenantOperations != null) {
		    tenantOperations.shutdownNow();
		}
		tenantOperations = Executors.newFixedThreadPool(TENANT_OPERATION_PARALLELISM,
			new TenantOperationsThreadFactory());

		for (ISmartThingTenantEngine engine : getTenantEnginesById().values()) {
		    tenantOperations.execute(new Runnable() {

			@Override
			public void run() {
			    try {
				LifecycleProgressMonitor threadMonitor = new LifecycleProgressMonitor(
					new LifecycleProgressContext(1,
						"Stopping tenant engine '" + engine.getTenant().getName() + "'"));
				stopAndTerminateTenantEngine(engine, threadMonitor);
			    } catch (SmartThingException e) {
				LOGGER.error("Tenant engine shutdown failed.", e);
			    }
			}
		    });
		}
		tenantOperations.shutdown();
		try {
		    tenantOperations.awaitTermination(5, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
		    LOGGER.info("Interrupted while waiting for tenants to shut down.");
		}
	    }
	});
    }

    /**
     * Stop and terminate a tenant engine.
     * 
     * @param engine
     * @param monitor
     * @throws SmartThingException
     */
    protected void stopAndTerminateTenantEngine(ISmartThingTenantEngine engine, ILifecycleProgressMonitor monitor)
	    throws SmartThingException {
	// TODO: Revisit tenant shutdown logic.
	if (engine.getLifecycleStatus() == LifecycleStatus.Started) {
	    engine.lifecycleStop(monitor);
	}
	engine.lifecycleTerminate(monitor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#terminate(com.smartthing
     * .spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    public void terminate(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Stop runtime resource manager.
	getRuntimeResourceManager().lifecycleStop(monitor);

	// Stop bootstrap resource manager.
	getBootstrapResourceManager().lifecycleStop(monitor);

	// Stop Hazelcast instance.
	getHazelcastConfiguration().lifecycleStop(monitor);
    }

    /**
     * Compute current server state.
     * 
     * @return
     * @throws SmartThingException
     */
    protected ISmartThingServerRuntime computeServerRuntime(boolean includeHistorical) throws SmartThingException {
	SmartThingServerRuntime state = new SmartThingServerRuntime();

	String osName = System.getProperty("os.name");
	String osVersion = System.getProperty("os.version");
	String javaVendor = System.getProperty("java.vendor");
	String javaVersion = System.getProperty("java.version");

	GeneralInformation general = new GeneralInformation();
	general.setEdition(getVersion().getEdition());
	general.setEditionIdentifier(getVersion().getEditionIdentifier());
	general.setVersionIdentifier(getVersion().getVersionIdentifier());
	general.setBuildTimestamp(getVersion().getBuildTimestamp());
	general.setUptime(System.currentTimeMillis() - uptime);
	general.setOperatingSystemName(osName);
	general.setOperatingSystemVersion(osVersion);
	state.setGeneral(general);

	JavaInformation java = new JavaInformation();
	java.setJvmVendor(javaVendor);
	java.setJvmVersion(javaVersion);
	state.setJava(java);

	Runtime runtime = Runtime.getRuntime();
	java.setJvmFreeMemory(runtime.freeMemory());
	java.setJvmTotalMemory(runtime.totalMemory());
	java.setJvmMaxMemory(runtime.maxMemory());

	if (includeHistorical) {
	    java.setJvmTotalMemoryHistory(jvmHistory.getTotalMemory());
	    java.setJvmFreeMemoryHistory(jvmHistory.getFreeMemory());
	}

	return state;
    }

    /**
     * Create a tenant engine.
     * 
     * @param tenant
     * @param parent
     * @param resolver
     * @return
     * @throws SmartThingException
     */
    protected ISmartThingTenantEngine createTenantEngine(ITenant tenant, ApplicationContext parent,
	    IGlobalConfigurationResolver resolver) throws SmartThingException {
	return new SmartThingTenantEngine(tenant, SERVER_SPRING_CONTEXT, getConfigurationResolver());
    }

    /**
     * Registers an initialized tenant engine with the server.
     * 
     * @param tenant
     * @param engine
     * @throws SmartThingException
     */
    protected void registerTenant(ITenant tenant, ISmartThingTenantEngine engine) throws SmartThingException {
	tenantsByAuthToken.put(tenant.getAuthenticationToken(), tenant);
	getTenantEnginesById().put(tenant.getId(), engine);
    }

    /**
     * Check whether user model is populated and bootstrap system if not.
     */
    protected void verifyUserModel() {
	try {
	    IUserModelInitializer init = (IUserModelInitializer) SERVER_SPRING_CONTEXT
		    .getBean(SmartThingServerBeans.BEAN_USER_MODEL_INITIALIZER);
	    init.initialize(getUserManagement());
	} catch (NoSuchBeanDefinitionException e) {
	    LOGGER.info("No user model initializer found in Spring bean configuration. Skipping.");
	    return;
	} catch (SmartThingException e) {
	    LOGGER.warn("Error verifying user model.", e);
	}
    }

    /**
     * Check whether tenant model is populated and bootstrap system if not.
     */
    protected void verifyTenantModel() {
	try {
	    ITenantModelInitializer init = (ITenantModelInitializer) SERVER_SPRING_CONTEXT
		    .getBean(SmartThingServerBeans.BEAN_TENANT_MODEL_INITIALIZER);
	    init.initialize(getTenantManagement());
	} catch (NoSuchBeanDefinitionException e) {
	    LOGGER.info("No tenant model initializer found in Spring bean configuration. Skipping.");
	    return;
	} catch (SmartThingException e) {
	    LOGGER.warn("Error verifying tenant model.", e);
	}
    }

    /** Used for naming tenant operation threads */
    private class TenantOperationsThreadFactory implements ThreadFactory {

	/** Counts threads */
	private AtomicInteger counter = new AtomicInteger();

	public Thread newThread(Runnable r) {
	    return new Thread(r, "Tenant Lifecycle " + counter.incrementAndGet());
	}
    }
}