/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.server.scheduling;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobExecutionException;

import com.smartthing.SmartThing;
import com.smartthing.device.marshaling.CommandHtmlHelper;
import com.smartthing.device.marshaling.DeviceAssignmentMarshalHelper;
import com.smartthing.device.marshaling.DeviceSpecificationMarshalHelper;
import com.smartthing.rest.model.common.MetadataProviderEntity;
import com.smartthing.rest.model.device.command.DeviceCommand;
import com.smartthing.rest.model.device.event.DeviceCommandInvocation;
import com.smartthing.rest.model.device.request.BatchCommandForCriteriaRequest;
import com.smartthing.rest.model.scheduling.ScheduledJob;
import com.smartthing.server.scheduling.jobs.BatchCommandInvocationJob;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.IAssetModuleManager;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.command.IDeviceCommand;
import com.smartthing.spi.scheduling.ISchedule;
import com.smartthing.spi.scheduling.IScheduleManagement;
import com.smartthing.spi.scheduling.IScheduledJob;
import com.smartthing.spi.scheduling.JobConstants;
import com.smartthing.spi.tenant.ITenant;

/**
 * Configurable helper class that allows {@link ScheduledJob} model objects to
 * be created from {@link IScheduledJob} SPI objects.
 * 
 * @author dadams
 */
public class ScheduledJobMarshalHelper {

    /** Static logger instance */
    @SuppressWarnings("unused")
    private static Logger LOGGER = LogManager.getLogger();

    /** Tenant */
    private ITenant tenant;

    /** Indicates whether to include context information */
    private boolean includeContextInfo = false;

    /** Used for marshaling device assignment info */
    private DeviceAssignmentMarshalHelper assignmentHelper;

    /** Used for marshaling device specification info */
    private DeviceSpecificationMarshalHelper specificationHelper;

    public ScheduledJobMarshalHelper(ITenant tenant) {
	this(tenant, false);
    }

    public ScheduledJobMarshalHelper(ITenant tenant, boolean includeContextInfo) {
	this.tenant = tenant;
	this.includeContextInfo = includeContextInfo;
	this.assignmentHelper = new DeviceAssignmentMarshalHelper(tenant).setIncludeDevice(true).setIncludeAsset(false);
	this.specificationHelper = new DeviceSpecificationMarshalHelper(tenant).setIncludeAsset(false);
    }

    /**
     * Convert an {@link IScheduledJob} to a {@link ScheduledJob} using the
     * specified marshaling parameters.
     * 
     * @param source
     * @return
     * @throws SmartThingException
     */
    public ScheduledJob convert(IScheduledJob source) throws SmartThingException {
	ScheduledJob job = new ScheduledJob();
	MetadataProviderEntity.copy(source, job);

	job.setToken(source.getToken());
	job.setScheduleToken(source.getScheduleToken());
	job.setJobType(source.getJobType());
	job.getJobConfiguration().putAll(source.getJobConfiguration());

	if (isIncludeContextInfo()) {
	    job.setContext(new HashMap<String, Object>());
	    ISchedule schedule = getScheduleManagement(tenant).getScheduleByToken(job.getScheduleToken());
	    if (schedule != null) {
		job.getContext().put("schedule", schedule);
	    }
	    switch (job.getJobType()) {
	    case CommandInvocation: {
		includeCommandInvocationContext(job);
		break;
	    }
	    case BatchCommandInvocation: {
		includeBatchCommandInvocationContext(job);
		break;
	    }
	    default: {
	    }
	    }
	}

	return job;
    }

    /**
     * Includes contextual information specific to a command invocation. This
     * data is useful for displaying the job in a user interface.
     * 
     * @param job
     * @throws SmartThingException
     */
    protected void includeCommandInvocationContext(ScheduledJob job) throws SmartThingException {
	String assnToken = job.getJobConfiguration().get(JobConstants.CommandInvocation.ASSIGNMENT_TOKEN);
	String commandToken = job.getJobConfiguration().get(JobConstants.CommandInvocation.COMMAND_TOKEN);
	if (assnToken != null) {
	    IDeviceAssignment assignment = getDeviceManagement(getTenant()).getDeviceAssignmentByToken(assnToken);
	    if (assignment != null) {
		job.getContext().put("assignment",
			getAssignmentHelper().convert(assignment, getAssetModuleManager(getTenant())));
	    }
	}
	if (commandToken != null) {
	    IDeviceCommand command = getDeviceManagement(getTenant()).getDeviceCommandByToken(commandToken);
	    if (command != null) {
		Map<String, String> paramValues = new HashMap<String, String>();
		for (String key : job.getJobConfiguration().keySet()) {
		    if (key.startsWith(JobConstants.CommandInvocation.PARAMETER_PREFIX)) {
			String paramKey = key.substring(JobConstants.CommandInvocation.PARAMETER_PREFIX.length());
			paramValues.put(paramKey, job.getJobConfiguration().get(key));
		    }
		}

		// Emulate an invocation to produce sample html.
		DeviceCommandInvocation invocation = new DeviceCommandInvocation();
		invocation.setCommand(DeviceCommand.copy(command));
		invocation.setParameterValues(paramValues);
		String html = CommandHtmlHelper.getHtml(invocation);

		job.getContext().put("command", command);
		job.getContext().put("invocationHtml", html);
	    }
	}
    }

    /**
     * Includes contextual information specific to a batch command invocation.
     * This data is useful for displaying the job in a user interface.
     * 
     * @param job
     * @throws SmartThingException
     */
    protected void includeBatchCommandInvocationContext(ScheduledJob job) throws SmartThingException {
	String specToken = job.getJobConfiguration().get(JobConstants.BatchCommandInvocation.SPECIFICATION_TOKEN);
	if (specToken != null) {
	    IDeviceSpecification specification = getDeviceManagement(getTenant())
		    .getDeviceSpecificationByToken(specToken);
	    if (specification != null) {
		job.getContext().put("specification",
			getSpecificationHelper().convert(specification, getAssetModuleManager(getTenant())));
	    }
	    try {
		BatchCommandForCriteriaRequest criteria = BatchCommandInvocationJob.parse(job.getJobConfiguration());
		String html = CommandHtmlHelper.getHtml(criteria, getDeviceManagement(getTenant()), "..");
		job.getContext().put("criteriaHtml", html);
	    } catch (JobExecutionException e) {
		throw new SmartThingException("Unable to parse batch command criteria.");
	    }
	}
	String commandToken = job.getJobConfiguration().get(JobConstants.CommandInvocation.COMMAND_TOKEN);
	if (commandToken != null) {
	    IDeviceCommand command = getDeviceManagement(getTenant()).getDeviceCommandByToken(commandToken);
	    if (command != null) {
		Map<String, String> paramValues = new HashMap<String, String>();
		for (String key : job.getJobConfiguration().keySet()) {
		    if (key.startsWith(JobConstants.CommandInvocation.PARAMETER_PREFIX)) {
			String paramKey = key.substring(JobConstants.CommandInvocation.PARAMETER_PREFIX.length());
			paramValues.put(paramKey, job.getJobConfiguration().get(key));
		    }
		}

		// Emulate an invocation to produce sample html.
		DeviceCommandInvocation invocation = new DeviceCommandInvocation();
		invocation.setCommand(DeviceCommand.copy(command));
		invocation.setParameterValues(paramValues);
		String html = CommandHtmlHelper.getHtml(invocation);

		job.getContext().put("command", command);
		job.getContext().put("invocationHtml", html);
	    }
	}
    }

    /**
     * Get the device management implementation.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    protected IDeviceManagement getDeviceManagement(ITenant tenant) throws SmartThingException {
	return SmartThing.getServer().getDeviceManagement(tenant);
    }

    /**
     * Get the schedule management implementation.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    protected IScheduleManagement getScheduleManagement(ITenant tenant) throws SmartThingException {
	return SmartThing.getServer().getScheduleManagement(tenant);
    }

    /**
     * Get the asset module manager implementation.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    protected IAssetModuleManager getAssetModuleManager(ITenant tenant) throws SmartThingException {
	return SmartThing.getServer().getAssetModuleManager(tenant);
    }

    /**
     * Get helper class for marshaling device assignment information.
     * 
     * @return
     */
    protected DeviceAssignmentMarshalHelper getAssignmentHelper() {
	return assignmentHelper;
    }

    /**
     * Get helper class for marshaling device specification information.
     * 
     * @return
     */
    protected DeviceSpecificationMarshalHelper getSpecificationHelper() {
	return specificationHelper;
    }

    public ITenant getTenant() {
	return tenant;
    }

    public void setTenant(ITenant tenant) {
	this.tenant = tenant;
    }

    public boolean isIncludeContextInfo() {
	return includeContextInfo;
    }

    public void setIncludeContextInfo(boolean includeContextInfo) {
	this.includeContextInfo = includeContextInfo;
    }
}