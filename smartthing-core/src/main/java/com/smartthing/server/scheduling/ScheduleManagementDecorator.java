/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.server.scheduling;

import com.smartthing.server.lifecycle.LifecycleComponentDecorator;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.scheduling.ISchedule;
import com.smartthing.spi.scheduling.IScheduleManagement;
import com.smartthing.spi.scheduling.IScheduledJob;
import com.smartthing.spi.scheduling.request.IScheduleCreateRequest;
import com.smartthing.spi.scheduling.request.IScheduledJobCreateRequest;
import com.smartthing.spi.search.ISearchCriteria;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.tenant.ITenant;

/**
 * Wraps an schedule management implementation. Subclasses can implement only
 * the methods they need to override.
 * 
 * @author Derek
 */
public class ScheduleManagementDecorator extends LifecycleComponentDecorator implements IScheduleManagement {

    /** Delegate */
    private IScheduleManagement delegate;

    public ScheduleManagementDecorator(IScheduleManagement delegate) {
	super(delegate);
	this.delegate = delegate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent#setTenant(
     * com.smartthing .spi.user.ITenant)
     */
    @Override
    public void setTenant(ITenant tenant) {
	delegate.setTenant(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent#getTenant()
     */
    @Override
    public ITenant getTenant() {
	return delegate.getTenant();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.scheduling.IScheduleManagement#createSchedule(com.
     * sitewhere.spi .scheduling.request.IScheduleCreateRequest)
     */
    @Override
    public ISchedule createSchedule(IScheduleCreateRequest request) throws SmartThingException {
	return delegate.createSchedule(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.scheduling.IScheduleManagement#updateSchedule(java.lang
     * .String, com.smartthing.spi.scheduling.request.IScheduleCreateRequest)
     */
    @Override
    public ISchedule updateSchedule(String token, IScheduleCreateRequest request) throws SmartThingException {
	return delegate.updateSchedule(token, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.scheduling.IScheduleManagement#getScheduleByToken(java.
     * lang.String )
     */
    @Override
    public ISchedule getScheduleByToken(String token) throws SmartThingException {
	return delegate.getScheduleByToken(token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.scheduling.IScheduleManagement#listSchedules(com.
     * sitewhere.spi .search.ISearchCriteria)
     */
    @Override
    public ISearchResults<ISchedule> listSchedules(ISearchCriteria criteria) throws SmartThingException {
	return delegate.listSchedules(criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.scheduling.IScheduleManagement#deleteSchedule(java.lang
     * .String, boolean)
     */
    @Override
    public ISchedule deleteSchedule(String token, boolean force) throws SmartThingException {
	return delegate.deleteSchedule(token, force);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.scheduling.IScheduleManagement#createScheduledJob(com.
     * sitewhere .spi.scheduling.request.IScheduledJobCreateRequest)
     */
    @Override
    public IScheduledJob createScheduledJob(IScheduledJobCreateRequest request) throws SmartThingException {
	return delegate.createScheduledJob(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.scheduling.IScheduleManagement#updateScheduledJob(java.
     * lang.String ,
     * com.smartthing.spi.scheduling.request.IScheduledJobCreateRequest)
     */
    @Override
    public IScheduledJob updateScheduledJob(String token, IScheduledJobCreateRequest request)
	    throws SmartThingException {
	return delegate.updateScheduledJob(token, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.scheduling.IScheduleManagement#getScheduledJobByToken(
     * java.lang .String)
     */
    @Override
    public IScheduledJob getScheduledJobByToken(String token) throws SmartThingException {
	return delegate.getScheduledJobByToken(token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.scheduling.IScheduleManagement#listScheduledJobs(com.
     * sitewhere .spi.search.ISearchCriteria)
     */
    @Override
    public ISearchResults<IScheduledJob> listScheduledJobs(ISearchCriteria criteria) throws SmartThingException {
	return delegate.listScheduledJobs(criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.scheduling.IScheduleManagement#deleteScheduledJob(java.
     * lang.String , boolean)
     */
    @Override
    public IScheduledJob deleteScheduledJob(String token, boolean force) throws SmartThingException {
	return delegate.deleteScheduledJob(token, force);
    }
}