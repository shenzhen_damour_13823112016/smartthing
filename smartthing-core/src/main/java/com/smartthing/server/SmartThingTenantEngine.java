/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

import com.smartthing.SmartThing;
import com.smartthing.common.MarshalUtils;
import com.smartthing.configuration.ConfigurationUtils;
import com.smartthing.configuration.ResourceManagerTenantConfigurationResolver;
import com.smartthing.device.DeviceEventManagementTriggers;
import com.smartthing.device.DeviceManagementTriggers;
import com.smartthing.groovy.configuration.TenantGroovyConfiguration;
import com.smartthing.hazelcast.DeviceManagementCacheProvider;
import com.smartthing.rest.model.resource.request.ResourceCreateRequest;
import com.smartthing.rest.model.server.TenantEngineComponent;
import com.smartthing.rest.model.server.TenantPersistentState;
import com.smartthing.rest.model.server.TenantRuntimeState;
import com.smartthing.server.asset.AssetManagementTriggers;
import com.smartthing.server.lifecycle.CompositeLifecycleStep;
import com.smartthing.server.lifecycle.SimpleLifecycleStep;
import com.smartthing.server.lifecycle.StartComponentLifecycleStep;
import com.smartthing.server.lifecycle.StopComponentLifecycleStep;
import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.server.scheduling.QuartzScheduleManager;
import com.smartthing.server.scheduling.ScheduleManagementTriggers;
import com.smartthing.server.search.SearchProviderManager;
import com.smartthing.server.tenant.SmartThingTenantEngineCommands;
import com.smartthing.server.tenant.TenantEngineCommand;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.asset.IAssetManagement;
import com.smartthing.spi.asset.IAssetModuleManager;
import com.smartthing.spi.command.ICommandResponse;
import com.smartthing.spi.configuration.IDefaultResourcePaths;
import com.smartthing.spi.configuration.IGlobalConfigurationResolver;
import com.smartthing.spi.configuration.ITenantConfigurationResolver;
import com.smartthing.spi.device.ICachingDeviceManagement;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.IDeviceManagementCacheProvider;
import com.smartthing.spi.device.communication.IDeviceCommunication;
import com.smartthing.spi.device.event.IDeviceEventManagement;
import com.smartthing.spi.device.event.IEventProcessing;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.resource.IMultiResourceCreateResponse;
import com.smartthing.spi.resource.IResource;
import com.smartthing.spi.resource.ResourceCreateMode;
import com.smartthing.spi.resource.ResourceType;
import com.smartthing.spi.resource.request.IResourceCreateRequest;
import com.smartthing.spi.scheduling.IScheduleManagement;
import com.smartthing.spi.scheduling.IScheduleManager;
import com.smartthing.spi.search.external.ISearchProviderManager;
import com.smartthing.spi.server.ITenantEngineComponent;
import com.smartthing.spi.server.ITenantRuntimeState;
import com.smartthing.spi.server.groovy.ITenantGroovyConfiguration;
import com.smartthing.spi.server.lifecycle.ICompositeLifecycleStep;
import com.smartthing.spi.server.lifecycle.IDiscoverableTenantLifecycleComponent;
import com.smartthing.spi.server.lifecycle.ILifecycleComponent;
import com.smartthing.spi.server.lifecycle.ILifecycleConstraints;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;
import com.smartthing.spi.server.lifecycle.LifecycleStatus;
import com.smartthing.spi.server.tenant.ISmartThingTenantEngine;
import com.smartthing.spi.server.tenant.ITenantBootstrapService;
import com.smartthing.spi.server.tenant.ITenantPersistentState;
import com.smartthing.spi.tenant.ITenant;

/**
 * Default implementation of {@link ISmartThingTenantEngine} for managing
 * processing and data for a SiteWhere tenant.
 *
 * @author Derek
 */
public class SmartThingTenantEngine extends TenantLifecycleComponent implements ISmartThingTenantEngine {

    /** Private logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Spring context for tenant */
    private ApplicationContext tenantContext;

    /** SiteWhere global application context */
    private ApplicationContext globalContext;

    /** Supports global configuration management */
    private IGlobalConfigurationResolver globalConfigurationResolver;

    /** Supports tenant configuration management */
    private ITenantConfigurationResolver tenantConfigurationResolver;

    /** Tenant-scoped Groovy configuration */
    private ITenantGroovyConfiguration groovyConfiguration;

    /** Components registered to participate in SiteWhere server lifecycle */
    private List<ILifecycleComponent> registeredLifecycleComponents = new ArrayList<ILifecycleComponent>();

    /** Map of component ids to lifecycle components */
    private Map<String, ILifecycleComponent> lifecycleComponentsById = new HashMap<String, ILifecycleComponent>();

    /** Device management cache provider implementation */
    private IDeviceManagementCacheProvider deviceManagementCacheProvider;

    /** Interface to device management implementation */
    private IDeviceManagement deviceManagement;

    /** Device management with associated triggers */
    private IDeviceManagement deviceManagementWithTriggers;

    /** Interface to device event management implementation */
    private IDeviceEventManagement deviceEventManagement;

    /** Interface to asset management implementation */
    private IAssetManagement assetManagement;

    /** Interface to schedule management implementation */
    private IScheduleManagement scheduleManagement;

    /** Interface to device communication subsystem implementation */
    private IDeviceCommunication deviceCommunication;

    /** Interface to event processing subsystem implementation */
    private IEventProcessing eventProcessing;

    /** Interface for the asset module manager */
    private IAssetModuleManager assetModuleManager;

    /** Interface for the search provider manager */
    private ISearchProviderManager searchProviderManager;

    /** Interface for the schedule manager */
    private IScheduleManager scheduleManager;

    /** Interface for tenant bootstrap service */
    private ITenantBootstrapService bootstrapService;

    /** Threads used to issue engine commands */
    private ExecutorService commandExecutor = Executors.newSingleThreadExecutor();

    public SmartThingTenantEngine(ITenant tenant, ApplicationContext parent, IGlobalConfigurationResolver global) {
	super(LifecycleComponentType.TenantEngine);
	setTenant(tenant);
	this.globalContext = parent;
	this.globalConfigurationResolver = global;
	this.tenantConfigurationResolver = new ResourceManagerTenantConfigurationResolver(tenant,
		SmartThing.getServer().getVersion(), global);
    }

    /**
     * Update persistent state values.
     *
     * @param desired
     * @param current
     * @return
     * @throws SmartThingException
     */
    protected ITenantPersistentState updatePersistentState(LifecycleStatus desired, LifecycleStatus current)
	    throws SmartThingException {
	ITenantPersistentState persisted = getPersistentState();
	if (persisted == null) {
	    TenantPersistentState initial = new TenantPersistentState();
	    initial.setDesiredState(LifecycleStatus.Started);
	    initial.setLastKnownState(LifecycleStatus.Stopped);
	    persisted = initial;
	}
	TenantPersistentState updated = TenantPersistentState.copy(persisted);
	if (desired != null) {
	    updated.setDesiredState(desired);
	}
	updated.setLastKnownState(getLifecycleStatus());
	persistState(updated);
	return updated;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.server.lifecycle.LifecycleComponent#canInitialize()
     */
    @Override
    public boolean canInitialize() throws SmartThingException {
	if (getLifecycleStatus() == LifecycleStatus.Started) {
	    throw new SmartThingSystemException(ErrorCode.TenantAlreadyStarted, ErrorLevel.ERROR);
	}
	return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.ISmartThingTenantEngine#initialize()
     */
    public void initialize(ILifecycleProgressMonitor monitor) {
	try {
	    // Copy tenant configuration from template if needed.
	    initializeBootstrapService(monitor);

	    // Initialize the tenant Spring context.
	    initializeSpringContext();

	    // Initialize the Groovy configuration.
	    initializeGroovyConfiguration();

	    // Register discoverable beans.
	    initializeDiscoverableBeans(monitor);

	    // Initialize event processing subsystem.
	    setEventProcessing(initializeEventProcessingSubsystem());

	    // Initialize device communication subsystem.
	    setDeviceCommunication(initializeDeviceCommunicationSubsystem());

	    // Initialize all management implementations.
	    initializeManagementImplementations(monitor);

	    // Initialize search provider management.
	    setSearchProviderManager(initializeSearchProviderManagement());

	    // Start core functions that must run regardless of whether the
	    // tenant is considered 'started'.
	    startCoreFunctions(monitor);

	    setLifecycleStatus(LifecycleStatus.Stopped);
	} catch (SmartThingException e) {
	    setLifecycleError(e);
	    setLifecycleStatus(LifecycleStatus.InitializationError);
	} catch (Throwable e) {
	    setLifecycleError(new SmartThingException("Unhandled exception in tenant engine initialization.", e));
	    setLifecycleStatus(LifecycleStatus.InitializationError);
	    LOGGER.error("Unhandled exception in tenant engine initialization.", e);
	}
    }

    /**
     * Start core functionality that must run regardless of whether the tenant
     * is truly 'started'.
     *
     * @param monitor
     * @throws SmartThingException
     */
    private void startCoreFunctions(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Organizes steps for starting server.
	ICompositeLifecycleStep start = new CompositeLifecycleStep("Started tenant '" + getTenant().getName() + "'");

	// Start base tenant services.
	startBaseServices(start);

	// Start tenant management API implementations.
	startManagementImplementations(start);

	// Start bootstrap service to generate sample data if necessary.
	start.addStep(new StartComponentLifecycleStep(this, getBootstrapService(), "Started tenant bootstrap service.",
		"Tenant bootstrap failed.", true));

	// Start asset module manager (after potentially bootstrapping assets).
	start.addStep(new StartComponentLifecycleStep(this, getAssetModuleManager(), "Started asset module manager",
		"Asset module manager startup failed.", true));

	// Execute all operations.
	start.execute(monitor);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.ISmartThingTenantEngine#getSpringContext()
     */
    @Override
    public ApplicationContext getSpringContext() {
	return tenantContext;
    }

    /**
     * Loads the tenant configuration file. If a new configuration is staged, it
     * is transitioned into the active configuration. If no configuration is
     * found for a tenant, a new one is created from the tenant template.
     *
     * @throws SmartThingException
     */
    protected void initializeSpringContext() throws SmartThingException {
	// Handle staged configuration if available.
	LOGGER.info("Checking for staged tenant configuration.");
	IResource config = getTenantConfigurationResolver().getStagedTenantConfiguration();
	if (config != null) {
	    LOGGER.info(
		    "Staged tenant configuration found for '" + getTenant().getName() + "'. Transitioning to active.");
	    getTenantConfigurationResolver().transitionStagedToActiveTenantConfiguration();
	} else {
	    LOGGER.info("No staged tenant configuration found.");
	}

	// Load the active configuration and copy the default if necessary.
	LOGGER.info("Loading active tenant configuration for '" + getTenant().getName() + "'.");
	config = getTenantConfigurationResolver().getActiveTenantConfiguration();
	if (config == null) {
	    throw new SmartThingException("Tenant configuration not found. Aborting initialization.");
	}
	this.tenantContext = ConfigurationUtils.buildTenantContext(config, getTenant(),
		SmartThing.getServer().getVersion(), globalContext);
    }

    /**
     * Initialize the Groovy configuration.
     *
     * @throws SmartThingException
     */
    protected void initializeGroovyConfiguration() throws SmartThingException {
	this.groovyConfiguration = new TenantGroovyConfiguration();
    }

    /**
     * Initialize beans marked with
     * {@link IDiscoverableTenantLifecycleComponent} interface and add them as
     * registered components.
     *
     * @param monitor
     * @throws SmartThingException
     */
    protected void initializeDiscoverableBeans(ILifecycleProgressMonitor monitor) throws SmartThingException {
	Map<String, IDiscoverableTenantLifecycleComponent> components = tenantContext
		.getBeansOfType(IDiscoverableTenantLifecycleComponent.class);
	getRegisteredLifecycleComponents().clear();

	LOGGER.info("Registering " + components.size() + " discoverable components.");
	for (IDiscoverableTenantLifecycleComponent component : components.values()) {
	    LOGGER.info("Registering " + component.getComponentName() + ".");
	    initializeNestedComponent(component, monitor);
	    getRegisteredLifecycleComponents().add(component);
	}
    }

    /**
     * Initialize all management implementations.
     *
     * @param monitor
     * @throws SmartThingException
     */
    protected void initializeManagementImplementations(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Initialize device management.
	setDeviceManagement(initializeDeviceManagement(monitor));

	// Initialize device event management.
	setDeviceEventManagement(initializeDeviceEventManagement());

	// Initialize asset management.
	setAssetManagement(initializeAssetManagement(monitor));

	// Initialize schedule management.
	setScheduleManagement(initializeScheduleManagement());

	// Update device management to include systems required by triggers.
	setDeviceManagement(
		new DeviceManagementTriggers(deviceManagement, getDeviceEventManagement(), getDeviceCommunication()));
    }

    /**
     * Initialize device management implementation and associated decorators.
     *
     * @param monitor
     * @return
     * @throws SmartThingException
     */
    protected IDeviceManagement initializeDeviceManagement(ILifecycleProgressMonitor monitor)
	    throws SmartThingException {
	// Load device management cache provider.
	this.deviceManagementCacheProvider = new DeviceManagementCacheProvider();
	initializeNestedComponent(deviceManagementCacheProvider, monitor);

	// Verify that a device management implementation exists.
	try {
	    deviceManagement = (IDeviceManagement) tenantContext.getBean(SmartThingServerBeans.BEAN_DEVICE_MANAGEMENT);
	    LOGGER.info("Device management implementation using: " + deviceManagement.getClass().getName());

	    return configureDeviceManagement(deviceManagement);
	} catch (NoSuchBeanDefinitionException e) {
	    throw new SmartThingException("No device management implementation configured.");
	}
    }

    /**
     * Configure device management implementation by injecting configured
     * options or wrapping to add functionality.
     *
     * @param management
     * @return
     * @throws SmartThingException
     */
    protected IDeviceManagement configureDeviceManagement(IDeviceManagement management) throws SmartThingException {
	if (management instanceof ICachingDeviceManagement) {
	    ((ICachingDeviceManagement) management).setCacheProvider(getDeviceManagementCacheProvider());
	    LOGGER.info("Device management implementation is using configured cache provider.");
	} else {
	    LOGGER.info("Device management implementation not using cache provider.");
	}
	return management;
    }

    /**
     * Initialize device event management implementation.
     *
     * @return
     * @throws SmartThingException
     */
    protected IDeviceEventManagement initializeDeviceEventManagement() throws SmartThingException {
	// Verify that a device event management implementation exists.
	try {
	    IDeviceEventManagement management = (IDeviceEventManagement) tenantContext
		    .getBean(SmartThingServerBeans.BEAN_DEVICE_EVENT_MANAGEMENT);
	    IDeviceEventManagement configured = configureDeviceEventManagement(management);
	    LOGGER.info("Device event management implementation using: " + configured.getClass().getName());
	    return configured;

	} catch (NoSuchBeanDefinitionException e) {
	    throw new SmartThingException("No device event management implementation configured.");
	}
    }

    /**
     * Configure device event management implementation by injecting configured
     * options or wrapping to add functionality.
     *
     * @param management
     * @return
     * @throws SmartThingException
     */
    protected IDeviceEventManagement configureDeviceEventManagement(IDeviceEventManagement management)
	    throws SmartThingException {
	// Add reference to device management implementation.
	management.setDeviceManagement(getDeviceManagement());

	// Routes stored events to outbound processing strategy.
	return new DeviceEventManagementTriggers(management, getEventProcessing());
    }

    /**
     * Verify and initialize device communication subsystem implementation.
     *
     * @throws SmartThingException
     */
    protected IDeviceCommunication initializeDeviceCommunicationSubsystem() throws SmartThingException {
	try {
	    return (IDeviceCommunication) tenantContext.getBean(SmartThingServerBeans.BEAN_DEVICE_COMMUNICATION);
	} catch (NoSuchBeanDefinitionException e) {
	    throw new SmartThingException("No device communication subsystem implementation configured.");
	}
    }

    /**
     * Verify and initialize event processing subsystem implementation.
     *
     * @throws SmartThingException
     */
    protected IEventProcessing initializeEventProcessingSubsystem() throws SmartThingException {
	try {
	    return (IEventProcessing) tenantContext.getBean(SmartThingServerBeans.BEAN_EVENT_PROCESSING);
	} catch (NoSuchBeanDefinitionException e) {
	    throw new SmartThingException("No event processing subsystem implementation configured.");
	}
    }

    /**
     * Verify and initialize asset module manager.
     *
     * @param monitor
     * @return
     * @throws SmartThingException
     */
    protected IAssetManagement initializeAssetManagement(ILifecycleProgressMonitor monitor) throws SmartThingException {
	try {
	    // Locate and initialize asset management implementation.
	    IAssetManagement implementation = (IAssetManagement) tenantContext
		    .getBean(SmartThingServerBeans.BEAN_ASSET_MANAGEMENT);
	    initializeNestedComponent(implementation, monitor);

	    // Create asset module manager.
	    assetModuleManager = (IAssetModuleManager) tenantContext
		    .getBean(SmartThingServerBeans.BEAN_ASSET_MODULE_MANAGER);
	    assetModuleManager.setAssetManagement(implementation);
	    initializeNestedComponent(assetModuleManager, monitor);

	    // Wrap triggers around implementation.
	    IAssetManagement withTriggers = new AssetManagementTriggers(implementation, assetModuleManager);
	    return withTriggers;
	} catch (NoSuchBeanDefinitionException e) {
	    throw new SmartThingException("No asset module manager implementation configured.");
	}
    }

    /**
     * Verify and initialize schedule manager.
     *
     * @return
     * @throws SmartThingException
     */
    protected IScheduleManagement initializeScheduleManagement() throws SmartThingException {
	try {
	    IScheduleManagement implementation = (IScheduleManagement) tenantContext
		    .getBean(SmartThingServerBeans.BEAN_SCHEDULE_MANAGEMENT);
	    scheduleManager = (IScheduleManager) new QuartzScheduleManager(implementation);

	    IScheduleManagement withTriggers = new ScheduleManagementTriggers(implementation, scheduleManager);
	    return withTriggers;
	} catch (NoSuchBeanDefinitionException e) {
	    throw new SmartThingException("No schedule manager implementation configured.");
	}
    }

    /**
     * Initialize the tenant bootstrap service.
     *
     * @return
     * @throws SmartThingException
     */
    protected ITenantBootstrapService initializeBootstrapService(ILifecycleProgressMonitor monitor)
	    throws SmartThingException {
	bootstrapService = new TenantBootstrapService(this);
	initializeNestedComponent(bootstrapService, monitor);
	return bootstrapService;
    }

    /**
     * Start base tenant services.
     *
     * @param start
     * @throws SmartThingException
     */
    protected void startBaseServices(ICompositeLifecycleStep start) throws SmartThingException {
	// Start Groovy configuration.
	start.addStep(new StartComponentLifecycleStep(this, getGroovyConfiguration(),
		"Started tenant Groovy script engine", "Groovy configuration startup failed.", true));

	// Start lifecycle components.
	for (ILifecycleComponent component : getRegisteredLifecycleComponents()) {
	    start.addStep(new StartComponentLifecycleStep(this, component, "Started " + component.getComponentName(),
		    component.getComponentName() + " startup failed.", true));
	}
    }

    /**
     * Start tenant management API implementations.
     *
     * @param start
     * @throws SmartThingException
     */
    protected void startManagementImplementations(ICompositeLifecycleStep start) throws SmartThingException {
	// Start asset management.
	start.addStep(new StartComponentLifecycleStep(this, getAssetManagement(), "Started asset management",
		"Asset management startup failed.", true));

	// Start device management cache provider.
	start.addStep(new StartComponentLifecycleStep(this, getDeviceManagementCacheProvider(),
		"Started device management cache provider", "Device management cache provider startup failed.", true));

	// Start device management.
	start.addStep(new StartComponentLifecycleStep(this, getDeviceManagement(), "Started device management",
		"Device management startup failed.", true));

	// Start device management.
	start.addStep(new StartComponentLifecycleStep(this, getDeviceEventManagement(),
		"Started device event management", "Device event management startup failed.", true));

	// Start device management.
	start.addStep(new StartComponentLifecycleStep(this, getScheduleManagement(), "Started schedule management",
		"Schedule management startup failed.", true));
    }

    /**
     * Verify and initialize search provider manager.
     *
     * @return
     * @throws SmartThingException
     */
    protected ISearchProviderManager initializeSearchProviderManagement() throws SmartThingException {
	try {
	    return (ISearchProviderManager) tenantContext.getBean(SmartThingServerBeans.BEAN_SEARCH_PROVIDER_MANAGER);
	} catch (NoSuchBeanDefinitionException e) {
	    return new SearchProviderManager();
	}
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#lifecycleStart(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void lifecycleStart(ILifecycleProgressMonitor monitor) {
	super.lifecycleStart(monitor);
	try {
	    updatePersistentState(null, getLifecycleStatus());
	} catch (SmartThingException e) {
	    LOGGER.warn("Unable to update tenant persistent state.");
	}
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.server.lifecycle.LifecycleComponent#canStart()
     */
    @Override
    public boolean canStart() throws SmartThingException {
	if (getLifecycleStatus() == LifecycleStatus.Started) {
	    throw new SmartThingSystemException(ErrorCode.TenantAlreadyStarted, ErrorLevel.ERROR);
	}
	return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
     * .server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Organizes steps for starting server.
	ICompositeLifecycleStep start = new CompositeLifecycleStep("Started tenant '" + getTenant().getName() + "'");

	// Clear the component list.
	start.addStep(new SimpleLifecycleStep("Prepared tenant") {

	    @Override
	    public void execute(ILifecycleProgressMonitor monitor) throws SmartThingException {
		getLifecycleComponents().clear();
	    }
	});

	// Start tenant services.
	startTenantServices(start);

	// Finish tenant startup.
	start.addStep(new SimpleLifecycleStep("Verified bootstrap data") {

	    @Override
	    public void execute(ILifecycleProgressMonitor monitor) throws SmartThingException {
		// Force refresh on components-by-id map.
		lifecycleComponentsById = buildComponentMap();
	    }
	});

	// Execute operation and report progress.
	updatePersistentState(LifecycleStatus.Started, getLifecycleStatus());
	start.execute(monitor);
    }

    /**
     * Start tenant services.
     *
     * @param start
     * @throws SmartThingException
     */
    protected void startTenantServices(ICompositeLifecycleStep start) throws SmartThingException {
	// Start search provider manager.
	start.addStep(new StartComponentLifecycleStep(this, getSearchProviderManager(),
		"Started search provider manager", "Search provider manager startup failed.", true));

	// Start event processing subsystem.
	start.addStep(new StartComponentLifecycleStep(this, getEventProcessing(), "Started event processing subsystem",
		"Event processing subsystem startup failed.", true));

	// Start device communication subsystem.
	start.addStep(new StartComponentLifecycleStep(this, getDeviceCommunication(),
		"Started device communication subsystem", "Device communication subsystem startup failed.", true));

	// Start schedule manager.
	start.addStep(new StartComponentLifecycleStep(this, getScheduleManager(), "Started schedule manager",
		"Schedule manager startup failed.", true));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.server.lifecycle.LifecycleComponent#lifecycleStop(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor,
     * com.smartthing.spi.server.lifecycle.ILifecycleConstraints)
     */
    @Override
    public void lifecycleStop(ILifecycleProgressMonitor monitor, ILifecycleConstraints constraints) {
	super.lifecycleStop(monitor, constraints);
	try {
	    updatePersistentState(null, getLifecycleStatus());
	} catch (SmartThingException e) {
	    LOGGER.warn("Unable to update tenant persistent state.");
	}
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.server.lifecycle.LifecycleComponent#canStop()
     */
    @Override
    public boolean canStop() throws SmartThingException {
	if (getLifecycleStatus() == LifecycleStatus.Stopped) {
	    throw new SmartThingSystemException(ErrorCode.TenantAlreadyStopped, ErrorLevel.ERROR);
	}
	return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#stop(com.smartthing.spi.
     * server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
	stop(monitor, false);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#stop(com.smartthing.spi.
     * server.lifecycle.ILifecycleProgressMonitor,
     * com.smartthing.spi.server.lifecycle.ILifecycleConstraints)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor, ILifecycleConstraints constraints) throws SmartThingException {
	boolean persist = ((constraints != null)
		&& (constraints instanceof SmartThingTenantEngineCommands.PersistentShutdownConstraint));
	stop(monitor, persist);
    }

    /**
     * Stops components, but differentiates between server shutdown and an
     * explicit stop request for the tenant.
     *
     * @param monitor
     * @param persist
     * @throws SmartThingException
     */
    protected void stop(ILifecycleProgressMonitor monitor, boolean persist) throws SmartThingException {
	LifecycleStatus desired = (persist) ? LifecycleStatus.Stopped : null;
	updatePersistentState(desired, getLifecycleStatus());

	// Organizes steps for stopping tenant.
	ICompositeLifecycleStep stop = new CompositeLifecycleStep("Stopped tenant '" + getTenant().getName() + "'");

	// Stop tenant services.
	stopTenantServices(stop);

	// Execute operation with progress monitoring.
	stop.execute(monitor);
    }

    /**
     * Stop tenant services.
     *
     * @param stop
     * @throws SmartThingException
     */
    protected void stopTenantServices(ICompositeLifecycleStep stop) throws SmartThingException {
	// Stop scheduling new jobs.
	stop.addStep(new StopComponentLifecycleStep(this, getScheduleManager(), "Stopped schedule manager"));

	// Disable device communications.
	stop.addStep(new StopComponentLifecycleStep(this, getDeviceCommunication(),
		"Stopped device communication subsystem"));
	stop.addStep(new StopComponentLifecycleStep(this, getEventProcessing(), "Stopped event processing subsystem"));

	// Stop search provider manager.
	stop.addStep(
		new StopComponentLifecycleStep(this, getSearchProviderManager(), "Stopped search provider manager"));

	// Stop the Groovy configuration.
	stop.addStep(new StopComponentLifecycleStep(this, getGroovyConfiguration(), "Stopped Groovy engine"));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#terminate(com.smartthing
     * .spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void terminate(ILifecycleProgressMonitor monitor) throws SmartThingException {
	stopCoreFunctions(monitor);
    }

    /**
     * Stop core functionality. This should only happen if the tenant is
     * completely terminated and not in the standard lifecycle loop.
     *
     * @param monitor
     * @throws SmartThingException
     */
    protected void stopCoreFunctions(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Organizes steps for stopping tenant.
	ICompositeLifecycleStep stop = new CompositeLifecycleStep("Stopped tenant '" + getTenant().getName() + "'");

	// Stop lifecycle components.
	stop.addStep(new SimpleLifecycleStep("Stopped registered components") {

	    @Override
	    public void execute(ILifecycleProgressMonitor monitor) throws SmartThingException {
		for (ILifecycleComponent component : getRegisteredLifecycleComponents()) {
		    component.lifecycleStop(monitor);
		}
	    }
	});

	// Stop core management implementations.
	stopManagementServices(stop);

	// Execute operation with progress monitoring.
	stop.execute(monitor);
    }

    /**
     * Stop tenant management services.
     *
     * @param stop
     * @throws SmartThingException
     */
    protected void stopManagementServices(ICompositeLifecycleStep stop) throws SmartThingException {
	// Stop schedule management.
	stop.addStep(new StopComponentLifecycleStep(this, getScheduleManagement(),
		"Stopped schedule management implementation"));

	// Stop device event management.
	stop.addStep(new StopComponentLifecycleStep(this, getDeviceEventManagement(),
		"Stopped device event management implementation"));

	// Stop device management.
	stop.addStep(new StopComponentLifecycleStep(this, getDeviceManagement(),
		"Stopped device management implementation"));

	// Stop device management cache provider if configured.
	stop.addStep(new StopComponentLifecycleStep(this, getDeviceManagementCacheProvider(),
		"Stopped device management cache provider"));

	// Stop asset module manager.
	stop.addStep(new StopComponentLifecycleStep(this, getAssetModuleManager(), "Stopped asset module manager"));

	// Stop asset management.
	stop.addStep(
		new StopComponentLifecycleStep(this, getAssetManagement(), "Stopped asset management implementation"));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.lifecycle.ILifecycleHierarchyRoot#
     * getRegisteredLifecycleComponents()
     */
    public List<ILifecycleComponent> getRegisteredLifecycleComponents() {
	return registeredLifecycleComponents;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.lifecycle.ILifecycleHierarchyRoot#
     * getLifecycleComponentById(java.lang.String)
     */
    @Override
    public ILifecycleComponent getLifecycleComponentById(String id) {
	return lifecycleComponentsById.get(id);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.server.tenant.ISmartThingTenantEngine#getPersistentState
     * ()
     */
    @Override
    public ITenantPersistentState getPersistentState() throws SmartThingException {
	IResource resource = getTenantConfigurationResolver()
		.getResourceForPath(IDefaultResourcePaths.TENANT_STATE_RESOURCE_NAME);
	if (resource == null) {
	    return null;
	}
	try {
	    return MarshalUtils.unmarshalJson(resource.getContent(), TenantPersistentState.class);
	} catch (Throwable t) {
	    LOGGER.warn("Unable to unmarshal persistent state. Assuming Stopped->Started transition.");
	    TenantPersistentState state = new TenantPersistentState();
	    state.setDesiredState(LifecycleStatus.Started);
	    state.setLastKnownState(LifecycleStatus.Stopped);
	    return state;
	}
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.server.tenant.ISmartThingTenantEngine#persistState(com.
     * sitewhere.spi.server.tenant.ITenantPersistentState)
     */
    @Override
    public void persistState(ITenantPersistentState state) throws SmartThingException {
	TenantPersistentState persist = TenantPersistentState.copy(state);
	byte[] content = MarshalUtils.marshalJson(persist);

	ResourceCreateRequest request = new ResourceCreateRequest();
	request.setContent(content);
	request.setPath(IDefaultResourcePaths.TENANT_STATE_RESOURCE_NAME);
	request.setResourceType(ResourceType.ConfigurationFile);
	List<IResourceCreateRequest> resources = new ArrayList<IResourceCreateRequest>();
	resources.add(request);

	IMultiResourceCreateResponse response =SmartThing.getServer().getRuntimeResourceManager()
		.createTenantResources(getTenant().getId(), resources, ResourceCreateMode.OVERWRITE);
	if (response.getErrors().size() > 0) {
	    throw new SmartThingException(
		    "Unable to persist tenant state. " + response.getErrors().get(0).getReason().toString());
	}
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.ISmartThingTenantEngine#getEngineState()
     */
    @Override
    public ITenantRuntimeState getEngineState() {
	TenantRuntimeState state = new TenantRuntimeState();
	state.setLifecycleStatus(getLifecycleStatus());
	if (getLifecycleStatus() == LifecycleStatus.Started) {
	    state.setComponentHierarchyState(getComponentHierarchyState());
	}
	state.setStaged(getTenantConfigurationResolver().hasStagedConfiguration());
	return state;
    }

    /**
     * Use recursion to get state of hierarchy of lifecycle components.
     *
     * @return
     */
    protected List<ITenantEngineComponent> getComponentHierarchyState() {
	List<ITenantEngineComponent> results = new ArrayList<ITenantEngineComponent>();

	TenantEngineComponent engine = new TenantEngineComponent();
	engine.setId(getComponentId());
	engine.setName(getComponentName());
	engine.setStatus(getLifecycleStatus());
	engine.setType(getComponentType());
	engine.setParentId(null);
	results.add(engine);

	getComponentHierarchyState(this, results);
	return results;
    }

    /**
     * Recursive call to capture hierarchy of components.
     *
     * @param parent
     * @param results
     */
    protected void getComponentHierarchyState(ILifecycleComponent parent, List<ITenantEngineComponent> results) {
	Map<String, ILifecycleComponent> children = parent.getLifecycleComponents();
	for (ILifecycleComponent child : children.values()) {
	    TenantEngineComponent component = new TenantEngineComponent();
	    component.setId(child.getComponentId());
	    component.setName(child.getComponentName());
	    component.setStatus(child.getLifecycleStatus());
	    component.setType(child.getComponentType());
	    component.setParentId(parent.getComponentId());
	    results.add(component);
	    getComponentHierarchyState(child, results);
	}
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.server.tenant.ISmartThingTenantEngine#issueCommand(java.
     * lang.String,
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public ICommandResponse issueCommand(String command, ILifecycleProgressMonitor monitor) throws SmartThingException {
	Class<? extends TenantEngineCommand> commandClass = SmartThingTenantEngineCommands.Command
		.getCommandClass(command);
	if (commandClass == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidTenantEngineCommand, ErrorLevel.ERROR);
	}
	try {
	    TenantEngineCommand cmd = commandClass.newInstance();
	    cmd.setEngine(this);
	    cmd.setProgressMonitor(monitor);
	    Future<ICommandResponse> response = commandExecutor.submit(cmd);
	    return response.get();
	} catch (InstantiationException e) {
	    throw new SmartThingException(e);
	} catch (IllegalAccessException e) {
	    throw new SmartThingException(e);
	} catch (InterruptedException e) {
	    throw new SmartThingException(e);
	} catch (ExecutionException e) {
	    throw new SmartThingException(e);
	}
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.server.lifecycle.LifecycleComponent#getComponentName()
     */
    @Override
    public String getComponentName() {
	return getClass().getSimpleName() + " '" + getTenant().getName() + "' (" + getTenant().getId() + ")";
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.ISmartThingTenantEngine#
     * getGlobalConfigurationResolver()
     */
    public IGlobalConfigurationResolver getGlobalConfigurationResolver() {
	return globalConfigurationResolver;
    }

    public void setConfigurationResolver(IGlobalConfigurationResolver configurationResolver) {
	this.globalConfigurationResolver = configurationResolver;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.ISmartThingTenantEngine#
     * getTenantConfigurationResolver()
     */
    public ITenantConfigurationResolver getTenantConfigurationResolver() {
	return tenantConfigurationResolver;
    }

    public void setTenantConfigurationResolver(ITenantConfigurationResolver tenantConfigurationResolver) {
	this.tenantConfigurationResolver = tenantConfigurationResolver;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.tenant.ISmartThingTenantEngine#
     * getGroovyConfiguration()
     */
    public ITenantGroovyConfiguration getGroovyConfiguration() {
	return groovyConfiguration;
    }

    public void setGroovyConfiguration(ITenantGroovyConfiguration groovyConfiguration) {
	this.groovyConfiguration = groovyConfiguration;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.ISmartThingTenantEngine#
     * getDeviceManagementCacheProvider()
     */
    public IDeviceManagementCacheProvider getDeviceManagementCacheProvider() {
	return deviceManagementCacheProvider;
    }

    public void setDeviceManagementCacheProvider(IDeviceManagementCacheProvider deviceManagementCacheProvider) {
	this.deviceManagementCacheProvider = deviceManagementCacheProvider;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.server.ISmartThingTenantEngine#getDeviceManagement()
     */
    public IDeviceManagement getDeviceManagement() {
	return deviceManagementWithTriggers;
    }

    public void setDeviceManagement(IDeviceManagement deviceManagement) {
	this.deviceManagementWithTriggers = deviceManagement;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.server.ISmartThingTenantEngine#getDeviceEventManagement(
     * )
     */
    public IDeviceEventManagement getDeviceEventManagement() {
	return deviceEventManagement;
    }

    public void setDeviceEventManagement(IDeviceEventManagement deviceEventManagement) {
	this.deviceEventManagement = deviceEventManagement;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.ISmartThingTenantEngine#getAssetManagement()
     */
    public IAssetManagement getAssetManagement() {
	return assetManagement;
    }

    public void setAssetManagement(IAssetManagement assetManagement) {
	this.assetManagement = assetManagement;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.server.ISmartThingTenantEngine#getScheduleManagement()
     */
    public IScheduleManagement getScheduleManagement() {
	return scheduleManagement;
    }

    public void setScheduleManagement(IScheduleManagement scheduleManagement) {
	this.scheduleManagement = scheduleManagement;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.tenant.ISmartThingTenantEngine#
     * getBootstrapService()
     */
    public ITenantBootstrapService getBootstrapService() {
	return bootstrapService;
    }

    public void setBootstrapService(ITenantBootstrapService bootstrapService) {
	this.bootstrapService = bootstrapService;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.server.ISmartThingTenantEngine#getDeviceCommunication()
     */
    public IDeviceCommunication getDeviceCommunication() {
	return deviceCommunication;
    }

    public void setDeviceCommunication(IDeviceCommunication deviceCommunication) {
	this.deviceCommunication = deviceCommunication;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.ISmartThingTenantEngine#getEventProcessing()
     */
    public IEventProcessing getEventProcessing() {
	return eventProcessing;
    }

    public void setEventProcessing(IEventProcessing eventProcessing) {
	this.eventProcessing = eventProcessing;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.server.ISmartThingTenantEngine#getAssetModuleManager()
     */
    public IAssetModuleManager getAssetModuleManager() {
	return assetModuleManager;
    }

    public void setAssetModuleManager(IAssetModuleManager assetModuleManager) {
	this.assetModuleManager = assetModuleManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.server.ISmartThingTenantEngine#getSearchProviderManager(
     * )
     */
    public ISearchProviderManager getSearchProviderManager() {
	return searchProviderManager;
    }

    public void setSearchProviderManager(ISearchProviderManager searchProviderManager) {
	this.searchProviderManager = searchProviderManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.ISmartThingTenantEngine#getScheduleManager()
     */
    public IScheduleManager getScheduleManager() {
	return scheduleManager;
    }

    public void setScheduleManager(IScheduleManager scheduleManager) {
	this.scheduleManager = scheduleManager;
    }
}