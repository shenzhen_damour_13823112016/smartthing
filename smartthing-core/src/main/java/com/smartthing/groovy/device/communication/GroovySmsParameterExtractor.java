package com.smartthing.groovy.device.communication;

import com.smartthing.device.communication.sms.SmsParameters;

/**
 * Command extractor that expects an {@link SmsParameters} object to be returned
 * from the Groovy script.
 * 
 * @author Derek
 */
public class GroovySmsParameterExtractor extends GroovyParameterExtractor<SmsParameters> {
}