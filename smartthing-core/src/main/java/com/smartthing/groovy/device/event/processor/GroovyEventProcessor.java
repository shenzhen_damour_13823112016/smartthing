/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.groovy.device.event.processor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.SmartThing;
import com.smartthing.device.event.processor.FilteredOutboundEventProcessor;
import com.smartthing.rest.model.device.event.request.scripting.DeviceEventRequestBuilder;
import com.smartthing.rest.model.device.event.scripting.DeviceEventSupport;
import com.smartthing.rest.model.device.request.scripting.DeviceManagementRequestBuilder;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceEvent;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;

import groovy.lang.Binding;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

/**
 * Outbound event processor that uses a Groovy script to process events.
 * 
 * @author Derek
 */
public class GroovyEventProcessor extends FilteredOutboundEventProcessor {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Relative path to Groovy script */
    private String scriptPath;

    /** Supports building device management entities */
    private DeviceManagementRequestBuilder deviceBuilder;

    /** Supports building various types of device events */
    private DeviceEventRequestBuilder eventsBuilder;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.event.processor.FilteredOutboundEventProcessor#start
     * (com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Required for filters.
	super.start(monitor);

	this.deviceBuilder = new DeviceManagementRequestBuilder(getDeviceManagement());
	this.eventsBuilder = new DeviceEventRequestBuilder(getDeviceManagement(), getEventManagement());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onMeasurementsNotFiltered(com.smartthing.spi.device.event.
     * IDeviceMeasurements)
     */
    @Override
    public void onMeasurementsNotFiltered(IDeviceMeasurements measurements) throws SmartThingException {
	processEvent(measurements);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onLocationNotFiltered(com.smartthing.spi.device.event.IDeviceLocation)
     */
    @Override
    public void onLocationNotFiltered(IDeviceLocation location) throws SmartThingException {
	processEvent(location);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onAlertNotFiltered(com.smartthing.spi.device.event.IDeviceAlert)
     */
    @Override
    public void onAlertNotFiltered(IDeviceAlert alert) throws SmartThingException {
	processEvent(alert);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onStateChangeNotFiltered(com.smartthing.spi.device.event.
     * IDeviceStateChange)
     */
    @Override
    public void onStateChangeNotFiltered(IDeviceStateChange state) throws SmartThingException {
	processEvent(state);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onCommandInvocationNotFiltered(com.smartthing.spi.device.event.
     * IDeviceCommandInvocation)
     */
    @Override
    public void onCommandInvocationNotFiltered(IDeviceCommandInvocation invocation) throws SmartThingException {
	processEvent(invocation);
    }

    @Override
    public void onCommandResponseNotFiltered(IDeviceCommandResponse response) throws SmartThingException {
	processEvent(response);
    }

    /**
     * Perform custom event processing in a Groovy script.
     * 
     * @param event
     * @throws SmartThingException
     */
    protected void processEvent(IDeviceEvent event) throws SmartThingException {
	// These should be cached, so no performance hit.
	IDeviceAssignment assignment = getDeviceManagement()
		.getDeviceAssignmentByToken(event.getDeviceAssignmentToken());
	IDevice device = getDeviceManagement().getDeviceForAssignment(assignment);

	// Create Groovy binding with handles to everything.
	Binding binding = new Binding();
	binding.setVariable("logger", getLogger());
	binding.setVariable("event", new DeviceEventSupport(event));
	binding.setVariable("assignment", assignment);
	binding.setVariable("device", device);
	binding.setVariable("deviceBuilder", deviceBuilder);
	binding.setVariable("eventBuilder", eventsBuilder);

	try {
	   SmartThing.getServer().getTenantGroovyConfiguration(getTenant()).getGroovyScriptEngine().run(getScriptPath(),
		    binding);
	} catch (ResourceException e) {
	    throw new SmartThingException("Unable to access Groovy script. " + e.getMessage(), e);
	} catch (ScriptException e) {
	    throw new SmartThingException("Unable to run Groovy script.", e);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    public String getScriptPath() {
	return scriptPath;
    }

    public void setScriptPath(String scriptPath) {
	this.scriptPath = scriptPath;
    }
}