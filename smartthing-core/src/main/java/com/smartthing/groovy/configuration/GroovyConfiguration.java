package com.smartthing.groovy.configuration;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.server.lifecycle.LifecycleComponent;
import com.smartthing.server.resource.SmartThingHomeResourceManager;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.groovy.IGroovyConfiguration;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

import groovy.util.GroovyScriptEngine;

/**
 * Provides common Groovy configuration for core server components.
 * 
 * @author Derek
 */
public class GroovyConfiguration extends LifecycleComponent implements IGroovyConfiguration {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Groovy script engine */
    private GroovyScriptEngine groovyScriptEngine;

    /** Field for setting GSE verbose flag */
    private boolean verbose = false;

    /** Field for setting GSE debug flag */
    private boolean debug = false;

    public GroovyConfiguration() {
	super(LifecycleComponentType.Other);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
     * .server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	File root = SmartThingHomeResourceManager.calculateConfigurationPath();
	File groovy = new File(root, "global/scripts/groovy");
	if (!groovy.exists()) {
	    getLogger().warn("Global Groovy scripts folder did not exist. Creating.");
	    groovy.mkdirs();
	}

	URL[] roots = null;
	try {
	    roots = new URL[] { groovy.toURI().toURL() };
	} catch (MalformedURLException e) {
	    throw new SmartThingException("Invalid Groovy script root.", e);
	}

	groovyScriptEngine = new GroovyScriptEngine(roots);

	groovyScriptEngine.getConfig().setVerbose(isVerbose());
	groovyScriptEngine.getConfig().setDebug(isDebug());
	LOGGER.info(
		"Global Groovy script engine configured with (verbose:" + isVerbose() + ") (debug:" + isDebug() + ").");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.groovy.IGroovyConfiguration#
     * getGroovyScriptEngine()
     */
    public GroovyScriptEngine getGroovyScriptEngine() {
	return groovyScriptEngine;
    }

    public void setGroovyScriptEngine(GroovyScriptEngine groovyScriptEngine) {
	this.groovyScriptEngine = groovyScriptEngine;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    public boolean isVerbose() {
	return verbose;
    }

    public void setVerbose(boolean verbose) {
	this.verbose = verbose;
    }

    public boolean isDebug() {
	return debug;
    }

    public void setDebug(boolean debug) {
	this.debug = debug;
    }
}