/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.web.rest.documentation;

import java.util.HashMap;
import java.util.Map;

import com.smartthing.device.DeviceManagementDecorator;
import com.smartthing.rest.model.device.Device;
import com.smartthing.rest.model.device.DeviceAssignment;
import com.smartthing.rest.model.device.DeviceSpecification;
import com.smartthing.rest.model.device.Site;
import com.smartthing.rest.model.device.command.DeviceCommand;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.ISite;
import com.smartthing.spi.device.command.IDeviceCommand;

/**
 * Device management facade using static sample data. This is used to generate
 * JSON for REST documentation.
 * 
 * @author Derek
 */
public class MockDeviceManagement extends DeviceManagementDecorator {

    /** Sites by token */
    private Map<String, Site> sites = new HashMap<String, Site>();

    /** Specifications by token */
    private Map<String, DeviceSpecification> specifications = new HashMap<String, DeviceSpecification>();

    /** Devices by hardware id */
    private Map<String, Device> devices = new HashMap<String, Device>();

    /** Assignments by token */
    private Map<String, DeviceAssignment> assignments = new HashMap<String, DeviceAssignment>();

    /** Device commands by token */
    private Map<String, DeviceCommand> commands = new HashMap<String, DeviceCommand>();

    public MockDeviceManagement() {
	super(null);

	sites.put(ExampleData.SITE_CONSTRUCTION.getToken(), ExampleData.SITE_CONSTRUCTION);

	specifications.put(ExampleData.SPEC_MEITRACK.getToken(), ExampleData.SPEC_MEITRACK);
	specifications.put(ExampleData.SPEC_HEART_MONITOR.getToken(), ExampleData.SPEC_HEART_MONITOR);

	commands.put(ExampleData.COMMAND_GET_FW_VER.getToken(), ExampleData.COMMAND_GET_FW_VER);
	commands.put(ExampleData.COMMAND_SET_RPT_INTV.getToken(), ExampleData.COMMAND_SET_RPT_INTV);

	devices.put(ExampleData.TRACKER.getHardwareId(), ExampleData.TRACKER);
	devices.put(ExampleData.HEART_MONITOR.getHardwareId(), ExampleData.HEART_MONITOR);

	assignments.put(ExampleData.TRACKER_TO_DEREK.getToken(), ExampleData.TRACKER_TO_DEREK);
	assignments.put(ExampleData.HEART_MONITOR_TO_DEREK.getToken(), ExampleData.HEART_MONITOR_TO_DEREK);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.DeviceManagementDecorator#getSiteByToken(java.lang.
     * String)
     */
    @Override
    public ISite getSiteByToken(String token) throws SmartThingException {
	return sites.get(token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.DeviceManagementDecorator#
     * getDeviceSpecificationByToken(java .lang.String)
     */
    @Override
    public IDeviceSpecification getDeviceSpecificationByToken(String token) throws SmartThingException {
	return specifications.get(token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.DeviceManagementDecorator#getDeviceByHardwareId(java
     * .lang. String)
     */
    @Override
    public IDevice getDeviceByHardwareId(String hardwareId) throws SmartThingException {
	return devices.get(hardwareId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.DeviceManagementDecorator#getDeviceAssignmentByToken
     * (java. lang.String)
     */
    @Override
    public IDeviceAssignment getDeviceAssignmentByToken(String token) throws SmartThingException {
	return assignments.get(token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.DeviceManagementDecorator#getDeviceCommandByToken(
     * java.lang .String)
     */
    @Override
    public IDeviceCommand getDeviceCommandByToken(String token) throws SmartThingException {
	return commands.get(token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.DeviceManagementDecorator#getCurrentDeviceAssignment
     * (com.smartthing .spi.device.IDevice)
     */
    @Override
    public IDeviceAssignment getCurrentDeviceAssignment(IDevice device) throws SmartThingException {
	return getDeviceAssignmentByToken(device.getAssignmentToken());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.DeviceManagementDecorator#getDeviceForAssignment(com
     * .sitewhere .spi.device.IDeviceAssignment)
     */
    @Override
    public IDevice getDeviceForAssignment(IDeviceAssignment assignment) throws SmartThingException {
	return getDeviceByHardwareId(assignment.getDeviceHardwareId());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.DeviceManagementDecorator#getSiteForAssignment(com.
     * sitewhere .spi.device.IDeviceAssignment)
     */
    @Override
    public ISite getSiteForAssignment(IDeviceAssignment assignment) throws SmartThingException {
	return getSiteByToken(assignment.getSiteToken());
    }
}