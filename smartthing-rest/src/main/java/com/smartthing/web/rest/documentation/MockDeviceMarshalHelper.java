/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.web.rest.documentation;

import com.smartthing.device.marshaling.DeviceAssignmentMarshalHelper;
import com.smartthing.device.marshaling.DeviceMarshalHelper;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.tenant.ITenant;

/**
 * Example marshaler that always returns the same device.
 * 
 * @author Derek s
 */
public class MockDeviceMarshalHelper extends DeviceMarshalHelper {

    /** Mock device management imlementation that wraps sample data */
    private MockDeviceManagement deviceManagement = new MockDeviceManagement();

    /** Mock device assignment marshal helper */
    private MockDeviceAssignmentMarshalHelper assignmentHelper;

    public MockDeviceMarshalHelper() {
	super(null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.marshaling.DeviceMarshalHelper#getDeviceManagement(
     * com.smartthing .spi.user.ITenant)
     */
    @Override
    protected IDeviceManagement getDeviceManagement(ITenant tenant) throws SmartThingException {
	return deviceManagement;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.marshaling.DeviceMarshalHelper#getAssignmentHelper()
     */
    @Override
    protected DeviceAssignmentMarshalHelper getAssignmentHelper() {
	if (assignmentHelper == null) {
	    assignmentHelper = new MockDeviceAssignmentMarshalHelper();
	    assignmentHelper.setIncludeAsset(false);
	    assignmentHelper.setIncludeDevice(false);
	    assignmentHelper.setIncludeSite(false);
	}
	return assignmentHelper;
    }
}