/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.web.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartthing.SmartThing;
import com.smartthing.rest.ISmartThingWebConstants;
import com.smartthing.security.LoginManager;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.error.ResourceExistsException;
import com.smartthing.spi.server.lifecycle.LifecycleStatus;
import com.smartthing.spi.server.tenant.ISmartThingTenantEngine;
import com.smartthing.spi.tenant.ITenant;
import com.smartthing.spi.tenant.TenantNotAvailableException;
import com.smartthing.spi.user.SiteWhereAuthority;

/**
 * Base class for common REST controller functionality.
 * 
 * @author Derek Adams
 */
public class RestController {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /**
     * Get a tenant based on the authentication token passed. Assume that the
     * current user should be validated for access to the given tenant.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    protected ITenant getTenant(HttpServletRequest request) throws SmartThingException {
	return getTenant(request, true);
    }

    /**
     * Get a tenant based on the authentication token passed.
     * 
     * @param request
     * @param checkAuthUser
     * @return
     * @throws SmartThingException
     */
    protected ITenant getTenant(HttpServletRequest request, boolean checkAuthUser) throws SmartThingException {
	String token = getTenantAuthToken(request);
	ITenant match =SmartThing.getServer().getTenantByAuthToken(token);
	if (match == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidTenantAuthToken, ErrorLevel.ERROR);
	}
	ISmartThingTenantEngine engine =SmartThing.getServer().getTenantEngine(match.getId());
	if (engine == null) {
	    LOGGER.error("No tenant engine for tenant: " + match.getName());
	    throw new TenantNotAvailableException();
	}
	if (engine.getEngineState().getLifecycleStatus() == LifecycleStatus.InitializationError) {
	    LOGGER.error("Engine not initialized for tenant: " + match.getName());
	    throw new TenantNotAvailableException();
	}

	if (checkAuthUser) {
	    String username = LoginManager.getCurrentlyLoggedInUser().getUsername();
	    if (match.getAuthorizedUserIds().contains(username)) {
		return match;
	    }
	    throw new SmartThingSystemException(ErrorCode.NotAuthorizedForTenant, ErrorLevel.ERROR);
	} else {
	    return match;
	}
    }

    /**
     * Get tenant authentication token from the servlet request.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    protected String getTenantAuthToken(HttpServletRequest request) throws SmartThingException {
	String token = request.getHeader(ISmartThingWebConstants.HEADER_TENANT_TOKEN);
	if (token == null) {
	    token = request.getParameter(ISmartThingWebConstants.REQUEST_TENANT_TOKEN);
	    if (token == null) {
		throw new SmartThingSystemException(ErrorCode.MissingTenantAuthToken, ErrorLevel.ERROR,
			HttpServletResponse.SC_UNAUTHORIZED);
	    }
	}
	return token;
    }

    /**
     * Send message back to called indicating successful add.
     * 
     * @param response
     */
    protected void handleSuccessfulAdd(HttpServletResponse response) {
	response.setStatus(HttpServletResponse.SC_CREATED);
	try {
	    response.flushBuffer();
	} catch (IOException e) {
	    // Ignore failed flush.
	}
    }

    /**
     * Handles a system exception by setting the HTML response code and response
     * headers.
     * 
     * @param e
     * @param response
     */
    @ExceptionHandler
    protected void handleSystemException(SmartThingException e, HttpServletRequest request,
	    HttpServletResponse response) {
	try {
	    String flexMode = request.getHeader("X-SiteWhere-Error-Mode");
	    if (flexMode != null) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(response.getOutputStream(), e);
		response.flushBuffer();
	    } else {
		if (e instanceof SmartThingSystemException) {
		    SmartThingSystemException sse = (SmartThingSystemException) e;
		    String combined = sse.getCode() + ":" + e.getMessage();
		    response.setHeader(ISmartThingWebConstants.HEADER_SMARTTHING_ERROR, e.getMessage());
		    response.setHeader(ISmartThingWebConstants.HEADER_SMARTTHING_ERROR_CODE,
			    String.valueOf(sse.getCode()));
		    if (sse.hasHttpResponseCode()) {
			response.sendError(sse.getHttpResponseCode(), combined);
		    } else {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, combined);
		    }
		} else {
		    response.setHeader(ISmartThingWebConstants.HEADER_SMARTTHING_ERROR, e.getMessage());
		    response.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
		}
		LOGGER.error("Exception thrown during REST processing.", e);
	    }
	} catch (IOException ioe) {
	    LOGGER.error("Error handling system exception.", ioe);
	} catch (Throwable t) {
	    LOGGER.error("Error handling system exception.", t);
	}
    }

    /**
     * Handles uncaught runtime exceptions such as null pointers.
     * 
     * @param e
     * @param response
     */
    @ExceptionHandler
    protected void handleRuntimeException(RuntimeException e, HttpServletResponse response) {
	LOGGER.error("Unhandled runtime exception.", e);
	try {
	    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    LOGGER.error("Unhandled runtime exception.", e);
	} catch (IOException e1) {
	    e1.printStackTrace();
	}
    }

    /**
     * Handles exception thrown when a tenant operation is requested on an
     * unavailable tenant.
     * 
     * @param e
     * @param response
     */
    @ExceptionHandler
    protected void handleTenantNotAvailable(TenantNotAvailableException e, HttpServletResponse response) {
	LOGGER.error("Operation invoked on unavailable tenant.", e);
	try {
	    response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The requested tenant is not available.");
	} catch (IOException e1) {
	    LOGGER.error(e1);
	}
    }

    /**
     * Handles exceptions where a new resource is to be created, but an existing
     * resource exists with the given key.
     * 
     * @param e
     * @param response
     */
    @ExceptionHandler
    protected void handleResourceExists(ResourceExistsException e, HttpServletResponse response) {
	try {
	    sendErrorResponse(e, e.getCode(), HttpServletResponse.SC_CONFLICT, response);
	    LOGGER.error("Resource with same key already exists.", e);
	} catch (IOException e1) {
	    e1.printStackTrace();
	}
    }

    /**
     * Send error response including SiteWhere headers.
     * 
     * @param e
     * @param errorCode
     * @param responseCode
     * @param response
     * @throws IOException
     */
    protected void sendErrorResponse(Exception e, ErrorCode errorCode, int responseCode, HttpServletResponse response)
	    throws IOException {
	response.setHeader(ISmartThingWebConstants.HEADER_SMARTTHING_ERROR, errorCode.getMessage());
	response.setHeader(ISmartThingWebConstants.HEADER_SMARTTHING_ERROR_CODE, String.valueOf(errorCode.getCode()));
	response.sendError(responseCode, errorCode.getMessage());
    }

    /**
     * Handles exceptions generated if {@link Secured} annotations are not
     * satisfied.
     * 
     * @param e
     * @param response
     */
    @ExceptionHandler
    protected void handleAccessDenied(AccessDeniedException e, HttpServletResponse response) {
	try {
	    response.sendError(HttpServletResponse.SC_FORBIDDEN);
	    LOGGER.error("Access denied.", e);
	} catch (IOException e1) {
	    e1.printStackTrace();
	}
    }

    /**
     * Handles situations where user does not pass exprected content for a POST.
     * 
     * @param e
     * @param response
     */
    @ExceptionHandler
    protected void handleMissingContent(HttpMessageNotReadableException e, HttpServletResponse response) {
	try {
	    LOGGER.error("Error handling REST request..", e);
	    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "No body content passed for POST request.");
	} catch (IOException e1) {
	    e1.printStackTrace();
	}
    }

    /**
     * Verifies that requestor has all of the given authorities or throws a
     * "forbidden" error.
     * 
     * @param request
     * @param response
     * @param roles
     * @throws SmartThingException
     */
    public static void checkAuthForAll(HttpServletRequest request, HttpServletResponse response,
	    SiteWhereAuthority... auths) throws SmartThingException {
	for (SiteWhereAuthority auth : auths) {
	    checkAuthFor(request, response, auth, true);
	}
    }

    /**
     * Verifies that requestor has the given authority and can throws a
     * "forbidden" error if not.
     * 
     * @param request
     * @param response
     * @param auth
     * @param throwException
     * @return
     * @throws SmartThingException
     */
    public static boolean checkAuthFor(HttpServletRequest request, HttpServletResponse response,
	    SiteWhereAuthority auth, boolean throwException) throws SmartThingException {
	if (!request.isUserInRole(auth.getRoleName())) {
	    if (throwException) {
		throw new SmartThingSystemException(ErrorCode.OperationNotPermitted, ErrorLevel.ERROR,
			HttpServletResponse.SC_FORBIDDEN);
	    }
	    return false;
	}
	return true;
    }
}