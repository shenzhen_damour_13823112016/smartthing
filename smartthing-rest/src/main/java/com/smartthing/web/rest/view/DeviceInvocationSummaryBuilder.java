/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.web.rest.view;

import java.util.List;

import com.smartthing.SmartThing;
import com.smartthing.rest.model.common.MetadataProvider;
import com.smartthing.rest.model.device.event.DeviceCommandInvocation;
import com.smartthing.rest.model.device.event.view.DeviceCommandInvocationSummary;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.command.ICommandParameter;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceEvent;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.tenant.ITenant;

/**
 * Used to build a {@link DeviceCommandInvocationSummary}.
 * 
 * @author Derek
 */
public class DeviceInvocationSummaryBuilder {

    /**
     * Creates a {@link DeviceCommandInvocationSummary} using data from a
     * {@link DeviceCommandInvocation} that has its command information
     * populated.
     * 
     * @param invocation
     * @param responses
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public static DeviceCommandInvocationSummary build(DeviceCommandInvocation invocation,
	    List<IDeviceCommandResponse> responses, ITenant tenant) throws SmartThingException {
	DeviceCommandInvocationSummary summary = new DeviceCommandInvocationSummary();
	summary.setName(invocation.getCommand().getName());
	summary.setNamespace(invocation.getCommand().getNamespace());
	summary.setInvocationDate(invocation.getEventDate());
	for (ICommandParameter parameter : invocation.getCommand().getParameters()) {
	    DeviceCommandInvocationSummary.Parameter param = new DeviceCommandInvocationSummary.Parameter();
	    param.setName(parameter.getName());
	    param.setType(parameter.getType().name());
	    param.setRequired(parameter.isRequired());
	    param.setValue(invocation.getParameterValues().get(parameter.getName()));
	    summary.getParameters().add(param);
	}
	for (IDeviceCommandResponse response : responses) {
	    DeviceCommandInvocationSummary.Response rsp = new DeviceCommandInvocationSummary.Response();
	    rsp.setDate(response.getEventDate());
	    if (response.getResponseEventId() != null) {
		IDeviceEvent event =SmartThing.getServer().getDeviceEventManagement(tenant)
			.getDeviceEventById(response.getResponseEventId());
		rsp.setDescription(getDeviceEventDescription(event));
	    } else if (response.getResponse() != null) {
		rsp.setDescription("Ack (\"" + response.getResponse() + "\")");
	    } else {
		rsp.setDescription("Response received.");
	    }
	    summary.getResponses().add(rsp);
	}
	MetadataProvider.copy(invocation, summary);
	return summary;
    }

    /**
     * Get a short description of a device event.
     * 
     * @param event
     * @return
     * @throws SmartThingException
     */
    public static String getDeviceEventDescription(IDeviceEvent event) throws SmartThingException {
	if (event instanceof IDeviceMeasurements) {
	    IDeviceMeasurements m = (IDeviceMeasurements) event;
	    return "Measurements (" + m.getMeasurementsSummary() + ")";
	} else if (event instanceof IDeviceLocation) {
	    IDeviceLocation l = (IDeviceLocation) event;
	    return "Location (" + l.getLatitude() + "/" + l.getLongitude() + "/" + l.getElevation() + ")";
	} else if (event instanceof IDeviceAlert) {
	    IDeviceAlert a = (IDeviceAlert) event;
	    return "Alert (\"" + a.getMessage() + "\")";
	}
	return "Unknown Event Type";
    }
}