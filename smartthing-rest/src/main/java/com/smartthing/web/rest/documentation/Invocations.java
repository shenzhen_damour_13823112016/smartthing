/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.web.rest.documentation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.smartthing.rest.model.device.event.DeviceCommandInvocation;
import com.smartthing.rest.model.search.SearchResults;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.web.rest.view.DeviceInvocationSummaryBuilder;

/**
 * Example of REST requests for interacting with device command invocations.
 * 
 * @author Derek
 */
public class Invocations {

    public static class GetDeviceCommandInvocationSummary {

	public Object generate() throws SmartThingException {
	    MockDeviceCommandInvocationMarshalHelper helper = new MockDeviceCommandInvocationMarshalHelper();
	    helper.setIncludeCommand(true);
	    DeviceCommandInvocation converted = helper.convert(ExampleData.INVOCATION_GET_FW_VER);
	    List<IDeviceCommandResponse> list = Arrays
		    .asList(new IDeviceCommandResponse[] { ExampleData.RESPONSE_GET_FW_VER });
	    ISearchResults<IDeviceCommandResponse> responses = new SearchResults<IDeviceCommandResponse>(list, 1);
	    return DeviceInvocationSummaryBuilder.build(converted, responses.getResults(), null);
	}
    }

    public static class GetDeviceCommandInvocationResponsesResponse {

	public Object generate() throws SmartThingException {
	    List<IDeviceCommandResponse> list = new ArrayList<IDeviceCommandResponse>();
	    list.add(ExampleData.RESPONSE_GET_FW_VER);
	    return new SearchResults<IDeviceCommandResponse>(list, 1);
	}
    }
}