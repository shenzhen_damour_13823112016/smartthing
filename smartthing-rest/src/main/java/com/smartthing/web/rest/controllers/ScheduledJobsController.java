/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.web.rest.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smartthing.SmartThing;
import com.smartthing.Tracer;
import com.smartthing.rest.model.scheduling.request.ScheduledJobCreateRequest;
import com.smartthing.rest.model.search.SearchCriteria;
import com.smartthing.rest.model.search.SearchResults;
import com.smartthing.server.scheduling.ScheduledJobMarshalHelper;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.scheduling.IScheduleManagement;
import com.smartthing.spi.scheduling.IScheduledJob;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.server.debug.TracerCategory;
import com.smartthing.spi.user.SiteWhereRoles;
import com.smartthing.web.rest.RestController;
import com.smartthing.web.rest.annotations.Concerns;
import com.smartthing.web.rest.annotations.Concerns.ConcernType;
import com.smartthing.web.rest.annotations.Documented;
import com.smartthing.web.rest.annotations.DocumentedController;
import com.smartthing.web.rest.annotations.Example;
import com.smartthing.web.rest.annotations.Example.Stage;
import com.smartthing.web.rest.documentation.Schedules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * Controller for scheduled jobs.
 * 
 * @author Derek Adams
 */
@Controller
@CrossOrigin(exposedHeaders = { "X-SiteWhere-Error", "X-SiteWhere-Error-Code" })
@RequestMapping(value = "/jobs")
@Api(value = "jobs", description = "Operations related to SiteWhere scheduled jobs.")
@DocumentedController(name = "Scheduled Jobs")
public class ScheduledJobsController extends RestController {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /**
     * Create a new scheduled job.
     * 
     * @param request
     * @param servletRequest
     * @return
     * @throws SmartThingException
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "Create new scheduled job")
    @Secured({ SiteWhereRoles.REST })
    @Documented(examples = {
	    @Example(stage = Stage.Request, json = Schedules.CreateScheduledJobRequest.class, description = "createScheduledJobRequest.md"),
	    @Example(stage = Stage.Response, json = Schedules.CreateScheduledJobResponse.class, description = "createScheduledJobResponse.md") })
    public IScheduledJob createScheduledJob(@RequestBody ScheduledJobCreateRequest request,
	    HttpServletRequest servletRequest) throws SmartThingException {
	Tracer.start(TracerCategory.RestApiCall, "createScheduledJob", LOGGER);
	try {
	    return getScheduleManagement(servletRequest).createScheduledJob(request);
	} finally {
	    Tracer.stop(LOGGER);
	}
    }

    @RequestMapping(value = "/{token}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get scheduled job by token")
    @Secured({ SiteWhereRoles.REST })
    @Documented(examples = {
	    @Example(stage = Stage.Response, json = Schedules.CreateScheduledJobResponse.class, description = "getScheduledJobByTokenResponse.md") })
    public IScheduledJob getScheduledJobByToken(@ApiParam(value = "Token", required = true) @PathVariable String token,
	    HttpServletRequest servletRequest) throws SmartThingException {
	Tracer.start(TracerCategory.RestApiCall, "getScheduledJobByToken", LOGGER);
	try {
	    return getScheduleManagement(servletRequest).getScheduledJobByToken(token);
	} finally {
	    Tracer.stop(LOGGER);
	}
    }

    /**
     * Update an existing scheduled job.
     * 
     * @param request
     * @param token
     * @param servletRequest
     * @return
     * @throws SmartThingException
     */
    @RequestMapping(value = "/{token}", method = RequestMethod.PUT)
    @ResponseBody
    @ApiOperation(value = "Update existing scheduled job")
    @Secured({ SiteWhereRoles.REST })
    @Documented(examples = {
	    @Example(stage = Stage.Request, json = Schedules.UpdateScheduledJobRequest.class, description = "updateScheduledJobRequest.md"),
	    @Example(stage = Stage.Response, json = Schedules.UpdateScheduledJobResponse.class, description = "updateScheduledJobResponse.md") })
    public IScheduledJob updateScheduledJob(@RequestBody ScheduledJobCreateRequest request,
	    @ApiParam(value = "Token", required = true) @PathVariable String token, HttpServletRequest servletRequest)
	    throws SmartThingException {
	Tracer.start(TracerCategory.RestApiCall, "updateScheduledJob", LOGGER);
	try {
	    return getScheduleManagement(servletRequest).updateScheduledJob(token, request);
	} finally {
	    Tracer.stop(LOGGER);
	}
    }

    /**
     * List scheduled jobs that match the criteria.
     * 
     * @param includeContext
     * @param page
     * @param pageSize
     * @param servletRequest
     * @return
     * @throws SmartThingException
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "List scheduled jobs matching criteria")
    @Secured({ SiteWhereRoles.REST })
    @Documented(examples = {
	    @Example(stage = Stage.Response, json = Schedules.ListScheduledjobsResponse.class, description = "listScheduledJobsResponse.md") })
    public ISearchResults<IScheduledJob> listScheduledJobs(
	    @ApiParam(value = "Include context information", required = false) @RequestParam(defaultValue = "false") boolean includeContext,
	    @ApiParam(value = "Page number", required = false) @RequestParam(required = false, defaultValue = "1") @Concerns(values = {
		    ConcernType.Paging }) int page,
	    @ApiParam(value = "Page size", required = false) @RequestParam(required = false, defaultValue = "100") @Concerns(values = {
		    ConcernType.Paging }) int pageSize,
	    HttpServletRequest servletRequest) throws SmartThingException {
	Tracer.start(TracerCategory.RestApiCall, "listScheduledJobs", LOGGER);
	try {
	    SearchCriteria criteria = new SearchCriteria(page, pageSize);
	    ISearchResults<IScheduledJob> results = getScheduleManagement(servletRequest).listScheduledJobs(criteria);
	    if (!includeContext) {
		return results;
	    } else {
		List<IScheduledJob> converted = new ArrayList<IScheduledJob>();
		ScheduledJobMarshalHelper helper = new ScheduledJobMarshalHelper(getTenant(servletRequest), true);
		for (IScheduledJob job : results.getResults()) {
		    converted.add(helper.convert(job));
		}
		return new SearchResults<IScheduledJob>(converted, results.getNumResults());
	    }
	} finally {
	    Tracer.stop(LOGGER);
	}
    }

    /**
     * Delete an existing scheduled job.
     * 
     * @param token
     * @param force
     * @param servletRequest
     * @return
     * @throws SmartThingException
     */
    @RequestMapping(value = "/{token}", method = RequestMethod.DELETE)
    @ResponseBody
    @ApiOperation(value = "Delete scheduled job")
    @Secured({ SiteWhereRoles.REST })
    @Documented(examples = {
	    @Example(stage = Stage.Response, json = Schedules.CreateScheduledJobResponse.class, description = "deleteScheduledJobResponse.md") })
    public IScheduledJob deleteScheduledJob(@ApiParam(value = "Token", required = true) @PathVariable String token,
	    @ApiParam(value = "Delete permanently", required = false) @RequestParam(defaultValue = "false") boolean force,
	    HttpServletRequest servletRequest) throws SmartThingException {
	Tracer.start(TracerCategory.RestApiCall, "deleteScheduledJob", LOGGER);
	try {
	    return getScheduleManagement(servletRequest).deleteScheduledJob(token, force);
	} finally {
	    Tracer.stop(LOGGER);
	}
    }

    /**
     * Get the schedule management implementation for the current tenant.
     * 
     * @param servletRequest
     * @return
     * @throws SmartThingException
     */
    protected IScheduleManagement getScheduleManagement(HttpServletRequest servletRequest) throws SmartThingException {
	return SmartThing.getServer().getScheduleManagement(getTenant(servletRequest));
    }
}