/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.web.rest.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smartthing.SmartThing;
import com.smartthing.Tracer;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.ISmartThingServerRuntime;
import com.smartthing.spi.server.debug.TracerCategory;
import com.smartthing.spi.system.IVersion;
import com.smartthing.spi.user.SiteWhereRoles;
import com.smartthing.web.rest.RestController;
import com.smartthing.web.rest.annotations.Documented;
import com.smartthing.web.rest.annotations.DocumentedController;
import com.smartthing.web.rest.annotations.Example;
import com.smartthing.web.rest.annotations.Example.Stage;
import com.smartthing.web.rest.documentation.SystemInfo;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * Controller for system operations.
 * 
 * @author Derek Adams
 */
@Controller
@CrossOrigin(exposedHeaders = { "X-SiteWhere-Error", "X-SiteWhere-Error-Code" })
@RequestMapping(value = "/system")
@Api(value = "system", description = "Operations related to SiteWhere CE system management.")
@DocumentedController(name = "System Information")
public class SystemController extends RestController {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /**
     * Get version information about the server.
     * 
     * @return
     * @throws SmartThingException
     */
    @RequestMapping(value = "/version", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get version information")
    @Secured({ SiteWhereRoles.REST })
    @Documented(examples = {
	    @Example(stage = Stage.Response, json = SystemInfo.GetVersionResponse.class, description = "getVersionResponse.md") })
    public IVersion getVersion() throws SmartThingException {
	Tracer.start(TracerCategory.RestApiCall, "getVersion", LOGGER);
	try {
	    return SmartThing.getServer().getVersion();
	} finally {
	    Tracer.stop(LOGGER);
	}
    }

    /**
     * Get runtime information about the server.
     * 
     * @return
     * @throws SmartThingException
     */
    @RequestMapping(value = "/runtime", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get server runtime information")
    @Secured({ SiteWhereRoles.REST })
    @Documented(examples = {
	    @Example(stage = Stage.Response, json = SystemInfo.GetServerRuntimeResponse.class, description = "getServerStateResponse.md") })
    public ISmartThingServerRuntime getServerRuntimeInformation() throws SmartThingException {
	Tracer.start(TracerCategory.RestApiCall, "getServerRuntimeInformation", LOGGER);
	try {
	    return SmartThing.getServer().getServerRuntimeInformation(true);
	} finally {
	    Tracer.stop(LOGGER);
	}
    }
}