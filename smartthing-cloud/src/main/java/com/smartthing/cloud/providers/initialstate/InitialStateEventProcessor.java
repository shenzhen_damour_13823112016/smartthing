/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.SmartThing.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.cloud.providers.initialstate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.smartthing.SmartThing;
import com.smartthing.device.event.processor.FilteredOutboundEventProcessor;
import com.smartthing.device.marshaling.DeviceAssignmentMarshalHelper;
import com.smartthing.rest.model.device.DeviceAssignment;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.processor.IOutboundEventProcessor;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;

/**
 * Implmentation of {@link IOutboundEventProcessor} that sends events to the
 * cloud provider at InitialState.com.
 * 
 * @author Derek
 */
public class InitialStateEventProcessor extends FilteredOutboundEventProcessor {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Base URI for REST calls */
    private static final String API_BASE = "https://groker.initialstate.com/api/";

    /** Header name for access key */
    private static final String HEADER_ACCESS_KEY = "X-IS-AccessKey";

    /** Header name for bucket key */
    private static final String HEADER_BUCKET_KEY = "X-IS-BucketKey";

    /** Use Spring RestTemplate to send requests */
    private RestTemplate client;

    /** Account-specific key for using streaming APIs */
    private String streamingAccessKey;

    /** Cache of assignment tokens to detailed assignment information */
    private Map<String, DeviceAssignment> assignmentsByToken = new HashMap<String, DeviceAssignment>();

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.event.processor.FilteredOutboundEventProcessor#start
     * (com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Required for filters.
	super.start(monitor);

	this.client = new RestTemplate();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onMeasurementsNotFiltered(com.smartthing.spi.device.event.
     * IDeviceMeasurements)
     */
    @Override
    public void onMeasurementsNotFiltered(IDeviceMeasurements measurements) throws SmartThingException {
	Map<String, Double> mx = measurements.getMeasurements();
	List<EventCreateRequest> events = new ArrayList<EventCreateRequest>();
	for (String name : mx.keySet()) {
	    EventCreateRequest event = new EventCreateRequest();
	    event.setKey(name);
	    event.setValue(String.valueOf(mx.get(name)));
	    event.setEpoch(((double) System.currentTimeMillis()) / ((double) 1000));
	    events.add(event);
	}
	DeviceAssignment assignment = assureBucket(measurements.getDeviceAssignmentToken());
	createEvents(assignment.getToken(), events);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * onLocationNotFiltered(com.smartthing.spi.device.event.IDeviceLocation)
     */
    @Override
    public void onLocationNotFiltered(IDeviceLocation location) throws SmartThingException {
	List<EventCreateRequest> events = new ArrayList<EventCreateRequest>();

	EventCreateRequest event = new EventCreateRequest();
	event.setKey("location");
	event.setValue("" + location.getLatitude() + "," + location.getLongitude());
	event.setEpoch(((double) System.currentTimeMillis()) / ((double) 1000));
	events.add(event);

	DeviceAssignment assignment = assureBucket(location.getDeviceAssignmentToken());
	createEvents(assignment.getToken(), events);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.processor.IFilteredOutboundEventProcessor#
     * onAlertNotFiltered(com.smartthing.spi.device.event.IDeviceAlert)
     */
    @Override
    public void onAlertNotFiltered(IDeviceAlert alert) throws SmartThingException {
	List<EventCreateRequest> events = new ArrayList<EventCreateRequest>();

	EventCreateRequest event = new EventCreateRequest();
	event.setKey(alert.getType());
	event.setValue(alert.getMessage());
	event.setEpoch(((double) System.currentTimeMillis()) / ((double) 1000));
	events.add(event);

	DeviceAssignment assignment = assureBucket(alert.getDeviceAssignmentToken());
	createEvents(assignment.getToken(), events);
    }

    /**
     * Assure that a bucket is created for the given assignment.
     * 
     * @param assignmentToken
     * @return
     * @throws SmartThingException
     */
    protected DeviceAssignment assureBucket(String assignmentToken) throws SmartThingException {
	DeviceAssignment cached = assignmentsByToken.get(assignmentToken);
	if (cached != null) {
	    return cached;
	}
	IDeviceAssignment assignment = SmartThing.getServer().getDeviceManagement(getTenant())
		.getDeviceAssignmentByToken(assignmentToken);
	if (assignment == null) {
	    throw new SmartThingException("Assignment not found.");
	}

	DeviceAssignmentMarshalHelper helper = new DeviceAssignmentMarshalHelper(getTenant());
	helper.setIncludeAsset(false);
	helper.setIncludeDevice(true);
	helper.setIncludeSite(false);
	DeviceAssignment converted = helper.convert(assignment,
		SmartThing.getServer().getAssetModuleManager(getTenant()));

	createBucket(converted.getToken(),
		converted.getAssetName() + " (" + converted.getDevice().getAssetName() + ")");
	assignmentsByToken.put(assignmentToken, converted);
	return converted;
    }

    /**
     * Create a new InitialState bucket. Returns true if created, false if it
     * already existed, and throws and exception on error.
     * 
     * @param bucketKey
     * @param bucketName
     * @return
     * @throws SmartThingException
     */
    protected boolean createBucket(String bucketKey, String bucketName) throws SmartThingException {
	try {
	    BucketCreateRequest request = new BucketCreateRequest();
	    request.setBucketKey(bucketKey);
	    request.setBucketName(bucketName);

	    HttpHeaders headers = new HttpHeaders();
	    headers.add(HEADER_ACCESS_KEY, getStreamingAccessKey());
	    HttpEntity<BucketCreateRequest> entity = new HttpEntity<BucketCreateRequest>(request, headers);
	    String url = API_BASE + "buckets";
	    Map<String, String> vars = new HashMap<String, String>();
	    ResponseEntity<String> response = getClient().exchange(url, HttpMethod.POST, entity, String.class, vars);
	    if (response.getStatusCode() == HttpStatus.CREATED) {
		return true;
	    }
	    if (response.getStatusCode() == HttpStatus.NO_CONTENT) {
		return false;
	    }
	    throw new SmartThingException("Unable to create bucket. Status code was: " + response.getStatusCode());
	} catch (ResourceAccessException e) {
	    if (e.getCause() instanceof SmartThingSystemException) {
		throw (SmartThingSystemException) e.getCause();
	    }
	    throw new RuntimeException(e);
	}
    }

    /**
     * Send a batch of events to the given bucket.
     * 
     * @param bucketKey
     * @param events
     * @return
     * @throws SmartThingException
     */
    protected boolean createEvents(String bucketKey, List<EventCreateRequest> events) throws SmartThingException {
	try {
	    HttpHeaders headers = new HttpHeaders();
	    headers.add(HEADER_ACCESS_KEY, getStreamingAccessKey());
	    headers.add(HEADER_BUCKET_KEY, bucketKey);
	    HttpEntity<List<EventCreateRequest>> entity = new HttpEntity<List<EventCreateRequest>>(events, headers);
	    String url = API_BASE + "events";
	    Map<String, String> vars = new HashMap<String, String>();
	    ResponseEntity<String> response = getClient().exchange(url, HttpMethod.POST, entity, String.class, vars);
	    if (response.getStatusCode() == HttpStatus.NO_CONTENT) {
		return true;
	    }
	    throw new SmartThingException("Unable to create event. Status code was: " + response.getStatusCode());
	} catch (ResourceAccessException e) {
	    throw new SmartThingException(e);
	}
    }

    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    public RestTemplate getClient() {
	return client;
    }

    public void setClient(RestTemplate client) {
	this.client = client;
    }

    public String getStreamingAccessKey() {
	return streamingAccessKey;
    }

    public void setStreamingAccessKey(String streamingAccessKey) {
	this.streamingAccessKey = streamingAccessKey;
    }
}