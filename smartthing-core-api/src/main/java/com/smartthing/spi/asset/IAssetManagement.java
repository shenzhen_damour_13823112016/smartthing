/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.asset;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.request.IAssetCategoryCreateRequest;
import com.smartthing.spi.asset.request.IHardwareAssetCreateRequest;
import com.smartthing.spi.asset.request.ILocationAssetCreateRequest;
import com.smartthing.spi.asset.request.IPersonAssetCreateRequest;
import com.smartthing.spi.search.ISearchCriteria;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Asset management interface implemented by datastores that can store assets.
 *
 * @author Derek
 */
public interface IAssetManagement extends ITenantLifecycleComponent {

    /**
     * Create a new asset category.
     *
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IAssetCategory createAssetCategory(IAssetCategoryCreateRequest request) throws SmartThingException;

    /**
     * Get an asset category by id.
     *
     * @param categoryId
     * @return
     * @throws SmartThingException
     */
    public IAssetCategory getAssetCategory(String categoryId) throws SmartThingException;

    /**
     * Update an existing asset category.
     *
     * @param categoryId
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IAssetCategory updateAssetCategory(String categoryId, IAssetCategoryCreateRequest request)
	    throws SmartThingException;

    /**
     * List asset categories.
     *
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IAssetCategory> listAssetCategories(ISearchCriteria criteria) throws SmartThingException;

    /**
     * Delete an asset category and all contained assets.
     *
     * @param categoryId
     * @return
     * @throws SmartThingException
     */
    public IAssetCategory deleteAssetCategory(String categoryId) throws SmartThingException;

    /**
     * Create a new {@link IPersonAsset}.
     *
     * @param categoryId
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IPersonAsset createPersonAsset(String categoryId, IPersonAssetCreateRequest request)
	    throws SmartThingException;

    /**
     * Update an existing person asset.
     *
     * @param categoryId
     * @param assetId
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IPersonAsset updatePersonAsset(String categoryId, String assetId, IPersonAssetCreateRequest request)
	    throws SmartThingException;

    /**
     * Create a new {@link IHardwareAsset}.
     *
     * @param categoryId
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IHardwareAsset createHardwareAsset(String categoryId, IHardwareAssetCreateRequest request)
	    throws SmartThingException;

    /**
     * Update an existing hardware asset.
     *
     * @param categoryId
     * @param assetId
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IHardwareAsset updateHardwareAsset(String categoryId, String assetId, IHardwareAssetCreateRequest request)
	    throws SmartThingException;

    /**
     * Create a new {@link ILocationAsset}.
     *
     * @param categoryId
     * @param request
     * @return
     * @throws SmartThingException
     */
    public ILocationAsset createLocationAsset(String categoryId, ILocationAssetCreateRequest request)
	    throws SmartThingException;

    /**
     * Update an existing location asset.
     *
     * @param categoryId
     * @param assetId
     * @param request
     * @return
     * @throws SmartThingException
     */
    public ILocationAsset updateLocationAsset(String categoryId, String assetId, ILocationAssetCreateRequest request)
	    throws SmartThingException;

    /**
     * Get the asset for the given id in the given category.
     *
     * @param categoryId
     * @param assetId
     * @return
     * @throws SmartThingException
     */
    public IAsset getAsset(String categoryId, String assetId) throws SmartThingException;

    /**
     * Delete the asset with the given id in the given category.
     *
     * @param categoryId
     * @param assetId
     * @return
     * @throws SmartThingException
     */
    public IAsset deleteAsset(String categoryId, String assetId) throws SmartThingException;

    /**
     * List assets in a category that meet the given criteria.
     *
     * @param categoryId
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IAsset> listAssets(String categoryId, ISearchCriteria criteria) throws SmartThingException;
/*=====================================================================================================================================*/
    /**
     * List assets in a category that meet the given criteria.
     *
     * @param categoryId
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IAsset> listAssetsByArange(String categoryId, String arange, ISearchCriteria criteria) throws SmartThingException;
/*=====================================================================================================================================*/
}
