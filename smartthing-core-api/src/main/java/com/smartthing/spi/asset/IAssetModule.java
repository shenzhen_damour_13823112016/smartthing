/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.asset;

import java.util.List;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.command.ICommandResponse;
import com.smartthing.spi.server.lifecycle.ILifecycleComponent;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;

/**
 * Interface for a module that provides access to assets of a given type.
 * 
 * @author dadams
 */
public interface IAssetModule<T extends IAsset> extends ILifecycleComponent, IAssetCategory {

    /**
     * Get an asset by unique id.
     * 
     * @param id
     * @return
     * @throws SmartThingException
     */
    public T getAsset(String id) throws SmartThingException;

    /**
     * Create or update an asset.
     * 
     * @param id
     * @param asset
     * @throws SmartThingException
     */
    public void putAsset(String id, T asset) throws SmartThingException;

    /**
     * Remove an existing asset.
     * 
     * @param id
     * @throws SmartThingException
     */
    public void removeAsset(String id) throws SmartThingException;

    /**
     * Search for all assets of a given type that meet the criteria.
     * 
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public List<T> search(String criteria) throws SmartThingException;

    /**
     * Refresh any cached data in the module.
     * 
     * @param monitor
     * @return
     * @throws SmartThingException
     */
    public ICommandResponse refresh(ILifecycleProgressMonitor monitor) throws SmartThingException;
}