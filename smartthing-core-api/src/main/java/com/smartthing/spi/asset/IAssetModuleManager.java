/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.asset;

import java.util.List;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Interface for interacting with the asset module manager.
 * 
 * @author dadams
 */
public interface IAssetModuleManager extends ITenantLifecycleComponent {

    /**
     * Set asset management implementation.
     * 
     * @param assetManagement
     */
    public void setAssetManagement(IAssetManagement assetManagement);

    /**
     * Get asset module by unique id.
     * 
     * @param assetModuleId
     * @return
     * @throws SmartThingException
     */
    public IAssetModule<?> getModule(String assetModuleId) throws SmartThingException;

    /**
     * Get the list of asset modules.
     * 
     * @return
     * @throws SmartThingException
     */
    public List<IAssetModule<?>> listModules() throws SmartThingException;

    /**
     * Calls the refresh method on all asset modules.
     * 
     * @param monitor
     * @throws SmartThingException
     */
    public void refreshModules(ILifecycleProgressMonitor monitor) throws SmartThingException;

    /**
     * Called when an asset category is added.
     * 
     * @param category
     * @param monitor
     * @throws SmartThingException
     */
    public void onAssetCategoryAdded(IAssetCategory category, ILifecycleProgressMonitor monitor)
	    throws SmartThingException;

    /**
     * Called when an asset category is updated.
     * 
     * @param category
     * @param monitor
     * @throws SmartThingException
     */
    public void onAssetCategoryUpdated(IAssetCategory category, ILifecycleProgressMonitor monitor)
	    throws SmartThingException;

    /**
     * Called when an asset category is removed.
     * 
     * @param category
     * @param monitor
     * @throws SmartThingException
     */
    public void onAssetCategoryRemoved(IAssetCategory category, ILifecycleProgressMonitor monitor)
	    throws SmartThingException;

    /**
     * Finds an asset in a given module.
     * 
     * @param assetModuleId
     * @param id
     * @return
     * @throws SmartThingException
     */
    public IAsset getAssetById(String assetModuleId, String id) throws SmartThingException;

    /**
     * Search an asset module for assets matching the given criteria.
     * 
     * @param assetModuleId
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public List<? extends IAsset> search(String assetModuleId, String criteria) throws SmartThingException;
}