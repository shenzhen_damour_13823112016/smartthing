package com.smartthing.spi.monitoring;

import com.smartthing.spi.error.ErrorLevel;

/**
 * Extends {@link IProgressMessage} to add error information.
 * 
 * @author Derek
 */
public interface IProgressErrorMessage extends IProgressMessage {

    /**
     * Get error level associated with message.
     * 
     * @return
     */
    public ErrorLevel getLevel();
}