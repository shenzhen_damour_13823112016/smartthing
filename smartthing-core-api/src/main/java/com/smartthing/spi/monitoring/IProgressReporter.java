package com.smartthing.spi.monitoring;

import com.smartthing.spi.SmartThingException;

/**
 * Allows long-running tasks to report their progress.
 * 
 * @author Derek
 */
public interface IProgressReporter {

    /**
     * Report progress for an operation.
     * 
     * @param message
     * @throws SmartThingException
     */
    public void reportProgress(IProgressMessage message) throws SmartThingException;

    /**
     * Report that an error occurred in a monitored operation.
     * 
     * @param error
     * @throws SmartThingException
     */
    public void reportError(IProgressErrorMessage error) throws SmartThingException;
}