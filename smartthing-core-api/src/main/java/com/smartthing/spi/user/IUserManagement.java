/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.user;

import java.util.List;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.lifecycle.ILifecycleComponent;
import com.smartthing.spi.user.request.IGrantedAuthorityCreateRequest;
import com.smartthing.spi.user.request.IUserCreateRequest;

/**
 * Interface for user management operations.
 * 
 * @author Derek
 */
public interface IUserManagement extends ILifecycleComponent {

    /**
     * Create a new user based on the given input.
     * 
     * @param request
     * @param encodePassword
     * @return
     * @throws SmartThingException
     */
    public IUser createUser(IUserCreateRequest request, boolean encodePassword) throws SmartThingException;

    /**
     * Imports a user (including encrypted password) from an external system.
     * 
     * @param user
     * @param overwrite
     * @return
     * @throws SmartThingException
     */
    public IUser importUser(IUser user, boolean overwrite) throws SmartThingException;

    /**
     * Authenticate the given username and password.
     * 
     * @param username
     * @param password
     * @param updateLastLogin
     * @return
     * @throws SmartThingException
     */
    public IUser authenticate(String username, String password, boolean updateLastLogin) throws SmartThingException;

    /**
     * Update details for a user.
     * 
     * @param username
     * @param request
     * @param encodePassword
     * @return
     * @throws SmartThingException
     */
    public IUser updateUser(String username, IUserCreateRequest request, boolean encodePassword)
	    throws SmartThingException;

    /**
     * Get a user given unique username.
     * 
     * @param username
     * @return
     * @throws SmartThingException
     */
    public IUser getUserByUsername(String username) throws SmartThingException;

    /**
     * Get the granted authorities for a specific user. Does not include any
     * authorities inherited from groups.
     * 
     * @param username
     * @return
     * @throws SmartThingException
     */
    public List<IGrantedAuthority> getGrantedAuthorities(String username) throws SmartThingException;

    /**
     * Add user authorities. Duplicates are ignored.
     * 
     * @param username
     * @param authorities
     * @return
     * @throws SmartThingException
     */
    public List<IGrantedAuthority> addGrantedAuthorities(String username, List<String> authorities)
	    throws SmartThingException;

    /**
     * Remove user authorities. Ignore if not previously granted.
     * 
     * @param username
     * @param authorities
     * @return
     * @throws SmartThingException
     */
    public List<IGrantedAuthority> removeGrantedAuthorities(String username, List<String> authorities)
	    throws SmartThingException;

    /**
     * Get the list of all users that meet the given criteria.
     * 
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public List<IUser> listUsers(IUserSearchCriteria criteria) throws SmartThingException;

    /**
     * Delete the user with the given username.
     * 
     * @param username
     * @param force
     * @return
     * @throws SmartThingException
     */
    public IUser deleteUser(String username, boolean force) throws SmartThingException;

    /**
     * Create a new granted authority.
     * 
     * @param request
     * @throws SmartThingException
     */
    public IGrantedAuthority createGrantedAuthority(IGrantedAuthorityCreateRequest request) throws SmartThingException;

    /**
     * Get a granted authority by name.
     * 
     * @param name
     * @return
     * @throws SmartThingException
     */
    public IGrantedAuthority getGrantedAuthorityByName(String name) throws SmartThingException;

    /**
     * Update a granted authority.
     * 
     * @param name
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IGrantedAuthority updateGrantedAuthority(String name, IGrantedAuthorityCreateRequest request)
	    throws SmartThingException;

    /**
     * List granted authorities that match the given criteria.
     * 
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public List<IGrantedAuthority> listGrantedAuthorities(IGrantedAuthoritySearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Delete a granted authority.
     * 
     * @param authority
     * @throws SmartThingException
     */
    public void deleteGrantedAuthority(String authority) throws SmartThingException;
}