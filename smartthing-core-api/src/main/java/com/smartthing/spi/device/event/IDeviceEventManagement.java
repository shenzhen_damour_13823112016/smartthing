/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.event;

import java.util.List;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.command.IDeviceCommand;
import com.smartthing.spi.device.event.request.IDeviceAlertCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceCommandInvocationCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceCommandResponseCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceEventCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceLocationCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceMeasurementsCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStateChangeCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest;
import com.smartthing.spi.search.IDateRangeSearchCriteria;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Interface for device event management operations.
 * 
 * @author Derek
 */
public interface IDeviceEventManagement extends ITenantLifecycleComponent {

    /**
     * Set the device management implementation.
     * 
     * @param deviceManagement
     * @throws SmartThingException
     */
    public void setDeviceManagement(IDeviceManagement deviceManagement) throws SmartThingException;

    /**
     * Get the device management implementation.
     * 
     * @return
     * @throws SmartThingException
     */
    public IDeviceManagement getDeviceManagement() throws SmartThingException;

    /**
     * Add a batch of events for the given assignment.
     * 
     * @param assignmentToken
     * @param batch
     * @return
     * @throws SmartThingException
     */
    public IDeviceEventBatchResponse addDeviceEventBatch(String assignmentToken, IDeviceEventBatch batch)
	    throws SmartThingException;

    /**
     * Get a device event by unique id.
     * 
     * @param id
     * @return
     * @throws SmartThingException
     */
    public IDeviceEvent getDeviceEventById(String id) throws SmartThingException;

    /**
     * Get a device event by alternate (external) id.
     * 
     * @param alternateId
     * @return
     * @throws SmartThingException
     */
    public IDeviceEvent getDeviceEventByAlternateId(String alternateId) throws SmartThingException;

    /**
     * List all events for the given assignment that meet the search criteria.
     * 
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceEvent> listDeviceEvents(String assignmentToken, IDateRangeSearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Add measurements for a given device assignment.
     * 
     * @param assignmentToken
     * @param measurements
     * @return
     * @throws SmartThingException
     */
    public IDeviceMeasurements addDeviceMeasurements(String assignmentToken,
	    IDeviceMeasurementsCreateRequest measurements) throws SmartThingException;

    /**
     * Gets device measurement entries for an assignment based on criteria.
     * 
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceMeasurements> listDeviceMeasurements(String assignmentToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * List device measurements for a site.
     * 
     * @param siteToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceMeasurements> listDeviceMeasurementsForSite(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * Add location for a given device assignment.
     * 
     * @param assignmentToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceLocation addDeviceLocation(String assignmentToken, IDeviceLocationCreateRequest request)
	    throws SmartThingException;

    /**
     * Gets device location entries for an assignment.
     * 
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceLocation> listDeviceLocations(String assignmentToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * List device locations for a site.
     * 
     * @param siteToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceLocation> listDeviceLocationsForSite(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * List device locations for the given tokens within the given time range.
     * 
     * @param assignmentTokens
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceLocation> listDeviceLocations(List<String> assignmentTokens,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * Add alert for a given device assignment.
     * 
     * @param assignmentToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceAlert addDeviceAlert(String assignmentToken, IDeviceAlertCreateRequest request)
	    throws SmartThingException;

    /**
     * Gets the most recent device alert entries for an assignment.
     * 
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceAlert> listDeviceAlerts(String assignmentToken, IDateRangeSearchCriteria criteria)
	    throws SmartThingException;

    /**
     * List device alerts for a site.
     * 
     * @param siteToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceAlert> listDeviceAlertsForSite(String siteToken, IDateRangeSearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Add a chunk of stream data for a given device assignment.
     * 
     * @param assignmentToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceStreamData addDeviceStreamData(String assignmentToken, IDeviceStreamDataCreateRequest request)
	    throws SmartThingException;

    /**
     * Get a single chunk of data from a device stream.
     * 
     * @param assignmentToken
     * @param streamId
     * @param sequenceNumber
     * @return
     * @throws SmartThingException
     */
    public IDeviceStreamData getDeviceStreamData(String assignmentToken, String streamId, long sequenceNumber)
	    throws SmartThingException;

    /**
     * List all chunks of data in a device assignment that belong to a given stream
     * and meet the criteria.
     * 
     * @param assignmentToken
     * @param streamId
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceStreamData> listDeviceStreamData(String assignmentToken, String streamId,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * Get a device command invocation by unique id.
     * 
     * @param id
     * @return
     * @throws SmartThingException
     */
    public IDeviceCommandInvocation getDeviceCommandInvocationById(String id) throws SmartThingException;

    /**
     * Add a device command invocation event for the given assignment.
     * 
     * @param assignmentToken
     * @param command
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceCommandInvocation addDeviceCommandInvocation(String assignmentToken, IDeviceCommand command,
	    IDeviceCommandInvocationCreateRequest request) throws SmartThingException;

    /**
     * Gets device command invocations for an assignment based on criteria.
     * 
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceCommandInvocation> listDeviceCommandInvocations(String assignmentToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * List device command invocations for a site.
     * 
     * @param siteToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceCommandInvocation> listDeviceCommandInvocationsForSite(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * List responses associated with a command invocation.
     * 
     * @param invocationId
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceCommandResponse> listDeviceCommandInvocationResponses(String invocationId)
	    throws SmartThingException;

    /**
     * Adds a new device command response event.
     * 
     * @param assignmentToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceCommandResponse addDeviceCommandResponse(String assignmentToken,
	    IDeviceCommandResponseCreateRequest request) throws SmartThingException;

    /**
     * Gets the most recent device command response entries for an assignment.
     * 
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceCommandResponse> listDeviceCommandResponses(String assignmentToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * List device command responses for a site.
     * 
     * @param siteToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceCommandResponse> listDeviceCommandResponsesForSite(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * Adds a new device state change event.
     * 
     * @param assignmentToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceStateChange addDeviceStateChange(String assignmentToken, IDeviceStateChangeCreateRequest request)
	    throws SmartThingException;

    /**
     * Gets the most recent device state change entries for an assignment.
     * 
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceStateChange> listDeviceStateChanges(String assignmentToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * List device state changes for a site.
     * 
     * @param siteToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceStateChange> listDeviceStateChangesForSite(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * Update information for an existing event.
     * 
     * @param eventId
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceEvent updateDeviceEvent(String eventId, IDeviceEventCreateRequest request) throws SmartThingException;
}