/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.communication;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceNestingContext;
import com.smartthing.spi.device.command.IDeviceCommandExecution;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Extracts delivery parameters from
 * 
 * @author Derek
 * 
 * @param <T>
 */
public interface ICommandDeliveryParameterExtractor<T> extends ITenantLifecycleComponent {

    /**
     * Extract required delivery parameters from the given sources.
     * 
     * @param nesting
     * @param assignment
     * @param execution
     * @return
     * @throws SmartThingException
     */
    public T extractDeliveryParameters(IDeviceNestingContext nesting, IDeviceAssignment assignment,
	    IDeviceCommandExecution execution) throws SmartThingException;
}