/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device;

import java.util.List;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.common.IMetadataProvider;
import com.smartthing.spi.device.batch.IBatchElement;
import com.smartthing.spi.device.batch.IBatchOperation;
import com.smartthing.spi.device.command.IDeviceCommand;
import com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest;
import com.smartthing.spi.device.group.IDeviceGroup;
import com.smartthing.spi.device.group.IDeviceGroupElement;
import com.smartthing.spi.device.request.IBatchCommandInvocationRequest;
import com.smartthing.spi.device.request.IBatchElementUpdateRequest;
import com.smartthing.spi.device.request.IBatchOperationCreateRequest;
import com.smartthing.spi.device.request.IBatchOperationUpdateRequest;
import com.smartthing.spi.device.request.IDeviceAssignmentCreateRequest;
import com.smartthing.spi.device.request.IDeviceCommandCreateRequest;
import com.smartthing.spi.device.request.IDeviceCreateRequest;
import com.smartthing.spi.device.request.IDeviceGroupCreateRequest;
import com.smartthing.spi.device.request.IDeviceGroupElementCreateRequest;
import com.smartthing.spi.device.request.IDeviceSpecificationCreateRequest;
import com.smartthing.spi.device.request.IDeviceStatusCreateRequest;
import com.smartthing.spi.device.request.ISiteCreateRequest;
import com.smartthing.spi.device.request.IZoneCreateRequest;
import com.smartthing.spi.device.streaming.IDeviceStream;
import com.smartthing.spi.search.IDateRangeSearchCriteria;
import com.smartthing.spi.search.ISearchCriteria;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.search.device.IAssignmentSearchCriteria;
import com.smartthing.spi.search.device.IAssignmentsForAssetSearchCriteria;
import com.smartthing.spi.search.device.IBatchElementSearchCriteria;
import com.smartthing.spi.search.device.IDeviceSearchCriteria;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Interface for device management operations.
 * 
 * @author Derek
 */
public interface IDeviceManagement extends ITenantLifecycleComponent {

    /**
     * Create a new device specification.
     * 
     * @param request
     *            information about new specification
     * @return device specification that was created
     * @throws SmartThingException
     */
    public IDeviceSpecification createDeviceSpecification(IDeviceSpecificationCreateRequest request)
	    throws SmartThingException;

    /**
     * Get a device specification by unique token.
     * 
     * @param token
     *            unique device specification token
     * @return corresponding specification or null if not found
     * @throws SmartThingException
     *             if implementation encountered an error
     */
    public IDeviceSpecification getDeviceSpecificationByToken(String token) throws SmartThingException;

    /**
     * Update an existing device specification.
     * 
     * @param token
     *            unique specification token
     * @param request
     *            updated information
     * @return updated device specification
     * @throws SmartThingException
     *             if implementation encountered an error
     */
    public IDeviceSpecification updateDeviceSpecification(String token, IDeviceSpecificationCreateRequest request)
	    throws SmartThingException;

    /**
     * List device specifications that match the search criteria.
     * 
     * @param includeDeleted
     *            include specifications marked as deleted
     * @param criteria
     *            search criteria
     * @return results corresponding to search criteria
     * @throws SmartThingException
     *             if implementation encountered an error
     */
    public ISearchResults<IDeviceSpecification> listDeviceSpecifications(boolean includeDeleted,
	    ISearchCriteria criteria) throws SmartThingException;

    /**
     * Delete an existing device specification.
     * 
     * @param token
     *            unique specification token
     * @param force
     *            if true, deletes specification. if false, marks as deleted.
     * @return the deleted specification
     * @throws SmartThingException
     *             if implementation encountered an error
     */
    public IDeviceSpecification deleteDeviceSpecification(String token, boolean force) throws SmartThingException;

    /**
     * Creates a device command associated with an existing device
     * specification.
     * 
     * @param spec
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceCommand createDeviceCommand(IDeviceSpecification spec, IDeviceCommandCreateRequest request)
	    throws SmartThingException;

    /**
     * Get a device command by unique token.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public IDeviceCommand getDeviceCommandByToken(String token) throws SmartThingException;

    /**
     * Update an existing device command.
     * 
     * @param token
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceCommand updateDeviceCommand(String token, IDeviceCommandCreateRequest request)
	    throws SmartThingException;

    /**
     * List device command objects associated with a device specification.
     * 
     * @param specToken
     * @param includeDeleted
     * @return
     * @throws SmartThingException
     */
    public List<IDeviceCommand> listDeviceCommands(String specToken, boolean includeDeleted) throws SmartThingException;

    /**
     * Delete an existing device command.
     * 
     * @param token
     * @param force
     * @return
     * @throws SmartThingException
     */
    public IDeviceCommand deleteDeviceCommand(String token, boolean force) throws SmartThingException;

    /**
     * Creates a device status associated with an existing device specification.
     * 
     * @param specToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceStatus createDeviceStatus(String specToken, IDeviceStatusCreateRequest request)
	    throws SmartThingException;

    /**
     * Get a device status by unique code.
     * 
     * @param code
     * @return
     * @throws SmartThingException
     */
    public IDeviceStatus getDeviceStatusByCode(String specToken, String code) throws SmartThingException;

    /**
     * Update an existing device status.
     * 
     * @param code
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceStatus updateDeviceStatus(String specToken, String code, IDeviceStatusCreateRequest request)
	    throws SmartThingException;

    /**
     * List device statuses associated with a device specification.
     * 
     * @param specToken
     * @return
     * @throws SmartThingException
     */
    public List<IDeviceStatus> listDeviceStatuses(String specToken) throws SmartThingException;

    /**
     * Delete an existing device status.
     * 
     * @param specToken
     * @param code
     * @return
     * @throws SmartThingException
     */
    public IDeviceStatus deleteDeviceStatus(String specToken, String code) throws SmartThingException;

    /**
     * Create a new device.
     * 
     * @param device
     * @return
     * @throws SmartThingException
     */
    public IDevice createDevice(IDeviceCreateRequest device) throws SmartThingException;

    /**
     * Gets a device by unique hardware id.
     * 
     * @param hardwareId
     * @return
     * @throws SmartThingException
     */
    public IDevice getDeviceByHardwareId(String hardwareId) throws SmartThingException;

    /**
     * Update device information.
     * 
     * @param hardwareId
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDevice updateDevice(String hardwareId, IDeviceCreateRequest request) throws SmartThingException;

    /**
     * Gets the current assignment for a device. Null if none.
     * 
     * @param device
     * @return
     * @throws SmartThingException
     */
    public IDeviceAssignment getCurrentDeviceAssignment(IDevice device) throws SmartThingException;

    /**
     * List devices that meet the given criteria.
     * 
     * @param includeDeleted
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDevice> listDevices(boolean includeDeleted, IDeviceSearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Create an {@link IDeviceElementMapping} for a nested device.
     * 
     * @param hardwareId
     * @param mapping
     * @return
     * @throws SmartThingException
     */
    public IDevice createDeviceElementMapping(String hardwareId, IDeviceElementMapping mapping)
	    throws SmartThingException;

    /**
     * Delete an exising {@link IDeviceElementMapping} from a device.
     * 
     * @param hardwareId
     * @param path
     * @return
     * @throws SmartThingException
     */
    public IDevice deleteDeviceElementMapping(String hardwareId, String path) throws SmartThingException;

    /**
     * Delete an existing device.
     * 
     * @param hardwareId
     * @param force
     * @return
     * @throws SmartThingException
     */
    public IDevice deleteDevice(String hardwareId, boolean force) throws SmartThingException;

    /**
     * Create a new device assignment.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceAssignment createDeviceAssignment(IDeviceAssignmentCreateRequest request) throws SmartThingException;

    /**
     * Get a device assignment by unique token.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public IDeviceAssignment getDeviceAssignmentByToken(String token) throws SmartThingException;

    /**
     * Delete a device assignment. Depending on 'force' flag the assignment will
     * be marked for delete or actually be deleted.
     * 
     * @param token
     * @param force
     * @return
     * @throws SmartThingException
     */
    public IDeviceAssignment deleteDeviceAssignment(String token, boolean force) throws SmartThingException;

    /**
     * Get the device associated with an assignment.
     * 
     * @param assignment
     * @return
     * @throws SmartThingException
     */
    public IDevice getDeviceForAssignment(IDeviceAssignment assignment) throws SmartThingException;

    /**
     * Get the site associated with an assignment.
     * 
     * @param assignment
     * @return
     * @throws SmartThingException
     */
    public ISite getSiteForAssignment(IDeviceAssignment assignment) throws SmartThingException;

    /**
     * Update metadata associated with a device assignment.
     * 
     * @param token
     * @param metadata
     * @return
     * @throws SmartThingException
     */
    public IDeviceAssignment updateDeviceAssignmentMetadata(String token, IMetadataProvider metadata)
	    throws SmartThingException;

    /**
     * Update the status of an existing device assignment.
     * 
     * @param token
     * @param status
     * @return
     * @throws SmartThingException
     */
    public IDeviceAssignment updateDeviceAssignmentStatus(String token, DeviceAssignmentStatus status)
	    throws SmartThingException;

    /**
     * Updates the current state of a device assignment.
     * 
     * @param token
     * @param state
     * @return
     * @throws SmartThingException
     */
    public IDeviceAssignment updateDeviceAssignmentState(String token, IDeviceAssignmentState state)
	    throws SmartThingException;

    /**
     * Ends a device assignment.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public IDeviceAssignment endDeviceAssignment(String token) throws SmartThingException;

    /**
     * Get the device assignment history for a given device.
     * 
     * @param hardwareId
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceAssignment> getDeviceAssignmentHistory(String hardwareId, ISearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Get a list of device assignments for a site.
     * 
     * @param siteToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceAssignment> getDeviceAssignmentsForSite(String siteToken,
	    IAssignmentSearchCriteria criteria) throws SmartThingException;

    /**
     * Finds all device assignments for a site with a last interaction date in
     * the given date range. Note that events must be posted with the
     * 'updateState' option in order for the last interaction date to be
     * updated.
     * 
     * @param siteToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceAssignment> getDeviceAssignmentsWithLastInteraction(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * Find all device assignments that have been marked missing by the presence
     * manager.
     * 
     * @param siteToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceAssignment> getMissingDeviceAssignments(String siteToken, ISearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Get a list of device assignments associated with a given asset.
     * 
     * @param assetModuleId
     * @param assetId
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceAssignment> getDeviceAssignmentsForAsset(String assetModuleId, String assetId,
	    IAssignmentsForAssetSearchCriteria criteria) throws SmartThingException;

    /**
     * Create a new {@link IDeviceStream} associated with an assignment.
     * 
     * @param assignmentToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceStream createDeviceStream(String assignmentToken, IDeviceStreamCreateRequest request)
	    throws SmartThingException;

    /**
     * Get an exsiting {@link IDeviceStream} for an assignment based on unique
     * stream id. Returns null if not found.
     * 
     * @param assignmentToken
     * @param streamId
     * @return
     * @throws SmartThingException
     */
    public IDeviceStream getDeviceStream(String assignmentToken, String streamId) throws SmartThingException;

    /**
     * List device streams for the assignment that meet the given criteria.
     * 
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceStream> listDeviceStreams(String assignmentToken, ISearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Create a site based on the given input.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    public ISite createSite(ISiteCreateRequest request) throws SmartThingException;

    /**
     * Delete a site based on unique site token. If 'force' is specified, the
     * database object will be deleted, otherwise the deleted flag will be set
     * to true.
     * 
     * @param siteToken
     * @param force
     * @return
     * @throws SmartThingException
     */
    public ISite deleteSite(String siteToken, boolean force) throws SmartThingException;

    /**
     * Update information for a site.
     * 
     * @param siteToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public ISite updateSite(String siteToken, ISiteCreateRequest request) throws SmartThingException;

    /**
     * Get a site by unique token.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public ISite getSiteByToken(String token) throws SmartThingException;

    /**
     * Get a list of all sites.
     * 
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<ISite> listSites(ISearchCriteria criteria) throws SmartThingException;

    /**
     * Create a new zone.
     * 
     * @param site
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IZone createZone(ISite site, IZoneCreateRequest request) throws SmartThingException;

    /**
     * Update an existing zone.
     * 
     * @param token
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IZone updateZone(String token, IZoneCreateRequest request) throws SmartThingException;

    /**
     * Get a zone by its unique token.
     * 
     * @param zoneToken
     * @return
     * @throws SmartThingException
     */
    public IZone getZone(String zoneToken) throws SmartThingException;

    /**
     * Get a list of all zones associated with a Site.
     * 
     * @param siteToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IZone> listZones(String siteToken, ISearchCriteria criteria) throws SmartThingException;

    /**
     * Delete a zone given its unique token.
     * 
     * @param zoneToken
     * @param force
     * @return
     * @throws SmartThingException
     */
    public IZone deleteZone(String zoneToken, boolean force) throws SmartThingException;

    /**
     * Create a new device group.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceGroup createDeviceGroup(IDeviceGroupCreateRequest request) throws SmartThingException;

    /**
     * Update an existing device group.
     * 
     * @param token
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IDeviceGroup updateDeviceGroup(String token, IDeviceGroupCreateRequest request) throws SmartThingException;

    /**
     * Get a device network by unique token.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public IDeviceGroup getDeviceGroup(String token) throws SmartThingException;

    /**
     * List device groups.
     * 
     * @param includeDeleted
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceGroup> listDeviceGroups(boolean includeDeleted, ISearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Lists all device groups that have the given role.
     * 
     * @param role
     * @param includeDeleted
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceGroup> listDeviceGroupsWithRole(String role, boolean includeDeleted,
	    ISearchCriteria criteria) throws SmartThingException;

    /**
     * Delete a device group.
     * 
     * @param token
     * @param force
     * @return
     * @throws SmartThingException
     */
    public IDeviceGroup deleteDeviceGroup(String token, boolean force) throws SmartThingException;

    /**
     * Add elements to a device group.
     * 
     * @param groupToken
     * @param elements
     * @param ignoreDuplicates
     * @return
     * @throws SmartThingException
     */
    public List<IDeviceGroupElement> addDeviceGroupElements(String groupToken,
	    List<IDeviceGroupElementCreateRequest> elements, boolean ignoreDuplicates) throws SmartThingException;

    /**
     * Remove selected elements from a device group.
     * 
     * @param groupToken
     * @param elements
     * @return
     * @throws SmartThingException
     */
    public List<IDeviceGroupElement> removeDeviceGroupElements(String groupToken,
	    List<IDeviceGroupElementCreateRequest> elements) throws SmartThingException;

    /**
     * List device group elements that meet the given criteria.
     * 
     * @param groupToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IDeviceGroupElement> listDeviceGroupElements(String groupToken, ISearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Creates an {@link IBatchOperation} to perform an operation on multiple
     * devices.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IBatchOperation createBatchOperation(IBatchOperationCreateRequest request) throws SmartThingException;

    /**
     * Update an existing {@link IBatchOperation}.
     * 
     * @param token
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IBatchOperation updateBatchOperation(String token, IBatchOperationUpdateRequest request)
	    throws SmartThingException;

    /**
     * Get an {@link IBatchOperation} by unique token.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public IBatchOperation getBatchOperation(String token) throws SmartThingException;

    /**
     * List batch operations based on the given criteria.
     * 
     * @param includeDeleted
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IBatchOperation> listBatchOperations(boolean includeDeleted, ISearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Deletes a batch operation and its elements.
     * 
     * @param token
     * @param force
     * @return
     * @throws SmartThingException
     */
    public IBatchOperation deleteBatchOperation(String token, boolean force) throws SmartThingException;

    /**
     * Lists elements for an {@link IBatchOperation} that meet the given
     * criteria.
     * 
     * @param batchToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IBatchElement> listBatchElements(String batchToken, IBatchElementSearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Updates an existing batch operation element.
     * 
     * @param operationToken
     * @param index
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IBatchElement updateBatchElement(String operationToken, long index, IBatchElementUpdateRequest request)
	    throws SmartThingException;

    /**
     * Creates an {@link ISearchResults} that will invoke a command on multiple
     * devices.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IBatchOperation createBatchCommandInvocation(IBatchCommandInvocationRequest request)
	    throws SmartThingException;
}