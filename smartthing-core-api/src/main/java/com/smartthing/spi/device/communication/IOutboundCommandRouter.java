/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.communication;

import java.util.List;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceNestingContext;
import com.smartthing.spi.device.command.IDeviceCommandExecution;
import com.smartthing.spi.device.command.ISystemCommand;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Routes commands to one or more {@link ICommandDestination} implementations.
 * 
 * @author Derek
 */
public interface IOutboundCommandRouter extends ITenantLifecycleComponent {

    /**
     * Initialize the router with destination information.
     * 
     * @param destinations
     * @throws SmartThingException
     */
    public void initialize(List<ICommandDestination<?, ?>> destinations) throws SmartThingException;

    /**
     * Route a command to one of the available destinations.
     * 
     * @param execution
     * @param nesting
     * @param assignment
     * @throws SmartThingException
     */
    public void routeCommand(IDeviceCommandExecution execution, IDeviceNestingContext nesting,
	    IDeviceAssignment assignment) throws SmartThingException;

    /**
     * Route a system command to one of the available destinations.
     * 
     * @param command
     * @param nesting
     * @param assignment
     * @throws SmartThingException
     */
    public void routeSystemCommand(ISystemCommand command, IDeviceNestingContext nesting, IDeviceAssignment assignment)
	    throws SmartThingException;
}