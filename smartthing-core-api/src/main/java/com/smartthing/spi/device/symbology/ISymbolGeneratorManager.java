/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.symbology;

import java.util.List;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Manages a list of symbol generators.
 * 
 * @author Derek
 */
public interface ISymbolGeneratorManager extends ITenantLifecycleComponent {

    /**
     * Get the list of available symbol generators.
     * 
     * @return
     * @throws SmartThingException
     */
    public List<ISymbolGenerator> getSymbolGenerators() throws SmartThingException;

    /**
     * Get a symbol generator by id.
     * 
     * @param id
     * @return
     * @throws SmartThingException
     */
    public ISymbolGenerator getSymbolGenerator(String id) throws SmartThingException;

    /**
     * Get the default symbol generator.
     * 
     * @return
     * @throws SmartThingException
     */
    public ISymbolGenerator getDefaultSymbolGenerator() throws SmartThingException;
}