/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.communication;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceNestingContext;
import com.smartthing.spi.device.command.IDeviceCommandExecution;
import com.smartthing.spi.device.command.ISystemCommand;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Encodes an {@link IDeviceCommandExecution} into a format that can be
 * transmitted.
 * 
 * @author Derek
 * 
 * @param <T>
 *            format for encoded command. Must be compatible with the
 *            {@link ICommandDeliveryProvider} that will deliver the command.
 */
public interface ICommandExecutionEncoder<T> extends ITenantLifecycleComponent {

    /**
     * Encodes a command execution.
     * 
     * @param command
     * @param nested
     * @param assignment
     * @return
     * @throws SmartThingException
     */
    public T encode(IDeviceCommandExecution command, IDeviceNestingContext nested, IDeviceAssignment assignment)
	    throws SmartThingException;

    /**
     * Encodes a SiteWhere system command.
     * 
     * @param command
     * @param nested
     * @param assignment
     * @return
     * @throws SmartThingException
     */
    public T encodeSystemCommand(ISystemCommand command, IDeviceNestingContext nested, IDeviceAssignment assignment)
	    throws SmartThingException;
}