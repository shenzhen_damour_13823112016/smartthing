/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.event.processor;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.device.event.IDeviceStreamData;
import com.smartthing.spi.device.event.request.IDeviceAlertCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceCommandResponseCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceLocationCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceMappingCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceMeasurementsCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceRegistrationRequest;
import com.smartthing.spi.device.event.request.IDeviceStateChangeCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest;
import com.smartthing.spi.device.event.request.ISendDeviceStreamDataRequest;
import com.smartthing.spi.device.streaming.IDeviceStream;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Allows interested entities to interact with SiteWhere inbound event
 * processing.
 * 
 * @author Derek
 */
public interface IInboundEventProcessor extends ITenantLifecycleComponent {

    /**
     * Called when a {@link IDeviceRegistrationRequest} is received.
     * 
     * @param hardwareId
     * @param originator
     * @param request
     * @throws SmartThingException
     */
    public void onRegistrationRequest(String hardwareId, String originator, IDeviceRegistrationRequest request)
	    throws SmartThingException;

    /**
     * Called when an {@link IDeviceCommandResponseCreateRequest} is received.
     * 
     * @param hardwareId
     * @param originator
     * @param request
     * @throws SmartThingException
     */
    public void onDeviceCommandResponseRequest(String hardwareId, String originator,
	    IDeviceCommandResponseCreateRequest request) throws SmartThingException;

    /**
     * Called to request the creation of a new {@link IDeviceMeasurements} based
     * on the given information.
     * 
     * @param hardwareId
     * @param originator
     * @param request
     * @throws SmartThingException
     */
    public void onDeviceMeasurementsCreateRequest(String hardwareId, String originator,
	    IDeviceMeasurementsCreateRequest request) throws SmartThingException;

    /**
     * Called to request the creation of a new {@link IDeviceLocation} based on
     * the given information.
     * 
     * @param hardwareId
     * @param originator
     * @param request
     * @throws SmartThingException
     */
    public void onDeviceLocationCreateRequest(String hardwareId, String originator,
	    IDeviceLocationCreateRequest request) throws SmartThingException;

    /**
     * Called to request the creation of a new {@link IDeviceAlert} based on the
     * given information.
     * 
     * @param hardwareId
     * @param originator
     * @param request
     * @throws SmartThingException
     */
    public void onDeviceAlertCreateRequest(String hardwareId, String originator, IDeviceAlertCreateRequest request)
	    throws SmartThingException;

    /**
     * Called to request the creation of a new {@link IDeviceStateChange} based
     * on the given information.
     * 
     * @param hardwareId
     * @param originator
     * @param request
     * @throws SmartThingException
     */
    public void onDeviceStateChangeCreateRequest(String hardwareId, String originator,
	    IDeviceStateChangeCreateRequest request) throws SmartThingException;

    /**
     * Called to request the creation of a new {@link IDeviceStream} based on
     * the given information.
     * 
     * @param hardwareId
     * @param originator
     * @param request
     * @throws SmartThingException
     */
    public void onDeviceStreamCreateRequest(String hardwareId, String originator, IDeviceStreamCreateRequest request)
	    throws SmartThingException;

    /**
     * Called to request the creation of a new {@link IDeviceStreamData} based
     * on the given information.
     * 
     * @param hardwareId
     * @param originator
     * @param request
     * @throws SmartThingException
     */
    public void onDeviceStreamDataCreateRequest(String hardwareId, String originator,
	    IDeviceStreamDataCreateRequest request) throws SmartThingException;

    /**
     * Called to request that a chunk of {@link IDeviceStreamData} be sent to a
     * device.
     * 
     * @param hardwareId
     * @param originator
     * @param request
     * @throws SmartThingException
     */
    public void onSendDeviceStreamDataRequest(String hardwareId, String originator,
	    ISendDeviceStreamDataRequest request) throws SmartThingException;

    /**
     * Called to request that a device be mapped to a path on a composite
     * device.
     * 
     * @param hardwareId
     * @param originator
     * @param request
     * @throws SmartThingException
     */
    public void onDeviceMappingCreateRequest(String hardwareId, String originator, IDeviceMappingCreateRequest request)
	    throws SmartThingException;
}