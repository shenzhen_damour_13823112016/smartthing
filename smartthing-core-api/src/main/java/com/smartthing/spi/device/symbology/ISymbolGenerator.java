/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.symbology;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.ISite;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Generates symbols which uniquely identify SiteWhere entities.
 * 
 * @author Derek
 */
public interface ISymbolGenerator extends ITenantLifecycleComponent {

    /**
     * Get unique generator id.
     * 
     * @return
     * @throws SmartThingException
     */
    public String getId() throws SmartThingException;

    /**
     * Get name of symbol generator.
     * 
     * @return
     * @throws SmartThingException
     */
    public String getName() throws SmartThingException;

    /**
     * Get symbol for a site.
     * 
     * @param site
     * @param provider
     * @return
     * @throws SmartThingException
     */
    public byte[] getSiteSymbol(ISite site, IEntityUriProvider provider) throws SmartThingException;

    /**
     * Get symbol for a device specification.
     * 
     * @param specification
     * @param provider
     * @return
     * @throws SmartThingException
     */
    public byte[] getDeviceSpecificationSymbol(IDeviceSpecification specification, IEntityUriProvider provider)
	    throws SmartThingException;

    /**
     * Get symbol for a device.
     * 
     * @param device
     * @param provider
     * @return
     * @throws SmartThingException
     */
    public byte[] getDeviceSymbol(IDevice device, IEntityUriProvider provider) throws SmartThingException;

    /**
     * Get symbol for a device assignment.
     * 
     * @param assignment
     * @param provider
     * @return
     * @throws SmartThingException
     */
    public byte[] getDeviceAssigmentSymbol(IDeviceAssignment assignment, IEntityUriProvider provider)
	    throws SmartThingException;
}