/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.cache.ICache;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Interface for entity that provides caching for device management objects.
 * 
 * @author Derek
 */
public interface IDeviceManagementCacheProvider extends ITenantLifecycleComponent {

    /**
     * Gets cache mapping site tokens to {@link ISite} objects.
     * 
     * @return
     * @throws SmartThingException
     */
    public ICache<String, ISite> getSiteCache() throws SmartThingException;

    /**
     * Gets cache mapping specification tokens for {@link IDeviceSpecification}
     * objects.
     * 
     * @return
     * @throws SmartThingException
     */
    public ICache<String, IDeviceSpecification> getDeviceSpecificationCache() throws SmartThingException;

    /**
     * Gets cache mapping hardware ids to {@link IDevice} objects.
     * 
     * @return
     * @throws SmartThingException
     */
    public ICache<String, IDevice> getDeviceCache() throws SmartThingException;

    /**
     * Get cache mapping assignment tokens to {@link IDeviceAssignment} objects.
     * 
     * @return
     * @throws SmartThingException
     */
    public ICache<String, IDeviceAssignment> getDeviceAssignmentCache() throws SmartThingException;
}