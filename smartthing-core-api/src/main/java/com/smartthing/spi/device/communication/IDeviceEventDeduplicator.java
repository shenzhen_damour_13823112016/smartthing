package com.smartthing.spi.device.communication;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Provides support for filtering events that are duplicates of events already
 * in the system. The deduplication logic is implemented in subclasses.
 * 
 * @author Derek
 */
public interface IDeviceEventDeduplicator extends ITenantLifecycleComponent {

    /**
     * Detects whether the given device event is a duplicate of another event in
     * the system.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    public boolean isDuplicate(IDecodedDeviceRequest<?> request) throws SmartThingException;
}