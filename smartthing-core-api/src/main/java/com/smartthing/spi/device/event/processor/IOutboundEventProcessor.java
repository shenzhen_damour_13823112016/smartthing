/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.event.processor;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Allows intereseted entities to interact with SiteWhere outbound event
 * processing.
 * 
 * @author Derek
 */
public interface IOutboundEventProcessor extends ITenantLifecycleComponent {

    /**
     * Executes code after device measurements have been successfully saved.
     * 
     * @param measurements
     *            event information
     * @throws SmartThingException
     *             if an error occurs in processing
     */
    public void onMeasurements(IDeviceMeasurements measurements) throws SmartThingException;

    /**
     * Executes code after device location has been successfully saved.
     * 
     * @param location
     *            event information
     * @throws SmartThingException
     *             if an error occurs in processing
     */
    public void onLocation(IDeviceLocation location) throws SmartThingException;

    /**
     * Executes code after device alert has been successfully saved.
     * 
     * @param alert
     *            event information
     * @throws SmartThingException
     *             if an error occurs in processing
     */
    public void onAlert(IDeviceAlert alert) throws SmartThingException;

    /**
     * Executes code after device command invocation has been successfully
     * saved.
     * 
     * @param invocation
     *            event information
     * @throws SmartThingException
     *             if an error occurs in processing
     */
    public void onCommandInvocation(IDeviceCommandInvocation invocation) throws SmartThingException;

    /**
     * Executes code after device command response has been successfully saved.
     * 
     * @param response
     *            event information
     * @throws SmartThingException
     *             if an error occurs in processing
     */
    public void onCommandResponse(IDeviceCommandResponse response) throws SmartThingException;

    /**
     * Executes code after device state change has been successfully saved.
     * 
     * @param state
     *            event information
     * @throws SmartThingException
     *             if an error occurs in processing
     */
    public void onStateChange(IDeviceStateChange state) throws SmartThingException;
}