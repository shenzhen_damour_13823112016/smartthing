/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.communication;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.processor.IInboundEventProcessorChain;
import com.smartthing.spi.device.event.request.IDeviceAlertCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceCommandResponseCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceLocationCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceMappingCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceMeasurementsCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceRegistrationRequest;
import com.smartthing.spi.device.event.request.IDeviceStateChangeCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest;
import com.smartthing.spi.device.event.request.ISendDeviceStreamDataRequest;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Provides a strategy for moving decoded events from an
 * {@link IInboundEventSource} onto the {@link IInboundEventProcessorChain}.
 * 
 * @author Derek
 */
public interface IInboundProcessingStrategy extends ITenantLifecycleComponent {

    /**
     * Process an {@link IDeviceRegistrationRequest}.
     * 
     * @param request
     * @throws SmartThingException
     */
    public void processRegistration(IDecodedDeviceRequest<IDeviceRegistrationRequest> request)
	    throws SmartThingException;

    /**
     * Process an {@link IDeviceCommandResponseCreateRequest}.
     * 
     * @param request
     * @throws SmartThingException
     */
    public void processDeviceCommandResponse(IDecodedDeviceRequest<IDeviceCommandResponseCreateRequest> request)
	    throws SmartThingException;

    /**
     * Process an {@link IDeviceMeasurementsCreateRequest}.
     * 
     * @param request
     * @throws SmartThingException
     */
    public void processDeviceMeasurements(IDecodedDeviceRequest<IDeviceMeasurementsCreateRequest> request)
	    throws SmartThingException;

    /**
     * Process an {@link IDeviceLocationCreateRequest}.
     * 
     * @param request
     * @throws SmartThingException
     */
    public void processDeviceLocation(IDecodedDeviceRequest<IDeviceLocationCreateRequest> request)
	    throws SmartThingException;

    /**
     * Process an {@link IDeviceAlertCreateRequest}.
     * 
     * @param request
     * @throws SmartThingException
     */
    public void processDeviceAlert(IDecodedDeviceRequest<IDeviceAlertCreateRequest> request) throws SmartThingException;

    /**
     * Process an {@link IDeviceStateChangeCreateRequest}.
     * 
     * @param request
     * @throws SmartThingException
     */
    public void processDeviceStateChange(IDecodedDeviceRequest<IDeviceStateChangeCreateRequest> request)
	    throws SmartThingException;

    /**
     * Process an {@link IDeviceStreamCreateRequest}.
     * 
     * @param request
     * @throws SmartThingException
     */
    public void processDeviceStream(IDecodedDeviceRequest<IDeviceStreamCreateRequest> request)
	    throws SmartThingException;

    /**
     * Process an {@link IDeviceStreamDataCreateRequest}.
     * 
     * @param request
     * @throws SmartThingException
     */
    public void processDeviceStreamData(IDecodedDeviceRequest<IDeviceStreamDataCreateRequest> request)
	    throws SmartThingException;

    /**
     * Process an {@link ISendDeviceStreamDataRequest}.
     * 
     * @param request
     * @throws SmartThingException
     */
    public void processSendDeviceStreamData(IDecodedDeviceRequest<ISendDeviceStreamDataRequest> request)
	    throws SmartThingException;

    /**
     * Process an {@link IDeviceMappingCreateRequest}.
     * 
     * @param request
     * @throws SmartThingException
     */
    public void processCreateDeviceMapping(IDecodedDeviceRequest<IDeviceMappingCreateRequest> request)
	    throws SmartThingException;

    /**
     * Sends a decoded request to the inbound processing chain.
     * 
     * @param request
     * @throws SmartThingException
     */
    public void sendToInboundProcessingChain(IDecodedDeviceRequest<?> request) throws SmartThingException;
}