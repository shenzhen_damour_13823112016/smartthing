/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.event.processor;

import java.util.List;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;

/**
 * Adds concept of filtering to outbound event processors.
 * 
 * @author Derek
 */
public interface IFilteredOutboundEventProcessor extends IOutboundEventProcessor {

    /**
     * Get the list of configured filters.
     * 
     * @return
     */
    public List<IDeviceEventFilter> getFilters();

    /**
     * Called if measurements data was not filtered.
     * 
     * @param measurements
     * @throws SmartThingException
     */
    public void onMeasurementsNotFiltered(IDeviceMeasurements measurements) throws SmartThingException;

    /**
     * Called if location data was not filtered.
     * 
     * @param location
     * @throws SmartThingException
     */
    public void onLocationNotFiltered(IDeviceLocation location) throws SmartThingException;

    /**
     * Called if alert data was not filtered.
     * 
     * @param alert
     * @throws SmartThingException
     */
    public void onAlertNotFiltered(IDeviceAlert alert) throws SmartThingException;

    /**
     * Called if state change data was not filtered.
     * 
     * @param state
     * @throws SmartThingException
     */
    public void onStateChangeNotFiltered(IDeviceStateChange state) throws SmartThingException;

    /**
     * Called if command invocation data was not filtered.
     * 
     * @param invocation
     * @throws SmartThingException
     */
    public void onCommandInvocationNotFiltered(IDeviceCommandInvocation invocation) throws SmartThingException;

    /**
     * Called if command response data was not filtered.
     * 
     * @param response
     * @throws SmartThingException
     */
    public void onCommandResponseNotFiltered(IDeviceCommandResponse response) throws SmartThingException;
}