/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.symbology;

import java.net.URI;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.ISite;

/**
 * Translates a SiteWhere entity into a unique URL that identifies it.
 * 
 * @author Derek
 */
public interface IEntityUriProvider {

    /**
     * Get unique identifier for a site.
     * 
     * @param site
     * @return
     * @throws SmartThingException
     */
    public URI getSiteIdentifier(ISite site) throws SmartThingException;

    /**
     * Get unique identifier for a device specification.
     * 
     * @param specification
     * @return
     * @throws SmartThingException
     */
    public URI getDeviceSpecificationIdentifier(IDeviceSpecification specification) throws SmartThingException;

    /**
     * Get unique identifier for a device.
     * 
     * @param device
     * @return
     * @throws SmartThingException
     */
    public URI getDeviceIdentifier(IDevice device) throws SmartThingException;

    /**
     * Get unique identifier for a device assignment.
     * 
     * @param assignment
     * @return
     * @throws SmartThingException
     */
    public URI getDeviceAssignmentIdentifier(IDeviceAssignment assignment) throws SmartThingException;
}