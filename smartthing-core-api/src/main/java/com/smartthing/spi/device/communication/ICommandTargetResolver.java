/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.communication;

import java.util.List;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Allows an {@link IDeviceCommandInvocation} to be resolved to one or more
 * {@link IDeviceAssignment} records that should receive the command.
 * 
 * @author Derek
 */
public interface ICommandTargetResolver extends ITenantLifecycleComponent {

    /**
     * Resolves a command invocation to a list of assignments that should
     * receive the command.
     * 
     * @param invocation
     * @return
     * @throws SmartThingException
     */
    public List<IDeviceAssignment> resolveTargets(IDeviceCommandInvocation invocation) throws SmartThingException;
}