/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.communication;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.command.ISystemCommand;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Defines the flow executed for processing a command for delivery.
 * 
 * @author Derek
 */
public interface ICommandProcessingStrategy extends ITenantLifecycleComponent {

    /**
     * Get the {@link ICommandTargetResolver} implementation.
     * 
     * @return
     */
    public ICommandTargetResolver getCommandTargetResolver();

    /**
     * Send a command using the given communication subsystem implementation.
     * 
     * @param communication
     * @param invocation
     * @throws SmartThingException
     */
    public void deliverCommand(IDeviceCommunication communication, IDeviceCommandInvocation invocation)
	    throws SmartThingException;

    /**
     * Delivers a system command using the given communication subsystem
     * implementation.
     * 
     * @param communication
     * @param hardwareId
     * @param command
     * @throws SmartThingException
     */
    public void deliverSystemCommand(IDeviceCommunication communication, String hardwareId, ISystemCommand command)
	    throws SmartThingException;
}