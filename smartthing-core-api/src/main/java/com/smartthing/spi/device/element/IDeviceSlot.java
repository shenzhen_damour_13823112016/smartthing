/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.device.element;

import com.smartthing.spi.device.IDevice;

/**
 * Available position where an {@link IDevice} may be inserted into a parent
 * device.
 * 
 * @author Derek
 */
public interface IDeviceSlot extends IDeviceElement {
}