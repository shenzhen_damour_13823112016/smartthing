/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.scheduling;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.scheduling.request.IScheduleCreateRequest;
import com.smartthing.spi.scheduling.request.IScheduledJobCreateRequest;
import com.smartthing.spi.search.ISearchCriteria;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Management interface for persistent scheduling implementations.
 * 
 * @author Derek
 */
public interface IScheduleManagement extends ITenantLifecycleComponent {

    /**
     * Create a new schedule.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    public ISchedule createSchedule(IScheduleCreateRequest request) throws SmartThingException;

    /**
     * Update an existing schedule.
     * 
     * @param token
     * @param request
     * @return
     * @throws SmartThingException
     */
    public ISchedule updateSchedule(String token, IScheduleCreateRequest request) throws SmartThingException;

    /**
     * Get a schedule by unique token.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public ISchedule getScheduleByToken(String token) throws SmartThingException;

    /**
     * List schedules that match the given criteria.
     * 
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<ISchedule> listSchedules(ISearchCriteria criteria) throws SmartThingException;

    /**
     * Delete an existing schedule.
     * 
     * @param token
     * @param force
     * @return
     * @throws SmartThingException
     */
    public ISchedule deleteSchedule(String token, boolean force) throws SmartThingException;

    /**
     * Create a new scheduled job.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IScheduledJob createScheduledJob(IScheduledJobCreateRequest request) throws SmartThingException;

    /**
     * Update an existing scheduled job.
     * 
     * @param token
     * @param request
     * @return
     * @throws SmartThingException
     */
    public IScheduledJob updateScheduledJob(String token, IScheduledJobCreateRequest request) throws SmartThingException;

    /**
     * Get a scheduled job by unique token.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public IScheduledJob getScheduledJobByToken(String token) throws SmartThingException;

    /**
     * List scheduled jobs that match the given criteria.
     * 
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<IScheduledJob> listScheduledJobs(ISearchCriteria criteria) throws SmartThingException;

    /**
     * Delete an existing scheduled job.
     * 
     * @param token
     * @param force
     * @return
     * @throws SmartThingException
     */
    public IScheduledJob deleteScheduledJob(String token, boolean force) throws SmartThingException;
}