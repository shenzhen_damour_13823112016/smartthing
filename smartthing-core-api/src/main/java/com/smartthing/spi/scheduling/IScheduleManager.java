/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.scheduling;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.lifecycle.ITenantLifecycleComponent;

/**
 * Manages a list of schedules that are applied to system actions.
 * 
 * @author Derek
 */
public interface IScheduleManager extends ITenantLifecycleComponent {

    /**
     * Called when a new schedule has been added.
     * 
     * @param schedule
     * @throws SmartThingException
     */
    public void scheduleAdded(ISchedule schedule) throws SmartThingException;

    /**
     * Called when a schedule is removed.
     * 
     * @param schedule
     * @throws SmartThingException
     */
    public void scheduleRemoved(ISchedule schedule) throws SmartThingException;

    /**
     * Adds a job to the scheduler.
     * 
     * @param job
     * @throws SmartThingException
     */
    public void scheduleJob(IScheduledJob job) throws SmartThingException;

    /**
     * Unschedules the given job if scheduled.
     * 
     * @param job
     * @throws SmartThingException
     */
    public void unscheduleJob(IScheduledJob job) throws SmartThingException;
}