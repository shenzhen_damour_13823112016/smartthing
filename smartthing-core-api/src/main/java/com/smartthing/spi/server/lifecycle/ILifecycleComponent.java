/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.server.lifecycle;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.smartthing.spi.SmartThingException;

/**
 * Lifecycle methods used in SiteWhere components.
 * 
 * @author Derek
 */
public interface ILifecycleComponent {

    /**
     * Get the unique component id.
     * 
     * @return
     */
    public String getComponentId();

    /**
     * Get human-readable name shown for component.
     * 
     * @return
     */
    public String getComponentName();

    /**
     * Get component type.
     * 
     * @return
     */
    public LifecycleComponentType getComponentType();

    /**
     * Get current lifecycle status.
     * 
     * @return
     */
    public LifecycleStatus getLifecycleStatus();

    /**
     * Gets the last lifecycle error that occurred.
     * 
     * @return
     */
    public SmartThingException getLifecycleError();

    /**
     * Get map of contained {@link ILifecycleComponent} elements by unique id.
     * 
     * @return
     */
    public Map<String, ILifecycleComponent> getLifecycleComponents();

    /**
     * Initializes the component while keeping up with lifeycle information.
     * 
     * @param monitor
     */
    public void lifecycleInitialize(ILifecycleProgressMonitor monitor);

    /**
     * Indicates to framework whether component can be initialized.
     * 
     * @return
     * @throws SmartThingException
     */
    public boolean canInitialize() throws SmartThingException;

    /**
     * Initialize the component.
     * 
     * @param monitor
     * @throws SmartThingException
     */
    public void initialize(ILifecycleProgressMonitor monitor) throws SmartThingException;

    /**
     * Initialize a nested component.
     * 
     * @param component
     * @param monitor
     * @throws SmartThingException
     */
    public void initializeNestedComponent(ILifecycleComponent component, ILifecycleProgressMonitor monitor)
	    throws SmartThingException;

    /**
     * Starts the component while keeping up with lifecycle information.
     */
    public void lifecycleStart(ILifecycleProgressMonitor monitor);

    /**
     * Indicates to framework whether component can be started.
     * 
     * @return
     * @throws SmartThingException
     */
    public boolean canStart() throws SmartThingException;

    /**
     * Start the component.
     * 
     * @throws SmartThingException
     */
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException;

    /**
     * Start component as a nested lifecycle component.
     * 
     * @param component
     * @param monitor
     * @param errorMessage
     * @param require
     * @throws SmartThingException
     */
    public void startNestedComponent(ILifecycleComponent component, ILifecycleProgressMonitor monitor,
	    String errorMessage, boolean require) throws SmartThingException;

    /**
     * Pauses the component while keeping up with lifecycle information.
     */
    public void lifecyclePause(ILifecycleProgressMonitor monitor);

    /**
     * Indicates to framework whether component can be paused.
     * 
     * @return
     * @throws SmartThingException
     */
    public boolean canPause() throws SmartThingException;

    /**
     * Pause the component.
     * 
     * @throws SmartThingException
     */
    public void pause(ILifecycleProgressMonitor monitor) throws SmartThingException;

    /**
     * Stops the component while keeping up with lifecycle information.
     * 
     * @param monitor
     */
    public void lifecycleStop(ILifecycleProgressMonitor monitor);

    /**
     * Stops the component while keeping up with lifecycle information. This
     * version allows constraints to be passed to the operation.
     * 
     * @param monitor
     * @param constraints
     */
    public void lifecycleStop(ILifecycleProgressMonitor monitor, ILifecycleConstraints constraints);

    /**
     * Indicates to framework whether component can be stopped.
     * 
     * @return
     * @throws SmartThingException
     */
    public boolean canStop() throws SmartThingException;

    /**
     * Stop the component.
     * 
     * @throws SmartThingException
     */
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException;

    /**
     * Stop the component. This version allows constraints to be passed to the
     * operation.
     * 
     * @throws SmartThingException
     */
    public void stop(ILifecycleProgressMonitor monitor, ILifecycleConstraints constraints) throws SmartThingException;

    /**
     * Terminates the component while keeping up with lifecycle information.
     * 
     * @param monitor
     */
    public void lifecycleTerminate(ILifecycleProgressMonitor monitor);

    /**
     * Terminate the component.
     * 
     * @throws SmartThingException
     */
    public void terminate(ILifecycleProgressMonitor monitor) throws SmartThingException;

    /**
     * Called to track component lifecycle changes.
     * 
     * @param before
     * @param after
     */
    public void lifecycleStatusChanged(LifecycleStatus before, LifecycleStatus after);

    /**
     * Find components (including this component and nested components) that are
     * of the given type.
     * 
     * @param type
     * @return
     * @throws SmartThingException
     */
    public List<ILifecycleComponent> findComponentsOfType(LifecycleComponentType type) throws SmartThingException;

    /**
     * Get date the component was created.
     * 
     * @return
     */
    public Date getCreatedDate();

    /**
     * Get component logger.
     * 
     * @return
     */
    public Logger getLogger();

    /**
     * Logs the state of this component and all nested components.
     */
    public void logState();
}