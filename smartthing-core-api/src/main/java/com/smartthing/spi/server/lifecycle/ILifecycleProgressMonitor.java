package com.smartthing.spi.server.lifecycle;

import java.util.Deque;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.monitoring.IProgressReporter;

/**
 * Allows progress to be monitored on long-running lifecycle tasks.
 * 
 * @author Derek
 */
public interface ILifecycleProgressMonitor extends IProgressReporter {

    /**
     * Get current list of nested contexts.
     * 
     * @return
     */
    public Deque<ILifecycleProgressContext> getContextStack();

    /**
     * Push a new nested context onto the stack.
     * 
     * @param context
     * @throws SmartThingException
     */
    public void pushContext(ILifecycleProgressContext context) throws SmartThingException;

    /**
     * Start progress on a new operation within the current nesting context.
     * 
     * @param operation
     * @throws SmartThingException
     */
    public void startProgress(String operation) throws SmartThingException;

    /**
     * Finish progress for the current operation. This results in reporting of
     * progress message.
     * 
     * @throws SmartThingException
     */
    public void finishProgress() throws SmartThingException;

    /**
     * Pop last context from the stack.
     * 
     * @return
     * @throws SmartThingException
     */
    public ILifecycleProgressContext popContext() throws SmartThingException;
}