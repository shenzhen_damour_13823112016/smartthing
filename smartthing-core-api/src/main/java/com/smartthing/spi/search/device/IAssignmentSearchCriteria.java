package com.smartthing.spi.search.device;

import com.smartthing.spi.device.DeviceAssignmentStatus;
import com.smartthing.spi.search.ISearchCriteria;

/**
 * Criteria available for filtering assignment search results.
 * 
 * @author Derek
 */
public interface IAssignmentSearchCriteria extends ISearchCriteria {

    /**
     * Only return assignments with the given status.
     * 
     * @return
     */
    public DeviceAssignmentStatus getStatus();
}