package com.smartthing.spi.resource;

import java.util.List;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.resource.request.IResourceCreateRequest;
import com.smartthing.spi.server.lifecycle.ILifecycleComponent;

/**
 * Interface for component that manages system resources.
 * 
 * @author Derek
 */
public interface IResourceManager extends ILifecycleComponent {

    /**
     * Create one or more global resources.
     * 
     * @param request
     * @param mode
     * @return
     * @throws SmartThingException
     */
    public IMultiResourceCreateResponse createGlobalResources(List<IResourceCreateRequest> request,
	    ResourceCreateMode mode) throws SmartThingException;

    /**
     * Get a global resource registered for the given path.
     * 
     * @param path
     * @return
     * @throws SmartThingException
     */
    public IResource getGlobalResource(String path) throws SmartThingException;

    /**
     * Get list of all global resources.
     * 
     * @return
     * @throws SmartThingException
     */
    public List<IResource> getGlobalResources() throws SmartThingException;

    /**
     * Get root names for tenant template resource trees.
     * 
     * @return
     * @throws SmartThingException
     */
    public List<String> getTenantTemplateRoots() throws SmartThingException;

    /**
     * Gets resources associated with the given tenant template.
     * 
     * @param templateId
     * @return
     * @throws SmartThingException
     */
    public List<IResource> getTenantTemplateResources(String templateId) throws SmartThingException;

    /**
     * Delete global resource at the given path.
     * 
     * @param path
     * @return
     * @throws SmartThingException
     */
    public IResource deleteGlobalResource(String path) throws SmartThingException;

    /**
     * Copies resources from a tenant template into a tenant with the given id.
     * 
     * @param prefix
     * @param tenantId
     * @param mode
     * @return
     * @throws SmartThingException
     */
    public IMultiResourceCreateResponse copyTemplateResourcesToTenant(String templateId, String tenantId,
	    ResourceCreateMode mode) throws SmartThingException;

    /**
     * Create one or more tenant resources.
     * 
     * @param tenantId
     * @param request
     * @param mode
     * @return
     * @throws SmartThingException
     */
    public IMultiResourceCreateResponse createTenantResources(String tenantId, List<IResourceCreateRequest> request,
	    ResourceCreateMode mode) throws SmartThingException;

    /**
     * Get a tenant resource registered for the given path.
     * 
     * @param tenantId
     * @param path
     * @return
     * @throws SmartThingException
     */
    public IResource getTenantResource(String tenantId, String path) throws SmartThingException;

    /**
     * Get list of all resources for the given tenant.
     * 
     * @param tenantId
     * @return
     * @throws SmartThingException
     */
    public List<IResource> getTenantResources(String tenantId) throws SmartThingException;

    /**
     * Delete tenant resource at the given path.
     * 
     * @param tenantId
     * @param path
     * @return
     * @throws SmartThingException
     */
    public IResource deleteTenantResource(String tenantId, String path) throws SmartThingException;

    /**
     * Delete all resources associated with the given tenant.
     * 
     * @param tenantId
     * @throws SmartThingException
     */
    public void deleteTenantResources(String tenantId) throws SmartThingException;
}