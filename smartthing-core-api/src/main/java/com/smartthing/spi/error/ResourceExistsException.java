package com.smartthing.spi.error;

import com.smartthing.spi.SmartThingException;

/**
 * Indicates that a "create" operation resulted in a duplicate key.
 * 
 * @author Derek
 */
public class ResourceExistsException extends SmartThingException {

    /** Serial version UID */
    private static final long serialVersionUID = 997625714231990638L;

    /** SiteWhere error code */
    private ErrorCode code;

    public ResourceExistsException(ErrorCode code) {
	this.code = code;
    }

    public ErrorCode getCode() {
	return code;
    }

    public void setCode(ErrorCode code) {
	this.code = code;
    }
}