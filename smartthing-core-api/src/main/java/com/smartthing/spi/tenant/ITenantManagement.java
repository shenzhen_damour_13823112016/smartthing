/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi.tenant;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.search.user.ITenantSearchCriteria;
import com.smartthing.spi.server.lifecycle.ILifecycleComponent;
import com.smartthing.spi.tenant.request.ITenantCreateRequest;

/**
 * Interface for tenant management operations.
 * 
 * @author Derek
 */
public interface ITenantManagement extends ILifecycleComponent {

    /**
     * Create a new tenant.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    public ITenant createTenant(ITenantCreateRequest request) throws SmartThingException;

    /**
     * Update an existing tenant.
     * 
     * @param id
     * @param request
     * @return
     * @throws SmartThingException
     */
    public ITenant updateTenant(String id, ITenantCreateRequest request) throws SmartThingException;

    /**
     * Get a tenant by tenant id.
     * 
     * @param id
     * @return
     * @throws SmartThingException
     */
    public ITenant getTenantById(String id) throws SmartThingException;

    /**
     * Get a tenant by authentication token sent by devices.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public ITenant getTenantByAuthenticationToken(String token) throws SmartThingException;

    /**
     * Find all tenants that match the given criteria.
     * 
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public ISearchResults<ITenant> listTenants(ITenantSearchCriteria criteria) throws SmartThingException;

    /**
     * Delete an existing tenant.
     * 
     * @param tenantId
     * @param force
     * @return
     * @throws SmartThingException
     */
    public ITenant deleteTenant(String tenantId, boolean force) throws SmartThingException;
}