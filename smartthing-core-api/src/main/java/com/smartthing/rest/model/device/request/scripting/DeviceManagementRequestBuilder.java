/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.rest.model.device.request.scripting;

import java.util.ArrayList;
import java.util.List;

import com.smartthing.rest.model.device.request.DeviceAssignmentCreateRequest;
import com.smartthing.rest.model.device.request.DeviceCommandCreateRequest;
import com.smartthing.rest.model.device.request.DeviceCreateRequest;
import com.smartthing.rest.model.device.request.DeviceGroupCreateRequest;
import com.smartthing.rest.model.device.request.DeviceGroupElementCreateRequest;
import com.smartthing.rest.model.device.request.DeviceSpecificationCreateRequest;
import com.smartthing.rest.model.device.request.SiteCreateRequest;
import com.smartthing.rest.model.device.request.ZoneCreateRequest;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.ISite;
import com.smartthing.spi.device.IZone;
import com.smartthing.spi.device.command.IDeviceCommand;
import com.smartthing.spi.device.group.IDeviceGroup;
import com.smartthing.spi.device.group.IDeviceGroupElement;
import com.smartthing.spi.device.request.IDeviceGroupElementCreateRequest;

/**
 * Builder that supports creating device management entities.
 * 
 * @author Derek
 */
public class DeviceManagementRequestBuilder {

    /** Device management implementation */
    private IDeviceManagement deviceManagement;

    public DeviceManagementRequestBuilder(IDeviceManagement deviceManagement) {
	this.deviceManagement = deviceManagement;
    }

    public SiteCreateRequest.Builder newSite(String token, String name) {
	return new SiteCreateRequest.Builder(token, name);
    }

    public ISite persist(SiteCreateRequest.Builder builder) throws SmartThingException {
	return getDeviceManagement().createSite(builder.build());
    }

    public ZoneCreateRequest.Builder newZone(String name) {
	return new ZoneCreateRequest.Builder(name);
    }

    public IZone persist(ISite site, ZoneCreateRequest.Builder builder) throws SmartThingException {
	return getDeviceManagement().createZone(site, builder.build());
    }

    public DeviceSpecificationCreateRequest.Builder newSpecification(String token, String name, String assetModuleId,
	    String assetId) {
	return new DeviceSpecificationCreateRequest.Builder(token, name, assetModuleId, assetId);
    }

    public IDeviceSpecification persist(DeviceSpecificationCreateRequest.Builder builder) throws SmartThingException {
	return getDeviceManagement().createDeviceSpecification(builder.build());
    }

    public DeviceCommandCreateRequest.Builder newCommand(String token, String namespace, String name) {
	return new DeviceCommandCreateRequest.Builder(token, namespace, name);
    }

    public IDeviceCommand persist(IDeviceSpecification specification, DeviceCommandCreateRequest.Builder builder)
	    throws SmartThingException {
	return getDeviceManagement().createDeviceCommand(specification, builder.build());
    }

    public DeviceCreateRequest.Builder newDevice(String siteToken, String specificationToken, String hardwareId) {
	return new DeviceCreateRequest.Builder(siteToken, specificationToken, hardwareId);
    }

    public DeviceCreateRequest.Builder fromDevice(IDevice device) {
	return new DeviceCreateRequest.Builder(device);
    }

    public IDevice persist(DeviceCreateRequest.Builder builder) throws SmartThingException {
	return getDeviceManagement().createDevice(builder.build());
    }

    public IDevice update(IDevice device, DeviceCreateRequest.Builder builder) throws SmartThingException {
	return getDeviceManagement().updateDevice(device.getHardwareId(), builder.build());
    }

    public DeviceAssignmentCreateRequest.Builder newAssignment(String hardwareId, String assetModuleId,
	    String assetId) {
	return new DeviceAssignmentCreateRequest.Builder(hardwareId, assetModuleId, assetId);
    }

    public IDeviceAssignment persist(DeviceAssignmentCreateRequest.Builder builder) throws SmartThingException {
	return getDeviceManagement().createDeviceAssignment(builder.build());
    }

    public DeviceGroupCreateRequest.Builder newGroup(String token, String name) {
	return new DeviceGroupCreateRequest.Builder(token, name);
    }

    public IDeviceGroup persist(DeviceGroupCreateRequest.Builder builder) throws SmartThingException {
	return getDeviceManagement().createDeviceGroup(builder.build());
    }

    public DeviceGroupElementCreateRequest.Builder newGroupElement(String id) {
	return new DeviceGroupElementCreateRequest.Builder(id);
    }

    public List<IDeviceGroupElement> persist(IDeviceGroup group, List<DeviceGroupElementCreateRequest.Builder> builders)
	    throws SmartThingException {
	List<IDeviceGroupElementCreateRequest> elements = new ArrayList<IDeviceGroupElementCreateRequest>();
	for (DeviceGroupElementCreateRequest.Builder builder : builders) {
	    elements.add(builder.build());
	}
	return getDeviceManagement().addDeviceGroupElements(group.getToken(), elements, true);
    }

    public IDeviceManagement getDeviceManagement() {
	return deviceManagement;
    }

    public void setDeviceManagement(IDeviceManagement deviceManagement) {
	this.deviceManagement = deviceManagement;
    }
}