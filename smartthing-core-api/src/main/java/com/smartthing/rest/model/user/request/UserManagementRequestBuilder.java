/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.rest.model.user.request;

import java.util.List;

import com.smartthing.rest.model.search.user.UserSearchCriteria;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.user.IGrantedAuthority;
import com.smartthing.spi.user.IUser;
import com.smartthing.spi.user.IUserManagement;

/**
 * Builder that supports creating user management entities.
 * 
 * @author Derek
 */
public class UserManagementRequestBuilder {

    /** Device management implementation */
    private IUserManagement userManagement;

    public UserManagementRequestBuilder(IUserManagement userManagement) {
	this.userManagement = userManagement;
    }

    /**
     * Create builder for new user request.
     * 
     * @param username
     * @param password
     * @param firstName
     * @param lastName
     * @return
     */
    public UserCreateRequest.Builder newUser(String username, String password, String firstName, String lastName) {
	return new UserCreateRequest.Builder(username, password, firstName, lastName);
    }

    /**
     * Persist user contructed via builder.
     * 
     * @param builder
     * @return
     * @throws SmartThingException
     */
    public IUser persist(UserCreateRequest.Builder builder) throws SmartThingException {
	return getUserManagement().createUser(builder.build(), true);
    }

    /**
     * Create builder for new granted authority request.
     * 
     * @param authority
     * @return
     */
    public GrantedAuthorityCreateRequest.Builder newGrantedAuthority(String authority) {
	return new GrantedAuthorityCreateRequest.Builder(authority);
    }

    /**
     * Persist granted authority constructed via builder.
     * 
     * @param builder
     * @return
     * @throws SmartThingException
     */
    public IGrantedAuthority persist(GrantedAuthorityCreateRequest.Builder builder) throws SmartThingException {
	return getUserManagement().createGrantedAuthority(builder.build());
    }

    /**
     * Get an existing authority by name.
     * 
     * @param authority
     * @return
     * @throws SmartThingException
     */
    public IGrantedAuthority getAuthority(String authority) throws SmartThingException {
	return getUserManagement().getGrantedAuthorityByName(authority);
    }

    /**
     * Indicates if the system already contains the given authority.
     * 
     * @param authority
     * @return
     * @throws SmartThingException
     */
    public boolean hasAuthority(String authority) throws SmartThingException {
	return getAuthority(authority) != null;
    }

    /**
     * List all users.
     * 
     * @return
     * @throws SmartThingException
     */
    public List<IUser> listUsers() throws SmartThingException {
	return getUserManagement().listUsers(new UserSearchCriteria());
    }

    /**
     * Indicates if the system has users defined.
     * 
     * @return
     * @throws SmartThingException
     */
    public boolean hasUsers() throws SmartThingException {
	return listUsers().size() > 0;
    }

    public IUserManagement getUserManagement() {
	return userManagement;
    }

    public void setUserManagement(IUserManagement userManagement) {
	this.userManagement = userManagement;
    }
}