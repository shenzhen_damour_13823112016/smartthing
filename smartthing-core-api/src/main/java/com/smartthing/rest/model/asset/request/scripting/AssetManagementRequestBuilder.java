package com.smartthing.rest.model.asset.request.scripting;

import java.util.List;

import com.smartthing.rest.model.asset.request.AssetCategoryCreateRequest;
import com.smartthing.rest.model.asset.request.HardwareAssetCreateRequest;
import com.smartthing.rest.model.asset.request.LocationAssetCreateRequest;
import com.smartthing.rest.model.asset.request.PersonAssetCreateRequest;
import com.smartthing.rest.model.search.SearchCriteria;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.IAsset;
import com.smartthing.spi.asset.IAssetCategory;
import com.smartthing.spi.asset.IAssetManagement;
import com.smartthing.spi.asset.IAssetModuleManager;
import com.smartthing.spi.asset.IHardwareAsset;
import com.smartthing.spi.asset.ILocationAsset;
import com.smartthing.spi.asset.IPersonAsset;

/**
 * Builder that supports creating asset management entities.
 * 
 * @author Derek
 */
public class AssetManagementRequestBuilder {

    /** Asset management implementation */
    private IAssetManagement assetManagement;

    /** Asset module manager */
    private IAssetModuleManager assetModuleManager;

    public AssetManagementRequestBuilder(IAssetManagement assetManagement, IAssetModuleManager assetModuleManager) {
	this.assetManagement = assetManagement;
	this.assetModuleManager = assetModuleManager;
    }

    public AssetCategoryCreateRequest.Builder newAssetCategory(String id, String name) {
	return new AssetCategoryCreateRequest.Builder(id, name);
    }

    public IAssetCategory persist(AssetCategoryCreateRequest.Builder builder) throws SmartThingException {
	return getAssetManagement().createAssetCategory(builder.build());
    }

    public HardwareAssetCreateRequest.Builder newHardwareAsset(String id, String name, String imageUrl) {
	return new HardwareAssetCreateRequest.Builder(id, name, imageUrl);
    }

    public IHardwareAsset persist(String categoryId, HardwareAssetCreateRequest.Builder builder)
	    throws SmartThingException {
	return getAssetManagement().createHardwareAsset(categoryId, builder.build());
    }

    public LocationAssetCreateRequest.Builder newLocationAsset(String id, String name, String imageUrl) {
	return new LocationAssetCreateRequest.Builder(id, name, imageUrl);
    }

    public ILocationAsset persist(String categoryId, LocationAssetCreateRequest.Builder builder)
	    throws SmartThingException {
	return getAssetManagement().createLocationAsset(categoryId, builder.build());
    }

    public PersonAssetCreateRequest.Builder newPersonAsset(String id, String name, String imageUrl) {
	return new PersonAssetCreateRequest.Builder(id, name, imageUrl);
    }

    public IPersonAsset persist(String categoryId, PersonAssetCreateRequest.Builder builder) throws SmartThingException {
	return getAssetManagement().createPersonAsset(categoryId, builder.build());
    }

    public List<? extends IAsset> allAssetsInModule(String moduleId) throws SmartThingException {
	return getAssetManagement().listAssets(moduleId, SearchCriteria.ALL).getResults();
    }

    public IAssetManagement getAssetManagement() {
	return assetManagement;
    }

    public void setAssetManagement(IAssetManagement assetManagement) {
	this.assetManagement = assetManagement;
    }

    public IAssetModuleManager getAssetModuleManager() {
	return assetModuleManager;
    }

    public void setAssetModuleManager(IAssetModuleManager assetModuleManager) {
	this.assetModuleManager = assetModuleManager;
    }
}