/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.rest.client;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;

import com.smartthing.rest.ISmartThingWebConstants;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;

/**
 * Uses extra information passed by SiteWhere in headers to provide more
 * information about errors.
 * 
 * @author Derek
 */
public class SmartThingErrorHandler implements ResponseErrorHandler {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Delegate to default error handler */
    private ResponseErrorHandler errorHandler = new DefaultResponseErrorHandler();

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.client.ResponseErrorHandler#handleError(org.
     * springframework .http.client. ClientHttpResponse)
     */
    public void handleError(ClientHttpResponse response) throws IOException {
	String errorCode = null;
	List<String> codeList = response.getHeaders().get(ISmartThingWebConstants.HEADER_SMARTTHING_ERROR_CODE);
	if ((codeList != null) && (codeList.size() > 0)) {
	    errorCode = codeList.get(0);
	}
	try {
	    errorHandler.handleError(response);
	} catch (RestClientException e) {
	    LOGGER.error("Exception in error handling.", e);
	    if (errorCode != null) {
		ErrorCode code = ErrorCode.valueOf(errorCode);
		throw new SmartThingSystemException(code, ErrorLevel.ERROR, response.getRawStatusCode());
	    } else {
		throw new SmartThingSystemException(ErrorCode.Unknown, ErrorLevel.ERROR, response.getRawStatusCode());
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.client.ResponseErrorHandler#hasError(org.
     * springframework. http.client. ClientHttpResponse)
     */
    public boolean hasError(ClientHttpResponse response) throws IOException {
	return errorHandler.hasError(response);
    }
}