/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.spi;

import java.util.List;
import java.util.Map;

import com.smartthing.rest.model.common.MetadataProvider;
import com.smartthing.rest.model.device.Device;
import com.smartthing.rest.model.device.DeviceAssignment;
import com.smartthing.rest.model.device.DeviceSpecification;
import com.smartthing.rest.model.device.Site;
import com.smartthing.rest.model.device.Zone;
import com.smartthing.rest.model.device.batch.BatchOperation;
import com.smartthing.rest.model.device.command.DeviceCommand;
import com.smartthing.rest.model.device.event.DeviceAlert;
import com.smartthing.rest.model.device.event.DeviceCommandInvocation;
import com.smartthing.rest.model.device.event.DeviceEventBatch;
import com.smartthing.rest.model.device.event.DeviceEventBatchResponse;
import com.smartthing.rest.model.device.event.DeviceLocation;
import com.smartthing.rest.model.device.event.DeviceMeasurements;
import com.smartthing.rest.model.device.event.request.DeviceAlertCreateRequest;
import com.smartthing.rest.model.device.event.request.DeviceCommandInvocationCreateRequest;
import com.smartthing.rest.model.device.event.request.DeviceLocationCreateRequest;
import com.smartthing.rest.model.device.event.request.DeviceMeasurementsCreateRequest;
import com.smartthing.rest.model.device.group.DeviceGroup;
import com.smartthing.rest.model.device.request.DeviceCommandCreateRequest;
import com.smartthing.rest.model.device.request.DeviceCreateRequest;
import com.smartthing.rest.model.device.request.DeviceGroupCreateRequest;
import com.smartthing.rest.model.device.request.DeviceGroupElementCreateRequest;
import com.smartthing.rest.model.device.request.DeviceSpecificationCreateRequest;
import com.smartthing.rest.model.device.request.DeviceStreamCreateRequest;
import com.smartthing.rest.model.device.request.SiteCreateRequest;
import com.smartthing.rest.model.device.request.ZoneCreateRequest;
import com.smartthing.rest.model.device.streaming.DeviceStream;
import com.smartthing.rest.model.search.AssetSearchResults;
import com.smartthing.rest.model.search.DateRangeSearchCriteria;
import com.smartthing.rest.model.search.DeviceAlertSearchResults;
import com.smartthing.rest.model.search.DeviceAssignmentSearchResults;
import com.smartthing.rest.model.search.DeviceCommandInvocationSearchResults;
import com.smartthing.rest.model.search.DeviceCommandSearchResults;
import com.smartthing.rest.model.search.DeviceGroupElementSearchResults;
import com.smartthing.rest.model.search.DeviceGroupSearchResults;
import com.smartthing.rest.model.search.DeviceLocationSearchResults;
import com.smartthing.rest.model.search.DeviceSearchResults;
import com.smartthing.rest.model.search.DeviceSpecificationSearchResults;
import com.smartthing.rest.model.search.DeviceStreamSearchResults;
import com.smartthing.rest.model.search.SearchCriteria;
import com.smartthing.rest.model.search.SearchResults;
import com.smartthing.rest.model.search.ZoneSearchResults;
import com.smartthing.rest.model.system.Version;
import com.smartthing.spi.device.DeviceAssignmentStatus;
import com.smartthing.spi.device.request.IDeviceAssignmentCreateRequest;

/**
 * Interface for SiteWhere client calls.
 * 
 * @author Derek Adams
 */
public interface ISmartThingClient {

    /**
     * Get SiteWhere version information.
     * 
     * @return
     * @throws SmartThingException
     */
    public Version getSiteWhereVersion() throws SmartThingException;

    /**
     * Create a new device specification.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    public DeviceSpecification createDeviceSpecification(DeviceSpecificationCreateRequest request)
	    throws SmartThingException;

    /**
     * Get a device specification by token.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public DeviceSpecification getDeviceSpecificationByToken(String token) throws SmartThingException;

    /**
     * Update an existing device specification.
     * 
     * @param token
     * @param request
     * @return
     * @throws SmartThingException
     */
    public DeviceSpecification updateDeviceSpecification(String token, DeviceSpecificationCreateRequest request)
	    throws SmartThingException;

    /**
     * List device specifications that meet the given criteria.
     * 
     * @param includeDeleted
     * @param includeDetailedAssetInfo
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public DeviceSpecificationSearchResults listDeviceSpecifications(boolean includeDeleted,
	    boolean includeDetailedAssetInfo, SearchCriteria criteria) throws SmartThingException;

    /**
     * Delete an existing device specification.
     * 
     * @param token
     * @param deletePermanently
     * @return
     * @throws SmartThingException
     */
    public DeviceSpecification deleteDeviceSpecification(String token, boolean deletePermanently)
	    throws SmartThingException;

    /**
     * Create a new device command for a specification.
     * 
     * @param specificationToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public DeviceCommand createDeviceCommand(String specificationToken, DeviceCommandCreateRequest request)
	    throws SmartThingException;

    /**
     * List all device commands for a device specification.
     * 
     * @param specificationToken
     * @param includeDeleted
     * @return
     * @throws SmartThingException
     */
    public DeviceCommandSearchResults listDeviceCommands(String specificationToken, boolean includeDeleted)
	    throws SmartThingException;

    /**
     * Create a new site.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    public Site createSite(SiteCreateRequest request) throws SmartThingException;

    /**
     * Get Site by unique token.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public Site getSiteByToken(String token) throws SmartThingException;

    /**
     * Create a new zone associated with a site.
     * 
     * @param siteToken
     *            unique token for site
     * @param request
     *            information for new zone
     * @return zone that was created.
     * @throws SmartThingException
     */
    public Zone createZone(String siteToken, ZoneCreateRequest request) throws SmartThingException;

    /**
     * List zones associated with a given site.
     * 
     * @param siteToken
     * @return
     * @throws SmartThingException
     */
    public ZoneSearchResults listZonesForSite(String siteToken) throws SmartThingException;

    /**
     * Create a new device.
     * 
     * @param request
     *            information about device to be created
     * @return the created device
     * @throws SmartThingException
     */
    public Device createDevice(DeviceCreateRequest request) throws SmartThingException;

    /**
     * Get a device by its unique hardware id.
     * 
     * @param hardwareId
     *            hardware id of device to return
     * @return device if found or null if not
     * @throws SmartThingException
     */
    public Device getDeviceByHardwareId(String hardwareId) throws SmartThingException;

    /**
     * Update information for an existing device.
     * 
     * @param hardwareId
     *            hardware id of device to update
     * @param request
     *            updated information
     * @throws SmartThingException
     */
    public Device updateDevice(String hardwareId, DeviceCreateRequest request) throws SmartThingException;

    /**
     * List devices that meet the given criteria.
     * 
     * @param includeDeleted
     * @param excludeAssigned
     * @param populateSpecification
     * @param populateAssignment
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public DeviceSearchResults listDevices(boolean includeDeleted, boolean excludeAssigned,
	    boolean populateSpecification, boolean populateAssignment, DateRangeSearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Delete a device.
     * 
     * @param hardwareId
     *            hardware id of device to delete
     * @param force
     *            if true, data is deleted. if false, delete flag is set to true
     * @return
     * @throws SmartThingException
     */
    public Device deleteDevice(String hardwareId, boolean force) throws SmartThingException;

    /**
     * Get current device assignment for a device based on hardware id.
     * 
     * @param hardwareId
     *            unique hardware id of device
     * @return device assignment information
     * @throws SmartThingException
     */
    public DeviceAssignment getCurrentAssignmentForDevice(String hardwareId) throws SmartThingException;

    /**
     * Get the history of device assignments for a given hardware id.
     * 
     * @param hardwareId
     * @return
     * @throws SmartThingException
     */
    public DeviceAssignmentSearchResults listDeviceAssignmentHistory(String hardwareId) throws SmartThingException;

    /**
     * Get all assignments at a site that are associated with a given asset.
     * 
     * @param siteToken
     * @param assetModuleId
     * @param assetId
     * @param status
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public DeviceAssignmentSearchResults getAssignmentsForAsset(String siteToken, String assetModuleId, String assetId,
	    DeviceAssignmentStatus status, SearchCriteria criteria) throws SmartThingException;

    /**
     * Add a batch of events to the current assignment for the given hardware
     * id.
     * 
     * @param hardwareId
     *            hardware id whose assignment will have events added
     * @param batch
     *            batch of events that will be added
     * @return response of events that were created
     * @throws SmartThingException
     */
    public DeviceEventBatchResponse addDeviceEventBatch(String hardwareId, DeviceEventBatch batch)
	    throws SmartThingException;

    /**
     * Create a new device assignment based on the given inputs.
     * 
     * @param request
     *            information about the new assignment
     * @return the assignment that was created.
     * @throws SmartThingException
     */
    public DeviceAssignment createDeviceAssignment(IDeviceAssignmentCreateRequest request) throws SmartThingException;

    /**
     * Get a device assignment by its unique token.
     * 
     * @param assignmentToken
     *            unique assignment token
     * @return the device assignment
     * @throws SmartThingException
     */
    public DeviceAssignment getDeviceAssignmentByToken(String assignmentToken) throws SmartThingException;

    /**
     * List all assignments for a site.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public DeviceAssignmentSearchResults listAssignmentsForSite(String token) throws SmartThingException;

    /**
     * Delete a device assignment based on its unique token.
     * 
     * @param assignmentToken
     *            unique assignment token
     * @param force
     *            value of false sets deleted flag, true deletes data.
     * @return assignment that was deleted
     * @throws SmartThingException
     */
    public DeviceAssignment deleteDeviceAssignment(String assignmentToken, boolean force) throws SmartThingException;

    /**
     * Update the metadata for an existing device assignment.
     * 
     * @param token
     * @param metadata
     * @return
     * @throws SmartThingException
     */
    public DeviceAssignment updateDeviceAssignmentMetadata(String token, MetadataProvider metadata)
	    throws SmartThingException;

    /**
     * Create measurements for an assignment.
     * 
     * @param assignmentToken
     * @param measurements
     * @return
     * @throws SmartThingException
     */
    public DeviceMeasurements createDeviceMeasurements(String assignmentToken,
	    DeviceMeasurementsCreateRequest measurements) throws SmartThingException;

    /**
     * Get most recent device measurements for a given assignment.
     * 
     * @param assignmentToken
     * @param searchCriteria
     * @return
     * @throws SmartThingException
     */
    public SearchResults<DeviceMeasurements> listDeviceMeasurements(String assignmentToken,
	    DateRangeSearchCriteria searchCriteria) throws SmartThingException;

    /**
     * Create a new device location for an assignment.
     * 
     * @param assignmentToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public DeviceLocation createDeviceLocation(String assignmentToken, DeviceLocationCreateRequest request)
	    throws SmartThingException;

    /**
     * Get most recent device locations for a given assignment.
     * 
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public DeviceLocationSearchResults listDeviceLocations(String assignmentToken, DateRangeSearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Create a new alert for a device assignment.
     * 
     * @param assignmentToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public DeviceAlert createDeviceAlert(String assignmentToken, DeviceAlertCreateRequest request)
	    throws SmartThingException;

    /**
     * Get most recent device alerts for a given assignment.
     * 
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public DeviceAlertSearchResults listDeviceAlerts(String assignmentToken, DateRangeSearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Create a new device command invocation for a device assignment.
     * 
     * @param assignmentToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public DeviceCommandInvocation createDeviceCommandInvocation(String assignmentToken,
	    DeviceCommandInvocationCreateRequest request) throws SmartThingException;

    /**
     * List device command invocations that match the given criteria.
     * 
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public DeviceCommandInvocationSearchResults listDeviceCommandInvocations(String assignmentToken,
	    DateRangeSearchCriteria criteria) throws SmartThingException;

    /**
     * Create a stream that will be associated with a device assignment.
     * 
     * @param assignmentToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public DeviceStream createDeviceStream(String assignmentToken, DeviceStreamCreateRequest request)
	    throws SmartThingException;

    /**
     * Get a stream from an assignment based on unique id.
     * 
     * @param assignmentToken
     * @param streamId
     * @return
     * @throws SmartThingException
     */
    public DeviceStream getDeviceStream(String assignmentToken, String streamId) throws SmartThingException;

    /**
     * List device streams for an assignment.
     * 
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public DeviceStreamSearchResults listDeviceStreams(String assignmentToken, DateRangeSearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Add a chunk of data to a device stream.
     * 
     * @param assignmentToken
     * @param streamId
     * @param sequenceNumber
     * @param data
     * @throws SmartThingException
     */
    public void addDeviceStreamData(String assignmentToken, String streamId, long sequenceNumber, byte[] data)
	    throws SmartThingException;

    /**
     * Get a chunk of data from a device stream.
     * 
     * @param assignmentToken
     * @param streamId
     * @param sequenceNumber
     * @return
     * @throws SmartThingException
     */
    public byte[] getDeviceStreamData(String assignmentToken, String streamId, long sequenceNumber)
	    throws SmartThingException;

    /**
     * List device stream data that meets the given criteria.
     * 
     * @param assignmentToken
     * @param streamId
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public byte[] listDeviceStreamData(String assignmentToken, String streamId, DateRangeSearchCriteria criteria)
	    throws SmartThingException;

    /**
     * Invokes a command on a list of devices as a batch operation.
     * 
     * @param batchToken
     * @param commandToken
     * @param parameters
     * @param hardwareIds
     * @return
     * @throws SmartThingException
     */
    public BatchOperation createBatchCommandInvocation(String batchToken, String commandToken,
	    Map<String, String> parameters, List<String> hardwareIds) throws SmartThingException;

    /**
     * Create a new device group.
     * 
     * @param request
     * @return
     * @throws SmartThingException
     */
    public DeviceGroup createDeviceGroup(DeviceGroupCreateRequest request) throws SmartThingException;

    /**
     * Get a device group by unique token.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public DeviceGroup getDeviceGroupByToken(String token) throws SmartThingException;

    /**
     * Delete a device group by unique token.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    public DeviceGroup deleteDeviceGroup(String token) throws SmartThingException;

    /**
     * List device groups that meet the given criteria.
     * 
     * @param role
     * @param criteria
     * @param includeDeleted
     * @return
     * @throws SmartThingException
     */
    public DeviceGroupSearchResults listDeviceGroups(String role, SearchCriteria criteria, boolean includeDeleted)
	    throws SmartThingException;

    /**
     * Add elements to an existing device group.
     * 
     * @param groupToken
     * @param elements
     * @return
     * @throws SmartThingException
     */
    public DeviceGroupElementSearchResults addDeviceGroupElements(String groupToken,
	    List<DeviceGroupElementCreateRequest> elements) throws SmartThingException;

    /**
     * List device group elements that meet the given criteria.
     * 
     * @param groupToken
     * @param includeDetails
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public DeviceGroupElementSearchResults listDeviceGroupElements(String groupToken, boolean includeDetails,
	    SearchCriteria criteria) throws SmartThingException;

    /**
     * Delete elements from an existing device group.
     * 
     * @param groupToken
     * @param elements
     * @return
     * @throws SmartThingException
     */
    public DeviceGroupElementSearchResults deleteDeviceGroupElements(String groupToken,
	    List<DeviceGroupElementCreateRequest> elements) throws SmartThingException;

    /**
     * List all assets in a given asset module that meet the given criteria.
     * 
     * @param moduleId
     * @return
     * @throws SmartThingException
     */
    public AssetSearchResults getAssetsByModuleId(String moduleId, String criteria) throws SmartThingException;
}