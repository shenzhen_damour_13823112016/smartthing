#!/bin/bash
echo ""
echo "  SmartThing Start Script for Linux/Unix v1.0"
echo ""

if hash java 2>/dev/null; then

#	JAVA_OPTS="$JAVA_OPTS -Djava.net.preferIPv4Stack=true"
#	JAVA_OPTS="$JAVA_OPTS -XX:-UseSplitVerifier -noverify"

	if [ -z "$SMARTTHING_HOME" ]; then
		SMARTTHING_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
	else
		SMARTTHING_FOLDER=$SMARTTHING_HOME
	fi

	if [ ! -d "$SMARTTHING_FOLDER" ]; then
		echo ERROR! SmartThing Home Folder not found.
	else

        if [ ! -w "$SMARTTHING_FOLDER" ]; then
            echo ERROR! SmartThing Home Folder Permissions not correct.
        else    

		  if [ ! -f "$SMARTTHING_FOLDER/lib/smartthing.war" ]; then
	           	echo ERROR! SmartThing WAR not found.
	           	echo $SMARTTHING_FOLDER;
		  else
			 #SMARTTHING_FOLDER=$(echo "$SMARTTHING_FOLDER" | sed 's/ /\\ /g')
			 HOME_OPT="-Dsmartthing.home=$SMARTTHING_FOLDER -Dspring.config.location=$SMARTTHING_FOLDER/conf/"
			 JAVA_OPTS="$JAVA_OPTS -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=$SMARTTHING_FOLDER/heap-dump.hprof"

			echo "-------------------------------------------------------------------------"
			echo ""
			echo "  SMARTTHING_HOME: $SMARTTHING_FOLDER"
			echo ""
			echo "  JAVA_OPTS: $JAVA_OPTS"
			echo ""
			echo "-------------------------------------------------------------------------"
			echo ""
			 # Run SmartThing
			 WAR_PATH="$SMARTTHING_FOLDER/lib/smartthing.war"
			 eval \"java\" "$HOME_OPT" "$JAVA_OPTS" -jar "$WAR_PATH"
		  fi
	   fi
    fi

else
	echo You do not have the Java Runtime Environment installed, please install Java JRE from java.com/en/download and try again.
fi
