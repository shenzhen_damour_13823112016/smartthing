  @echo off
  echo.
  echo   SmartThing Start Script for Windows v1.0
  echo.

  java -version >nul 2>&1
  if errorlevel 1 goto NOJAVA

  rem set "JAVA_OPTS=-Djava.net.preferIPv4Stack=true -noverify %JAVA_OPTS%"
  rem set "JPDA_OPTS=-agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n"


  IF "%SMARTTHING_HOME%" == "" GOTO NOPATH
  set SMARTTHING_FOLDER="%SMARTTHING_HOME%";
  GOTO CHECKPATH

:NOPATH
  SET ROOT=%~dp0
  rem Resolve is fetching the parent directory
  CALL :RESOLVE "%ROOT%\.." PARENT_ROOT
  set SMARTTHING_FOLDER="%PARENT_ROOT%"
:CHECKPATH
  set SMARTTHING_FOLDER=%SMARTTHING_FOLDER:;=%
  rem Check if directory exists
  FOR %%i IN (%SMARTTHING_FOLDER%) DO IF EXIST %%~si\NUL  GOTO CHECKFILE
  echo ERROR! SmartThing Home Folder not found or readable.
  GOTO EXIT

:CHECKFILE
  rem Check if file exists
  FOR %%i IN (%SMARTTHING_FOLDER%/lib/smartthing.war) DO IF EXIST %%~si  GOTO START
  echo ERROR! SmartThing WAR not found.
  GOTO EXIT

:START
      set "JAVA_OPTS=-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=%SMARTTHING_FOLDER%/heap-dump.hprof %JAVA_OPTS%"

      echo -------------------------------------------------------------------------
      echo.
      echo   SMARTTHING_HOME: %SMARTTHING_FOLDER%
      echo.
      echo   JAVA_OPTS: %JAVA_OPTS%
      echo.
      echo -------------------------------------------------------------------------
      echo.
    
  set "JAVA_OPTS=-Dsmartthing.home=%SMARTTHING_FOLDER% -Dspring.config.location=%SMARTTHING_FOLDER%/conf/ %JAVA_OPTS%"
  
  java %JAVA_OPTS% %JPDA_OPTS% -jar %SMARTTHING_FOLDER%/lib/smartthing.war
  GOTO EXIT

:NOJAVA
  echo You do not have the Java Runtime Environment installed, please install Java JRE from java.com/en/download and try again.

:EXIT
  pause

:EXIT_WO_PAUSE

GOTO :EOF

:RESOLVE
SET %2=%~f1
GOTO :EOF