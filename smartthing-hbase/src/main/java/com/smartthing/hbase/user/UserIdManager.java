/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.user;

import com.smartthing.hbase.IHBaseContext;
import com.smartthing.hbase.uid.UniqueIdCounterMap;
import com.smartthing.hbase.uid.UniqueIdType;
import com.smartthing.spi.SmartThingException;

/**
 * Singleton that keeps up with asset management entities.
 * 
 * @author Derek
 */
public class UserIdManager implements IUserIdManager {

    /** Manager for tenant ids */
    private UniqueIdCounterMap tenantKeys;

    /** Manager for tenant group ids */
    private UniqueIdCounterMap tenantGroupKeys;

    /**
     * Load existing keys from table.
     * 
     * @param context
     * @throws SmartThingException
     */
    public void load(IHBaseContext context) throws SmartThingException {
	tenantKeys = new UniqueIdCounterMap(context, UniqueIdType.TenantKey.getIndicator(),
		UniqueIdType.TenantValue.getIndicator());
	tenantKeys.refresh();
	tenantGroupKeys = new UniqueIdCounterMap(context, UniqueIdType.TenantGroupKey.getIndicator(),
		UniqueIdType.TenantGroupValue.getIndicator());
	tenantGroupKeys.refresh();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.user.IUserIdManager#getTenantKeys()
     */
    public UniqueIdCounterMap getTenantKeys() {
	return tenantKeys;
    }

    public void setTenantKeys(UniqueIdCounterMap tenantKeys) {
	this.tenantKeys = tenantKeys;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.user.IUserIdManager#getTenantGroupKeys()
     */
    public UniqueIdCounterMap getTenantGroupKeys() {
	return tenantGroupKeys;
    }

    public void setTenantGroupKeys(UniqueIdCounterMap tenantGroupKeys) {
	this.tenantGroupKeys = tenantGroupKeys;
    }
}