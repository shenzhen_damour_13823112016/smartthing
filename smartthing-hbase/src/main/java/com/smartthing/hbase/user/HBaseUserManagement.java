/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.user;

import java.util.List;

import org.apache.hadoop.hbase.regionserver.BloomType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.hbase.HBaseContext;
import com.smartthing.hbase.ISmartThingHBase;
import com.smartthing.hbase.ISmartThingHBaseClient;
import com.smartthing.hbase.common.SmartThingTables;
import com.smartthing.hbase.encoder.IPayloadMarshaler;
import com.smartthing.hbase.encoder.JsonPayloadMarshaler;
import com.smartthing.server.lifecycle.LifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;
import com.smartthing.spi.user.IGrantedAuthority;
import com.smartthing.spi.user.IGrantedAuthoritySearchCriteria;
import com.smartthing.spi.user.IUser;
import com.smartthing.spi.user.IUserManagement;
import com.smartthing.spi.user.IUserSearchCriteria;
import com.smartthing.spi.user.request.IGrantedAuthorityCreateRequest;
import com.smartthing.spi.user.request.IUserCreateRequest;

/**
 * HBase implementation of SiteWhere user management.
 * 
 * @author Derek
 */
public class HBaseUserManagement extends LifecycleComponent implements IUserManagement {

    /** Static logger instance */
    private static final Logger LOGGER = LogManager.getLogger();

    /** Used to communicate with HBase */
    private ISmartThingHBaseClient client;

    /** Injected payload encoder */
    private IPayloadMarshaler payloadMarshaler = new JsonPayloadMarshaler();

    /** Supplies context to implementation methods */
    private HBaseContext context;

    /** User id manager */
    private UserIdManager userIdManager;

    public HBaseUserManagement() {
	super(LifecycleComponentType.DataStore);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
     * .server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	ensureTablesExist();

	// Create context from configured options.
	this.context = new HBaseContext();
	context.setClient(getClient());
	context.setPayloadMarshaler(getPayloadMarshaler());

	// Create device id manager instance.
	userIdManager = new UserIdManager();
	userIdManager.load(context);
	context.setUserIdManager(userIdManager);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /**
     * Ensure that the tables this implementation depends on are there.
     * 
     * @throws SmartThingException
     */
    protected void ensureTablesExist() throws SmartThingException {
	SmartThingTables.assureTable(client, ISmartThingHBase.USERS_TABLE_NAME, BloomType.ROW);
	SmartThingTables.assureTable(client, ISmartThingHBase.UID_TABLE_NAME, BloomType.ROW);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#createUser(com.smartthing.spi.user.
     * request .IUserCreateRequest)
     */
    @Override
    public IUser createUser(IUserCreateRequest request, boolean encodePassword) throws SmartThingException {
	return HBaseUser.createUser(context, request, encodePassword);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#importUser(com.smartthing.spi.user.
     * IUser, boolean)
     */
    @Override
    public IUser importUser(IUser user, boolean overwrite) throws SmartThingException {
	return HBaseUser.importUser(context, user, overwrite);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#authenticate(java.lang.String,
     * java.lang.String)
     */
    @Override
    public IUser authenticate(String username, String password, boolean updateLastLogin) throws SmartThingException {
	return HBaseUser.authenticate(context, username, password);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.user.IUserManagement#updateUser(java.lang.String,
     * com.smartthing.spi.user.request.IUserCreateRequest, boolean)
     */
    @Override
    public IUser updateUser(String username, IUserCreateRequest request, boolean encodePassword)
	    throws SmartThingException {
	return HBaseUser.updateUser(context, username, request, encodePassword);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.user.IUserManagement#getUserByUsername(java.lang.
     * String)
     */
    @Override
    public IUser getUserByUsername(String username) throws SmartThingException {
	return HBaseUser.getUserByUsername(context, username);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#getGrantedAuthorities(java.lang.
     * String)
     */
    @Override
    public List<IGrantedAuthority> getGrantedAuthorities(String username) throws SmartThingException {
	return HBaseUser.getGrantedAuthorities(context, username);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#addGrantedAuthorities(java.lang.
     * String, java.util.List)
     */
    @Override
    public List<IGrantedAuthority> addGrantedAuthorities(String username, List<String> authorities)
	    throws SmartThingException {
	throw new SmartThingException("Not implemented.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#removeGrantedAuthorities(java.lang
     * .String, java.util.List)
     */
    @Override
    public List<IGrantedAuthority> removeGrantedAuthorities(String username, List<String> authorities)
	    throws SmartThingException {
	throw new SmartThingException("Not implemented.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#listUsers(com.smartthing.spi.user.
     * IUserSearchCriteria)
     */
    @Override
    public List<IUser> listUsers(IUserSearchCriteria criteria) throws SmartThingException {
	return HBaseUser.listUsers(context, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.user.IUserManagement#deleteUser(java.lang.String,
     * boolean)
     */
    @Override
    public IUser deleteUser(String username, boolean force) throws SmartThingException {
	return HBaseUser.deleteUser(context, username, force);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.user.IUserManagement#createGrantedAuthority(com.
     * sitewhere.spi .user.request.IGrantedAuthorityCreateRequest)
     */
    @Override
    public IGrantedAuthority createGrantedAuthority(IGrantedAuthorityCreateRequest request) throws SmartThingException {
	return HBaseGrantedAuthority.createGrantedAuthority(context, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#getGrantedAuthorityByName(java.
     * lang.String)
     */
    @Override
    public IGrantedAuthority getGrantedAuthorityByName(String name) throws SmartThingException {
	return HBaseGrantedAuthority.getGrantedAuthorityByName(context, name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#updateGrantedAuthority(java.lang.
     * String, com.smartthing.spi.user.request.IGrantedAuthorityCreateRequest)
     */
    @Override
    public IGrantedAuthority updateGrantedAuthority(String name, IGrantedAuthorityCreateRequest request)
	    throws SmartThingException {
	throw new SmartThingException("Not implemented.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.user.IUserManagement#listGrantedAuthorities(com.
     * sitewhere.spi .user.IGrantedAuthoritySearchCriteria)
     */
    @Override
    public List<IGrantedAuthority> listGrantedAuthorities(IGrantedAuthoritySearchCriteria criteria)
	    throws SmartThingException {
	return HBaseGrantedAuthority.listGrantedAuthorities(context, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.user.IUserManagement#deleteGrantedAuthority(java.lang.
     * String)
     */
    @Override
    public void deleteGrantedAuthority(String authority) throws SmartThingException {
	throw new SmartThingException("Not implemented.");
    }

    public ISmartThingHBaseClient getClient() {
	return client;
    }

    public void setClient(ISmartThingHBaseClient client) {
	this.client = client;
    }

    public IPayloadMarshaler getPayloadMarshaler() {
	return payloadMarshaler;
    }

    public void setPayloadMarshaler(IPayloadMarshaler payloadMarshaler) {
	this.payloadMarshaler = payloadMarshaler;
    }
}