/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.user;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import com.smartthing.core.SmartThingPersistence;
import com.smartthing.hbase.IHBaseContext;
import com.smartthing.hbase.ISmartThingHBase;
import com.smartthing.hbase.common.HBaseUtils;
import com.smartthing.hbase.encoder.PayloadMarshalerResolver;
import com.smartthing.rest.model.user.GrantedAuthoritySearchCriteria;
import com.smartthing.rest.model.user.User;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.user.IGrantedAuthority;
import com.smartthing.spi.user.IUser;
import com.smartthing.spi.user.IUserSearchCriteria;
import com.smartthing.spi.user.request.IUserCreateRequest;

/**
 * HBase specifics for dealing with SiteWhere users.
 * 
 * @author Derek
 */
public class HBaseUser {

    /**
     * Create a new user.
     * 
     * @param context
     * @param request
     * @param encodePassword
     * @return
     * @throws SmartThingException
     */
    public static User createUser(IHBaseContext context, IUserCreateRequest request, boolean encodePassword)
	    throws SmartThingException {
	User existing = getUserByUsername(context, request.getUsername());
	if (existing != null) {
	    throw new SmartThingSystemException(ErrorCode.DuplicateUser, ErrorLevel.ERROR,
		    HttpServletResponse.SC_CONFLICT);
	}

	// Create the new user and store it.
	User user = SmartThingPersistence.userCreateLogic(request, encodePassword);
	byte[] primary = getUserRowKey(request.getUsername());
	byte[] payload = context.getPayloadMarshaler().encodeUser(user);

	Table users = null;
	try {
	    users = getUsersTableInterface(context);
	    Put put = new Put(primary);
	    HBaseUtils.addPayloadFields(context.getPayloadMarshaler().getEncoding(), put, payload);
	    users.put(put);
	} catch (IOException e) {
	    throw new SmartThingException("Unable to set JSON for user.", e);
	} finally {
	    HBaseUtils.closeCleanly(users);
	}

	return user;
    }

    /**
     * Import a user (including hashed password).
     * 
     * @param context
     * @param imported
     * @param overwrite
     * @return
     * @throws SmartThingException
     */
    public static User importUser(IHBaseContext context, IUser imported, boolean overwrite) throws SmartThingException {
	if (!overwrite) {
	    User existing = getUserByUsername(context, imported.getUsername());
	    if (existing != null) {
		throw new SmartThingSystemException(ErrorCode.DuplicateUser, ErrorLevel.ERROR,
			HttpServletResponse.SC_CONFLICT);
	    }
	}

	User user = User.copy(imported);
	byte[] primary = getUserRowKey(imported.getUsername());
	byte[] payload = context.getPayloadMarshaler().encodeUser(user);

	Table users = null;
	try {
	    users = getUsersTableInterface(context);
	    Put put = new Put(primary);
	    HBaseUtils.addPayloadFields(context.getPayloadMarshaler().getEncoding(), put, payload);
	    users.put(put);
	} catch (IOException e) {
	    throw new SmartThingException("Unable to set JSON for user.", e);
	} finally {
	    HBaseUtils.closeCleanly(users);
	}

	return user;
    }

    /**
     * Update an existing user.
     * 
     * @param context
     * @param username
     * @param request
     * @param encodePassword
     * @return
     * @throws SmartThingException
     */
    public static User updateUser(IHBaseContext context, String username, IUserCreateRequest request,
	    boolean encodePassword) throws SmartThingException {
	User updated = getUserByUsername(context, username);
	if (updated == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidUsername, ErrorLevel.ERROR);
	}
	SmartThingPersistence.userUpdateLogic(request, updated, encodePassword);

	byte[] primary = getUserRowKey(username);
	byte[] payload = context.getPayloadMarshaler().encodeUser(updated);

	Table users = null;
	try {
	    users = getUsersTableInterface(context);
	    Put put = new Put(primary);
	    HBaseUtils.addPayloadFields(context.getPayloadMarshaler().getEncoding(), put, payload);
	    users.put(put);
	} catch (IOException e) {
	    throw new SmartThingException("Unable to set JSON for user.", e);
	} finally {
	    HBaseUtils.closeCleanly(users);
	}
	return updated;
    }

    /**
     * Delete an existing user.
     * 
     * @param context
     * @param username
     * @param force
     * @return
     * @throws SmartThingException
     */
    public static User deleteUser(IHBaseContext context, String username, boolean force) throws SmartThingException {
	User existing = getUserByUsername(context, username);
	if (existing == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidUsername, ErrorLevel.ERROR);
	}
	existing.setDeleted(true);
	byte[] primary = getUserRowKey(username);
	if (force) {
	    Table users = null;
	    try {
		users = getUsersTableInterface(context);
		Delete delete = new Delete(primary);
		users.delete(delete);
	    } catch (IOException e) {
		throw new SmartThingException("Unable to delete user.", e);
	    } finally {
		HBaseUtils.closeCleanly(users);
	    }
	} else {
	    byte[] marker = { (byte) 0x01 };
	    SmartThingPersistence.setUpdatedEntityMetadata(existing);
	    byte[] payload = context.getPayloadMarshaler().encodeUser(existing);
	    Table users = null;
	    try {
		users = getUsersTableInterface(context);
		Put put = new Put(primary);
		HBaseUtils.addPayloadFields(context.getPayloadMarshaler().getEncoding(), put, payload);
		put.addColumn(ISmartThingHBase.FAMILY_ID, ISmartThingHBase.DELETED, marker);
		users.put(put);
	    } catch (IOException e) {
		throw new SmartThingException("Unable to set deleted flag for user.", e);
	    } finally {
		HBaseUtils.closeCleanly(users);
	    }
	}
	SmartThingPersistence.userDeleteLogic(username);
	return existing;
    }

    /**
     * Get a user by unique username. Returns null if not found.
     * 
     * @param context
     * @param username
     * @return
     * @throws SmartThingException
     */
    public static User getUserByUsername(IHBaseContext context, String username) throws SmartThingException {
	byte[] rowkey = getUserRowKey(username);

	Table users = null;
	try {
	    users = getUsersTableInterface(context);
	    Get get = new Get(rowkey);
	    HBaseUtils.addPayloadFields(get);
	    Result result = users.get(get);

	    byte[] type = result.getValue(ISmartThingHBase.FAMILY_ID, ISmartThingHBase.PAYLOAD_TYPE);
	    byte[] payload = result.getValue(ISmartThingHBase.FAMILY_ID, ISmartThingHBase.PAYLOAD);
	    if ((type == null) || (payload == null)) {
		return null;
	    }

	    return PayloadMarshalerResolver.getInstance().getMarshaler(type).decodeUser(payload);
	} catch (IOException e) {
	    throw new SmartThingException("Unable to load user by username.", e);
	} finally {
	    HBaseUtils.closeCleanly(users);
	}
    }

    /**
     * Authenticate a username password combination.
     * 
     * @param context
     * @param username
     * @param password
     * @return
     * @throws SmartThingException
     */
    public static User authenticate(IHBaseContext context, String username, String password) throws SmartThingException {
	if (password == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidPassword, ErrorLevel.ERROR,
		    HttpServletResponse.SC_BAD_REQUEST);
	}
	User existing = getUserByUsername(context, username);
	if (existing == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidUsername, ErrorLevel.ERROR,
		    HttpServletResponse.SC_UNAUTHORIZED);
	}
	String inPassword = SmartThingPersistence.encodePassword(password);
	if (!existing.getHashedPassword().equals(inPassword)) {
	    throw new SmartThingSystemException(ErrorCode.InvalidPassword, ErrorLevel.ERROR,
		    HttpServletResponse.SC_UNAUTHORIZED);
	}

	// Update last login date.
	existing.setLastLogin(new Date());
	byte[] primary = getUserRowKey(username);
	byte[] payload = context.getPayloadMarshaler().encodeUser(existing);

	Table users = null;
	try {
	    users = getUsersTableInterface(context);
	    Put put = new Put(primary);
	    HBaseUtils.addPayloadFields(context.getPayloadMarshaler().getEncoding(), put, payload);
	    users.put(put);
	} catch (IOException e) {
	    throw new SmartThingException("Unable to set deleted flag for user.", e);
	} finally {
	    HBaseUtils.closeCleanly(users);
	}
	return existing;
    }

    /**
     * List users that match certain criteria.
     * 
     * @param context
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public static List<IUser> listUsers(IHBaseContext context, IUserSearchCriteria criteria) throws SmartThingException {

	Table users = null;
	ResultScanner scanner = null;
	try {
	    users = getUsersTableInterface(context);
	    Scan scan = new Scan();
	    scan.setStartRow(new byte[] { UserRecordType.User.getType() });
	    scan.setStopRow(new byte[] { UserRecordType.GrantedAuthority.getType() });
	    scanner = users.getScanner(scan);

	    ArrayList<IUser> matches = new ArrayList<IUser>();
	    for (Result result : scanner) {
		boolean shouldAdd = true;
		Map<byte[], byte[]> row = result.getFamilyMap(ISmartThingHBase.FAMILY_ID);

		byte[] payloadType = null;
		byte[] payload = null;
		for (byte[] qualifier : row.keySet()) {
		    if ((Bytes.equals(ISmartThingHBase.DELETED, qualifier)) && (!criteria.isIncludeDeleted())) {
			shouldAdd = false;
		    }
		    if (Bytes.equals(ISmartThingHBase.PAYLOAD_TYPE, qualifier)) {
			payloadType = row.get(qualifier);
		    }
		    if (Bytes.equals(ISmartThingHBase.PAYLOAD, qualifier)) {
			payload = row.get(qualifier);
		    }
		}
		if ((shouldAdd) && (payloadType != null) && (payload != null)) {
		    matches.add(PayloadMarshalerResolver.getInstance().getMarshaler(payloadType).decodeUser(payload));
		}
	    }
	    return matches;
	} catch (IOException e) {
	    throw new SmartThingException("Error scanning user rows.", e);
	} finally {
	    if (scanner != null) {
		scanner.close();
	    }
	    HBaseUtils.closeCleanly(users);
	}
    }

    /**
     * Get the list of granted authorities for a user.
     * 
     * @param context
     * @param username
     * @return
     * @throws SmartThingException
     */
    public static List<IGrantedAuthority> getGrantedAuthorities(IHBaseContext context, String username)
	    throws SmartThingException {
	IUser user = getUserByUsername(context, username);
	List<String> userAuths = user.getAuthorities();
	List<IGrantedAuthority> all = HBaseGrantedAuthority.listGrantedAuthorities(context,
		new GrantedAuthoritySearchCriteria());
	List<IGrantedAuthority> matched = new ArrayList<IGrantedAuthority>();
	for (IGrantedAuthority auth : all) {
	    if (userAuths.contains(auth.getAuthority())) {
		matched.add(auth);
	    }
	}
	return matched;
    }

    /**
     * Get row key for a user.
     * 
     * @param username
     * @return
     */
    public static byte[] getUserRowKey(String username) {
	byte[] userBytes = Bytes.toBytes(username);
	ByteBuffer buffer = ByteBuffer.allocate(1 + userBytes.length);
	buffer.put(UserRecordType.User.getType());
	buffer.put(userBytes);
	return buffer.array();
    }

    /**
     * Get users table based on context.
     * 
     * @param context
     * @return
     * @throws SmartThingException
     */
    protected static Table getUsersTableInterface(IHBaseContext context) throws SmartThingException {
	return context.getClient().getTableInterface(ISmartThingHBase.USERS_TABLE_NAME);
    }
}