/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.encoder;

import com.smartthing.common.MarshalUtils;
import com.smartthing.rest.model.asset.AssetCategory;
import com.smartthing.rest.model.asset.HardwareAsset;
import com.smartthing.rest.model.asset.LocationAsset;
import com.smartthing.rest.model.asset.PersonAsset;
import com.smartthing.rest.model.device.Device;
import com.smartthing.rest.model.device.DeviceAssignment;
import com.smartthing.rest.model.device.DeviceAssignmentState;
import com.smartthing.rest.model.device.DeviceSpecification;
import com.smartthing.rest.model.device.Site;
import com.smartthing.rest.model.device.Zone;
import com.smartthing.rest.model.device.batch.BatchElement;
import com.smartthing.rest.model.device.batch.BatchOperation;
import com.smartthing.rest.model.device.command.DeviceCommand;
import com.smartthing.rest.model.device.event.DeviceAlert;
import com.smartthing.rest.model.device.event.DeviceCommandInvocation;
import com.smartthing.rest.model.device.event.DeviceCommandResponse;
import com.smartthing.rest.model.device.event.DeviceLocation;
import com.smartthing.rest.model.device.event.DeviceMeasurements;
import com.smartthing.rest.model.device.event.DeviceStateChange;
import com.smartthing.rest.model.device.event.DeviceStreamData;
import com.smartthing.rest.model.device.group.DeviceGroup;
import com.smartthing.rest.model.device.group.DeviceGroupElement;
import com.smartthing.rest.model.device.streaming.DeviceStream;
import com.smartthing.rest.model.tenant.Tenant;
import com.smartthing.rest.model.user.GrantedAuthority;
import com.smartthing.rest.model.user.User;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.IAssetCategory;
import com.smartthing.spi.asset.IHardwareAsset;
import com.smartthing.spi.asset.ILocationAsset;
import com.smartthing.spi.asset.IPersonAsset;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceAssignmentState;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.ISite;
import com.smartthing.spi.device.IZone;
import com.smartthing.spi.device.batch.IBatchElement;
import com.smartthing.spi.device.batch.IBatchOperation;
import com.smartthing.spi.device.command.IDeviceCommand;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.device.event.IDeviceStreamData;
import com.smartthing.spi.device.group.IDeviceGroup;
import com.smartthing.spi.device.group.IDeviceGroupElement;
import com.smartthing.spi.device.streaming.IDeviceStream;
import com.smartthing.spi.tenant.ITenant;
import com.smartthing.spi.user.IGrantedAuthority;
import com.smartthing.spi.user.IUser;

/**
 * Implementation of {@link IPayloadMarshaler} that marshals objects to JSON.
 * 
 * @author Derek
 */
public class JsonPayloadMarshaler implements IPayloadMarshaler {

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.encoder.IPayloadMarshaler#getEncoding()
     */
    @Override
    public PayloadEncoding getEncoding() throws SmartThingException {
	return PayloadEncoding.Json;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encode(java.lang.Object)
     */
    @Override
    public byte[] encode(Object obj) throws SmartThingException {
	return MarshalUtils.marshalJson(obj);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.encoder.IPayloadMarshaler#decode(byte[],
     * java.lang.Class)
     */
    @Override
    public <T> T decode(byte[] payload, Class<T> type) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, type);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeSite(com.smartthing.
     * spi.device .ISite)
     */
    @Override
    public byte[] encodeSite(ISite site) throws SmartThingException {
	return MarshalUtils.marshalJson(site);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.encoder.IPayloadMarshaler#decodeSite(byte[])
     */
    @Override
    public Site decodeSite(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, Site.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeZone(com.smartthing.
     * spi.device .IZone)
     */
    @Override
    public byte[] encodeZone(IZone zone) throws SmartThingException {
	return MarshalUtils.marshalJson(zone);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.encoder.IPayloadMarshaler#decodeZone(byte[])
     */
    @Override
    public Zone decodeZone(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, Zone.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceSpecification(
     * com.smartthing .spi.device.IDeviceSpecification)
     */
    @Override
    public byte[] encodeDeviceSpecification(IDeviceSpecification specification) throws SmartThingException {
	return MarshalUtils.marshalJson(specification);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceSpecification(
     * byte[])
     */
    @Override
    public DeviceSpecification decodeDeviceSpecification(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceSpecification.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDevice(com.smartthing.
     * spi.device .IDevice)
     */
    @Override
    public byte[] encodeDevice(IDevice device) throws SmartThingException {
	return MarshalUtils.marshalJson(device);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDevice(byte[])
     */
    @Override
    public Device decodeDevice(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, Device.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceAssignment(com.
     * sitewhere .spi.device.IDeviceAssignment)
     */
    @Override
    public byte[] encodeDeviceAssignment(IDeviceAssignment assignment) throws SmartThingException {
	return MarshalUtils.marshalJson(assignment);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceAssignment(byte
     * [])
     */
    @Override
    public DeviceAssignment decodeDeviceAssignment(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceAssignment.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceStream(com.
     * sitewhere. spi.device.streaming.IDeviceStream)
     */
    @Override
    public byte[] encodeDeviceStream(IDeviceStream stream) throws SmartThingException {
	return MarshalUtils.marshalJson(stream);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceStream(byte[])
     */
    @Override
    public DeviceStream decodeDeviceStream(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceStream.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceAssignmentState
     * (com.smartthing .spi.device.IDeviceAssignmentState)
     */
    @Override
    public byte[] encodeDeviceAssignmentState(IDeviceAssignmentState state) throws SmartThingException {
	return MarshalUtils.marshalJson(state);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceAssignmentState
     * (byte[])
     */
    @Override
    public DeviceAssignmentState decodeDeviceAssignmentState(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceAssignmentState.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceMeasurements(
     * com.smartthing .spi.device.event.IDeviceMeasurements)
     */
    @Override
    public byte[] encodeDeviceMeasurements(IDeviceMeasurements measurements) throws SmartThingException {
	return MarshalUtils.marshalJson(measurements);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceMeasurements(
     * byte[])
     */
    @Override
    public DeviceMeasurements decodeDeviceMeasurements(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceMeasurements.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceLocation(com.
     * sitewhere .spi.device.event.IDeviceLocation)
     */
    @Override
    public byte[] encodeDeviceLocation(IDeviceLocation location) throws SmartThingException {
	return MarshalUtils.marshalJson(location);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceLocation(byte[]
     * )
     */
    @Override
    public DeviceLocation decodeDeviceLocation(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceLocation.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceAlert(com.
     * sitewhere.spi .device.event.IDeviceAlert)
     */
    @Override
    public byte[] encodeDeviceAlert(IDeviceAlert alert) throws SmartThingException {
	return MarshalUtils.marshalJson(alert);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceAlert(byte[])
     */
    @Override
    public DeviceAlert decodeDeviceAlert(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceAlert.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceStreamData(com.
     * sitewhere .spi.device.event.IDeviceStreamData)
     */
    @Override
    public byte[] encodeDeviceStreamData(IDeviceStreamData streamData) throws SmartThingException {
	return MarshalUtils.marshalJson(streamData);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceStreamData(byte
     * [])
     */
    @Override
    public DeviceStreamData decodeDeviceStreamData(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceStreamData.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.encoder.IPayloadMarshaler#
     * encodeDeviceCommandInvocation(com
     * .sitewhere.spi.device.event.IDeviceCommandInvocation)
     */
    @Override
    public byte[] encodeDeviceCommandInvocation(IDeviceCommandInvocation invocation) throws SmartThingException {
	return MarshalUtils.marshalJson(invocation);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.encoder.IPayloadMarshaler#
     * decodeDeviceCommandInvocation(byte[])
     */
    @Override
    public DeviceCommandInvocation decodeDeviceCommandInvocation(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceCommandInvocation.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceStateChange(com
     * .sitewhere .spi.device.event.IDeviceStateChange)
     */
    @Override
    public byte[] encodeDeviceStateChange(IDeviceStateChange change) throws SmartThingException {
	return MarshalUtils.marshalJson(change);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceStateChange(
     * byte[])
     */
    @Override
    public DeviceStateChange decodeDeviceStateChange(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceStateChange.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceCommandResponse
     * (com.smartthing .spi.device.event.IDeviceCommandResponse)
     */
    @Override
    public byte[] encodeDeviceCommandResponse(IDeviceCommandResponse response) throws SmartThingException {
	return MarshalUtils.marshalJson(response);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceCommandResponse
     * (byte[])
     */
    @Override
    public DeviceCommandResponse decodeDeviceCommandResponse(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceCommandResponse.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeBatchOperation(com.
     * sitewhere .spi.device.batch.IBatchOperation)
     */
    @Override
    public byte[] encodeBatchOperation(IBatchOperation operation) throws SmartThingException {
	return MarshalUtils.marshalJson(operation);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeBatchOperation(byte[]
     * )
     */
    @Override
    public BatchOperation decodeBatchOperation(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, BatchOperation.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeBatchElement(com.
     * sitewhere. spi.device.batch.IBatchElement)
     */
    @Override
    public byte[] encodeBatchElement(IBatchElement element) throws SmartThingException {
	return MarshalUtils.marshalJson(element);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeBatchElement(byte[])
     */
    @Override
    public BatchElement decodeBatchElement(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, BatchElement.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceGroup(com.
     * sitewhere.spi .device.group.IDeviceGroup)
     */
    @Override
    public byte[] encodeDeviceGroup(IDeviceGroup group) throws SmartThingException {
	return MarshalUtils.marshalJson(group);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceGroup(byte[])
     */
    @Override
    public DeviceGroup decodeDeviceGroup(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceGroup.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceGroupElement(
     * com.smartthing .spi.device.group.IDeviceGroupElement)
     */
    @Override
    public byte[] encodeDeviceGroupElement(IDeviceGroupElement element) throws SmartThingException {
	return MarshalUtils.marshalJson(element);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceGroupElement(
     * byte[])
     */
    @Override
    public DeviceGroupElement decodeDeviceGroupElement(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceGroupElement.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeDeviceCommand(com.
     * sitewhere .spi.device.command.IDeviceCommand)
     */
    @Override
    public byte[] encodeDeviceCommand(IDeviceCommand command) throws SmartThingException {
	return MarshalUtils.marshalJson(command);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeDeviceCommand(byte[])
     */
    @Override
    public DeviceCommand decodeDeviceCommand(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, DeviceCommand.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeUser(com.smartthing.
     * spi.user .IUser)
     */
    @Override
    public byte[] encodeUser(IUser user) throws SmartThingException {
	return MarshalUtils.marshalJson(user);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.encoder.IPayloadMarshaler#decodeUser(byte[])
     */
    @Override
    public User decodeUser(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, User.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeGrantedAuthority(com.
     * sitewhere .spi.user.IGrantedAuthority)
     */
    @Override
    public byte[] encodeGrantedAuthority(IGrantedAuthority auth) throws SmartThingException {
	return MarshalUtils.marshalJson(auth);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeGrantedAuthority(byte
     * [])
     */
    @Override
    public GrantedAuthority decodeGrantedAuthority(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, GrantedAuthority.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeAssetCategory(com.
     * sitewhere .spi.asset.IAssetCategory)
     */
    @Override
    public byte[] encodeAssetCategory(IAssetCategory category) throws SmartThingException {
	return MarshalUtils.marshalJson(category);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeAssetCategory(byte[])
     */
    @Override
    public AssetCategory decodeAssetCategory(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, AssetCategory.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.encoder.IPayloadMarshaler#encodePersonAsset(com.
     * sitewhere.spi .asset.IPersonAsset)
     */
    @Override
    public byte[] encodePersonAsset(IPersonAsset asset) throws SmartThingException {
	return MarshalUtils.marshalJson(asset);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodePersonAsset(byte[])
     */
    @Override
    public PersonAsset decodePersonAsset(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, PersonAsset.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeHardwareAsset(com.
     * sitewhere .spi.asset.IHardwareAsset)
     */
    @Override
    public byte[] encodeHardwareAsset(IHardwareAsset asset) throws SmartThingException {
	return MarshalUtils.marshalJson(asset);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeHardwareAsset(byte[])
     */
    @Override
    public HardwareAsset decodeHardwareAsset(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, HardwareAsset.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeLocationAsset(com.
     * sitewhere .spi.asset.ILocationAsset)
     */
    @Override
    public byte[] encodeLocationAsset(ILocationAsset asset) throws SmartThingException {
	return MarshalUtils.marshalJson(asset);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#decodeLocationAsset(byte[])
     */
    @Override
    public LocationAsset decodeLocationAsset(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, LocationAsset.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.hbase.encoder.IPayloadMarshaler#encodeTenant(com.smartthing.
     * spi.user .ITenant)
     */
    @Override
    public byte[] encodeTenant(ITenant tenant) throws SmartThingException {
	return MarshalUtils.marshalJson(tenant);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.encoder.IPayloadMarshaler#decodeTenant(byte[])
     */
    @Override
    public Tenant decodeTenant(byte[] payload) throws SmartThingException {
	return MarshalUtils.unmarshalJson(payload, Tenant.class);
    }
}