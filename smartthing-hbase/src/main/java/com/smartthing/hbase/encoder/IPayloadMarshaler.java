/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.encoder;

import com.smartthing.rest.model.asset.AssetCategory;
import com.smartthing.rest.model.asset.HardwareAsset;
import com.smartthing.rest.model.asset.LocationAsset;
import com.smartthing.rest.model.asset.PersonAsset;
import com.smartthing.rest.model.device.Device;
import com.smartthing.rest.model.device.DeviceAssignment;
import com.smartthing.rest.model.device.DeviceAssignmentState;
import com.smartthing.rest.model.device.DeviceSpecification;
import com.smartthing.rest.model.device.Site;
import com.smartthing.rest.model.device.Zone;
import com.smartthing.rest.model.device.batch.BatchElement;
import com.smartthing.rest.model.device.batch.BatchOperation;
import com.smartthing.rest.model.device.command.DeviceCommand;
import com.smartthing.rest.model.device.event.DeviceAlert;
import com.smartthing.rest.model.device.event.DeviceCommandInvocation;
import com.smartthing.rest.model.device.event.DeviceCommandResponse;
import com.smartthing.rest.model.device.event.DeviceLocation;
import com.smartthing.rest.model.device.event.DeviceMeasurements;
import com.smartthing.rest.model.device.event.DeviceStateChange;
import com.smartthing.rest.model.device.event.DeviceStreamData;
import com.smartthing.rest.model.device.group.DeviceGroup;
import com.smartthing.rest.model.device.group.DeviceGroupElement;
import com.smartthing.rest.model.device.streaming.DeviceStream;
import com.smartthing.rest.model.tenant.Tenant;
import com.smartthing.rest.model.user.GrantedAuthority;
import com.smartthing.rest.model.user.User;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.IAssetCategory;
import com.smartthing.spi.asset.IHardwareAsset;
import com.smartthing.spi.asset.ILocationAsset;
import com.smartthing.spi.asset.IPersonAsset;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceAssignmentState;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.ISite;
import com.smartthing.spi.device.IZone;
import com.smartthing.spi.device.batch.IBatchElement;
import com.smartthing.spi.device.batch.IBatchOperation;
import com.smartthing.spi.device.command.IDeviceCommand;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.device.event.IDeviceStreamData;
import com.smartthing.spi.device.group.IDeviceGroup;
import com.smartthing.spi.device.group.IDeviceGroupElement;
import com.smartthing.spi.device.streaming.IDeviceStream;
import com.smartthing.spi.tenant.ITenant;
import com.smartthing.spi.user.IGrantedAuthority;
import com.smartthing.spi.user.IUser;

/**
 * Interface for classes that can encode SiteWhere objects into byte arrays.
 * 
 * @author Derek
 */
public interface IPayloadMarshaler {

    /**
     * Gets encoding type for the encoder.
     * 
     * @return
     * @throws SmartThingException
     */
    public PayloadEncoding getEncoding() throws SmartThingException;

    /**
     * Encode an object.
     * 
     * @param obj
     * @return
     * @throws SmartThingException
     */
    public byte[] encode(Object obj) throws SmartThingException;

    /**
     * Decode a payload into an object.
     * 
     * @param payload
     * @param type
     * @return
     * @throws SmartThingException
     */
    public <T> T decode(byte[] payload, Class<T> type) throws SmartThingException;

    /**
     * Encode an {@link ISite}.
     * 
     * @param site
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeSite(ISite site) throws SmartThingException;

    /**
     * Decode a {@link Site} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public Site decodeSite(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IZone}.
     * 
     * @param zone
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeZone(IZone zone) throws SmartThingException;

    /**
     * Decode a {@link Zone} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public Zone decodeZone(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceSpecification}.
     * 
     * @param specification
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceSpecification(IDeviceSpecification specification) throws SmartThingException;

    /**
     * Decode a {@link DeviceSpecification} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceSpecification decodeDeviceSpecification(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDevice}.
     * 
     * @param device
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDevice(IDevice device) throws SmartThingException;

    /**
     * Decodea {@link Device} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public Device decodeDevice(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceAssignment}.
     * 
     * @param assignment
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceAssignment(IDeviceAssignment assignment) throws SmartThingException;

    /**
     * Decode a {@link DeviceAssignment} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceAssignment decodeDeviceAssignment(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceStream}.
     * 
     * @param stream
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceStream(IDeviceStream stream) throws SmartThingException;

    /**
     * Decode a {@link DeviceStream} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceStream decodeDeviceStream(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceAssignmentState}.
     * 
     * @param state
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceAssignmentState(IDeviceAssignmentState state) throws SmartThingException;

    /**
     * Decode a {@link DeviceAssignmentState} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceAssignmentState decodeDeviceAssignmentState(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceMeasurements}.
     * 
     * @param measurements
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceMeasurements(IDeviceMeasurements measurements) throws SmartThingException;

    /**
     * Decode a {@link DeviceMeasurements} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceMeasurements decodeDeviceMeasurements(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceLocation}.
     * 
     * @param location
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceLocation(IDeviceLocation location) throws SmartThingException;

    /**
     * Decode a {@link DeviceLocation} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceLocation decodeDeviceLocation(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceAlert}.
     * 
     * @param alert
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceAlert(IDeviceAlert alert) throws SmartThingException;

    /**
     * Decode a {@link DeviceAlert} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceAlert decodeDeviceAlert(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceStreamData}.
     * 
     * @param streamData
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceStreamData(IDeviceStreamData streamData) throws SmartThingException;

    /**
     * Decode a {@link DeviceStreamData} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceStreamData decodeDeviceStreamData(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceCommandInvocation}.
     * 
     * @param invocation
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceCommandInvocation(IDeviceCommandInvocation invocation) throws SmartThingException;

    /**
     * Decode a {@link DeviceCommandInvocation} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceCommandInvocation decodeDeviceCommandInvocation(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceStateChange}.
     * 
     * @param change
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceStateChange(IDeviceStateChange change) throws SmartThingException;

    /**
     * Decode a {@link DeviceStateChange} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceStateChange decodeDeviceStateChange(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceCommandResponse}.
     * 
     * @param response
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceCommandResponse(IDeviceCommandResponse response) throws SmartThingException;

    /**
     * Decode a {@link DeviceCommandResponse} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceCommandResponse decodeDeviceCommandResponse(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IBatchOperation}.
     * 
     * @param operation
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeBatchOperation(IBatchOperation operation) throws SmartThingException;

    /**
     * Decode a {@link BatchOperation} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public BatchOperation decodeBatchOperation(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IBatchElement}.
     * 
     * @param element
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeBatchElement(IBatchElement element) throws SmartThingException;

    /**
     * Decode a {@link BatchElement} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public BatchElement decodeBatchElement(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceGroup}.
     * 
     * @param group
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceGroup(IDeviceGroup group) throws SmartThingException;

    /**
     * Decode a {@link DeviceGroup} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceGroup decodeDeviceGroup(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceGroupElement}.
     * 
     * @param element
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceGroupElement(IDeviceGroupElement element) throws SmartThingException;

    /**
     * Decode a {@link DeviceGroupElement} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceGroupElement decodeDeviceGroupElement(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IDeviceCommand}.
     * 
     * @param command
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeDeviceCommand(IDeviceCommand command) throws SmartThingException;

    /**
     * Decode a {@link DeviceCommand} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public DeviceCommand decodeDeviceCommand(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IUser}.
     * 
     * @param user
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeUser(IUser user) throws SmartThingException;

    /**
     * Decode a {@link User} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public User decodeUser(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IGrantedAuthority}.
     * 
     * @param auth
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeGrantedAuthority(IGrantedAuthority auth) throws SmartThingException;

    /**
     * Decode a {@link GrantedAuthority} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public GrantedAuthority decodeGrantedAuthority(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IAssetCategory}.
     * 
     * @param category
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeAssetCategory(IAssetCategory category) throws SmartThingException;

    /**
     * Decode an {@link IAssetCategory} from the binary payload.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public AssetCategory decodeAssetCategory(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IPersonAsset}.
     * 
     * @param asset
     * @return
     * @throws SmartThingException
     */
    public byte[] encodePersonAsset(IPersonAsset asset) throws SmartThingException;

    /**
     * Decode a {@link PersonAsset}.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public PersonAsset decodePersonAsset(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link IHardwareAsset}.
     * 
     * @param asset
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeHardwareAsset(IHardwareAsset asset) throws SmartThingException;

    /**
     * Decode a {@link HardwareAsset}.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public HardwareAsset decodeHardwareAsset(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link ILocationAsset}.
     * 
     * @param asset
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeLocationAsset(ILocationAsset asset) throws SmartThingException;

    /**
     * Decode a {@link LocationAsset}.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public LocationAsset decodeLocationAsset(byte[] payload) throws SmartThingException;

    /**
     * Encode an {@link ITenant}.
     * 
     * @param tenant
     * @return
     * @throws SmartThingException
     */
    public byte[] encodeTenant(ITenant tenant) throws SmartThingException;

    /**
     * Deocde a {@link Tenant}.
     * 
     * @param payload
     * @return
     * @throws SmartThingException
     */
    public Tenant decodeTenant(byte[] payload) throws SmartThingException;
}