/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.common;

import com.smartthing.hbase.IHBaseContext;
import com.smartthing.spi.SmartThingException;

/**
 * Abstracts the idea of building a row key based on a hierarchical scheme.
 * 
 * @author Derek
 */
public interface IRowKeyBuilder {

    /**
     * Get number of bytes (max of 8) saved as part of the record key.
     * 
     * @return
     */
    public int getKeyIdLength();

    /**
     * Throws an exception if an invalid key is referenced.
     * 
     * @throws SmartThingException
     */
    public void throwInvalidKey() throws SmartThingException;

    /**
     * Gets identifier that marks a row of the given type.
     * 
     * @return
     */
    public byte getTypeIdentifier();

    /**
     * Get identifier that marks a row as primary.
     * 
     * @return
     */
    public byte getPrimaryIdentifier();

    /**
     * Builds a primary entity row key based on a unique token.
     * 
     * @param context
     * @param token
     * @return
     * @throws SmartThingException
     */
    public byte[] buildPrimaryKey(IHBaseContext context, String token) throws SmartThingException;

    /**
     * Builds a subordinate key based on the type indicator.
     * 
     * @param context
     * @param token
     * @param type
     * @return
     * @throws SmartThingException
     */
    public byte[] buildSubkey(IHBaseContext context, String token, byte type) throws SmartThingException;

    /**
     * Deletes a reference to a token.
     * 
     * @param context
     * @param token
     * @throws SmartThingException
     */
    public void deleteReference(IHBaseContext context, String token) throws SmartThingException;
}