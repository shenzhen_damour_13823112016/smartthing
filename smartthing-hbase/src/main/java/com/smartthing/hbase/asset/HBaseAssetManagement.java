/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.asset;

import org.apache.hadoop.hbase.regionserver.BloomType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.hbase.HBaseContext;
import com.smartthing.hbase.ISmartThingHBase;
import com.smartthing.hbase.ISmartThingHBaseClient;
import com.smartthing.hbase.common.SmartThingTables;
import com.smartthing.hbase.encoder.IPayloadMarshaler;
import com.smartthing.hbase.encoder.ProtobufPayloadMarshaler;
import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.asset.IAsset;
import com.smartthing.spi.asset.IAssetCategory;
import com.smartthing.spi.asset.IAssetManagement;
import com.smartthing.spi.asset.IHardwareAsset;
import com.smartthing.spi.asset.ILocationAsset;
import com.smartthing.spi.asset.IPersonAsset;
import com.smartthing.spi.asset.request.IAssetCategoryCreateRequest;
import com.smartthing.spi.asset.request.IHardwareAssetCreateRequest;
import com.smartthing.spi.asset.request.ILocationAssetCreateRequest;
import com.smartthing.spi.asset.request.IPersonAssetCreateRequest;
import com.smartthing.spi.search.ISearchCriteria;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * HBase implementation of {@link IAssetManagement} interface.
 *
 * @author Derek
 */
public class HBaseAssetManagement extends TenantLifecycleComponent implements IAssetManagement {

    /** Static logger instance */
    private static final Logger LOGGER = LogManager.getLogger();

    /** Used to communicate with HBase */
    private ISmartThingHBaseClient client;

    /** Injected payload encoder */
    private IPayloadMarshaler payloadMarshaler = new ProtobufPayloadMarshaler();

    /** Supplies context to implementation methods */
    private HBaseContext context;

    /** Asset id manager */
    private AssetIdManager assetIdManager;

    public HBaseAssetManagement() {
	super(LifecycleComponentType.DataStore);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
     * .server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Create context from configured options.
	this.context = new HBaseContext();
	context.setTenant(getTenant());
	context.setClient(getClient());
	context.setPayloadMarshaler(getPayloadMarshaler());

	ensureTablesExist();

	// Create device id manager instance.
	assetIdManager = new AssetIdManager();
	assetIdManager.load(context);
	context.setAssetIdManager(assetIdManager);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /**
     * Make sure that all SiteWhere tables exist, creating them if necessary.
     *
     * @throws SmartThingException
     */
    protected void ensureTablesExist() throws SmartThingException {
	SmartThingTables.assureTenantTable(context, ISmartThingHBase.UID_TABLE_NAME, BloomType.ROW);
	SmartThingTables.assureTenantTable(context, ISmartThingHBase.ASSETS_TABLE_NAME, BloomType.ROW);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.asset.IAssetManagement#createAssetCategory(com.
     * sitewhere.spi. asset.request.IAssetCategoryCreateRequest)
     */
    @Override
    public IAssetCategory createAssetCategory(IAssetCategoryCreateRequest request) throws SmartThingException {
	return HBaseAssetCategory.createAssetCategory(context, request);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.asset.IAssetManagement#getAssetCategory(java.lang.
     * String)
     */
    @Override
    public IAssetCategory getAssetCategory(String categoryId) throws SmartThingException {
	return HBaseAssetCategory.getAssetCategoryById(context, categoryId);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.asset.IAssetManagement#updateAssetCategory(java.lang.
     * String, com.smartthing.spi.asset.request.IAssetCategoryCreateRequest)
     */
    @Override
    public IAssetCategory updateAssetCategory(String categoryId, IAssetCategoryCreateRequest request)
	    throws SmartThingException {
	return HBaseAssetCategory.updateAssetCategory(context, categoryId, request);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.asset.IAssetManagement#listAssetCategories(com.
     * sitewhere.spi. search.ISearchCriteria)
     */
    @Override
    public ISearchResults<IAssetCategory> listAssetCategories(ISearchCriteria criteria) throws SmartThingException {
	return HBaseAssetCategory.listAssetCategories(context, criteria);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.asset.IAssetManagement#deleteAssetCategory(java.lang.
     * String)
     */
    @Override
    public IAssetCategory deleteAssetCategory(String categoryId) throws SmartThingException {
	return HBaseAssetCategory.deleteAssetCategory(context, categoryId);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.asset.IAssetManagement#createPersonAsset(java.lang.
     * String, com.smartthing.spi.asset.request.IPersonAssetCreateRequest)
     */
    @Override
    public IPersonAsset createPersonAsset(String categoryId, IPersonAssetCreateRequest request)
	    throws SmartThingException {
	return HBaseAsset.createOrUpdatePersonAsset(context, categoryId, null, request);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.asset.IAssetManagement#updatePersonAsset(java.lang.
     * String, java.lang.String,
     * com.smartthing.spi.asset.request.IPersonAssetCreateRequest)
     */
    @Override
    public IPersonAsset updatePersonAsset(String categoryId, String assetId, IPersonAssetCreateRequest request)
	    throws SmartThingException {
	return HBaseAsset.createOrUpdatePersonAsset(context, categoryId, assetId, request);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.asset.IAssetManagement#createHardwareAsset(java.lang.
     * String, com.smartthing.spi.asset.request.IHardwareAssetCreateRequest)
     */
    @Override
    public IHardwareAsset createHardwareAsset(String categoryId, IHardwareAssetCreateRequest request)
	    throws SmartThingException {
	return HBaseAsset.createOrUpdateHardwareAsset(context, categoryId, null, request);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.asset.IAssetManagement#updateHardwareAsset(java.lang.
     * String, java.lang.String,
     * com.smartthing.spi.asset.request.IHardwareAssetCreateRequest)
     */
    @Override
    public IHardwareAsset updateHardwareAsset(String categoryId, String assetId, IHardwareAssetCreateRequest request)
	    throws SmartThingException {
	return HBaseAsset.createOrUpdateHardwareAsset(context, categoryId, assetId, request);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.asset.IAssetManagement#createLocationAsset(java.lang.
     * String, com.smartthing.spi.asset.request.ILocationAssetCreateRequest)
     */
    @Override
    public ILocationAsset createLocationAsset(String categoryId, ILocationAssetCreateRequest request)
	    throws SmartThingException {
	return HBaseAsset.createOrUpdateLocationAsset(context, categoryId, null, request);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.asset.IAssetManagement#updateLocationAsset(java.lang.
     * String, java.lang.String,
     * com.smartthing.spi.asset.request.ILocationAssetCreateRequest)
     */
    @Override
    public ILocationAsset updateLocationAsset(String categoryId, String assetId, ILocationAssetCreateRequest request)
	    throws SmartThingException {
	return HBaseAsset.createOrUpdateLocationAsset(context, categoryId, assetId, request);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.smartthing.spi.asset.IAssetManagement#getAsset(java.lang.String,
     * java.lang.String)
     */
    @Override
    public IAsset getAsset(String categoryId, String assetId) throws SmartThingException {
	return HBaseAsset.getAsset(context, categoryId, assetId);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.asset.IAssetManagement#deleteAsset(java.lang.String,
     * java.lang.String)
     */
    @Override
    public IAsset deleteAsset(String categoryId, String assetId) throws SmartThingException {
	return HBaseAsset.deleteAsset(context, categoryId, assetId);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.smartthing.spi.asset.IAssetManagement#listAssets(java.lang.String,
     * com.smartthing.spi.search.ISearchCriteria)
     */
    @Override
    public ISearchResults<IAsset> listAssets(String categoryId, ISearchCriteria criteria) throws SmartThingException {
	return HBaseAsset.listAssets(context, categoryId, criteria);
    }

/*=====================================================================================================================================*/
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.smartthing.spi.asset.IAssetManagement#listAssets(java.lang.String,
	 * com.smartthing.spi.search.ISearchCriteria)
	 */
	@Override
	public ISearchResults<IAsset> listAssetsByArange(String categoryId, String arange, ISearchCriteria criteria) throws SmartThingException {
		//TODO
		return null;
	}
/*=====================================================================================================================================*/

    public ISmartThingHBaseClient getClient() {
	return client;
    }

    public void setClient(ISmartThingHBaseClient client) {
	this.client = client;
    }

    public IPayloadMarshaler getPayloadMarshaler() {
	return payloadMarshaler;
    }

    public void setPayloadMarshaler(IPayloadMarshaler payloadMarshaler) {
	this.payloadMarshaler = payloadMarshaler;
    }
}
