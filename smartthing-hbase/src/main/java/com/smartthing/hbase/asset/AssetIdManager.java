/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.asset;

import com.smartthing.hbase.IHBaseContext;
import com.smartthing.hbase.uid.UniqueIdCounterMap;
import com.smartthing.hbase.uid.UniqueIdType;
import com.smartthing.spi.SmartThingException;

/**
 * Singleton that keeps up with asset management entities.
 * 
 * @author Derek
 */
public class AssetIdManager implements IAssetIdManager {

    /** Manager for category ids */
    private UniqueIdCounterMap categoryKeys;

    /**
     * Load existing keys from table.
     * 
     * @param context
     * @throws SmartThingException
     */
    public void load(IHBaseContext context) throws SmartThingException {
	categoryKeys = new UniqueIdCounterMap(context, UniqueIdType.AssetKey.getIndicator(),
		UniqueIdType.AssetValue.getIndicator());
	categoryKeys.refresh();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.hbase.asset.IAssetIdManager#getAssetKeys()
     */
    public UniqueIdCounterMap getAssetKeys() {
	return categoryKeys;
    }
}