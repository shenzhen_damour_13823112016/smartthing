/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.tenant;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import com.smartthing.core.SmartThingPersistence;
import com.smartthing.hbase.IHBaseContext;
import com.smartthing.hbase.ISmartThingHBase;
import com.smartthing.hbase.common.HBaseUtils;
import com.smartthing.hbase.uid.UniqueIdCounterMap;
import com.smartthing.hbase.uid.UniqueIdCounterMapRowKeyBuilder;
import com.smartthing.hbase.user.UserRecordType;
import com.smartthing.rest.model.search.SearchResults;
import com.smartthing.rest.model.search.tenant.TenantSearchCriteria;
import com.smartthing.rest.model.tenant.Tenant;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.common.IFilter;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.search.user.ITenantSearchCriteria;
import com.smartthing.spi.tenant.ITenant;
import com.smartthing.spi.tenant.request.ITenantCreateRequest;

/**
 * HBase specifics for dealing with SiteWhere tenants.
 * 
 * @author Derek
 */
public class HBaseTenant {

    /** Length of group identifier (subset of 8 byte long) */
    public static final int IDENTIFIER_LENGTH = 4;

    /** Used to look up row keys from tokens */
    public static UniqueIdCounterMapRowKeyBuilder KEY_BUILDER = new UniqueIdCounterMapRowKeyBuilder() {

	@Override
	public UniqueIdCounterMap getMap(IHBaseContext context) {
	    return context.getUserIdManager().getTenantKeys();
	}

	@Override
	public byte getTypeIdentifier() {
	    return UserRecordType.Tenant.getType();
	}

	@Override
	public byte getPrimaryIdentifier() {
	    return TenantSubtype.Tenant.getType();
	}

	@Override
	public int getKeyIdLength() {
	    return IDENTIFIER_LENGTH;
	}

	@Override
	public void throwInvalidKey() throws SmartThingException {
	    throw new SmartThingSystemException(ErrorCode.InvalidTenantId, ErrorLevel.ERROR);
	}
    };

    /**
     * Create a new tenant.
     * 
     * @param context
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static Tenant createTenant(IHBaseContext context, ITenantCreateRequest request) throws SmartThingException {
	if (getTenantById(context, request.getId()) != null) {
	    throw new SmartThingSystemException(ErrorCode.DuplicateTenantId, ErrorLevel.ERROR);
	}

	// Add new key to table.
	String id = KEY_BUILDER.getMap(context).useExistingId(request.getId());

	// Use common logic so all backend implementations work the same.
	Tenant tenant = SmartThingPersistence.tenantCreateLogic(request);

	Map<byte[], byte[]> qualifiers = new HashMap<byte[], byte[]>();
	return HBaseUtils.createOrUpdate(context, context.getPayloadMarshaler(), ISmartThingHBase.USERS_TABLE_NAME,
		tenant, id, KEY_BUILDER, qualifiers);
    }

    /**
     * Get an existing tenant by unique id.
     * 
     * @param context
     * @param id
     * @return
     * @throws SmartThingException
     */
    public static Tenant getTenantById(IHBaseContext context, String id) throws SmartThingException {
	if (KEY_BUILDER.getMap(context).getValue(id) == null) {
	    return null;
	}
	return HBaseUtils.get(context, ISmartThingHBase.USERS_TABLE_NAME, id, KEY_BUILDER, Tenant.class);
    }

    /**
     * Update an existing tenant.
     * 
     * @param context
     * @param id
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static Tenant updateTenant(IHBaseContext context, String id, ITenantCreateRequest request)
	    throws SmartThingException {
	Tenant updated = assertTenant(context, id);
	SmartThingPersistence.tenantUpdateLogic(request, updated);
	return HBaseUtils.put(context, context.getPayloadMarshaler(), ISmartThingHBase.USERS_TABLE_NAME, updated, id,
		KEY_BUILDER);
    }

    /**
     * Get tenant based on unique authentication token.
     * 
     * @param context
     * @param authToken
     * @return
     * @throws SmartThingException
     */
    public static ITenant getTenantByAuthenticationToken(IHBaseContext context, String authToken)
	    throws SmartThingException {
	SearchResults<ITenant> all = listTenants(context, new TenantSearchCriteria(1, 0));
	for (ITenant tenant : all.getResults()) {
	    if (tenant.getAuthenticationToken().equals(authToken)) {
		return tenant;
	    }
	}
	return null;
    }

    /**
     * List tenants that match the given criteria.
     * 
     * @param context
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public static SearchResults<ITenant> listTenants(IHBaseContext context, final ITenantSearchCriteria criteria)
	    throws SmartThingException {
	Comparator<Tenant> comparator = new Comparator<Tenant>() {

	    public int compare(Tenant a, Tenant b) {
		return a.getName().compareTo(b.getName());
	    }

	};
	IFilter<Tenant> filter = new IFilter<Tenant>() {

	    // Regular expression used for searching name and id.
	    Pattern regex = criteria.getTextSearch() != null ? Pattern.compile(Pattern.quote(criteria.getTextSearch()))
		    : null;

	    public boolean isExcluded(Tenant item) {
		if (regex != null) {
		    if ((!regex.matcher(item.getId()).matches()) && (!regex.matcher(item.getName()).matches())) {
			return true;
		    }
		}
		if (criteria.getUserId() != null) {
		    if (item.getAuthorizedUserIds().contains(criteria.getUserId())) {
			return false;
		    } else {
			return true;
		    }
		}
		return false;
	    }
	};
	SearchResults<ITenant> list = HBaseUtils.getFilteredList(context, ISmartThingHBase.USERS_TABLE_NAME, KEY_BUILDER,
		true, ITenant.class, Tenant.class, filter, criteria, comparator);
	SmartThingPersistence.tenantListLogic(list.getResults(), criteria);
	return list;
    }

    /**
     * Delete an existing tenant.
     * 
     * @param context
     * @param id
     * @param force
     * @return
     * @throws SmartThingException
     */
    public static Tenant deleteTenant(IHBaseContext context, String id, boolean force) throws SmartThingException {
	return HBaseUtils.delete(context, context.getPayloadMarshaler(), ISmartThingHBase.USERS_TABLE_NAME, id, force,
		KEY_BUILDER, Tenant.class);
    }

    /**
     * Get a tenant by unique id. Throw exception if not found.
     * 
     * @param context
     * @param id
     * @return
     * @throws SmartThingException
     */
    public static Tenant assertTenant(IHBaseContext context, String id) throws SmartThingException {
	Tenant existing = getTenantById(context, id);
	if (existing == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidTenantId, ErrorLevel.ERROR);
	}
	return existing;
    }
}