/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.device;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.Tracer;
import com.smartthing.core.SmartThingPersistence;
import com.smartthing.hbase.IHBaseContext;
import com.smartthing.hbase.ISmartThingHBase;
import com.smartthing.hbase.common.HBaseUtils;
import com.smartthing.hbase.encoder.PayloadMarshalerResolver;
import com.smartthing.rest.model.device.DeviceAssignment;
import com.smartthing.rest.model.device.streaming.DeviceStream;
import com.smartthing.rest.model.search.Pager;
import com.smartthing.rest.model.search.SearchResults;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest;
import com.smartthing.spi.device.streaming.IDeviceStream;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.search.ISearchCriteria;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.server.debug.TracerCategory;

/**
 * HBase specifics for dealing with SiteWhere device streams.
 * 
 * @author Derek
 */
public class HBaseDeviceStream {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /**
     * Create a new {@link IDeviceStream}.
     * 
     * @param context
     * @param assignmentToken
     * @param request
     * @return
     * @throws SmartThingException
     */
    public static IDeviceStream createDeviceStream(IHBaseContext context, String assignmentToken,
	    IDeviceStreamCreateRequest request) throws SmartThingException {
	Tracer.push(TracerCategory.DeviceManagementApiCall, "createDeviceStream (HBase)", LOGGER);
	try {
	    // Verify that the assignment token is valid.
	    DeviceAssignment assignment = HBaseDeviceAssignment.getDeviceAssignment(context, assignmentToken);
	    byte[] assnKey = context.getDeviceIdManager().getAssignmentKeys().getValue(assignmentToken);
	    if (assignment == null) {
		throw new SmartThingSystemException(ErrorCode.InvalidDeviceAssignmentToken, ErrorLevel.ERROR);
	    }

	    // Verify that the device stream does not exist.
	    DeviceStream stream = HBaseDeviceStream.getDeviceStream(context, assignmentToken, request.getStreamId());
	    if (stream != null) {
		throw new SmartThingSystemException(ErrorCode.DuplicateStreamId, ErrorLevel.ERROR);
	    }

	    byte[] streamKey = getDeviceStreamKey(assnKey, request.getStreamId());

	    DeviceStream newStream = SmartThingPersistence.deviceStreamCreateLogic(assignment, request);
	    byte[] payload = context.getPayloadMarshaler().encode(newStream);

	    Table sites = null;
	    try {
		sites = getSitesTableInterface(context);
		Put put = new Put(streamKey);
		HBaseUtils.addPayloadFields(context.getPayloadMarshaler().getEncoding(), put, payload);
		sites.put(put);
	    } catch (IOException e) {
		throw new SmartThingException("Unable to create device stream.", e);
	    } finally {
		HBaseUtils.closeCleanly(sites);
	    }

	    return newStream;
	} finally {
	    Tracer.pop(LOGGER);
	}
    }

    /**
     * Get a {@link DeviceStream} based on assignment and stream id.
     * 
     * @param context
     * @param assignmentToken
     * @param streamId
     * @return
     * @throws SmartThingException
     */
    public static DeviceStream getDeviceStream(IHBaseContext context, String assignmentToken, String streamId)
	    throws SmartThingException {
	byte[] assnKey = context.getDeviceIdManager().getAssignmentKeys().getValue(assignmentToken);
	if (assnKey == null) {
	    return null;
	}
	byte[] streamKey = getDeviceStreamKey(assnKey, streamId);

	Table sites = null;
	try {
	    sites = getSitesTableInterface(context);
	    Get get = new Get(streamKey);
	    HBaseUtils.addPayloadFields(get);
	    Result result = sites.get(get);

	    byte[] type = result.getValue(ISmartThingHBase.FAMILY_ID, ISmartThingHBase.PAYLOAD_TYPE);
	    byte[] payload = result.getValue(ISmartThingHBase.FAMILY_ID, ISmartThingHBase.PAYLOAD);
	    if ((type == null) || (payload == null)) {
		return null;
	    }

	    return PayloadMarshalerResolver.getInstance().getMarshaler(type).decodeDeviceStream(payload);
	} catch (IOException e) {
	    throw new SmartThingException("Unable to load device stream by token.", e);
	} finally {
	    HBaseUtils.closeCleanly(sites);
	}
    }

    /**
     * List all device streams for an assignment.
     * 
     * @param context
     * @param assignmentToken
     * @param criteria
     * @return
     * @throws SmartThingException
     */
    public static ISearchResults<IDeviceStream> listDeviceStreams(IHBaseContext context, String assignmentToken,
	    ISearchCriteria criteria) throws SmartThingException {
	byte[] assnKey = context.getDeviceIdManager().getAssignmentKeys().getValue(assignmentToken);
	if (assnKey == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidDeviceAssignmentToken, ErrorLevel.ERROR);
	}

	Table sites = null;
	ResultScanner scanner = null;
	try {
	    sites = getSitesTableInterface(context);
	    Scan scan = new Scan();
	    scan.setStartRow(HBaseDeviceAssignment.getStreamRowkey(assnKey));
	    scan.setStopRow(HBaseDeviceAssignment.getEndMarkerKey(assnKey));
	    scanner = sites.getScanner(scan);

	    Pager<IDeviceStream> pager = new Pager<IDeviceStream>(criteria);
	    for (Result result : scanner) {
		byte[] payloadType = result.getValue(ISmartThingHBase.FAMILY_ID, ISmartThingHBase.PAYLOAD_TYPE);
		byte[] payload = result.getValue(ISmartThingHBase.FAMILY_ID, ISmartThingHBase.PAYLOAD);

		if ((payloadType != null) && (payload != null)) {
		    pager.process((IDeviceStream) PayloadMarshalerResolver.getInstance().getMarshaler(payloadType)
			    .decodeDeviceStream(payload));
		}
	    }
	    return new SearchResults<IDeviceStream>(pager.getResults(), pager.getTotal());
	} catch (IOException e) {
	    throw new SmartThingException("Error scanning device stream rows.", e);
	} finally {
	    if (scanner != null) {
		scanner.close();
	    }
	    HBaseUtils.closeCleanly(sites);
	}
    }

    /**
     * Get a
     * 
     * @param assnKey
     * @param streamId
     * @return
     */
    public static byte[] getDeviceStreamKey(byte[] assnKey, String streamId) {
	byte[] streamBase = HBaseDeviceAssignment.getStreamRowkey(assnKey);
	byte[] streamIdBytes = streamId.getBytes();
	ByteBuffer buffer = ByteBuffer.allocate(streamBase.length + streamIdBytes.length);
	buffer.put(streamBase);
	buffer.put(streamIdBytes);
	return buffer.array();
    }

    /**
     * Get sites table based on context.
     * 
     * @param context
     * @return
     * @throws SmartThingException
     */
    protected static Table getSitesTableInterface(IHBaseContext context) throws SmartThingException {
	return context.getClient().getTableInterface(context.getTenant(), ISmartThingHBase.SITES_TABLE_NAME);
    }
}