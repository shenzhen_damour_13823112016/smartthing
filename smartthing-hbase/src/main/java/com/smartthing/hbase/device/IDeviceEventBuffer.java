/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.device;

import org.apache.hadoop.hbase.client.Put;

import com.smartthing.spi.SmartThingException;

/**
 * Interface for buffer used for saving device events.
 * 
 * @author Derek
 */
public interface IDeviceEventBuffer {

    /**
     * Start buffer lifecycle.
     * 
     * @throws SmartThingException
     */
    public void start() throws SmartThingException;

    /**
     * Stop buffer lifecycle.
     * 
     * @throws SmartThingException
     */
    public void stop() throws SmartThingException;

    /**
     * Add a {@link Put} to be buffered.
     * 
     * @param put
     * @throws SmartThingException
     */
    public void add(Put put) throws SmartThingException;
}