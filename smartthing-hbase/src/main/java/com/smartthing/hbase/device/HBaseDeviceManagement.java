/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.device;

import java.util.List;
import java.util.UUID;

import org.apache.hadoop.hbase.regionserver.BloomType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.core.SmartThingPersistence;
import com.smartthing.hbase.HBaseContext;
import com.smartthing.hbase.ISmartThingHBase;
import com.smartthing.hbase.ISmartThingHBaseClient;
import com.smartthing.hbase.common.SmartThingTables;
import com.smartthing.hbase.encoder.IPayloadMarshaler;
import com.smartthing.hbase.encoder.ProtobufPayloadMarshaler;
import com.smartthing.rest.model.search.SearchResults;
import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.common.IMetadataProvider;
import com.smartthing.spi.device.DeviceAssignmentStatus;
import com.smartthing.spi.device.ICachingDeviceManagement;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceAssignmentState;
import com.smartthing.spi.device.IDeviceElementMapping;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.IDeviceManagementCacheProvider;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.IDeviceStatus;
import com.smartthing.spi.device.ISite;
import com.smartthing.spi.device.IZone;
import com.smartthing.spi.device.batch.IBatchElement;
import com.smartthing.spi.device.batch.IBatchOperation;
import com.smartthing.spi.device.command.IDeviceCommand;
import com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest;
import com.smartthing.spi.device.group.IDeviceGroup;
import com.smartthing.spi.device.group.IDeviceGroupElement;
import com.smartthing.spi.device.request.IBatchCommandInvocationRequest;
import com.smartthing.spi.device.request.IBatchElementUpdateRequest;
import com.smartthing.spi.device.request.IBatchOperationCreateRequest;
import com.smartthing.spi.device.request.IBatchOperationUpdateRequest;
import com.smartthing.spi.device.request.IDeviceAssignmentCreateRequest;
import com.smartthing.spi.device.request.IDeviceCommandCreateRequest;
import com.smartthing.spi.device.request.IDeviceCreateRequest;
import com.smartthing.spi.device.request.IDeviceGroupCreateRequest;
import com.smartthing.spi.device.request.IDeviceGroupElementCreateRequest;
import com.smartthing.spi.device.request.IDeviceSpecificationCreateRequest;
import com.smartthing.spi.device.request.IDeviceStatusCreateRequest;
import com.smartthing.spi.device.request.ISiteCreateRequest;
import com.smartthing.spi.device.request.IZoneCreateRequest;
import com.smartthing.spi.device.streaming.IDeviceStream;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.search.IDateRangeSearchCriteria;
import com.smartthing.spi.search.ISearchCriteria;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.search.device.IAssignmentSearchCriteria;
import com.smartthing.spi.search.device.IAssignmentsForAssetSearchCriteria;
import com.smartthing.spi.search.device.IBatchElementSearchCriteria;
import com.smartthing.spi.search.device.IDeviceSearchCriteria;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * HBase implementation of SiteWhere device management.
 * 
 * @author Derek
 */
public class HBaseDeviceManagement extends TenantLifecycleComponent
	implements IDeviceManagement, ICachingDeviceManagement {

    /** Static logger instance */
    private static final Logger LOGGER = LogManager.getLogger();

    /** Used to communicate with HBase */
    private ISmartThingHBaseClient client;

    /** Injected cache provider */
    private IDeviceManagementCacheProvider cacheProvider;

    /** Injected payload encoder */
    private IPayloadMarshaler payloadMarshaler = new ProtobufPayloadMarshaler();

    /** Supplies context to implementation methods */
    private HBaseContext context;

    /** Device id manager */
    private DeviceIdManager deviceIdManager;

    public HBaseDeviceManagement() {
	super(LifecycleComponentType.DataStore);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
     * .server.lifecycle.ILifecycleProgressMonitor)
     */
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Create context from configured options.
	this.context = new HBaseContext();
	context.setTenant(getTenant());
	context.setClient(getClient());
	context.setCacheProvider(getCacheProvider());
	context.setPayloadMarshaler(getPayloadMarshaler());

	ensureTablesExist();

	// Create device id manager instance.
	deviceIdManager = new DeviceIdManager();
	deviceIdManager.load(context);
	context.setDeviceIdManager(deviceIdManager);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /**
     * Make sure that all SiteWhere tables exist, creating them if necessary.
     * 
     * @throws SmartThingException
     */
    protected void ensureTablesExist() throws SmartThingException {
	SmartThingTables.assureTenantTable(context, ISmartThingHBase.UID_TABLE_NAME, BloomType.ROW);
	SmartThingTables.assureTenantTable(context, ISmartThingHBase.SITES_TABLE_NAME, BloomType.ROW);
	SmartThingTables.assureTenantTable(context, ISmartThingHBase.DEVICES_TABLE_NAME, BloomType.ROW);
	SmartThingTables.assureTenantTable(context, ISmartThingHBase.STREAMS_TABLE_NAME, BloomType.ROW);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.ICachingDeviceManagement#setCacheProvider(com.
     * sitewhere .spi.device.IDeviceManagementCacheProvider)
     */
    @Override
    public void setCacheProvider(IDeviceManagementCacheProvider cacheProvider) {
	this.cacheProvider = cacheProvider;
    }

    public IDeviceManagementCacheProvider getCacheProvider() {
	return cacheProvider;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#createDeviceSpecification(com.
     * sitewhere .spi.device.request.IDeviceSpecificationCreateRequest)
     */
    @Override
    public IDeviceSpecification createDeviceSpecification(IDeviceSpecificationCreateRequest request)
	    throws SmartThingException {
	return HBaseDeviceSpecification.createDeviceSpecification(context, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getDeviceSpecificationByToken(
     * java.lang .String)
     */
    @Override
    public IDeviceSpecification getDeviceSpecificationByToken(String token) throws SmartThingException {
	return HBaseDeviceSpecification.getDeviceSpecificationByToken(context, token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#updateDeviceSpecification(java
     * .lang. String,
     * com.smartthing.spi.device.request.IDeviceSpecificationCreateRequest)
     */
    @Override
    public IDeviceSpecification updateDeviceSpecification(String token, IDeviceSpecificationCreateRequest request)
	    throws SmartThingException {
	return HBaseDeviceSpecification.updateDeviceSpecification(context, token, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagement#listDeviceSpecifications(
     * boolean, com.smartthing.spi.search.ISearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceSpecification> listDeviceSpecifications(boolean includeDeleted,
	    ISearchCriteria criteria) throws SmartThingException {
	return HBaseDeviceSpecification.listDeviceSpecifications(context, includeDeleted, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#deleteDeviceSpecification(java
     * .lang. String, boolean)
     */
    @Override
    public IDeviceSpecification deleteDeviceSpecification(String token, boolean force) throws SmartThingException {
	return HBaseDeviceSpecification.deleteDeviceSpecification(context, token, force);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagement#createDeviceCommand(com.
     * sitewhere.spi .device.IDeviceSpecification,
     * com.smartthing.spi.device.request.IDeviceCommandCreateRequest)
     */
    @Override
    public IDeviceCommand createDeviceCommand(IDeviceSpecification spec, IDeviceCommandCreateRequest request)
	    throws SmartThingException {
	return HBaseDeviceCommand.createDeviceCommand(context, spec, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getDeviceCommandByToken(java.
     * lang.String )
     */
    @Override
    public IDeviceCommand getDeviceCommandByToken(String token) throws SmartThingException {
	return HBaseDeviceCommand.getDeviceCommandByToken(context, token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#updateDeviceCommand(java.lang.
     * String, com.smartthing.spi.device.request.IDeviceCommandCreateRequest)
     */
    @Override
    public IDeviceCommand updateDeviceCommand(String token, IDeviceCommandCreateRequest request)
	    throws SmartThingException {
	return HBaseDeviceCommand.updateDeviceCommand(context, token, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#listDeviceCommands(java.lang.
     * String, boolean)
     */
    @Override
    public List<IDeviceCommand> listDeviceCommands(String specToken, boolean includeDeleted) throws SmartThingException {
	return HBaseDeviceCommand.listDeviceCommands(context, specToken, includeDeleted);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#deleteDeviceCommand(java.lang.
     * String, boolean)
     */
    @Override
    public IDeviceCommand deleteDeviceCommand(String token, boolean force) throws SmartThingException {
	return HBaseDeviceCommand.deleteDeviceCommand(context, token, force);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#createDeviceStatus(java.lang.
     * String, com.smartthing.spi.device.request.IDeviceStatusCreateRequest)
     */
    @Override
    public IDeviceStatus createDeviceStatus(String specToken, IDeviceStatusCreateRequest request)
	    throws SmartThingException {
	throw new SmartThingException("Not implemented yet for HBase device managment.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getDeviceStatusByCode(java.
     * lang.String)
     */
    @Override
    public IDeviceStatus getDeviceStatusByCode(String specToken, String code) throws SmartThingException {
	throw new SmartThingException("Not implemented yet for HBase device managment.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#updateDeviceStatus(java.lang.
     * String, com.smartthing.spi.device.request.IDeviceStatusCreateRequest)
     */
    @Override
    public IDeviceStatus updateDeviceStatus(String specToken, String code, IDeviceStatusCreateRequest request)
	    throws SmartThingException {
	throw new SmartThingException("Not implemented yet for HBase device managment.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#listDeviceStatuses(java.lang.
     * String)
     */
    @Override
    public List<IDeviceStatus> listDeviceStatuses(String specToken) throws SmartThingException {
	throw new SmartThingException("Not implemented yet for HBase device managment.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#deleteDeviceStatus(java.lang.
     * String, java.lang.String)
     */
    @Override
    public IDeviceStatus deleteDeviceStatus(String specToken, String code) throws SmartThingException {
	throw new SmartThingException("Not implemented yet for HBase device managment.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#createDevice(com.smartthing.spi
     * .device .request.IDeviceCreateRequest)
     */
    public IDevice createDevice(IDeviceCreateRequest device) throws SmartThingException {
	return HBaseDevice.createDevice(context, device);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getDeviceByHardwareId(java.
     * lang.String)
     */
    public IDevice getDeviceByHardwareId(String hardwareId) throws SmartThingException {
	return HBaseDevice.getDeviceByHardwareId(context, hardwareId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#updateDevice(java.lang.String,
     * com.smartthing.spi.device.request.IDeviceCreateRequest)
     */
    public IDevice updateDevice(String hardwareId, IDeviceCreateRequest request) throws SmartThingException {
	return HBaseDevice.updateDevice(context, hardwareId, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getCurrentDeviceAssignment(com
     * .sitewhere .spi.device.IDevice)
     */
    public IDeviceAssignment getCurrentDeviceAssignment(IDevice device) throws SmartThingException {
	String token = HBaseDevice.getCurrentAssignmentId(context, device.getHardwareId());
	if (token == null) {
	    return null;
	}
	return HBaseDeviceAssignment.getDeviceAssignment(context, token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagement#listDevices(boolean,
     * com.smartthing.spi.search.device.IDeviceSearchCriteria)
     */
    public SearchResults<IDevice> listDevices(boolean includeDeleted, IDeviceSearchCriteria criteria)
	    throws SmartThingException {
	return HBaseDevice.listDevices(context, includeDeleted, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#createDeviceElementMapping(
     * java.lang .String, com.smartthing.spi.device.IDeviceElementMapping)
     */
    @Override
    public IDevice createDeviceElementMapping(String hardwareId, IDeviceElementMapping mapping)
	    throws SmartThingException {
	return SmartThingPersistence.deviceElementMappingCreateLogic(this, hardwareId, mapping);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#deleteDeviceElementMapping(
     * java.lang .String, java.lang.String)
     */
    @Override
    public IDevice deleteDeviceElementMapping(String hardwareId, String path) throws SmartThingException {
	return SmartThingPersistence.deviceElementMappingDeleteLogic(this, hardwareId, path);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#deleteDevice(java.lang.String,
     * boolean)
     */
    public IDevice deleteDevice(String hardwareId, boolean force) throws SmartThingException {
	return HBaseDevice.deleteDevice(context, hardwareId, force);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#createDeviceAssignment(com.
     * sitewhere .spi.device.request.IDeviceAssignmentCreateRequest)
     */
    public IDeviceAssignment createDeviceAssignment(IDeviceAssignmentCreateRequest request) throws SmartThingException {
	return HBaseDeviceAssignment.createDeviceAssignment(context, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getDeviceAssignmentByToken(
     * java.lang .String)
     */
    public IDeviceAssignment getDeviceAssignmentByToken(String token) throws SmartThingException {
	return HBaseDeviceAssignment.getDeviceAssignment(context, token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#deleteDeviceAssignment(java.
     * lang.String, boolean)
     */
    public IDeviceAssignment deleteDeviceAssignment(String token, boolean force) throws SmartThingException {
	return HBaseDeviceAssignment.deleteDeviceAssignment(context, token, force);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getDeviceForAssignment(com.
     * sitewhere .spi.device.IDeviceAssignment)
     */
    public IDevice getDeviceForAssignment(IDeviceAssignment assignment) throws SmartThingException {
	return HBaseDevice.getDeviceByHardwareId(context, assignment.getDeviceHardwareId());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagement#getSiteForAssignment(com.
     * sitewhere.spi .device.IDeviceAssignment)
     */
    public ISite getSiteForAssignment(IDeviceAssignment assignment) throws SmartThingException {
	return HBaseSite.getSiteByToken(context, assignment.getSiteToken());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#updateDeviceAssignmentMetadata
     * (java. lang.String, com.smartthing.spi.common.IMetadataProvider)
     */
    public IDeviceAssignment updateDeviceAssignmentMetadata(String token, IMetadataProvider metadata)
	    throws SmartThingException {
	return HBaseDeviceAssignment.updateDeviceAssignmentMetadata(context, token, metadata);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#updateDeviceAssignmentStatus(
     * java.lang .String, com.smartthing.spi.device.DeviceAssignmentStatus)
     */
    public IDeviceAssignment updateDeviceAssignmentStatus(String token, DeviceAssignmentStatus status)
	    throws SmartThingException {
	return HBaseDeviceAssignment.updateDeviceAssignmentStatus(context, token, status);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#updateDeviceAssignmentState(
     * java.lang .String, com.smartthing.spi.device.IDeviceAssignmentState)
     */
    public IDeviceAssignment updateDeviceAssignmentState(String token, IDeviceAssignmentState state)
	    throws SmartThingException {
	return HBaseDeviceAssignment.updateDeviceAssignmentState(context, token, state);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#endDeviceAssignment(java.lang.
     * String)
     */
    public IDeviceAssignment endDeviceAssignment(String token) throws SmartThingException {
	return HBaseDeviceAssignment.endDeviceAssignment(context, token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getDeviceAssignmentHistory(
     * java.lang .String, com.smartthing.spi.common.ISearchCriteria)
     */
    public SearchResults<IDeviceAssignment> getDeviceAssignmentHistory(String hardwareId, ISearchCriteria criteria)
	    throws SmartThingException {
	return HBaseDevice.getDeviceAssignmentHistory(context, hardwareId, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getDeviceAssignmentsForSite(
     * java.lang.String,
     * com.smartthing.spi.search.device.IAssignmentSearchCriteria)
     */
    public SearchResults<IDeviceAssignment> getDeviceAssignmentsForSite(String siteToken,
	    IAssignmentSearchCriteria criteria) throws SmartThingException {
	return HBaseSite.listDeviceAssignmentsForSite(context, siteToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagement#
     * getDeviceAssignmentsWithLastInteraction( java.lang.String,
     * com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceAssignment> getDeviceAssignmentsWithLastInteraction(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException {
	return HBaseSite.listDeviceAssignmentsWithLastInteraction(context, siteToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getMissingDeviceAssignments(
     * java.lang. String, com.smartthing.spi.search.ISearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceAssignment> getMissingDeviceAssignments(String siteToken, ISearchCriteria criteria)
	    throws SmartThingException {
	return HBaseSite.listMissingDeviceAssignments(context, siteToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getDeviceAssignmentsForAsset(
     * java.lang.String, java.lang.String,
     * com.smartthing.spi.search.device.IAssignmentsForAssetSearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceAssignment> getDeviceAssignmentsForAsset(String assetModuleId, String assetId,
	    IAssignmentsForAssetSearchCriteria criteria) throws SmartThingException {
	return HBaseSite.listDeviceAssignmentsForAsset(context, assetModuleId, assetId, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#createDeviceStream(java.lang.
     * String,
     * com.smartthing.spi.device.event.request.IDeviceStreamCreateRequest)
     */
    @Override
    public IDeviceStream createDeviceStream(String assignmentToken, IDeviceStreamCreateRequest request)
	    throws SmartThingException {
	return HBaseDeviceStream.createDeviceStream(context, assignmentToken, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getDeviceStream(java.lang.
     * String, java.lang.String)
     */
    @Override
    public IDeviceStream getDeviceStream(String assignmentToken, String streamId) throws SmartThingException {
	return HBaseDeviceStream.getDeviceStream(context, assignmentToken, streamId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#listDeviceStreams(java.lang.
     * String, com.smartthing.spi.search.ISearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceStream> listDeviceStreams(String assignmentToken, ISearchCriteria criteria)
	    throws SmartThingException {
	return HBaseDeviceStream.listDeviceStreams(context, assignmentToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#createSite(com.smartthing.spi.
     * device. request.ISiteCreateRequest)
     */
    @Override
    public ISite createSite(ISiteCreateRequest request) throws SmartThingException {
	return HBaseSite.createSite(context, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#deleteSite(java.lang.String,
     * boolean)
     */
    @Override
    public ISite deleteSite(String siteToken, boolean force) throws SmartThingException {
	return HBaseSite.deleteSite(context, siteToken, force);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#updateSite(java.lang.String,
     * com.smartthing.spi.device.request.ISiteCreateRequest)
     */
    @Override
    public ISite updateSite(String siteToken, ISiteCreateRequest request) throws SmartThingException {
	return HBaseSite.updateSite(context, siteToken, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagement#getSiteByToken(java.lang.
     * String)
     */
    @Override
    public ISite getSiteByToken(String token) throws SmartThingException {
	return HBaseSite.getSiteByToken(context, token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#listSites(com.smartthing.spi.
     * common. ISearchCriteria)
     */
    @Override
    public SearchResults<ISite> listSites(ISearchCriteria criteria) throws SmartThingException {
	return HBaseSite.listSites(context, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#createZone(com.smartthing.spi.
     * device. ISite, com.smartthing.spi.device.request.IZoneCreateRequest)
     */
    @Override
    public IZone createZone(ISite site, IZoneCreateRequest request) throws SmartThingException {
	return HBaseZone.createZone(context, site, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#updateZone(java.lang.String,
     * com.smartthing.spi.device.request.IZoneCreateRequest)
     */
    @Override
    public IZone updateZone(String token, IZoneCreateRequest request) throws SmartThingException {
	return HBaseZone.updateZone(context, token, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagement#getZone(java.lang.String)
     */
    @Override
    public IZone getZone(String zoneToken) throws SmartThingException {
	return HBaseZone.getZone(context, zoneToken);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#listZones(java.lang.String,
     * com.smartthing.spi.common.ISearchCriteria)
     */
    @Override
    public SearchResults<IZone> listZones(String siteToken, ISearchCriteria criteria) throws SmartThingException {
	return HBaseSite.listZonesForSite(context, siteToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#deleteZone(java.lang.String,
     * boolean)
     */
    @Override
    public IZone deleteZone(String zoneToken, boolean force) throws SmartThingException {
	return HBaseZone.deleteZone(context, zoneToken, force);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagement#createDeviceGroup(com.
     * sitewhere.spi. device.request.IDeviceGroupCreateRequest)
     */
    @Override
    public IDeviceGroup createDeviceGroup(IDeviceGroupCreateRequest request) throws SmartThingException {
	return HBaseDeviceGroup.createDeviceGroup(context, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#updateDeviceGroup(java.lang.
     * String, com.smartthing.spi.device.request.IDeviceGroupCreateRequest)
     */
    @Override
    public IDeviceGroup updateDeviceGroup(String token, IDeviceGroupCreateRequest request) throws SmartThingException {
	return HBaseDeviceGroup.updateDeviceGroup(context, token, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagement#getDeviceGroup(java.lang.
     * String)
     */
    @Override
    public IDeviceGroup getDeviceGroup(String token) throws SmartThingException {
	return HBaseDeviceGroup.getDeviceGroupByToken(context, token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagement#listDeviceGroups(boolean,
     * com.smartthing.spi.search.ISearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceGroup> listDeviceGroups(boolean includeDeleted, ISearchCriteria criteria)
	    throws SmartThingException {
	return HBaseDeviceGroup.listDeviceGroups(context, includeDeleted, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#listDeviceGroupsWithRole(java.
     * lang. String , boolean, com.smartthing.spi.search.ISearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceGroup> listDeviceGroupsWithRole(String role, boolean includeDeleted,
	    ISearchCriteria criteria) throws SmartThingException {
	return HBaseDeviceGroup.listDeviceGroupsWithRole(context, role, includeDeleted, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#deleteDeviceGroup(java.lang.
     * String, boolean)
     */
    @Override
    public IDeviceGroup deleteDeviceGroup(String token, boolean force) throws SmartThingException {
	return HBaseDeviceGroup.deleteDeviceGroup(context, token, force);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#addDeviceGroupElements(java.
     * lang.String, java.util.List, boolean)
     */
    @Override
    public List<IDeviceGroupElement> addDeviceGroupElements(String networkToken,
	    List<IDeviceGroupElementCreateRequest> elements, boolean ignoreDuplicates) throws SmartThingException {
	return HBaseDeviceGroupElement.createDeviceGroupElements(context, networkToken, elements, ignoreDuplicates);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#removeDeviceGroupElements(java
     * .lang. String, java.util.List)
     */
    @Override
    public List<IDeviceGroupElement> removeDeviceGroupElements(String networkToken,
	    List<IDeviceGroupElementCreateRequest> elements) throws SmartThingException {
	return HBaseDeviceGroupElement.removeDeviceGroupElements(context, networkToken, elements);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#listDeviceGroupElements(java.
     * lang.String , com.smartthing.spi.search.ISearchCriteria)
     */
    @Override
    public SearchResults<IDeviceGroupElement> listDeviceGroupElements(String networkToken, ISearchCriteria criteria)
	    throws SmartThingException {
	return HBaseDeviceGroupElement.listDeviceGroupElements(context, networkToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.IDeviceManagement#createBatchOperation(com.
     * sitewhere.spi .device.request.IBatchOperationCreateRequest)
     */
    @Override
    public IBatchOperation createBatchOperation(IBatchOperationCreateRequest request) throws SmartThingException {
	return HBaseBatchOperation.createBatchOperation(context, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#updateBatchOperation(java.lang
     * .String, com.smartthing.spi.device.request.IBatchOperationUpdateRequest)
     */
    @Override
    public IBatchOperation updateBatchOperation(String token, IBatchOperationUpdateRequest request)
	    throws SmartThingException {
	return HBaseBatchOperation.updateBatchOperation(context, token, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#getBatchOperation(java.lang.
     * String)
     */
    @Override
    public IBatchOperation getBatchOperation(String token) throws SmartThingException {
	return HBaseBatchOperation.getBatchOperationByToken(context, token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#listBatchOperations(boolean,
     * com.smartthing.spi.search.ISearchCriteria)
     */
    @Override
    public ISearchResults<IBatchOperation> listBatchOperations(boolean includeDeleted, ISearchCriteria criteria)
	    throws SmartThingException {
	return HBaseBatchOperation.listBatchOperations(context, includeDeleted, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#deleteBatchOperation(java.lang
     * .String, boolean)
     */
    @Override
    public IBatchOperation deleteBatchOperation(String token, boolean force) throws SmartThingException {
	return HBaseBatchOperation.deleteBatchOperation(context, token, force);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#listBatchElements(java.lang.
     * String, com.smartthing.spi.search.device.IBatchElementSearchCriteria)
     */
    @Override
    public SearchResults<IBatchElement> listBatchElements(String batchToken, IBatchElementSearchCriteria criteria)
	    throws SmartThingException {
	return HBaseBatchElement.listBatchElements(context, batchToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#updateBatchElement(java.lang.
     * String, long,
     * com.smartthing.spi.device.request.IBatchElementUpdateRequest)
     */
    @Override
    public IBatchElement updateBatchElement(String operationToken, long index, IBatchElementUpdateRequest request)
	    throws SmartThingException {
	return HBaseBatchElement.updateBatchElement(context, operationToken, index, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.IDeviceManagement#createBatchCommandInvocation(
     * com. sitewhere .spi.device.request.IBatchCommandInvocationRequest)
     */
    @Override
    public IBatchOperation createBatchCommandInvocation(IBatchCommandInvocationRequest request)
	    throws SmartThingException {
	String uuid = ((request.getToken() != null) ? request.getToken() : UUID.randomUUID().toString());
	IBatchOperationCreateRequest generic = SmartThingPersistence.batchCommandInvocationCreateLogic(request, uuid);
	return createBatchOperation(generic);
    }

    /**
     * Verify that the given assignment exists.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    protected IDeviceAssignment assertDeviceAssignment(String token) throws SmartThingException {
	IDeviceAssignment result = HBaseDeviceAssignment.getDeviceAssignment(context, token);
	if (result == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidDeviceAssignmentToken, ErrorLevel.ERROR);
	}
	return result;
    }

    public ISmartThingHBaseClient getClient() {
	return client;
    }

    public void setClient(ISmartThingHBaseClient client) {
	this.client = client;
    }

    public IPayloadMarshaler getPayloadMarshaler() {
	return payloadMarshaler;
    }

    public void setPayloadMarshaler(IPayloadMarshaler payloadMarshaler) {
	this.payloadMarshaler = payloadMarshaler;
    }
}