/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase.device;

import java.util.List;

import org.apache.hadoop.hbase.regionserver.BloomType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.core.SmartThingPersistence;
import com.smartthing.device.AssignmentStateManager;
import com.smartthing.hbase.HBaseContext;
import com.smartthing.hbase.ISmartThingHBase;
import com.smartthing.hbase.ISmartThingHBaseClient;
import com.smartthing.hbase.common.SmartThingTables;
import com.smartthing.hbase.encoder.IPayloadMarshaler;
import com.smartthing.hbase.encoder.ProtobufPayloadMarshaler;
import com.smartthing.rest.model.search.SearchResults;
import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.command.IDeviceCommand;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceEvent;
import com.smartthing.spi.device.event.IDeviceEventBatch;
import com.smartthing.spi.device.event.IDeviceEventBatchResponse;
import com.smartthing.spi.device.event.IDeviceEventManagement;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.device.event.IDeviceStreamData;
import com.smartthing.spi.device.event.request.IDeviceAlertCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceCommandInvocationCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceCommandResponseCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceEventCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceLocationCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceMeasurementsCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStateChangeCreateRequest;
import com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.search.IDateRangeSearchCriteria;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * HBase implementation of SiteWhere device event management.
 * 
 * @author Derek
 */
public class HBaseDeviceEventManagement extends TenantLifecycleComponent implements IDeviceEventManagement {

    /** Static logger instance */
    private static final Logger LOGGER = LogManager.getLogger();

    /** Device management implementation */
    private IDeviceManagement deviceManagement;

    /** Used to communicate with HBase */
    private ISmartThingHBaseClient client;

    /** Injected payload encoder */
    private IPayloadMarshaler payloadMarshaler = new ProtobufPayloadMarshaler();

    /** Supplies context to implementation methods */
    private HBaseContext context;

    /** Allows puts to be buffered for device events */
    private DeviceEventBuffer buffer;

    /** Assignment state manager */
    private AssignmentStateManager assignmentStateManager;

    /** Device id manager */
    private DeviceIdManager deviceIdManager;

    public HBaseDeviceEventManagement() {
	super(LifecycleComponentType.DataStore);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#start(com.
     * sitewhere.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Create context from configured options.
	this.context = new HBaseContext();
	context.setTenant(getTenant());
	context.setClient(getClient());
	context.setPayloadMarshaler(getPayloadMarshaler());

	ensureTablesExist();

	// Create device id manager instance.
	deviceIdManager = new DeviceIdManager();
	deviceIdManager.load(context);
	context.setDeviceIdManager(deviceIdManager);

	// Start buffer for saving device events.
	buffer = new DeviceEventBuffer(context);
	buffer.start();
	context.setDeviceEventBuffer(buffer);

	// Create assignment state manager and start it.
	assignmentStateManager = new AssignmentStateManager(getDeviceManagement());
	startNestedComponent(assignmentStateManager, monitor, true);
	context.setAssignmentStateManager(assignmentStateManager);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.server.lifecycle.ILifecycleComponent#stop(com.smartthing
     * .spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
	if (buffer != null) {
	    buffer.stop();
	}

	// Stop the assignment state manager.
	if (assignmentStateManager != null) {
	    assignmentStateManager.lifecycleStop(monitor);
	}
    }

    /**
     * Make sure that all SiteWhere tables exist, creating them if necessary.
     * 
     * @throws SmartThingException
     */
    protected void ensureTablesExist() throws SmartThingException {
	SmartThingTables.assureTenantTable(context, ISmartThingHBase.UID_TABLE_NAME, BloomType.ROW);
	SmartThingTables.assureTenantTable(context, ISmartThingHBase.EVENTS_TABLE_NAME, BloomType.ROW);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.IDeviceEventManagement#addDeviceEventBatch
     * (java. lang.String, com.smartthing.spi.device.event.IDeviceEventBatch)
     */
    @Override
    public IDeviceEventBatchResponse addDeviceEventBatch(String assignmentToken, IDeviceEventBatch batch)
	    throws SmartThingException {
	return SmartThingPersistence.deviceEventBatchLogic(assignmentToken, batch, this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.IDeviceEventManagement#getDeviceEventById(
     * java.lang .String)
     */
    @Override
    public IDeviceEvent getDeviceEventById(String id) throws SmartThingException {
	return HBaseDeviceEvent.getDeviceEvent(context, id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * getDeviceEventByAlternateId(java.lang.String)
     */
    @Override
    public IDeviceEvent getDeviceEventByAlternateId(String alternateId) throws SmartThingException {
	throw new SmartThingException("Not implemented yet for HBase device management.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#listDeviceEvents(
     * java.lang .String, com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceEvent> listDeviceEvents(String assignmentToken, IDateRangeSearchCriteria criteria)
	    throws SmartThingException {
	return HBaseDeviceEvent.listDeviceEvents(context, assignmentToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * addDeviceMeasurements(java .lang.String,
     * com.smartthing.spi.device.event.request.IDeviceMeasurementsCreateRequest)
     */
    @Override
    public IDeviceMeasurements addDeviceMeasurements(String assignmentToken,
	    IDeviceMeasurementsCreateRequest measurements) throws SmartThingException {
	IDeviceAssignment assignment = assertDeviceAssignment(assignmentToken);
	return HBaseDeviceEvent.createDeviceMeasurements(context, assignment, measurements);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * listDeviceMeasurements(java .lang.String,
     * com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public SearchResults<IDeviceMeasurements> listDeviceMeasurements(String token, IDateRangeSearchCriteria criteria)
	    throws SmartThingException {
	return HBaseDeviceEvent.listDeviceMeasurements(context, token, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * listDeviceMeasurementsForSite (java.lang.String,
     * com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public SearchResults<IDeviceMeasurements> listDeviceMeasurementsForSite(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException {
	return HBaseDeviceEvent.listDeviceMeasurementsForSite(context, siteToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#addDeviceLocation(
     * java.lang .String,
     * com.smartthing.spi.device.event.request.IDeviceLocationCreateRequest)
     */
    @Override
    public IDeviceLocation addDeviceLocation(String assignmentToken, IDeviceLocationCreateRequest request)
	    throws SmartThingException {
	IDeviceAssignment assignment = assertDeviceAssignment(assignmentToken);
	return HBaseDeviceEvent.createDeviceLocation(context, assignment, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.IDeviceEventManagement#listDeviceLocations
     * (java. lang.String, com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public SearchResults<IDeviceLocation> listDeviceLocations(String assignmentToken, IDateRangeSearchCriteria criteria)
	    throws SmartThingException {
	return HBaseDeviceEvent.listDeviceLocations(context, assignmentToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * listDeviceLocationsForSite (java.lang.String,
     * com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public SearchResults<IDeviceLocation> listDeviceLocationsForSite(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException {
	return HBaseDeviceEvent.listDeviceLocationsForSite(context, siteToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.IDeviceEventManagement#listDeviceLocations
     * (java. util.List, com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public SearchResults<IDeviceLocation> listDeviceLocations(List<String> assignmentTokens,
	    IDateRangeSearchCriteria criteria) throws SmartThingException {
	throw new SmartThingException("Not implemented yet for HBase device management.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.IDeviceEventManagement#addDeviceAlert(java
     * .lang. String,
     * com.smartthing.spi.device.event.request.IDeviceAlertCreateRequest)
     */
    @Override
    public IDeviceAlert addDeviceAlert(String assignmentToken, IDeviceAlertCreateRequest request)
	    throws SmartThingException {
	IDeviceAssignment assignment = assertDeviceAssignment(assignmentToken);
	return HBaseDeviceEvent.createDeviceAlert(context, assignment, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#listDeviceAlerts(
     * java.lang .String, com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public SearchResults<IDeviceAlert> listDeviceAlerts(String assignmentToken, IDateRangeSearchCriteria criteria)
	    throws SmartThingException {
	return HBaseDeviceEvent.listDeviceAlerts(context, assignmentToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * listDeviceAlertsForSite(java .lang.String,
     * com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public SearchResults<IDeviceAlert> listDeviceAlertsForSite(String siteToken, IDateRangeSearchCriteria criteria)
	    throws SmartThingException {
	return HBaseDeviceEvent.listDeviceAlertsForSite(context, siteToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.IDeviceEventManagement#addDeviceStreamData
     * (java. lang.String,
     * com.smartthing.spi.device.event.request.IDeviceStreamDataCreateRequest)
     */
    @Override
    public IDeviceStreamData addDeviceStreamData(String assignmentToken, IDeviceStreamDataCreateRequest request)
	    throws SmartThingException {
	IDeviceAssignment assignment = assertDeviceAssignment(assignmentToken);
	return HBaseDeviceStreamData.createDeviceStreamData(context, assignment, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.IDeviceEventManagement#getDeviceStreamData
     * (java. lang.String, java.lang.String, long)
     */
    @Override
    public IDeviceStreamData getDeviceStreamData(String assignmentToken, String streamId, long sequenceNumber)
	    throws SmartThingException {
	IDeviceAssignment assignment = assertDeviceAssignment(assignmentToken);
	return HBaseDeviceStreamData.getDeviceStreamData(context, assignment, streamId, sequenceNumber);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * listDeviceStreamData(java .lang.String, java.lang.String,
     * com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceStreamData> listDeviceStreamData(String assignmentToken, String streamId,
	    IDateRangeSearchCriteria criteria) throws SmartThingException {
	IDeviceAssignment assignment = assertDeviceAssignment(assignmentToken);
	return HBaseDeviceEvent.listDeviceStreamData(context, assignment, streamId, criteria);
    }

    /*
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * getDeviceCommandInvocationById(java.lang.String)
     */
    @Override
    public IDeviceCommandInvocation getDeviceCommandInvocationById(String id) throws SmartThingException {
	return (IDeviceCommandInvocation) getDeviceEventById(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * addDeviceCommandInvocation (java.lang.String,
     * com.smartthing.spi.device.command.IDeviceCommand,
     * com.smartthing.spi.device.event.request.
     * IDeviceCommandInvocationCreateRequest)
     */
    @Override
    public IDeviceCommandInvocation addDeviceCommandInvocation(String assignmentToken, IDeviceCommand command,
	    IDeviceCommandInvocationCreateRequest request) throws SmartThingException {
	IDeviceAssignment assignment = assertDeviceAssignment(assignmentToken);
	return HBaseDeviceEvent.createDeviceCommandInvocation(context, assignment, command, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * listDeviceCommandInvocations (java.lang.String,
     * com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceCommandInvocation> listDeviceCommandInvocations(String assignmentToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException {
	return HBaseDeviceEvent.listDeviceCommandInvocations(context, assignmentToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * listDeviceCommandInvocationsForSite(java.lang.String,
     * com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceCommandInvocation> listDeviceCommandInvocationsForSite(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException {
	return HBaseDeviceEvent.listDeviceCommandInvocationsForSite(context, siteToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * listDeviceCommandInvocationResponses(java.lang.String)
     */
    @Override
    public ISearchResults<IDeviceCommandResponse> listDeviceCommandInvocationResponses(String invocationId)
	    throws SmartThingException {
	return HBaseDeviceEvent.listDeviceCommandInvocationResponses(context, invocationId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * addDeviceCommandResponse( java.lang.String,
     * com.smartthing.spi.device.event.request. IDeviceCommandResponseCreateRequest)
     */
    @Override
    public IDeviceCommandResponse addDeviceCommandResponse(String assignmentToken,
	    IDeviceCommandResponseCreateRequest request) throws SmartThingException {
	IDeviceAssignment assignment = assertDeviceAssignment(assignmentToken);
	return HBaseDeviceEvent.createDeviceCommandResponse(context, assignment, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * listDeviceCommandResponses (java.lang.String,
     * com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceCommandResponse> listDeviceCommandResponses(String assignmentToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException {
	return HBaseDeviceEvent.listDeviceCommandResponses(context, assignmentToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * listDeviceCommandResponsesForSite (java.lang.String,
     * com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceCommandResponse> listDeviceCommandResponsesForSite(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException {
	return HBaseDeviceEvent.listDeviceCommandResponsesForSite(context, siteToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * addDeviceStateChange(java .lang.String,
     * com.smartthing.spi.device.event.request.IDeviceStateChangeCreateRequest)
     */
    @Override
    public IDeviceStateChange addDeviceStateChange(String assignmentToken, IDeviceStateChangeCreateRequest request)
	    throws SmartThingException {
	IDeviceAssignment assignment = assertDeviceAssignment(assignmentToken);
	return HBaseDeviceEvent.createDeviceStateChange(context, assignment, request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * listDeviceStateChanges(java .lang.String,
     * com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceStateChange> listDeviceStateChanges(String assignmentToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException {
	return HBaseDeviceEvent.listDeviceStateChanges(context, assignmentToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#
     * listDeviceStateChangesForSite (java.lang.String,
     * com.smartthing.spi.search.IDateRangeSearchCriteria)
     */
    @Override
    public ISearchResults<IDeviceStateChange> listDeviceStateChangesForSite(String siteToken,
	    IDateRangeSearchCriteria criteria) throws SmartThingException {
	return HBaseDeviceEvent.listDeviceStateChangesForSite(context, siteToken, criteria);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.IDeviceEventManagement#updateDeviceEvent(
     * java.lang.String,
     * com.smartthing.spi.device.event.request.IDeviceEventCreateRequest)
     */
    @Override
    public IDeviceEvent updateDeviceEvent(String eventId, IDeviceEventCreateRequest request) throws SmartThingException {
	throw new SmartThingException("Not implemented yet for HBase device management.");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.IDeviceEventManagement#getDeviceManagement ()
     */
    public IDeviceManagement getDeviceManagement() {
	return deviceManagement;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.device.event.IDeviceEventManagement#setDeviceManagement
     * (com. sitewhere .spi.device.IDeviceManagement)
     */
    public void setDeviceManagement(IDeviceManagement deviceManagement) {
	this.deviceManagement = deviceManagement;
    }

    /**
     * Verify that the given assignment exists.
     * 
     * @param token
     * @return
     * @throws SmartThingException
     */
    protected IDeviceAssignment assertDeviceAssignment(String token) throws SmartThingException {
	IDeviceAssignment result = getDeviceManagement().getDeviceAssignmentByToken(token);
	if (result == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidDeviceAssignmentToken, ErrorLevel.ERROR);
	}
	return result;
    }

    public ISmartThingHBaseClient getClient() {
	return client;
    }

    public void setClient(ISmartThingHBaseClient client) {
	this.client = client;
    }

    public IPayloadMarshaler getPayloadMarshaler() {
	return payloadMarshaler;
    }

    public void setPayloadMarshaler(IPayloadMarshaler payloadMarshaler) {
	this.payloadMarshaler = payloadMarshaler;
    }
}