/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.BufferedMutator;
import org.apache.hadoop.hbase.client.Table;

import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.tenant.ITenant;

/**
 * Interface expected for HBase client implementations.
 * 
 * @author Derek
 */
public interface ISmartThingHBaseClient {

    /**
     * Get client configuration.
     * 
     * @return
     */
    public Configuration getConfiguration();

    /**
     * Get HBase admin interface.
     * 
     * @return
     */
    public Admin getAdmin();

    /**
     * Get a table that has global scope.
     * 
     * @param tableName
     * @return
     * @throws SmartThingException
     */
    public Table getTableInterface(byte[] tableName) throws SmartThingException;

    /**
     * Get a table with tenant scope. Auto flush is disabled.
     * 
     * @param tenant
     * @param tableName
     * @return
     * @throws SmartThingException
     */
    public Table getTableInterface(ITenant tenant, byte[] tableName) throws SmartThingException;

    /**
     * Get a table with tenant scope.
     * 
     * @param tenant
     * @param tableName
     * @param autoFlush
     * @return
     * @throws SmartThingException
     */
    @Deprecated
    public Table getTableInterface(ITenant tenant, byte[] tableName, boolean autoFlush) throws SmartThingException;

    /**
     * Get buffered mutator with tenant scope.
     * 
     * @param tenant
     * @param tableName
     * @return
     * @throws SmartThingException
     */
    public BufferedMutator getBufferedMutator(ITenant tenant, byte[] tableName) throws SmartThingException;
}