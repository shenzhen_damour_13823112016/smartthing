/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.mongodb;

import java.util.HashMap;
import java.util.Map;

import com.smartthing.mongodb.asset.MongoAsset;
import com.smartthing.mongodb.asset.MongoAssetCategory;
import com.smartthing.mongodb.asset.MongoHardwareAsset;
import com.smartthing.mongodb.asset.MongoLocationAsset;
import com.smartthing.mongodb.asset.MongoPersonAsset;
import com.smartthing.mongodb.device.MongoBatchElement;
import com.smartthing.mongodb.device.MongoBatchOperation;
import com.smartthing.mongodb.device.MongoDevice;
import com.smartthing.mongodb.device.MongoDeviceAlert;
import com.smartthing.mongodb.device.MongoDeviceAssignment;
import com.smartthing.mongodb.device.MongoDeviceCommand;
import com.smartthing.mongodb.device.MongoDeviceCommandInvocation;
import com.smartthing.mongodb.device.MongoDeviceCommandResponse;
import com.smartthing.mongodb.device.MongoDeviceGroup;
import com.smartthing.mongodb.device.MongoDeviceGroupElement;
import com.smartthing.mongodb.device.MongoDeviceLocation;
import com.smartthing.mongodb.device.MongoDeviceMeasurements;
import com.smartthing.mongodb.device.MongoDeviceSpecification;
import com.smartthing.mongodb.device.MongoDeviceStateChange;
import com.smartthing.mongodb.device.MongoDeviceStatus;
import com.smartthing.mongodb.device.MongoDeviceStream;
import com.smartthing.mongodb.device.MongoDeviceStreamData;
import com.smartthing.mongodb.device.MongoSite;
import com.smartthing.mongodb.device.MongoZone;
import com.smartthing.mongodb.scheduling.MongoSchedule;
import com.smartthing.mongodb.scheduling.MongoScheduledJob;
import com.smartthing.mongodb.tenant.MongoTenant;
import com.smartthing.spi.asset.IAsset;
import com.smartthing.spi.asset.IAssetCategory;
import com.smartthing.spi.asset.IHardwareAsset;
import com.smartthing.spi.asset.ILocationAsset;
import com.smartthing.spi.asset.IPersonAsset;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceSpecification;
import com.smartthing.spi.device.IDeviceStatus;
import com.smartthing.spi.device.ISite;
import com.smartthing.spi.device.IZone;
import com.smartthing.spi.device.batch.IBatchElement;
import com.smartthing.spi.device.batch.IBatchOperation;
import com.smartthing.spi.device.command.IDeviceCommand;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.device.event.IDeviceStreamData;
import com.smartthing.spi.device.group.IDeviceGroup;
import com.smartthing.spi.device.group.IDeviceGroupElement;
import com.smartthing.spi.device.streaming.IDeviceStream;
import com.smartthing.spi.scheduling.ISchedule;
import com.smartthing.spi.scheduling.IScheduledJob;
import com.smartthing.spi.tenant.ITenant;

/**
 * Manages classes used to convert between Mongo and SPI objects.
 * 
 * @author Derek
 */
public class MongoConverters implements IMongoConverterLookup {

    /** Maps interface classes to their associated converters */
    private static Map<Class<?>, MongoConverter<?>> CONVERTERS = new HashMap<Class<?>, MongoConverter<?>>();

    /** Create a list of converters for various types */
    static {
	// Converters for device management.
	CONVERTERS.put(IDeviceSpecification.class, new MongoDeviceSpecification());
	CONVERTERS.put(IDeviceCommand.class, new MongoDeviceCommand());
	CONVERTERS.put(IDeviceStatus.class, new MongoDeviceStatus());
	CONVERTERS.put(IDevice.class, new MongoDevice());
	CONVERTERS.put(IDeviceAssignment.class, new MongoDeviceAssignment());
	CONVERTERS.put(IDeviceMeasurements.class, new MongoDeviceMeasurements());
	CONVERTERS.put(IDeviceAlert.class, new MongoDeviceAlert());
	CONVERTERS.put(IDeviceLocation.class, new MongoDeviceLocation());
	CONVERTERS.put(IDeviceStream.class, new MongoDeviceStream());
	CONVERTERS.put(IDeviceStreamData.class, new MongoDeviceStreamData());
	CONVERTERS.put(IDeviceCommandInvocation.class, new MongoDeviceCommandInvocation());
	CONVERTERS.put(IDeviceCommandResponse.class, new MongoDeviceCommandResponse());
	CONVERTERS.put(IDeviceStateChange.class, new MongoDeviceStateChange());
	CONVERTERS.put(ISite.class, new MongoSite());
	CONVERTERS.put(IZone.class, new MongoZone());
	CONVERTERS.put(IDeviceGroup.class, new MongoDeviceGroup());
	CONVERTERS.put(IDeviceGroupElement.class, new MongoDeviceGroupElement());
	CONVERTERS.put(IBatchOperation.class, new MongoBatchOperation());
	CONVERTERS.put(IBatchElement.class, new MongoBatchElement());

	// Converters for tenant management.
	CONVERTERS.put(ITenant.class, new MongoTenant());

	// Converters for asset management.
	CONVERTERS.put(IAssetCategory.class, new MongoAssetCategory());
	CONVERTERS.put(IHardwareAsset.class, new MongoHardwareAsset());
	CONVERTERS.put(IPersonAsset.class, new MongoPersonAsset());
	CONVERTERS.put(ILocationAsset.class, new MongoLocationAsset());
	CONVERTERS.put(IAsset.class, new MongoAsset());

	// Converters for schedule management.
	CONVERTERS.put(ISchedule.class, new MongoSchedule());
	CONVERTERS.put(IScheduledJob.class, new MongoScheduledJob());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.mongodb.IMongoConverterLookup#getConverterFor(java.lang.
     * Class)
     */
    @SuppressWarnings("unchecked")
    public <T> MongoConverter<T> getConverterFor(Class<T> api) {
	MongoConverter<T> result = (MongoConverter<T>) CONVERTERS.get(api);
	if (result == null) {
	    throw new RuntimeException("No Mongo converter registered for " + api.getName());
	}
	return result;
    }
}