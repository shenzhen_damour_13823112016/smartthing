/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.mongodb.tenant;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoTimeoutException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.IndexOptions;
import com.smartthing.core.SmartThingPersistence;
import com.smartthing.mongodb.IGlobalManagementMongoClient;
import com.smartthing.mongodb.MongoPersistence;
import com.smartthing.mongodb.common.MongoSiteWhereEntity;
import com.smartthing.rest.model.tenant.Tenant;
import com.smartthing.server.lifecycle.LifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.SmartThingSystemException;
import com.smartthing.spi.error.ErrorCode;
import com.smartthing.spi.error.ErrorLevel;
import com.smartthing.spi.search.ISearchResults;
import com.smartthing.spi.search.user.ITenantSearchCriteria;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;
import com.smartthing.spi.tenant.ITenant;
import com.smartthing.spi.tenant.ITenantManagement;
import com.smartthing.spi.tenant.request.ITenantCreateRequest;

/**
 * Tenant management implementation that uses MongoDB for persistence.
 * 
 * @author dadams
 */
public class MongoTenantManagement extends LifecycleComponent implements ITenantManagement {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Injected with global SiteWhere Mongo client */
    private IGlobalManagementMongoClient mongoClient;

    public MongoTenantManagement() {
	super(LifecycleComponentType.DataStore);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
     * .server.lifecycle.ILifecycleProgressMonitor)
     */
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	/** Ensure that expected indexes exist */
	ensureIndexes();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /**
     * Ensure that expected collection indexes exist.
     * 
     * @throws SmartThingException
     */
    protected void ensureIndexes() throws SmartThingException {
	getMongoClient().getTenantsCollection().createIndex(new Document(MongoTenant.PROP_ID, 1),
		new IndexOptions().unique(true));
	getMongoClient().getTenantsCollection().createIndex(new Document(MongoTenant.PROP_AUTH_TOKEN, 1),
		new IndexOptions().unique(true));
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.tenant.ITenantManagement#createTenant(com.smartthing.spi
     * .tenant. request.ITenantCreateRequest)
     */
    @Override
    public ITenant createTenant(ITenantCreateRequest request) throws SmartThingException {
	// Use common logic so all backend implementations work the same.
	Tenant tenant = SmartThingPersistence.tenantCreateLogic(request);

	MongoCollection<Document> tenants = getMongoClient().getTenantsCollection();
	Document created = MongoTenant.toDocument(tenant);
	MongoPersistence.insert(tenants, created, ErrorCode.DuplicateTenantId);

	return MongoTenant.fromDocument(created);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.tenant.ITenantManagement#updateTenant(java.lang.String,
     * com.smartthing.spi.tenant.request.ITenantCreateRequest)
     */
    @Override
    public ITenant updateTenant(String id, ITenantCreateRequest request) throws SmartThingException {
	Document dbExisting = assertTenant(id);
	Tenant existing = MongoTenant.fromDocument(dbExisting);

	// Use common update logic so that backend implemetations act the same
	// way.
	SmartThingPersistence.tenantUpdateLogic(request, existing);
	Document updated = MongoTenant.toDocument(existing);

	Document query = new Document(MongoTenant.PROP_ID, id);
	MongoCollection<Document> tenants = getMongoClient().getTenantsCollection();
	MongoPersistence.update(tenants, query, updated);

	return MongoTenant.fromDocument(updated);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.tenant.ITenantManagement#getTenantById(java.lang.
     * String)
     */
    @Override
    public ITenant getTenantById(String id) throws SmartThingException {
	Document dbExisting = getTenantDocumentById(id);
	if (dbExisting == null) {
	    return null;
	}
	return MongoTenant.fromDocument(dbExisting);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.tenant.ITenantManagement#getTenantByAuthenticationToken
     * (java.lang .String)
     */
    @Override
    public ITenant getTenantByAuthenticationToken(String token) throws SmartThingException {
	try {
	    MongoCollection<Document> tenants = getMongoClient().getTenantsCollection();
	    Document query = new Document(MongoTenant.PROP_AUTH_TOKEN, token);
	    Document match = tenants.find(query).first();
	    if (match == null) {
		return null;
	    }
	    return MongoTenant.fromDocument(match);
	} catch (MongoTimeoutException e) {
	    throw new SmartThingException("Connection to MongoDB lost.", e);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.tenant.ITenantManagement#listTenants(com.smartthing.spi.
     * search. user.ITenantSearchCriteria)
     */
    @Override
    public ISearchResults<ITenant> listTenants(ITenantSearchCriteria criteria) throws SmartThingException {
	MongoCollection<Document> tenants = getMongoClient().getTenantsCollection();
	Document dbCriteria = new Document();
	if (criteria.getTextSearch() != null) {
	    try {
		Pattern regex = Pattern.compile(Pattern.quote(criteria.getTextSearch()));
		DBObject idSearch = new BasicDBObject(MongoTenant.PROP_ID, regex);
		DBObject nameSearch = new BasicDBObject(MongoTenant.PROP_NAME, regex);
		BasicDBList or = new BasicDBList();
		or.add(idSearch);
		or.add(nameSearch);
		dbCriteria.append("$or", or);
	    } catch (PatternSyntaxException e) {
		LOGGER.warn("Invalid regex for searching tenant list. Ignoring.");
	    }
	}
	if (criteria.getUserId() != null) {
	    dbCriteria.append(MongoTenant.PROP_AUTH_USERS, criteria.getUserId());
	}
	Document sort = new Document(MongoTenant.PROP_NAME, 1);
	ISearchResults<ITenant> list = MongoPersistence.search(ITenant.class, tenants, dbCriteria, sort, criteria);
	SmartThingPersistence.tenantListLogic(list.getResults(), criteria);
	return list;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.spi.tenant.ITenantManagement#deleteTenant(java.lang.String,
     * boolean)
     */
    @Override
    public ITenant deleteTenant(String tenantId, boolean force) throws SmartThingException {
	Document existing = assertTenant(tenantId);
	if (force) {
	    MongoCollection<Document> tenants = getMongoClient().getTenantsCollection();
	    MongoPersistence.delete(tenants, existing);
	    getMongoClient().deleteTenantData(tenantId);
	    return MongoTenant.fromDocument(existing);
	} else {
	    MongoSiteWhereEntity.setDeleted(existing, true);
	    Document query = new Document(MongoTenant.PROP_ID, tenantId);
	    MongoCollection<Document> tenants = getMongoClient().getTenantsCollection();
	    MongoPersistence.update(tenants, query, existing);
	    return MongoTenant.fromDocument(existing);
	}
    }

    /**
     * Get the {@link Document} for a tenant given id. Throw an exception if not
     * found.
     * 
     * @param id
     * @return
     * @throws SmartThingException
     */
    protected Document assertTenant(String id) throws SmartThingException {
	Document match = getTenantDocumentById(id);
	if (match == null) {
	    throw new SmartThingSystemException(ErrorCode.InvalidTenantId, ErrorLevel.ERROR,
		    HttpServletResponse.SC_NOT_FOUND);
	}
	return match;
    }

    /**
     * Get the {@link Document} for a Tenant given unique id.
     * 
     * @param id
     * @return
     * @throws SmartThingException
     */
    protected Document getTenantDocumentById(String id) throws SmartThingException {
	try {
	    MongoCollection<Document> tenants = getMongoClient().getTenantsCollection();
	    Document query = new Document(MongoTenant.PROP_ID, id);
	    return tenants.find(query).first();
	} catch (MongoTimeoutException e) {
	    throw new SmartThingException("Connection to MongoDB lost.", e);
	}
    }

    public IGlobalManagementMongoClient getMongoClient() {
	return mongoClient;
    }

    public void setMongoClient(IGlobalManagementMongoClient mongoClient) {
	this.mongoClient = mongoClient;
    }
}
