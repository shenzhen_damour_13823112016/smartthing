/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.rabbitmq;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.smartthing.SmartThing;
import com.smartthing.common.MarshalUtils;
import com.smartthing.device.event.processor.FilteredOutboundEventProcessor;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.IDevice;
import com.smartthing.spi.device.IDeviceAssignment;
import com.smartthing.spi.device.IDeviceManagement;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceCommandInvocation;
import com.smartthing.spi.device.event.IDeviceCommandResponse;
import com.smartthing.spi.device.event.IDeviceEvent;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.IDeviceStateChange;
import com.smartthing.spi.device.event.processor.IMulticastingOutboundEventProcessor;
import com.smartthing.spi.device.event.processor.multicast.IDeviceEventMulticaster;
import com.smartthing.spi.device.event.processor.routing.IRouteBuilder;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;

/**
 * Extension of {@link FilteredOutboundEventProcessor} that sends messages to
 * RabbitMQ via AMQP.
 * 
 * @author Derek
 */
public class RabbitMqOutboundEventProcessor extends FilteredOutboundEventProcessor
	implements IMulticastingOutboundEventProcessor<String> {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Default connection URI */
    private static final String DEFAULT_CONNECTION_URI = "amqp://localhost";

    /** Default exchange name suffix */
    private static final String DEFAULT_EXCHANGE_SUFFIX = "-outbound";

    /** Default topic name */
    private static final String DEFAULT_TOPIC = "sitewhere.output";

    /** Connection URI */
    private String connectionUri = DEFAULT_CONNECTION_URI;

    /** Topic name */
    private String topic = DEFAULT_TOPIC;

    /** Exchange name */
    private String exchange;

    /** RabbitMQ connection */
    private Connection connection;

    /** RabbitMQ channel */
    private Channel channel;

    /** Multicaster for events */
    private IDeviceEventMulticaster<String> multicaster;

    /** Route builder for generating topics */
    private IRouteBuilder<String> routeBuilder;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.event.processor.FilteredOutboundEventProcessor#start
     * (com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	super.start(monitor);

	// Start multicaster if configured.
	if (multicaster != null) {
	    startNestedComponent(multicaster, monitor, true);
	}

	// Start route builder if configured.
	if (routeBuilder != null) {
	    startNestedComponent(routeBuilder, monitor, true);
	}
	try {
	    ConnectionFactory factory = new ConnectionFactory();
	    factory.setUri(getConnectionUri());
	    this.connection = factory.newConnection();
	    this.channel = connection.createChannel();
	    this.exchange = getTenant().getId() + DEFAULT_EXCHANGE_SUFFIX;
	    channel.exchangeDeclare(exchange, "topic");
	    LOGGER.info("RabbitMQ outbound processor connected to: " + getConnectionUri());
	} catch (Exception e) {
	    throw new SmartThingException("Unable to start RabbitMQ event processor.", e);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.event.processor.FilteredOutboundEventProcessor#stop(
     * com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void stop(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Stop multicaster if configured.
	if (multicaster != null) {
	    multicaster.lifecycleStop(monitor);
	}

	// Stop route builder if configured.
	if (routeBuilder != null) {
	    routeBuilder.lifecycleStop(monitor);
	}

	try {
	    if (channel != null) {
		channel.close();
	    }
	    if (connection != null) {
		connection.close();
	    }
	} catch (Exception e) {
	    throw new SmartThingException("Error stopping RabbitMQ event processor.", e);
	}
	super.stop(monitor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onMeasurementsNotFiltered(com.smartthing.spi.device.event.
     * IDeviceMeasurements)
     */
    @Override
    public void onMeasurementsNotFiltered(IDeviceMeasurements measurements) throws SmartThingException {
	sendEvent(measurements);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onLocationNotFiltered(com.smartthing.spi.device.event.IDeviceLocation)
     */
    @Override
    public void onLocationNotFiltered(IDeviceLocation location) throws SmartThingException {
	sendEvent(location);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onAlertNotFiltered(com.smartthing.spi.device.event.IDeviceAlert)
     */
    @Override
    public void onAlertNotFiltered(IDeviceAlert alert) throws SmartThingException {
	sendEvent(alert);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onStateChangeNotFiltered(com.smartthing.spi.device.event.
     * IDeviceStateChange)
     */
    @Override
    public void onStateChangeNotFiltered(IDeviceStateChange state) throws SmartThingException {
	sendEvent(state);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onCommandInvocationNotFiltered(com.smartthing.spi.device.event.
     * IDeviceCommandInvocation)
     */
    @Override
    public void onCommandInvocationNotFiltered(IDeviceCommandInvocation invocation) throws SmartThingException {
	sendEvent(invocation);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onCommandResponseNotFiltered(com.smartthing.spi.device.event.
     * IDeviceCommandResponse)
     */
    @Override
    public void onCommandResponseNotFiltered(IDeviceCommandResponse response) throws SmartThingException {
	sendEvent(response);
    }

    /**
     * Send an {@link IDeviceEvent} to the configured topic.
     * 
     * @param event
     * @throws SmartThingException
     */
    protected void sendEvent(IDeviceEvent event) throws SmartThingException {
	IDeviceManagement dm =SmartThing.getServer().getDeviceManagement(getTenant());
	IDeviceAssignment assignment = dm.getDeviceAssignmentByToken(event.getDeviceAssignmentToken());
	IDevice device = dm.getDeviceByHardwareId(assignment.getDeviceHardwareId());
	if (getMulticaster() != null) {
	    List<String> routes = getMulticaster().calculateRoutes(event, device, assignment);
	    for (String route : routes) {
		publish(event, route);
	    }
	} else {
	    if (getRouteBuilder() != null) {
		publish(event, getRouteBuilder().build(event, device, assignment));
	    } else {
		publish(event, getTopic());
	    }
	}
    }

    /**
     * Publish an event to an MQTT topic.
     * 
     * @param event
     * @throws SmartThingException
     */
    protected void publish(IDeviceEvent event, String topic) throws SmartThingException {
	try {
	    channel.basicPublish(exchange, topic, null, MarshalUtils.marshalJson(event));
	    LOGGER.debug("Publishing event " + event.getId() + " to topic: " + topic);
	} catch (IOException e) {
	    throw new SmartThingException("Unable to publish to RabbitMQ topic.", e);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.
     * IMulticastingOutboundEventProcessor# getMulticaster()
     */
    @Override
    public IDeviceEventMulticaster<String> getMulticaster() {
	return this.multicaster;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.device.event.processor.
     * IMulticastingOutboundEventProcessor# getRouteBuilder()
     */
    @Override
    public IRouteBuilder<String> getRouteBuilder() {
	return this.routeBuilder;
    }

    public void setMulticaster(IDeviceEventMulticaster<String> multicaster) {
	this.multicaster = multicaster;
    }

    public void setRouteBuilder(IRouteBuilder<String> routeBuilder) {
	this.routeBuilder = routeBuilder;
    }

    public String getConnectionUri() {
	return connectionUri;
    }

    public void setConnectionUri(String connectionUri) {
	this.connectionUri = connectionUri;
    }

    public String getTopic() {
	return topic;
    }

    public void setTopic(String topic) {
	this.topic = topic;
    }
}