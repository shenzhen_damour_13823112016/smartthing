/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.influx.device;

import java.util.Map;

import org.influxdb.dto.Point;

import com.smartthing.rest.model.device.event.DeviceAlert;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.AlertLevel;
import com.smartthing.spi.device.event.AlertSource;
import com.smartthing.spi.device.event.DeviceEventType;

/**
 * Class for saving device alert data to InfluxDB.
 * 
 * @author Derek
 */
public class InfluxDbDeviceAlert {

    /** Alert type tag */
    public static final String ALERT_TYPE = "alert";

    /** Alert source tag */
    public static final String ALERT_SOURCE = "source";

    /** Alert level tag */
    public static final String ALERT_LEVEL = "level";

    /** Alert message field */
    public static final String ALERT_MESSAGE = "message";

    /**
     * Parse domain object from a value map.
     * 
     * @param values
     * @return
     * @throws SmartThingException
     */
    public static DeviceAlert parse(Map<String, Object> values) throws SmartThingException {
	DeviceAlert alert = new DeviceAlert();
	InfluxDbDeviceAlert.loadFromMap(alert, values);
	return alert;
    }

    /**
     * Load fields from value map.
     * 
     * @param event
     * @param values
     * @throws SmartThingException
     */
    public static void loadFromMap(DeviceAlert event, Map<String, Object> values) throws SmartThingException {
	event.setEventType(DeviceEventType.Alert);
	event.setType((String) values.get(ALERT_TYPE));
	event.setSource(AlertSource.valueOf((String) values.get(ALERT_SOURCE)));
	event.setLevel(AlertLevel.valueOf((String) values.get(ALERT_LEVEL)));
	event.setMessage((String) values.get(ALERT_MESSAGE));
	InfluxDbDeviceEvent.loadFromMap(event, values);
    }

    /**
     * Save fields to builder.
     * 
     * @param event
     * @param builder
     * @throws SmartThingException
     */
    public static void saveToBuilder(DeviceAlert event, Point.Builder builder) throws SmartThingException {
	builder.tag(ALERT_TYPE, event.getType());
	builder.tag(ALERT_SOURCE, event.getSource().name());
	builder.tag(ALERT_LEVEL, event.getLevel().name());
	builder.addField(ALERT_MESSAGE, event.getMessage());
	InfluxDbDeviceEvent.saveToBuilder(event, builder);
    }
}