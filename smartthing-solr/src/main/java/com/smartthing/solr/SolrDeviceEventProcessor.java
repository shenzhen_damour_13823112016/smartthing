/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.solr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;

import com.smartthing.device.event.processor.FilteredOutboundEventProcessor;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.processor.IOutboundEventProcessor;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;

/**
 * {@link IOutboundEventProcessor} implementation that takes saved events and
 * indexes them in Apache Solr for advanced analytics processing.
 * 
 * @author Derek
 */
public class SolrDeviceEventProcessor extends FilteredOutboundEventProcessor {

    /** Static logger instance */
    private static Logger LOGGER = LogManager.getLogger();

    /** Number of documents to buffer before blocking calls */
    private static final int BUFFER_SIZE = 1000;

    /** Interval at which batches are written */
    private static final int BATCH_INTERVAL = 2 * 1000;

    /** Maximum count of documents to send in a batch */
    private static final int MAX_BATCH_SIZE = 200;

    /** Interval by which documents should be committed */
    private static final int COMMIT_INTERVAL = 60 * 1000;

    /** Injected Solr configuration */
    private SmartThingSolrConfiguration solr;

    /** Bounded queue that holds documents to be processed */
    private BlockingQueue<SolrInputDocument> queue = new ArrayBlockingQueue<SolrInputDocument>(BUFFER_SIZE);

    /** Used to execute Solr indexing in a separate thread */
    /** TODO: Use a better approach for scalability */
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.smartthing.device.event.processor.FilteredOutboundEventProcessor#start
     * (com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
     */
    @Override
    public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
	// Required for filters.
	super.start(monitor);

	if (getSolr() == null) {
	    throw new SmartThingException("No Solr configuration provided to " + getClass().getName());
	}
	try {
	    LOGGER.info("Attempting to ping Solr server to verify availability...");
	    SolrPingResponse response = getSolr().getSolrClient().ping();
	    int pingTime = response.getQTime();
	    LOGGER.info("Solr server location verified. Ping responded in " + pingTime + " ms.");
	} catch (SolrServerException e) {
	    throw new SmartThingException("Ping failed. Verify that Solr server is available.", e);
	} catch (IOException e) {
	    throw new SmartThingException("Exception in ping. Verify that Solr server is available.", e);
	}
	LOGGER.info("Solr event processor indexing events to server at: " + getSolr().getSolrServerUrl());
	executor.execute(new SolrDocumentQueueProcessor());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
     */
    @Override
    public Logger getLogger() {
	return LOGGER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onMeasurementsNotFiltered(com.smartthing.spi.device.event.
     * IDeviceMeasurements)
     */
    @Override
    public void onMeasurementsNotFiltered(IDeviceMeasurements measurements) throws SmartThingException {
	SolrInputDocument document = SmartThingSolrFactory.createDocumentFromMeasurements(measurements);
	try {
	    queue.put(document);
	} catch (InterruptedException e) {
	    throw new SmartThingException("Interrupted during indexing.", e);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onLocationNotFiltered(com.smartthing.spi.device.event.IDeviceLocation)
     */
    @Override
    public void onLocationNotFiltered(IDeviceLocation location) throws SmartThingException {
	SolrInputDocument document = SmartThingSolrFactory.createDocumentFromLocation(location);
	try {
	    queue.put(document);
	} catch (InterruptedException e) {
	    throw new SmartThingException("Interrupted during indexing.", e);
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
     * onAlertNotFiltered (com.smartthing.spi.device.event.IDeviceAlert)
     */
    @Override
    public void onAlertNotFiltered(IDeviceAlert alert) throws SmartThingException {
	SolrInputDocument document = SmartThingSolrFactory.createDocumentFromAlert(alert);
	try {
	    queue.put(document);
	} catch (InterruptedException e) {
	    throw new SmartThingException("Interrupted during indexing.", e);
	}
    }

    /**
     * Class that processes documents in the queue asynchronously.
     * 
     * @author Derek
     */
    private class SolrDocumentQueueProcessor implements Runnable {

	@Override
	public void run() {
	    LOGGER.info("Started Solr indexing thread.");
	    boolean interrupted = false;

	    while (!interrupted) {
		long start = System.currentTimeMillis();
		List<SolrInputDocument> batch = new ArrayList<SolrInputDocument>();

		while (((System.currentTimeMillis() - start) < BATCH_INTERVAL) && (batch.size() <= MAX_BATCH_SIZE)) {
		    try {
			SolrInputDocument document = queue.poll(BATCH_INTERVAL, TimeUnit.MILLISECONDS);
			if (document != null) {
			    batch.add(document);
			}
		    } catch (InterruptedException e) {
			LOGGER.error("Solr indexing thread interrupted.", e);
			interrupted = true;
			break;
		    }
		}
		if (!batch.isEmpty()) {
		    try {
			UpdateResponse response = getSolr().getSolrClient().add(batch, COMMIT_INTERVAL);
			if (response.getStatus() != 0) {
			    LOGGER.warn("Bad response code indexing documents: " + response.getStatus());
			}
			LOGGER.debug("Indexed " + batch.size() + " documents in Solr.");
		    } catch (SolrServerException e) {
			LOGGER.error("Exception indexing SiteWhere document.", e);
		    } catch (IOException e) {
			LOGGER.error("IOException indexing SiteWhere document.", e);
		    } catch (Throwable e) {
			LOGGER.error("Unhandled exception indexing SiteWhere document.", e);
		    }
		}
	    }
	}
    }

    public SmartThingSolrConfiguration getSolr() {
	return solr;
    }

    public void setSolr(SmartThingSolrConfiguration solr) {
	this.solr = solr;
    }
}