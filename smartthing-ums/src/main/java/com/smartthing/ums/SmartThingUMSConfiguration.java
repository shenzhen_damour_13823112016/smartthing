/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.ums;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.server.lifecycle.TenantLifecycleComponent;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.server.lifecycle.IDiscoverableTenantLifecycleComponent;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;
import com.smartthing.spi.server.lifecycle.LifecycleComponentType;

/**
 * Class for base Solr client configuration settings.
 *
 * @author Derek
 */
public class SmartThingUMSConfiguration extends TenantLifecycleComponent
		implements IDiscoverableTenantLifecycleComponent {

	/**
	 * Static logger instance
	 */
	private static Logger LOGGER = LogManager.getLogger();

	/**
	 * Bean name where global UMS configuration is expected
	 */
	public static final String UMS_CONFIGURATION_BEAN = "swUMSConfiguration";

	/**
	 * Default URL for contacting UMS server
	 */
	private static final String DEFAULT_UMS_URL = "http://localhost:8983/user";

	/**
	 * URL used to interact with ums server
	 */
	private String umsServerUrl = DEFAULT_UMS_URL;



	public SmartThingUMSConfiguration() {
		super(LifecycleComponentType.Other);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.smartthing.server.lifecycle.LifecycleComponent#start(com.smartthing.spi
	 * .server.lifecycle.ILifecycleProgressMonitor)
	 */
	@Override
	public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
		LOGGER.info("UMS initializing with URL: " + getUmsServerUrl());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
	 */
	@Override
	public Logger getLogger() {
		return LOGGER;
	}

	public String getUmsServerUrl() {
		return umsServerUrl;
	}

	public void setUmsServerUrl(String umsServerUrl) {
		this.umsServerUrl = umsServerUrl;
	}
}
