/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package com.smartthing.ums;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartthing.device.event.processor.FilteredOutboundEventProcessor;
import com.smartthing.spi.SmartThingException;
import com.smartthing.spi.device.event.IDeviceAlert;
import com.smartthing.spi.device.event.IDeviceLocation;
import com.smartthing.spi.device.event.IDeviceMeasurements;
import com.smartthing.spi.device.event.processor.IOutboundEventProcessor;
import com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor;

/**
 * {@link IOutboundEventProcessor} implementation that takes saved events and
 * indexes them in Apache Solr for advanced analytics processing.
 *
 * @author Derek
 */
public class UMSDeviceEventProcessor extends FilteredOutboundEventProcessor {

	/**
	 * Static logger instance
	 */
	private static Logger LOGGER = LogManager.getLogger();

	/**
	 * Injected Solr configuration
	 */
	private SmartThingUMSConfiguration ums;

	/**
	 * Bounded queue that holds documents to be processed
	 */


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.smartthing.device.event.processor.FilteredOutboundEventProcessor#start
	 * (com.smartthing.spi.server.lifecycle.ILifecycleProgressMonitor)
	 */
	@Override
	public void start(ILifecycleProgressMonitor monitor) throws SmartThingException {
		// Required for filters.
		super.start(monitor);

		if (getUms() == null) {
			throw new SmartThingException("No UMS configuration provided to " + getClass().getName());
		}
		LOGGER.info("UMS event processor indexing events to server at: " + getUms().getUmsServerUrl());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.smartthing.spi.server.lifecycle.ILifecycleComponent#getLogger()
	 */
	@Override
	public Logger getLogger() {
		return LOGGER;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
	 * onMeasurementsNotFiltered(com.smartthing.spi.device.event.
	 * IDeviceMeasurements)
	 */
	@Override
	public void onMeasurementsNotFiltered(IDeviceMeasurements measurements) throws SmartThingException {

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
	 * onLocationNotFiltered(com.smartthing.spi.device.event.IDeviceLocation)
	 */
	@Override
	public void onLocationNotFiltered(IDeviceLocation location) throws SmartThingException {

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.smartthing.device.event.processor.FilteredOutboundEventProcessor#
	 * onAlertNotFiltered (com.smartthing.spi.device.event.IDeviceAlert)
	 */
	@Override
	public void onAlertNotFiltered(IDeviceAlert alert) throws SmartThingException {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			HttpUtil.doPost(ums.getUmsServerUrl(), objectMapper.writeValueAsString(alert));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		LOGGER.info("send UMS message success!!!");
	}

	/**
	 * Class that processes documents in the queue asynchronously.
	 *
	 * @author Derek
	 */

	public SmartThingUMSConfiguration getUms() {
		return ums;
	}

	public void setUms(SmartThingUMSConfiguration ums) {
		this.ums = ums;
	}
}
