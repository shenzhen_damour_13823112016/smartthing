package com.smartthing.ums;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class HttpUtil {
	/**
	 * post请求
	 *
	 * @param url
	 * @param json
	 * @return
	 */
	public static String doPost(String url, String json) {

		CloseableHttpClient httpclient = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);
		String response = null;
		try {
			//StringEntity s = new StringEntity(json.toString());
			StringEntity s;
			s = new StringEntity(json, "UTF-8");
			s.setContentEncoding("UTF-8");
			//发送json数据需要设置contentType
			s.setContentType("application/json");
			post.setEntity(s);
			HttpResponse res = httpclient.execute(post);
			if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				response = EntityUtils.toString(res.getEntity());
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return response;
	}
}
